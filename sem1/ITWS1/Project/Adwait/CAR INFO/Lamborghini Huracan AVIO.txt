OVERVIEW

Lamborghini Huracan AVIO is the mid petrol variant in the Huracan lineup and is priced at Rs. 3.71 crore (ex-showroom, Delhi). This base variant comes with an engine putting out 602.12bhp@8250rpm and 560Nm@6500rpm of max power and max torque respectively. The gasoline motor comes mated to 7 Speed Automatic transmission which is quite a joy to use. Lamborghini claims an average of 10.6 kmpl for this mid AVIO variant and with the healthy ground clearance of 125mm.

3.71 CRORE

Engine Displ. : 5204 cc
Power : 602.12bhp@8250rpm
Torque : 560Nm@6500rpm
Top Speed (KMPH) : 325
Sunroof : NA
Adjustable Driver Seat : Y
Adj. Front Passenger Seat : Y
Heated/Cooled Seats : NA

Engine Type: V Type Petrol Engine
Engine Displacement: 5204 cc
Fuel Type: Petrol
Power: 602.12bhp@8250rpm
Torque: 560Nm@6500rpm
No Of Cylinders: 10
Transmission: Automatic
Gear Box: 7 Speed
Drive Type: AWD
Paddle Shift: Y
Kerb Weight: 1422kg

Mileage (ARAI) kmpl: 10.6
Top Speed (Km/h): 325


https://www.lamborghini.com/en-en/models/huracan/huracan-avio