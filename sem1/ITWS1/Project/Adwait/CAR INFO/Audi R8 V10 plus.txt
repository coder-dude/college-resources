
BORN ON THE TRACK. BUILT FOR THE ROAD.


SPECS

Created in the world�s toughest development lab: the world of motorsport. We drew on our experiences with motorsport for the development of the Audi R8. For example, the many national and international GT3 titles won by the R8 LMS, including victories at the classic 24-hour races in the N�rburgring, Spa, and Daytona.** This means that the Audi R8 Coup� V10 plus* is rooted in the tradition of GT3 racing � and you can feel it the instant you turn on the engine.


BREATHTAKING ACCELERATION.

A NEW EXPERIENCE. ON EVERY ROAD.

5.2-liter V10 FSI engine with 397-449 kW depending on the model
Seven-speed S tronic dual-clutch transmission
Mid-engine design
100% lightweight construction with Audi Space Frame (ASF)
Permanent quattro four wheel drive
Carbon fiber-reinforced ceramic brake discs
R8 performance steering wheel with multifunction plus and performance mode
Audi virtual cockpit
Bucket seats

V10 Plus
Rs. 2.63 crore*

Engine Displ. : 5204 cc
Power : 601.4bhp@8250rpm
Torque : 560Nm@6500rpm
Top Speed (KMPH) : 205
Sunroof : NA
Adjustable Driver Seat : Y
Adj. Front Passenger Seat : Y
Heated/Cooled Seats : NA

Mileage (ARAI) kmpl: 7.75
Top Speed (Km/h): 205

Suspension Front: Sport
Suspension Rear: Sport
Brakes Front: Disc
Brakes Rear: Disc
Steering Type: Power
Minimum Turning Radius: 5.9 metres
Tyre Size: 245/35 R19,295/35 R19
Alloy Wheel Size: 19 Inch
Wheel Size: 19
Tyre Type: Tubeless,Radial

Standard Warranty (Years): 2
Standard Warranty (kilometers): Unlimited

https://www.audiusa.com/models/audi-r8