but then came the urgals and then the humans and the elves amended their spells so humans might be riders as well
and then did we seek to be included in their accord as well we might have
as was our right orik shook his head
our pride would not allow it
why should we the oldest race in the land beg the elves for the favor of their magic we did not need to chain our fate to the dragons in order to save our race from destruction as had the elves and humans
we ignored of course the battles we waged among ourselves
those we reasoned were private affairs and of no concern to anyone else
the listening clan chiefs stirred
many of them bore expressions of dissatisfaction at orik is criticism whereas the rest seemed more receptive to his comments and were thoughtful of countenance
orik continued while the riders watched over alagaesia we enjoyed the greatest period of prosperity ever recorded in the annals of our realm
we flourished as never before and yet we had no share in the cause of it the dragon riders
when the riders fell our fortunes faltered but again we had no share in the cause of it the riders
neither state of affairs is i deem fitting for a race of our stature
we are not a country of vassals subject to the whims of foreign masters
nor should those who are not the descendants of odgar and hlordis dictate our fate
this line of reasoning was more to the liking of the clan chiefs they nodded and smiled and havard even clapped a few times at the final line
consider now our present era said orik
galbatorix is ascendant and every race fights to remain free of his rule
he has grown so powerful the only reason we are not already his slaves is that so far he has not chosen to fly out upon his black dragon and attack us directly
if he did we would fall before him like saplings before an avalanche
fortunately he seems content to wait for us to slaughter our way to the gates of his citadel in uru baen
now i remind you that before eragon and saphira turned up wet and bedraggled on our front doorstep with a hundred yammering kull hard upon their heels our only hope of defeating galbatorix was that someday somewhere saphira would hatch for her chosen rider and that this unknown person would perhaps perchance if we were luckier than every gambler who has ever won a toss of dice be able to overthrow galbatorix
hope ** we did not even have hope we had a hope of a hope
when eragon first presented himself many of us were dismayed by his appearance myself included
he is but a boy we said
it would have been better if he had been an elf we said
but lo he has shown himself to be the embodiment of our every ** he slew durza and so allowed us to save our most beloved city tronjheim
his dragon saphira has promised to restore the star rose to its former glory
during the battle of the burning plains he drove off murtagh and thorn and so allowed us to win the day
and ** he even now wears the semblance of an elf and through their strange magics he has acquired their speed and their strength
orik raised a finger for emphasis
moreover king hrothgar in his wisdom did what no other king or grimstborith has ever done he offered to adopt eragon into durgrimst ingeitum and to make him a member of his own family
eragon was under no obligation to accept this offer
indeed he was aware that many of the families of the ingeitum objected to it and that in general many knurlan would not regard it with favor
yet in spite of that discouragement and in spite of the fact that he was already bound in fealty to nasuada eragon accepted hrothgar is gift knowing full well that it would only make his life harder
as he has told me himself eragon swore the hall oath upon the heart of stone because of the sense of obligation he feels toward all the races of alagaesia and especially toward us since we by the actions of hrothgar showed him and saphira such kindness
because of hrothgar is genius the last free rider of alagaesia and our one and only hope against galbatorix freely chose to become a knurla in all but blood