he was careful to not wake the other animals as he maneuvered the body up to the surface it seemed cruel to terrify them with the knowledge that an invisible predator could kill them in their most secret havens
he gutted skinned and otherwise cleaned the lizards and rodent burying the offal deep enough to hide it from scavengers
gathering thin flat stones he built a small oven lit a fire within and started the meat cooking
without salt he could not properly season any sort of food but some of the native plants released a pleasant smell when he crushed them between his fingers and those he rubbed over and packed into the carcasses
the rodent was ready first being smaller than the lizards
lifting it off the top of the makeshift oven eragon held the meat in front of his mouth
he grimaced and would have remained locked in the grip of his revulsion except that he had to continue tending the fire and the lizards
those two activities distracted him enough that without thinking he obeyed the strident command of his hunger and ate
the initial bite was the worst it stuck in his throat and the taste of hot grease threatened to make him sick
then he shivered and dry swallowed twice and the urge passed
after that it was easier
he was actually grateful the meat was rather bland for the lack of flavor helped him to forget what he was chewing
he consumed the entire rodent and then part of a lizard
tearing the last bit of flesh off a thin leg bone he heaved a sigh of contentment and then hesitated chagrined to realize that in spite of himself he had enjoyed the meal
he was so hungry the meager supper had seemed delicious once he overcame his inhibitions
perhaps he mused perhaps when i return
if i am at nasuada is table or king orrin is and meat is served
perhaps if i feel like it and it would be rude to refuse i might have a few bites
i wo not eat the way i used to but neither shall i be as strict as the elves
moderation is a wiser policy than zealotry i think
by the light from the coals in the oven eragon studied sloan is hands the butcher lay a yard or two away where eragon had placed him
dozens of thin white scars crisscrossed his long bony fingers with their oversized knuckles and long fingernails that while they had been meticulous in carvahall were now ragged torn and blackened with accumulated filth
the scars testified to the relatively few mistakes sloan had made during the decades he had spent wielding knives
his skin was wrinkled and weathered and bulged with wormlike veins yet the muscles underneath were hard and lean
eragon sat on his haunches and crossed his arms over his knees
i can not just let him go he murmured
if he did sloan might track down roran and katrina a prospect that eragon considered unacceptable
besides even though he was not going to kill sloan he believed the butcher should be punished for his crimes
eragon had not been close friends with byrd but he had known him to be a good man honest and steadfast and he remembered byrd is wife felda and their children with some fondness for garrow roran and eragon had eaten and slept in their house on several occasions
byrd is death then struck eragon as being particularly cruel and he felt the watchman is family deserved justice even if they never learned about it
what however would constitute proper punishment i refused to become an executioner thought eragon only to make myself an arbiter
what do i know about the law
rising to his feet he walked over to sloan and bent toward his ear and said vakna
with a jolt sloan woke scrabbling at the ground with his sinewy hands
the remnants of his eyelids quivered as by instinct the butcher tried to lift them and look at his surroundings
instead he remained trapped in his own personal night
eragon said here eat ** he thrust the remaining half of his lizard toward sloan who although he could not see it surely must have smelled the food
where am i asked sloan
with trembling hands he began to explore the rocks and plants in front of him
he touched his torn wrists and ankles and appeared confused to discover that his fetters were gone