yet he did not resent the discomfort
he paid the price gladly if it meant he could help saphira
after a time she said i have been a fool
that makes it no easier when it is your turn to play dunce
i have always known what to do
when garrow died i knew it was the right thing to pursue the ra zac
when brom died i knew that we should go to gil ead and thence to the varden
and when ajihad died i knew that you should pledge yourself to nasuada
the path has always been clear to me
except now
in this issue alone i am lost
instead of answering she turned the subject and said do you know why this is called the stone of broken eggs
because during the war between dragons and elves the elves tracked us to this location and killed us while we slept
they tore apart our nests then shattered our eggs with their magic
that day it rained blood in the forest below
no dragon has lived here since
eragon remained silent
that was not why he was here
he would wait until she could bring herself to address the situation at hand
will you let me heal your leg
then i shall remain as mute as a statue and sit here until i turn to dust for i have the patience of a dragon from you
when they came her words were halting bitter and self mocking it shames me to admit it
when we first came here and i saw glaedr i felt such joy that another member of my race survived besides shruikan
i had never even seen another dragon before except in brom is memories
and i thought
i thought that glaedr would be as pleased by my existence as i was by his
you do not understand
i thought that he would be the mate i never expected to have and that together we could rebuild our ** snorted and a burst of flame escaped her ** was mistaken
he does not want me
eragon chose his response with care to avoid offending her and to provide a modicum of ** is because he knows you are destined for someone else one of the two remaining eggs
nor would it be proper for him to mate with you when he is your mentor
or perhaps he does not find me comely enough
saphira no dragon is ugly and you are the fairest of dragons
i am a fool she said
but she raised her left wing and kept it in the air as permission for him to tend to her injury
eragon limped to saphira is side where he examined the crimson wound glad that oromis had given him so many scrolls on anatomy to read
the blow by claw or tooth he was not sure had torn the quadriceps muscle beneath saphira is hide but not so much as to bare the bone
merely closing the surface of the wound as eragon had done so many times would not be enough
the muscle had to be knitted back together
the spell eragon used was long and complex and even he did not understand all its parts for he had memorized it from an ancient text that offered little explanation beyond the statement that given no bones were broken and the internal organs were whole this charm will heal any ailment of violent origins excepting that of grim ** once he uttered it eragon watched with fascination as saphira is muscle writhed beneath his hand veins nerves and fibers weaving together and became whole once more
the wound was big enough that in his weakened state he dared not heal it with just the energy from his body so he drew upon saphira is strength as well
it itches said saphira when he finished
eragon sighed and leaned his back against the rough basalt looking at the sunset through his ** fear that you will have to carry me off this rock
i am too tired to move
with a dry rustle she twisted in place and laid her head on the bones beside ** have treated you poorly ever since we came to ellesmera
i ignored your advice when i should have listened
you warned me about glaedr but i was too proud to see the truth in your words
i have failed to be a good companion for you betrayed what it means to be a dragon and tarnished the honor of the riders
no never that he said ** you have not failed your duty
you may have made a mistake but it was an honest one and one that anyone might have committed in your position
that does not excuse my behavior toward you