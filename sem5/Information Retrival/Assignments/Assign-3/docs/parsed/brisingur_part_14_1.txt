he mentioned it to arya and she nodded
i noticed them as ** neither he nor she voiced any concerns but worry began to gnaw at eragon is belly and he saw how arya is eyebrows lowered into a fierce frown
the land around them was open and flat devoid of any cover
they had encountered groups of soldiers before but always in the company of other travelers
now they were alone on the faint trail of a road
we could dig a hole with magic cover the top with brush and hide in it until they leave said eragon
arya shook her head without breaking stride
what would we do with the excess dirt they d think they had discovered the biggest badger den in existence
besides i would rather save our energy for running
eragon grunted
i am not sure how many more miles i have left in me
he was not winded but the relentless pounding was wearing him down
his knees hurt his ankles were sore his left big toe was red and swollen and blisters continued to break out on his heels no matter how tightly he bound them
the previous night he had healed several of the aches and pains troubling him and while that had provided a measure of relief the spells only exacerbated his exhaustion
the patrol was visible as a plume of dust for half an hour before eragon was able to make out the shapes of the men and the horses at the base of the yellow cloud
since he and arya had keener eyesight than most humans it was unlikely the horsemen could see them at that distance so they continued to run for another ten minutes
then they stopped
arya removed her skirt from her pack and tied it over the leggings she wore while running and eragon stored brom is ring in his own pack and smeared dirt over his right palm to hide his silvery gedwey ignasia
they resumed their journey with bowed heads hunched shoulders and dragging feet
if all went well the soldiers would assume they were just another pair of refugees
although eragon could feel the rumble of approaching hoofbeats and hear the cries of the men driving their steeds it still took the better part of an hour for their two groups to meet on the vast plain
when they did eragon and arya moved off the road and stood looking down between their feet
eragon caught a glimpse of horse legs from under the edge of his brow as the first few riders pounded past but then the choking dust billowed over him obscuring the rest of the patrol
the dirt in the air was so thick he had to close his eyes
listening carefully he counted until he was sure that more than half the patrol had gone by
they re not going to bother questioning ** he thought
his elation was short lived
a moment later someone in the swirling blizzard of dust shouted company ** a chorus of whoas steady theres and hey there nells rang out as the fifteen men coaxed their mounts to form a circle around eragon and arya
before the soldiers completed their maneuver and the air cleared eragon pawed the ground for a large pebble then stood back up
while he waited for the soldiers to make their intentions known eragon strove to calm his racing heart by rehearsing the story he and arya had concocted to explain their presence so close to the border with surda
his efforts failed for notwithstanding his strength his training the knowledge of the battles he had won and the half dozen wards protecting him his flesh remained convinced that imminent injury or death awaited him
his gut twisted his throat constricted and his limbs were light and unsteady
oh get on with ** he thought
he longed to tear something apart with his hands as if an act of destruction would relieve the pressure building inside of him but the urge only heightened his frustration for he dared not move
the one thing that steadied him was arya is presence
he would sooner cut off a hand than have her consider him a coward
and although she was a mighty warrior in her own right he still felt the desire to defend her
the voice that had ordered the patrol to halt again issued forth
let me see your ** raising his head eragon saw a man sitting before them on a roan charger his gloved hands folded over the pommel of his saddle