he pressed his cheek against the cool sapphire trying to see through it
distorted lines and wavering spots of color glimmered through the stone but its thickness made it impossible to discern anything clearly on the floor of the chamber a mile below them
will i have to sleep apart from you
saphira shook her enormous ** there is a bed for you in my cave
come see
she turned and without opening her wings jumped twenty feet into the air landing in a medium sized cave
he clambered up after her
the cave was dark brown on the inside and deeper than he had expected
the roughly chiseled walls gave the impression of a natural formation
near the far wall was a thick cushion large enough for saphira to curl up on
beside it was a bed built into the side of the wall
the cave was lit by a single red lantern equipped with a shutter so its glow could be muted
i like this said ** feels safe
** curled up on the cushion watching him
with a sigh he sank onto the mattress weariness seeping through him
saphira you have not said much while we ve been here
what do you think of tronjheim and ajihad
we shall see
it seems eragon that we are embroiled in a new type of warfare here
swords and claws are useless but words and alliances may have the same effect
the twins dislike us we should be on our guard for any duplicities they might attempt
not many of the dwarves trust us
the elves did not want a human rider so there will be opposition from them as well
the best thing we can do is identify those in power and befriend them
and quickly too
do you think it is possible to remain independent of the different leaders
she shuffled her wings into a more comfortable ** supports our freedom but we may be unable to survive without pledging our loyalty to one group or another
we will soon know either way
the blankets were bunched underneath eragon when he woke but he was still warm
saphira was asleep on her cushion her breath coming in steady gusts
for the first time since entering farthen dur eragon felt secure and hopeful
he was warm and fed and had been able to sleep as long as he liked
tension unknotted inside him tension that had been accumulating since brom is death and even before since leaving palancar valley
i do not have to be afraid anymore
but what about murtagh no matter the varden is hospitality eragon could not accept it in good conscience knowing that intentionally or not he had led murtagh to his imprisonment
somehow the situation had to be resolved
his gaze roamed the cave is rough ceiling as he thought of arya
chiding himself for daydreaming he tilted his head and looked out at the dragonhold
a large cat sat on the edge of the cave licking a paw
it glanced at him and he saw a flash of slanted red eyes
** werecat shook his rough mane and yawned languorously displaying his long fangs
he stretched then jumped out of the cave landing with a solid thump on isidar mithrim twenty feet **
eragon looked at saphira
she was awake now watching him **
i will be fine she murmured
solembum was waiting for him under the arch that led to the rest of tronjheim
the moment eragon is feet touched isidar mithrim the werecat turned with a flick of his paws and disappeared through the arch
eragon chased after him rubbing the sleep from his face
he stepped through the archway and found himself standing at the top of vol turin the endless staircase
there was nowhere else to go so he descended to the next level
he stood in an open arcade that curved gently to the left and encircled tronjheim is central chamber
between the slender columns supporting the arches eragon could see isidar mithrim sparkling brilliantly above him as well as the city mountain is distant base
the circumference of the central chamber increased with each successive level
the staircase cut through the arcade is floor to an identical level below and descended through scores of arcades until it disappeared in the distance
the sliding trough ran along the outside curve of the stairs
at the top of vol turin was a pile of leather squares to slide on
to eragon is right a dusty corridor led to that level is rooms and apartments