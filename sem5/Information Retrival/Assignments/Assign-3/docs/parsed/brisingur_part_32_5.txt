i do not like the look of that gash
with an enormous effort of will roran turned and trudged across the bank to the nearest wagon
blinking away the sweat that dripped from his brow he saw that of their original force only nine were still fit to stand
he pushed the observation out of his mind
mourn later not now
as martland redbeard walked across the corpse strewn encampment a soldier who roran had assumed was dead flipped over and from the ground lopped off the earl is right hand
with a movement so graceful it appeared practiced martland kicked the sword out of the soldier is grip then knelt on the soldier is throat and using his left hand drew a dagger from his belt and stabbed the man through one of his ears killing him
his face flushed and strained martland shoved the stump of his wrist under his left armpit and waved away everyone who rushed over to him
leave me ** it is hardly a wound at all
get to those ** unless you wastrels hurry up we will be here so long my beard will turn white as snow
go ** when carn refused to budge however martland scowled and shouted begone with you or i will have you flogged for insubordination i **
carn held up martland is wayward hand
i might be able to re attach it but i will need a few minutes
ah confound it give me ** exclaimed martland and snatched his hand away from carn
he tucked it inside his tunic
stop fretting about me and save welmar and lindel if you can
you can try reattaching it once we ve put a few leagues between us and these monsters
it might be too late then said carn
that was an order spellcaster not a ** thundered mart land
as carn retreated the earl used his teeth to tie off the sleeve of his tunic over the stump of his arm which he again stuck in his left armpit
sweat beaded his face
right ** what misbegotten items are hidden in those confounded wagons
martland grunted
ulhart you record the figures for me
roran helped the others as they rifled through each of the wagons calling out the contents to ulhart
afterward they slaughtered the teams of oxen and lit the wagons on fire as before
then they rounded up their horses and mounted them tying the injured into their saddles
when they were ready to depart carn gestured toward the flare of light in the sky and murmured a long tangled word
night enveloped the world
glancing up roran beheld a throbbing after image of carn is face superimposed over the faint stars and then as he became accustomed to the darkness he beheld the soft gray shapes of thousands of disoriented moths scattering across the sky like the shades of men is souls
his heart heavy within him roran touched his heels to snowfire is flanks and rode away from the remnants of the convoy