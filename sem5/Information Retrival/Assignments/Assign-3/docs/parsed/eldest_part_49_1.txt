eragon was alone when he woke
he opened his eyes to stare at the carved ceiling in the tree house he and saphira shared
outside night still reigned and the sounds of the elves revels drifted from the glittering city below
before he noticed more than that saphira leaped into his mind radiating concern and anxiety
an image passed to him of her standing beside islanzadi at the menoa tree then she asked how are you
i feel
good
better than i ve felt in a long time
how long have i
only an hour
i would have stayed with you but they needed oromis glaedr and me to complete the ceremony
you should have seen the elves reaction when you fainted
nothing like this has occurred before
it was not my work alone nor glaedr is
the memories of our race which were given form and substance by the elves magic anointed you with what skill we dragons possess for you are our best hope to avoid extinction
look in a mirror she ** rest and recover and i shall rejoin you at dawn
she left and eragon got to his feet and stretched amazed by the sense of well being that pervaded him
going to the wash closet he retrieved the mirror he used for shaving and brought it into the light of a nearby lantern
it was as if the numerous physical changes that over time alter the appearance of a human rider and which eragon had already begun to experience since bonding with saphira had been completed while he was unconscious
his face was now as smooth and angled as an elf is with ears tapered like theirs and eyes slanted like theirs and his skin was as pale as alabaster and seemed to emit a faint glow as if with the sheen of ** look like a princeling
eragon had never before applied the term to a man least of all himself but the only word that described him now wasbeautiful
yet he was not entirely an elf
his jaw was stronger his brow thicker his face broader
he was fairer than any human and more rugged than any elf
with trembling fingers eragon reached around the nape of his neck in search of his scar
eragon tore off his tunic and twisted in front of the mirror to examine his back
it was as smooth as it had been before the battle of farthen dur
tears sprang to eragon is eyes as he slid his hand over the place where durza had maimed him
he knew that his back would never trouble him again
not only was the savage blight he had elected to keep gone but every other scar and blemish had vanished from his body leaving him as unmarked as a newborn babe
eragon traced a line upon his wrist where he had cut himself while sharpening garrow is scythe
no evidence of the wound remained
the blotchy scars on the insides of his thighs remnants from his first flight with saphira had also disappeared
for a moment he missed them as a record of his life but his regret was short lived as he realized that the damage from every injury he had ever suffered no matter how small had been repaired
i have become what i was meant to be he thought and took a deep breath of the intoxicating air
he dropped the mirror on the bed and garbed himself in his finest clothes a crimson tunic stitched with gold thread a belt studded with white jade warm felted leggings a pair of the cloth boots favored by the elves and upon his forearms leather vambraces the dwarves had given him
descending from the tree eragon wandered the shadows of ellesmera and observed the elves carousing in the fever of the night
none of them recognized him though they greeted him as one of their own and invited him to share in their saturnalias
eragon floated in a state of heightened awareness his senses thrumming with the multitude of new sights sounds smells and feelings that assailed him
he could see in darkness that would have blinded him before
he could touch a leaf and by touch alone count the individual hairs that grew upon it
he could identify the odors wafting about him as well as a wolf or a dragon
and he could hear the patter of mice in the underbrush and the noise a flake of bark makes as it falls to earth the beating of his heart was as a drum to him
his aimless path led him past the menoa tree where he paused to watch saphira among the festivities though he did not reveal himself to those in the glade