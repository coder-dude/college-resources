the amount of energy contained within this stone is formidable eragon with it i could shift an entire mountain
it is a small matter then to defend glaedr and myself from swords and spears and arrows or even from a boulder cast by a siege engine
as for my seizures i have attached certain wards to the stone in naegling that will protect me from harm if i become incapacitated upon the battlefield
so you see eragon glaedr and i are far from helpless
chastened eragon dipped his head and murmured yes master
oromis is expression softened somewhat
i appreciate your concern eragon and you are right to be concerned for war is a perilous endeavor and even the most accomplished warrior may find death waiting for him amid the heated frenzy of battle
however our cause is a worthy one
if glaedr and i go to our deaths then we go willingly for by our sacrifice we may help to free alagaesia from the shadow of galbatorix is tyranny
but if you die said eragon feeling very small and yet we still succeed in killing galbatorix and freeing the last dragon egg who will train that dragon and his rider
oromis surprised eragon by reaching out and clasping him by the shoulder
if that should come to pass said the elf his face grave then it shall be your responsibility eragon and yours saphira to instruct the new dragon and rider in the ways of our order
ah do not look so apprehensive eragon
you would not be alone in the task
no doubt islanzadi and nasuada would ensure that the wisest scholars of both our races would be there to help you
a strange sense of unease troubled eragon
he had often longed to be treated as more of an adult but nevertheless he did not feel ready to take oromis is place
it seemed wrong to even contemplate the notion
for the first time eragon understood that he would eventually become part of the older generation and that when he did he would have no mentor to rely upon for guidance
his throat tightened
releasing eragon is shoulder oromis gestured at brisingr which lay in eragon is arms and said the entire forest shuddered when you woke the menoa tree saphira and half the elves in ellesmera contacted glaedr and me with frantic pleas for us to rush to her aid
moreover we had to intervene on your behalf with gilderien the wise so as to prevent him from punishing you for employing such violent methods
i shall not apologize said saphira
we had not the time to wait for gentle persuasion to work
oromis nodded
i understand and i am not criticizing you saphira
i only wanted you to be aware of the consequences of your ** at his request eragon handed his newly forged sword to oromis and held his helm for him while the elf examined the sword
rhunon has outdone herself oromis declared
few weapons swords or otherwise are the equal of this
you are fortunate to wield such an impressive blade ** one of oromis is sharp eyebrows rose a fraction of an inch as he read the glyph on the blade
brisingr
a most apt name for the sword of a dragon rider
aye said eragon
but for some reason every time i utter its name the blade bursts into
he hesitated and instead of saying fire which of course was brisingr in the ancient language he said flames
oromis is eyebrow climbed even higher
indeed did rhunon have an explanation for this unique phenomenon as he spoke oromis returned brisingr to eragon in exchange for his helm
yes master said eragon
and he recounted rhunon is two theories
when he had finished oromis murmured i wonder
and his gaze drifted past eragon toward the horizon
then oromis gave a brief shake of his head and again focused his gray eyes upon eragon and saphira
his face became even more solemn than before
i am afraid i have let my pride speak for me
glaedr and i may not be helpless but neither as you pointed out eragon are we entirely whole
glaedr has his wound and i have my own
impairments
it is not for nothing i am called the cripple who is whole
our disabilities would not be a problem if our only enemies were mortal men
even in our current state we could easily slay a hundred ordinary humans a hundred or a thousand it would matter little which