he lowered his eyes and said softly it is indeed a privilege to meet you
he will do said saphira respectfully
she swung her head to face eragon
tell him that i am impressed both with tronjheim and with him
the empire is right to fear him
let him know however that if he had decided to kill you i would have destroyed tronjheim and torn him apart with my teeth
eragon hesitated surprised by the venom in her voice then relayed the message
ajihad looked at her seriously
i would expect nothing less from one so noble but i doubt you could have gotten past the twins
knowing what she meant eragon said then they must be much stronger than they appear
i think they would be sorely dismayed if they ever faced a dragon is wrath
the two of them might be able to defeat me but never saphira
you should know a rider is dragon strengthens his magic beyond what a normal magician might have
brom was always weaker than me because of that
i think that in the absence of riders the twins have overestimated their power
ajihad looked troubled
brom was considered one of our strongest spell weavers
only the elves surpassed him
if what you say is true we will have to reconsider a great many ** he bowed to saphira
as it is i am glad it was not necessary to harm either of ** saphira dipped her head in return
ajihad straightened with a lordly air and called ** the dwarf hurried into the room and stood before the desk crossing his arms
ajihad frowned at him irritated
you ve caused me a great deal of trouble orik
i ve had to listen to one of the twins complain all morning about your insubordination
they wo not let it rest until you are punished
unfortunately they re right
it is a serious matter that cannot be ignored
an accounting is due
orik is eyes flicked toward eragon but his face betrayed no emotion
he spoke quickly in rough tones
the kull were almost around kostha merna
they were shooting arrows at the dragon eragon and murtagh but the twins did nothing to stop it
like
sheilven they refused to open the gates even though we could see eragon shouting the opening phrase on the other side of the waterfall
and they refused to take action when eragon did not rise from the water
perhaps i did wrong but i could not let a rider die
i was not strong enough to get out of the water myself offered eragon
i would have drowned if he had not pulled me out
ajihad glanced at him then asked orik seriously and later why did you oppose them
orik raised his chin defiantly
it was not right for them to force their way into murtagh is mind
but i would not have stopped them if i d known who he was
no you did the right thing though it would be simpler if you had not
it is not our place to force our way into people is minds no matter who they ** ajihad fingered his dense beard
your actions were honorable but you did defy a direct order from your commander
the penalty for that has always been ** orik is back stiffened
you can not kill him for ** he was only helping me cried eragon
it is not your place to interfere said ajihad sternly
orik broke the law and must suffer the ** eragon started to argue again but ajihad stopped him with a raised hand
but you are right
the sentence will be mitigated because of the circumstances
as of now orik you are removed from active service and forbidden to engage in any military activities under my command
do you understand
orik is face darkened but then he only looked confused
he nodded sharply
yes
furthermore in the absence of your regular duties i appoint you eragon and saphira is guide for the duration of their stay
you are to make sure they receive every comfort and amenity we have to offer
saphira will stay above isidar mithrim
eragon may have quarters wherever he wants
when he recovers from his trip take him to the training fields
they re expecting him said ajihad a twinkle of amusement in his eye
very well you all may go
send in the twins as you leave
eragon bowed and began to leave then asked where can i find arya i would like to see her
no one is allowed to visit her
you will have to wait until she comes to ** ajihad looked down at his desk in a clear dismissal