the woman grew rigid her breath caught in her chest
saphira lowered her snout and brushed the baby between the eyes with the tip of her nose then smoothly lifted away
a gasp ran through the crowd for on the child is forehead where saphira had touched her was a star shaped patch of skin as white and silvery as eragon is gedwey ignasia
the woman stared at saphira with a feverish gaze wordless thanks in her eyes
immediately saphira took flight battering the awestruck spectators with the wind from her powerful wing strokes
as the ground dwindled away eragon took a deep breath and hugged her neck ** did you do he asked softly
i gave her hope
and you gave her a future
loneliness suddenly flowered within eragon despite saphira is presence
their surroundings were so foreign it struck him for the first time exactly how far he was from home
a destroyed home but still where his heart ** have i become saphira he ** am only in the first year of manhood yet i ve consulted with the leader of the varden am pursued by galbatorix and have traveled with morzan is son and now blessings are sought from ** what wisdom can i give people that they have not already learned what feats can i achieve that an army could not do better it is ** i should be back in carvahall with roran
saphira took a long time to answer but her words were gentle when they ** hatchling that is what you are
a hatchling struggling into the world
i may be younger than you in years but i am ancient in my thoughts
do not worry about these things
find peace in where and what you are
people often know what must be done
all you need do is show them the way that is wisdom
as for feats no army could have given the blessing you did
but it was nothing he ** trifle
nay it was not
what you saw was the beginning of another story another legend
do you think that child will ever be content to be a tavern keeper or a farmer when her brow is dragon marked and your words hang over her you underestimate our power and that of fate
eragon bowed his ** is overwhelming
i feel as if i am living in an illusion a dream where all things are possible
amazing things do happen i know but always to someone else always in some far off place and time
but i found your egg was tutored by a rider and dueled a shade those can not be the actions of the farm boy i am or was
something is changing me
it is your wyrd that shapes you said ** age needs an icon perhaps that lot has fallen to you
farm boys are not named for the first rider without cause
your namesake was the beginning and now you are the continuation
or the end
ach said eragon shaking his ** is like speaking in riddles
but if all is foreordained do our choices mean anything or must we just learn to accept our fate
saphira said firmly eragon i chose you from within my egg
you have been given a chance most would die for
are you unhappy with that clear your mind of such thoughts
they cannot be answered and will make you no happier
true he said ** the same they continue to bounce around within my skull
things have been
unsettled
ever since brom died
it has made me uneasy acknowledged saphira which surprised him because she rarely seemed perturbed
they were above tronjheim now
eragon looked down through the opening in its peak and saw the floor of the dragonhold isidar mithrim the great star sapphire
he knew that beneath it was nothing but tronjheim is great central chamber
saphira descended to the dragonhold on silent wings
she slipped over its rim and dropped to isidar mithrim landing with the sharp clack of claws
i think not
it is no ordinary ** slid off her back and slowly turned in a circle absorbing the unusual sight
they were in a round roofless room sixty feet high and sixty feet across
the walls were lined with the dark openings of caves which differed in size from grottoes no larger than a man to a gaping cavern larger than a house
shiny rungs were set into the marble walls so that people could reach the highest caves
an enormous archway led out of the dragonhold
eragon examined the great gem under his feet and impulsively lay down on it