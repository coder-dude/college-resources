can you not even hold on to your blade rider demanded vanir
i apologize vanir vodhr gasped eragon
he clutched his elbow rubbing the bruised joint to lessen the pain
i misjudged my strength
see that it does not happen ** going to the tree vanir gripped zar roc is hilt and tried to pull the sword free
the weapon remained motionless
vanir is eyebrows met as he frowned at the unyielding crimson blade as if he suspected some form of trickery
bracing himself the elf heaved backward and with the crack of wood yanked zar roc out of the pine
eragon accepted the sword from vanir and hefted zar roc troubled by how light it ** is wrong he thought
this time it was vanir who initiated the fight
in a single bound he crossed the distance between them and thrust his blade toward eragon is right shoulder
to eragon it seemed as if the elf moved slower than usual as if vanir is reflexes had been reduced to the level of a human is
it was easy for eragon to deflect vanir is sword blue sparks flying from the metal as their blades grated against one another
vanir landed with an astonished expression
he struck again and eragon evaded the sword by leaning back like a tree swaying in the wind
in quick succession vanir rained a score of heavy blows upon eragon each of which eragon dodged or blocked using zar roc is sheath as often as the sword to foil vanir is onslaught
eragon soon realized that the spectral dragon from the agaeti blodhren had done more than alter his appearance it had also granted him the elves physical abilities
in strength and speed eragon now matched even the most athletic elf
fired by that knowledge and a desire to test his limits eragon jumped as high as he could
zar roc flashed crimson in the sunlight as he flew skyward soaring more than ten feet above the ground before he flipped like an acrobat and came down behind vanir facing the direction from which he had started
a fierce laugh erupted from eragon
no more was he helpless before elves shades and other creatures of magic
no more would he suffer the elves contempt
no more would he have to rely on saphira or arya to rescue him from enemies like durza
he charged vanir and the field rang with a furious din as they strove against each other raging back and forth upon the trampled grass
the force of their blows created gusts of wind that whipped their hair into tangled disarray
overhead the trees shook and dropped their needles
the duel lasted long into the morning for even with eragon is newfound skill vanir was still a formidable opponent
but in the end eragon would not be denied
playing zar roc in a circle he darted past vanir is guard and struck him upon the upper arm breaking the bone
vanir dropped his blade his face turning white with shock
how swift is your sword he said and eragon recognized the famous line fromthe lay of umhodan
by the ** exclaimed orik
that was the best swordsmanship i ve ever seen and i was there when you fought arya in farthen dur
then vanir did what eragon had never expected the elf twisted his uninjured hand in the gesture of fealty placed it upon his sternum and bowed
i beg your pardon for my earlier behavior eragon elda
i thought that you had consigned my race to the void and out of my fear i acted most shamefully
however it seems that your race no longer endangers our ** in a grudging voice he added you are now worthy of the title rider
eragon bowed in return
you honor me
i am sorry that i injured you so badly
will you allow me to heal your arm
no i shall let nature tend to it at her own pace as a memento that i once crossed blades with eragon shadeslayer
you need not fear that it will disrupt our sparring tomorrow i am equally good with my left hand
they both bowed again and then vanir departed
orik slapped a hand on his thigh and said now we have a chance at victory a real ** i can feel it in my bones
bones like stone they say
ah this will please hrothgar and nasuada to no end
eragon kept his peace and concentrated on removing the block from zar roc is edges but he said to saphira if brawn were all that was required to depose galbatorix the elves would have done it long ago