my day was a little more fruitful than yours
i heard the same thing you did so i went to the warehouse and talked with the workers
it did not take much cajoling before they revealed that the cases of seithr oil are always sent from the warehouse to the palace
and that is when you came back here finished eragon
no it is ** do not interrupt
after that i went to the palace and got myself invited into the servants quarters as a bard
for several hours i wandered about amusing the maids and others with songs and poems and asking questions all the ** brom slowly filled his pipe with tobacco
it is really amazing all the things servants find out
did you know that one of the earls hasthree mistresses and they all live in the same wing of the palace he shook his head and lit the pipe
aside from the fascinating tidbits i was told quite by accident where the oil is taken from the palace
and that is
asked eragon impatiently
brom puffed on his pipe and blew a smoke ring
out of the city of course
every full moon two slaves are sent to the base of helgrind with a month is worth of provisions
whenever the seithr oil arrives in dras leona they send it along with the provisions
the slaves are never seen again
and the one time someone followed them he disappeared too
i thought the riders demolished the slave trade said eragon
unfortunately it has flourished under the king is reign
so the ra zac are in helgrind said eragon thinking of the rock mountain
if theyare in helgrind they will be either at the bottom and protected by a thick stone door or higher up where only their flying mounts or saphira can reach
top or bottom their shelter will no doubt be ** he thought for a moment
if saphira and i go flying around helgrind the ra zac are sure to see us not to mention all of dras leona
it is a problem agreed brom
eragon frowned
what if we took the place of the two slaves the full moon is not far off
it would give us a perfect opportunity to get close to the ra zac
brom tugged his beard thoughtfully
that is chancy at best
if the slaves are killed from a distance we will be in trouble
we can not harm the ra zac if they are not in sight
we do not know if the slaves are killed at all eragon pointed out
i am sure they are said brom his face grave
then his eyes sparkled and he blew another smoke ring
still it is an intriguing idea
if it were done with saphira hidden nearby and a
his voice trailed off
it might work but we will have to move quickly
with the king coming there is not much time
should we go to helgrind and look around it would be good to see the land in daylight so we wo not be surprised by any ambushes said eragon
brom fingered his staff
that can be done later
tomorrow i will return to the palace and figure out how we can replace the slaves
i have to be careful not to arouse suspicion though i could easily be revealed by spies and courtiers who know about the ra zac
i can not believe it we actually found them said eragon quietly
an image of his dead uncle and burned farm flashed through his mind
his jaw tightened
the toughest part is yet to come but yes we ve done well said brom
if fortune smiles on us you may soon have your revenge and the varden will be rid of a dangerous enemy
what comes after that will be up to you
eragon opened his mind and jubilantly told saphira we found the ra zac is lair
where he quickly explained what they had ** she ** fitting place for them
eragon ** we re done here maybe we could visit carvahall
what is it you want she asked suddenly ** go back to your previous life you know that wo not happen so stop mooning after ** at a certain point you have to decide what to commit to
will you hide for the rest of your life or will you help the varden those are the only options left to you unless you join forces with galbatorix which i do not and never will accept
softly he said if i must choose i cast my fate with the varden as you well know
yes but sometimes you have to hear yourself say ** left him to ponder her words
eragon was alone in the room when he woke
scrawled onto the wall with a charcoal stick was a note that read