i bound myself with them so that i might live long enough to witness the birth of the last dragons and to foster the riders resurrection from the ruin of our mistakes
how long until
oromis lifted a sharp eyebrow
how long until i die we have time but precious little for you or me especially if the varden decide to call upon your help
as a result to answer your question saphira we will begin your instruction immediately and we will train faster than any rider ever has or ever will for i must condense decades of knowledge into months and weeks
you do know said eragon struggling against the embarrassment and shame that made his cheeks burn about my
my owninfirmity
he ground out the last word hating the sound of it
i am as crippled as you are
sympathy tempered oromis is gaze though his voice was firm
eragon you are only a cripple if you consider yourself one
i understand how you feel but you must remain optimistic for a negative outlook is more of a handicap than any physical injury
i speak from personal experience
pitying yourself serves neither you nor saphira
i and the other spellweavers will study your malady to see if we might devise a way to alleviate it but in the meantime your training will proceed as if nothing were amiss
eragon is gut clenched and he tasted bile as he considered the ** oromis would not make me endure that torment ** the pain is unbearable he said frantically
it would kill me
i
no eragon
it will not kill you
that much i know about your curse
however we both have our duty you to the varden and i to you
we cannot shirk it for the sake of mere pain
far too much is at risk and we can ill afford to ** all eragon could do was shake his head as panic threatened to overwhelm him
he tried to deny oromis is words but their truth was inescapable
eragon
you must accept this burden freely
have you no one or nothing that you are willing to sacrifice yourself for
his first thought was of saphira but he was not doing this for her
nor for nasuada
nor even for arya
what drove him then when he had pledged fealty to nasuada he had done so for the good of roran and the other people trapped within the empire
but did they mean enough to him to put himself through such anguish yes he ** they do because i am the only one who has a chance to help them and because i wo not be free of galbatorix is shadow until they are as well
and because this is my only purpose in life
what else would i do he shuddered as he mouthed the ghastly phrase i accept on behalf of those i fight for the people of alagaesia of all races who have suffered from galbatorix is brutality
no matter the pain i swear that i will study harder than any student you ve had before
oromis nodded gravely
i ask for nothing ** he looked at glaedr for a moment then said stand and remove your tunic
let me see what you are made of
wait said ** brom aware of your existence here master eragon paused struck by the possibility
of course said oromis
he was my pupil as a boy in ilirea
i am glad that you gave him a proper burial for he had a hard life and few enough ever showed him kindness
i hope that he found peace before he entered the void
eragon slowly frowned
did you know morzan as well
he was my apprentice before brom
i was one of the elders who denied him another dragon after his first was killed but no i never had the misfortune to teach him
he made sure to personally hunt down and kill each of his mentors
eragon wanted to inquire further but he knew that it would be better to wait so he stood and unlaced the top of his ** seems he said to saphira that we will never learn all of brom is secrets
he shivered as he pulled off the tunic in the cool air then squared his shoulders and lifted his chest
oromis circled him stopping with an astonished exclamation as he saw the scar that crossed eragon is back
did not arya or one of the varden is healers offer to remove this weal you should not have to carry it
arya did offer but
eragon stopped unable to articulate his feelings
finally he just said it is part of me now just as murtagh is scar is part of him