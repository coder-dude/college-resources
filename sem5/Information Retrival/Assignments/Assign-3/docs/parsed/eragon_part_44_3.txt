and i carried her down added saphira
eragon struggled to understand as another bout of lightheadedness made him close his eyes
but why did not any of the pieces hit you or me
i did not allow them to
when we were almost to the floor i held them motionless in the air then slowly lowered them to the floor else they would have shattered into a thousand pieces and killed you stated arya simply
her words betrayed the power within her
angela added sourly yes and it almost killed you as well
it is taken all of my skill to keep the two of you alive
a twinge of unease shot through eragon matching the intensity of his throbbing ** back
but he felt no bandages there
how long have i been here he asked with trepidation
only a day and a half answered angela
you re lucky i was around otherwise it would ve taken you weeks to heal if you had even ** alarmed eragon pushed the blankets off his torso and twisted around to feel his back
angela caught his wrist with her small hand worry reflected in her eyes
eragon
you have to understand my power is not like yours or arya is
it depends on the use of herbs and potions
there are limits to what i can do especially with such a large
he yanked his hand out of her grip and reached back fingers groping
the skin on his back was smooth and warm flawless
hard muscles flexed under his fingertips as he moved
he slid his hand toward the base of his neck and unexpectedly felt a hard bump about a half inch wide
he followed it down his back with growing horror
durza is blow had left him with a huge ropy scar stretching from his right shoulder to the opposite hip
pity showed on arya is face as she murmured you have paid a terrible price for your deed eragon shadeslayer
murtagh laughed harshly
yes
now you re just like me
dismay filled eragon and he closed his eyes
he was disfigured
then he remembered something from when he was unconscious
a figure in white who had helped him
a cripple who was whole togira ikonoka
he had said think of what you have done and rejoice for you have rid the land of a great evil
you have wrought a deed no one else could
many are in your debt
come to me eragon for i have answers to all you ask
a measure of peace and satisfaction consoled eragon