your training however is going to present a problem for both varden and elves
brom obviously had a chance to teach you but we need to know how thorough he was
for that reason you will have to be tested to determine the extent of your abilities
also the elves will expect you to finish your training with them though i am not sure if there is time for that
for several reasons
chief among them the tidings you brought about the urgals said ajihad his eyes straying to saphira
you see eragon the varden are in an extremely delicate position
on one hand we have to comply with the elves wishes if we want to keep them as allies
at the same time we cannot anger the dwarves if we wish to lodge in tronjheim
are not the dwarves part of the varden asked eragon
ajihad hesitated
in a sense yes
they allow us to live here and provide assistance in our struggle against the empire but they are loyal only to their king
i have no power over them except for what hrothgar gives me and even he often has trouble with the dwarf clans
the thirteen clans are subservient to hrothgar but each clan chief wields enormous power they choose the new dwarf king when the old one dies
hrothgar is sympathetic to our cause but many of the chiefs are not
he can not afford to anger them unnecessarily or he will lose the support of his people so his actions on our behalf have been severely circumscribed
these clan chiefs said eragon are they against me as well
even more so i am afraid said ajihad wearily
there has long been enmity between dwarves and dragons before the elves came and made peace dragons made a regular habit of eating the dwarves flocks and stealing their gold and the dwarves are slow to forget past wrongs
indeed they never fully accepted the riders or allowed them to police their kingdom
galbatorix is rise to power has only served to convince many of them that it would be better never to deal with riders or dragons ever ** he directed his last words at saphira
eragon said slowly why does not galbatorix know where farthen dur and ellesmera are surely he was told of them when he was instructed by the riders
told of them yes shown where they are no
it is one thing to know that farthen dur lies within these mountains quite another to find it
galbatorix had not been taken to either place before his dragon was killed
after that of course the riders did not trust him
he tried to force the information out of several riders during his rebellion but they chose to die rather than reveal it to him
as for the dwarves he is never managed to capture one alive though it is only a matter of time
then why does not he just take an army and march through du weldenvarden until he finds ellesmera asked eragon
because the elves still have enough power to resist him said ajihad
he does not dare test his strength against theirs at least not yet
but his cursed sorcery grows stronger each year
with another rider at his side he would be unstoppable
he keeps trying to get one of his two eggs to hatch but so far he is been unsuccessful
eragon was puzzled
how can his power be increasing the strength of his body limits his abilities it can not build itself up forever
we do not know said ajihad shrugging his broad shoulders and neither do the elves
we can only hope that someday he will be destroyed by one of his own ** he reached inside his vest and somberly pulled out a battered piece of parchment
do you know what this is he asked placing it on the desk
eragon bent forward and examined it
lines of black script written in an alien language were inked across the page
large sections of the writing had been destroyed by blots of blood
one edge of the parchment was charred
he shook his head
no i do not
it was taken from the leader of the urgal host we destroyed last night
it cost us twelve men to do so they sacrificed themselves so that you might escape safely
the writing is the king is invention a script he uses to communicate with his servants
it took me a while but i was able to devise its meaning at least where it is legible
it reads
gatekeeper at ithro zhada is to let this bearer and his minions pass