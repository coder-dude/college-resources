the tip of saphira is tail twitched and her scales clinked against a worn dome of rock that protruded from the ground
oh he is hopeless
lifting and extending her neck saphira nipped the venison spit and all from roran is other hand
the wood cracked between her serrated teeth as she bit down and then it and the meat vanished into the fiery depths of her belly
mmm
you did not exaggerate she said to roran
what a sweet and succulent morsel so soft so salty so deliciously delectable it makes me want to wiggle with delight
you should cook for me more often roran stronghammer
only next time i think you should prepare several deer at once
otherwise i wo not get a proper meal
roran hesitated as if unable to decide whether her request was serious and if so how he could politely extricate himself from such an unlooked for and rather onerous obligation
he cast a pleading glance at eragon who burst out laughing both at roran is expression and at his predicament
the rise and fall of saphira is sonorous laugh joined with eragon is and reverberated throughout the hollow
her teeth gleamed madder red in the light from the embers
without moving eragon whispered in his mind saphira
what if i am right and he is in helgrind i do not know what i should do then
tell me what i should do
i cannot little one
this is a decision you have to make by yourself
the ways of men are not the ways of dragons
i would tear off his head and feast on his body but that would be wrong for you i think
will you stand by me whatever i decide
always little one
now rest
all will be well
comforted eragon gazed into the void between the stars and slowed his breathing as he drifted into the trance that had replaced sleep for him
he remained conscious of his surroundings but against the backdrop of the white constellations the figures of his waking dreams strode forth and performed confused and shadowy plays as was their wont