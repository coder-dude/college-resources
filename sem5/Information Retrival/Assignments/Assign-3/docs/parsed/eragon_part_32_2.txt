very quietly he whispered seven words from the ancient language then even more softly told him what they meant
that is all i can give you
use them only in great need
brom blindly turned his eyes to the ceiling
and now he murmured for the greatest adventure of all
weeping eragon held his hand comforting him as best he could
his vigil was unwavering and steadfast unbroken by food or drink
as the long hours passed a gray pallor crept over brom and his eyes slowly dimmed
his hands grew icy the air around him took on an evil humor
powerless to help eragon could only watch as the ra zac is wound took its toll
the evening hours were young and the shadows long when brom suddenly stiffened
eragon called his name and cried for murtagh is help but they could do nothing
as a barren silence dampened the air brom locked his eyes with eragon is
then contentment spread across the old man is face and a whisper of breath escaped his lips
and so it was that brom the storyteller died
with shaking fingers eragon closed brom is eyes and stood
saphira raised her head behind him and roared mournfully at the sky keening her lamentation
tears rolled down eragon is cheeks as a sense of horrible loss bled through him
haltingly he said we have to bury him
we might be seen warned murtagh
murtagh hesitated then bore brom is body out of the cave along with his sword and staff
saphira followed them
to the top eragon said thickly indicating the crown of the sandstone hill
we can not dig a grave out of stone objected murtagh
eragon climbed onto the smooth hilltop struggling because of his ribs
there murtagh lay brom on the stone
eragon wiped his eyes and fixed his gaze on the sandstone
gesturing with his hand he said moi ** the stone rippled
it flowed like water forming a body length depression in the hilltop
molding the sandstone like wet clay he raised waist high walls around it
they laid brom inside the unfinished sandstone vault with his staff and sword
stepping back eragon again shaped the stone with magic
it joined over brom is motionless face and flowed upward into a tall faceted spire
as a final tribute eragon set runes into the stone