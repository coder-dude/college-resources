roran was helping elain hang a hammock when he became embroiled in a heated dispute between odele her family and frewin who had apparently deserted torson is crew to stay with odele
the two of them wanted to marry which odele is parents vehemently opposed on the grounds that the young sailor lacked a family of his own a respectable profession and the means to provide even a modicum of comfort for their daughter
roran thought it best if the enamored couple remained together it seemed impractical to try and separate them while they remained confined to the same ship but odele is parents refused to give his arguments credence
frustrated roran said what would you do then you can not lock her away and i believe frewin has proved his devotion more than
the cry came from the crow is nest
without a second thought roran yanked his hammer from his belt whirled about and scrambled up the ladder through the fore hatchway barking his shin on the way
he sprinted toward the knot of people on the quarterdeck coming to a halt beside horst
one of the ra zac is dread steeds drifted like a tattered shadow above the edge of the coastline a ra zac on its back
seeing the two monsters exposed in daylight in no way diminished the creeping horror they inspired in roran
he shuddered as the winged creature uttered its terrifying shriek and then the ra zac is insectile voice drifted across the water faint but distinct you shall not **
roran looked at the ballistae but they could not turn far enough to aim at the ra zac or its mount
does anyone have a bow
i do said baldor
he dropped to one knee and began to string his weapon
do not let them see ** everyone on the quarterdeck gathered in a tight circle around baldor shielding him with their bodies from the ra zac is malevolent gaze
why do not they attack growled horst
puzzled roran searched for an explanation but found none
it was jeod who suggested perhaps it is too bright for them
the ra zac hunt at night and so far as i know they do not willingly venture forth from their lairs while the sun is yet in the sky
it is not just that said gertrude slowly
i think they re afraid of the ocean
afraid of the ocean scoffed horst
watch them they do not fly more than a yard over the water at any given time
she is right said ** last a weakness i can use against them
a few seconds later baldor said **
at his word the ranks of people who stood before him jumped aside clearing the path for his arrow
baldor sprang to his feet and in a single motion pulled the feather to his cheek and loosed the reed shaft
it was a heroic shot
the ra zac was at the extreme edge of a longbow is range far beyond any mark roran had seen an archer hit and yet baldor is aim was true
his arrow struck the flying creature on the right flank and the beast gave a scream of pain so great that the glass on the deck was shattered and the stones on the shore were riven in shards
roran clapped his hands over his ears to protect them from the hideous blast
still screaming the monster veered inland and dropped behind a line of misty hills
did you kill it asked jeod his face pale
i fear not replied baldor
it was naught but a flesh wound
loring who had just arrived observed with satisfaction aye
but at least you hurt him and i d wager they will think twice about bothering us again
gloom settled over roran
save your triumph for later loring
this was no victory
because now the empire knows exactly where we ** the quarterdeck fell silent as they grasped the implications of what he had said
and this said trianna is the latest pattern we ve invented
nasuada took the black veil from the sorceress and ran it through her hands marveling at its quality
no human could throw lace that fine
she gazed with satisfaction at the rows of boxes on her desk which contained samples of the many designs du vrangr gata now produced
you ve done well she said
far better than i had hoped
tell your spellcasters how pleased i am with their work
it means much to the varden
trianna inclined her head at the praise
i will convey your message to them lady nasuada