now everyone ** he backed into the tunnel pulling murtagh with him and keeping his eyes on eragon
saphira what should i do eragon asked quickly as the men and dwarves followed murtagh is captor leading the horses along with them
go with them she counseled and hope that we live
she entered the tunnel herself eliciting nervous glances from those around her
reluctantly eragon followed her aware that the warriors eyes were upon him
his rescuer the dwarf walked alongside him with a hand on the haft of his war ax
utterly exhausted eragon staggered into the mountain
the stone doors swung shut behind them with only a whisper of sound
he looked back and saw a seamless wall where the opening had been
they were trapped inside
but were they any safer