she is very young as are you
it took glaedr and i decades before we fully understood each other
a rider is bond with his dragon is like any relationship that is a work in progress
do you trust her
then humor her
you were brought up as an orphan
she was brought up to believe that she was the last sane individual of her entire race
and now she has been proved wrong
do not be surprised if it takes some months before she stops pestering glaedr and returns her attention to you
eragon rolled a blueberry between his thumb and forefinger his appetite had vanished
why do not elves eat meat
why should we oromis held up a strawberry and rotated it so that the light reflected off its dimpled skin and illuminated the tiny hairs that bearded the fruit
everything that we need or want we sing from the plants including our food
it would be barbaric to make animals suffer that we might have additional courses on the table
our choice will make greater sense to you before long
eragon frowned
he had always eaten meat and did not look forward to living solely on fruit and vegetables while in ellesmera
do not you miss the taste
you cannot miss that which you have never had
what about glaedr though he can not live off grass
no but neither does he needlessly inflict pain
we each do the best we can with what we are given
you cannot help who or what you are born as
and islanzadi her cape was made of swan feathers
loose feathers gathered over the course of many years
no birds were killed to make her garment
they finished the meal and eragon helped oromis to scour the dishes clean with sand
as the elf stacked them in the cupboard he asked did you bathe this morning the question startled eragon but he answered that no he had not
please do so tomorrow then and every day following
every ** the water is too cold for that
i will catch the ague
oromis eyed him oddly
then make it warmer
now it was eragon is turn to look askance
i am not strong enough to heat an entire stream with magic he protested
the house echoed as oromis laughed
outside glaedr swung his head toward the window and inspected the elf then returned to his earlier position
i assume that you explored your quarters last ** eragon nodded
and you saw a small room with a depression in the floor
i thought that it might be for washing clothes or linens
it is for washingyou
two nozzles are concealed in the side of the wall above the hollow
open them and you can bathe in water of any temperature
also he gestured at eragon is chin while you are my student i expect you to keep yourself clean shaven until you can grow a full beard if you so choose and not look like a tree with half its leaves blown off
elves do not shave but i will have a razor and mirror found and sent to you
wincing at the blow to his pride eragon agreed
they returned outside whereupon oromis looked at glaedr and the dragon said we have decided upon a curriculum for saphira and you
the elf said you will start
an hour after sunrise tomorrow in the time of the red lily
return here then
and bring the saddle that brom made for you saphira continued oromis
do what you wish in the meantime ellesmera holds many wonders for a foreigner if you care to see them
i will keep that in mind said eragon bowing his head
before i go master i want to thank you for helping me in tronjheim after i killed durza
i doubt that i would have survived without your assistance
i am in your debt
we are both in your debt added saphira
oromis smiled slightly and inclined his head