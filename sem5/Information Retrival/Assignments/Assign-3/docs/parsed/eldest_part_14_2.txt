even as he spoke people congregated on the edge of the field staring at what was left of the ra zac is camp and the dead soldier
what happened cried fisk
loring scuttled forward and stared the carpenter in the eye
what happened i will tell you whathappened
we routed the dung beardlings
caught them with their boots off and whipped them like **
i am ** the strong voice came from birgit an auburn haired woman who clasped nolfavrell against her bosom ignoring the blood smeared across his face
they deserve to die like cowards for my husband is death
the villagers murmured in agreement but then thane spoke have you gone mad horst even if you frightened off the ra zac and their soldiers galbatorix will just send more men
the empire will never give up until they get roran
we should hand him over snarled sloan
horst raised his hands
i agree no one is worth more than all of carvahall
but if we surrender roran do you really think galbatorix will let us escape punishment for our resistance in his eyes we re no better than the varden
thenwhy did you attack demanded thane
who gave you the authority to make this decision you ve doomed us **
this time birgit answered
would you let them kill your wife she pressed her hands on either side of her son is face then showed thane her bloody palms like an accusation
would you let them burn us
where is your manhood loam breaker
he lowered his gaze unable to face her stark expression
they burned my farm said roran devoured quimby and nearly destroyed carvahall
such crimes cannot go unpunished
are we frightened rabbits to cower down and accept our fate ** we have a right to defend ** he stopped as albriech and baldor trudged up the street dragging the wagon
we can debate later
now we have to prepare
who will help us
forty or more men volunteered
together they set about the difficult task of making carvahall impenetrable
roran worked incessantly nailing fence slats between houses piling barrels full of rocks for makeshift walls and dragging logs across the main road which they blocked with two wagons tipped on their sides
as roran hurried from one chore to another katrina waylaid him in an alley
she hugged him then said i am glad you re back and that you re safe
he kissed her lightly
katrina
i have to speak with you as soon as we re ** she smiled uncertainly but with a spark of hope
you were right it was foolish of me to delay
every moment we spend together is precious and i have no desire to squander what time we have when a whim of fate could tear us apart
roran was tossing water on the thatching of kiselt is house so it could not catch fire when parr shouted ra **
dropping the bucket roran ran to the wagons where he had left his hammer
as he grabbed the weapon he saw a single ra zac sitting on a horse far down the road almost out of bowshot
the creature was illuminated by a torch in its left hand while its right was drawn back as if to throw something
roran laughed
is he going to toss rocks at us he is too far away to even hit he was cut off as the ra zac whipped down its arm and a glass vial arched across the distance between them and shattered against the wagon to his right
an instant later a fireball launched the wagon into the air while a fist of burning air flung roran against a wall
dazed he fell to his hands and knees gasping for breath
through the roaring in his ears came the tattoo of galloping horses
he forced himself upright and faced the sound only to dive aside as the ra zac raced into carvahall through the burning gap in the wagons
the ra zac reined in their steeds blades flashing as they hacked at the people strewn around them
roran saw three men die then horst and loring reached the ra zac and began pressing them back with pitchforks
before the villagers could rally soldiers poured through the breach killing indiscriminately in the darkness
roran knew they had to be stopped else carvahall would be taken
he jumped at a soldier catching him by surprise and hit him in the face with the hammer is blade
the soldier crumpled without a sound
as the man is compatriots rushed toward him roran wrestled the corpse is shield off his limp arm