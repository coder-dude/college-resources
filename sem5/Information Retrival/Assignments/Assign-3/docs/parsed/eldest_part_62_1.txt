from there it took them almost half an hour to locate trianna is tent which apparently served as the unofficial headquarters of du vrangr gata
they had difficulty finding the tent because few people knew of its existence and even fewer could tell them where it lay because the tent was hidden behind a spur of rock that served to conceal it from the gaze of enemy magicians in galbatorix is army
as eragon and saphira approached the black tent the entrance was thrust open and trianna strode out her arms bare to the elbow in preparation to use magic
behind her clustered a group of determined if frightened looking spellcasters many of whom eragon had seen during the battle in farthen dur either fighting or healing the wounded
eragon watched as trianna and the others reacted with the now expected surprise at his altered appearance
lowering her arms trianna said shadeslayer saphira
you should have told us sooner that you were here
we ve been preparing to confront and battle what we thought was a mighty foe
i did not mean to upset you said eragon but we had to report to nasuada and king orrin immediately after we landed
and why have you graced us with your presence now you never deigned to visit us before we who are more your brethren than any in the varden
i have come to take command of du vrangr ** the assembled spellcasters muttered with surprise at his announcement and trianna stiffened
eragon felt several magicians probe his consciousness in an attempt to divine his true intentions
instead of guarding himself which would blind him to impending attacks eragon retaliated by jabbing the minds of the would be invaders hard enough that they retreated behind their own barriers
as he did eragon had the satisfaction of seeing two men and a woman flinch and avert their gazes
ah said the sorceress with a triumphant smile but nasuada has no direct authority over us
we help the varden of our own free will
her resistance puzzled eragon
i am sure nasuada would be surprised to hear that after everything she and her father have done for du vrangr gata
it might give her the impression that you no longer wanted the support and protection of the ** he let the threat hang in the air for a moment
besides i seem to remember you were willing to give me this post before
why not now
trianna lifted an eyebrow
you refused my offer shadeslayer
or have you forgotten composed as she was a trace of defensiveness colored her response and eragon suspected she knew her position was untenable
she seemed more mature to him than when they last met and he had to remind himself of the hardships she must have endured since marching across alagaesia to surda supervising the magicians of du vrangr gata and preparing for war
we could not accept then
it was the wrong time
abruptly changing tack she asked why does nasuada believe you should command us anyway surely you and saphira would be more useful elsewhere
nasuada wants me to lead you du vrangr gata in the coming battle and so i ** eragon thought it best not to mention that it was his idea
a dark scowl gave trianna a fierce appearance
she pointed at the cluster of spellcasters behind her
we have devoted our lives to the study of our art
you have been casting spells for less than two years
what makes you more qualified for this task than any of us
no matter
tell me what is your strategy how do you plan to employ us
my plan is simple he said
the lot of you will join minds and search for enemy spellcasters
when you find one i will add my strength to yours and together we can crush the spellcaster is resistance
then we can slay the troops that previously were protected by his or her wards
and what will you be doing the rest of the time
after an awkward silence one of the men behind trianna said it is a good ** he quailed as trianna cast an angry glare at him
she slowly faced eragon again
ever since the twins died i have led du vrangr gata
under my guidance they have provided the means to fund the varden is war effort ferreted out the black hand galbatorix is network of spies that tried to assassinate nasuada as well as performing innumerable other services