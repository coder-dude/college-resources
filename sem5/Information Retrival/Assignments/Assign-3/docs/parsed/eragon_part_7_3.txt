an amulet or trinket for a lady with a twirl he pulled out a delicately carved silver rose of excellent workmanship
the polished metal caught eragon is attention and he eyed it appreciatively
the trader continued not even three crowns though it has come all the way from the famed craftsmen of belatona
garrow spoke in a quiet voice
we are not looking to buy but to ** merlock immediately covered the rose and looked at them with new interest
i see
maybe if this item is of any value you would like to trade it for one or two of these exquisite ** he paused for a moment while eragon and his uncle stood uncomfortably then continued you didbring the object of consideration
we have it but we would rather show it to you elsewhere said garrow in a firm voice
merlock raised an eyebrow but spoke smoothly
in that case let me invite you to my ** he gathered up his wares and gently laid them in an iron bound chest which he locked
then he ushered them up the street and into the temporary camp
they wound between the wagons to a tent removed from the rest of the traders
it was crimson at the top and sable at the bottom with thin triangles of colors stabbing into each other
merlock untied the opening and swung the flap to one side
small trinkets and strange pieces of furniture such as a round bed and three seats carved from tree stumps filled the tent
a gnarled dagger with a ruby in the pommel rested on a white cushion
merlock closed the flap and turned to them
please seat ** when they had he said now show me why we are meeting in ** eragon unwrapped the stone and set it between the two men
merlock reached for it with a gleam in his eye then stopped and asked may i when garrow indicated his approval merlock picked it up
he put the stone in his lap and reached to one side for a thin box
opened it revealed a large set of copper scales which he set on the ground
after weighing the stone he scrutinized its surface under a jeweler is glass tapped it gently with a wooden mallet and drew the point of a tiny clear stone over it
he measured its length and diameter then recorded the figures on a slate
he considered the results for a while
do you know what this is worth
no admitted garrow
his cheek twitched and he shifted uncomfortably on the seat
merlock grimaced
unfortunately neither do i
but i can tell you this much the white veins are the same material as the blue that surrounds them only a different color
what that material might be though i have not a clue
it is harder than any rock i have seen harder even than diamond
whoever shaped it used tools i have never seen or magic
also it is hollow
an irritated edge crept into merlock is voice
did you ever hear a rock sound like this he grabbed the dagger from the cushion and slapped the stone with the flat of the blade
a pure note filled the air then faded away smoothly
eragon was alarmed afraid that the stone had been damaged
merlock tilted the stone toward them
you will find no scratches or blemishes where the dagger struck
i doubt i could do anything to harm this stone even if i took a hammer to it
garrow crossed his arms with a reserved expression
a wall of silence surrounded him
eragon was ** knew that the stone appeared in the spine through magic but made by magic what for and why he blurted but what is it worth
i can not tell you that said merlock in a pained voice
i am sure there are people who would pay dearly to have it but none of them are in carvahall
you would have to go to the southern cities to find a buyer
this is a curiosity for most people not an item to spend money on when practical things are needed
garrow stared at the tent ceiling like a gambler calculating the odds
will you buy it
the trader answered instantly it is not worth the risk
i might be able to find a wealthy buyer during my spring travels but i can not be certain
even if i did you would not be paid until i returned next year
no you will have to find someone else to trade with
i am curious however
why did you insist on talking to me in private
eragon put the stone away before answering