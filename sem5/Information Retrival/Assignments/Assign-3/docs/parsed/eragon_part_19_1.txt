dawn was gray and overcast with a cutting wind
the forest was quiet
after a light breakfast brom and eragon doused the fire and shouldered their packs preparing to leave
eragon hung his bow and quiver on the side of his pack where he could easily reach them
saphira wore the saddle she would have to carry it until they got horses
eragon carefully tied zar roc onto her back too as he did not want the extra weight
besides in his hands the sword would be no better than a club
eragon had felt safe inside the bramble but outside wariness crept into his movements
saphira took off and circled overhead
the trees thinned as they returned to the farm
i will see this place again eragon insisted to himself looking at the ruined ** cannot will not be a permanent exile
someday when it is safe i will return
throwing back his shoulders he faced south and the strange barbaric lands that lay there
as they walked saphira veered west toward the mountains and out of sight
eragon felt uncomfortable as he watched her go
even now with no one around they could not spend their days together
she had to stay hidden in case they met a fellow traveler
the ra zac is footprints were faint on the eroding snow but eragon was unconcerned
it was unlikely that they had forsaken the road which was the easiest way out of the valley for the wilderness
once outside the valley however the road divided in several places
it would be difficult to ascertain which branch the ra zac had taken
they traveled in silence concentrating on speed
eragon is legs continued to bleed where the scabs had cracked
to take his mind off the discomfort he asked so what exactly can dragons do you said that you knew something of their abilities
brom laughed his sapphire ring flashing in the air as he gestured
unfortunately it is a pitiful amount compared to what i would like to know
your question is one people have been trying to answer for centuries so understand that what i tell you is by its very nature incomplete
dragons have always been mysterious though maybe not on purpose
before i can truly answer your question you need a basic education on the subject of dragons
it is hopelessly confusing to start in the middle of such a complex topic without understanding the foundation on which it stands
i will begin with the life cycle of dragons and if that does not wear you out we can continue to another topic
brom explained how dragons mate and what it took for their eggs to hatch
you see he said when a dragon lays an egg the infant inside is ready to hatch
but it waits sometimes for years for the right circumstances
when dragons lived in the wild those circumstances were usually dictated by the availability of food
however once they formed an alliance with the elves a certain number of their eggs usually no more than one or two were given to the riders each year
these eggs or rather the infants inside would not hatch until the person destined to be its rider came into their presence though how they sensed that is not known
people used to line up to touch the eggs hoping that one of them might be picked
do you mean that saphira might not have hatched for me asked eragon
quite possibly if she had not liked you
he felt honored that of all the people in alagaesia she had chosen him
he wondered how long she had been waiting then shuddered at the thought of being cramped inside an egg surrounded by darkness
brom continued his lecture
he explained what and when dragons ate
a fully grown sedentary dragon could go for months without food but in mating season they had to eat every week
some plants could heal their sicknesses while others would make them ill
there were various ways to care for their claws and clean their scales
he explained the techniques to use when attacking from a dragon and what to do if you were fighting one whether on foot horseback or with another dragon
their bellies were armored their armpits were not
eragon constantly interrupted to ask questions and brom seemed pleased by the inquiries
hours passed unheeded as they talked
when evening came they were near therinsford