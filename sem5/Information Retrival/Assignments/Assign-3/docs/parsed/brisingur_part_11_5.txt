thank you said eragon accepting them
tenga ignored him and sat cross legged next to the fireplace
he continued to grumble and mutter into his beard as he devoured his lunch
after eragon had scraped his plate clean and drained the last drops of the fine harvest ale and tenga had also nearly completed his repast eragon could not help but ask did the elves build this tower
tenga fixed him with a pointed gaze as if the question made him doubt eragon is intelligence
aye
the tricky elves built edur ithindra
what is it you do here are you all alone or
i search for the ** exclaimed tenga
a key to an unopened door the secret of the trees and the plants
fire heat lightning light
most do not know the question and wander in ignorance
others know the question but fear what the answer will mean
** for thousands of years we have lived like savages
** i shall end that
i shall usher in the age of light and all shall praise my deed
pray tell what exactly do you search for
a frown twisted tenga is face
you do not know the question i thought you might
but no i was mistaken
still i see you understand my search
you search for a different answer but you search nevertheless
the same brand burns in your heart as burns in mine
who else but a fellow pilgrim can appreciate what we must sacrifice to find the answer
he is mad thought eragon
casting about for something with which he could distract tenga his gaze lit upon a row of small wood animal statues arranged on the sill below a teardrop shaped window
those are beautiful he said indicating the statues
who made them
she did
before she left
she was always making ** tenga bounded upright and placed the tip of his left index finger on the first of the statues
here the squirrel with his waving tail he so bright and swift and full of laughing ** his finger drifted to the next statue in line
here the savage boar so deadly with his slashing tusks
here the raven with
tenga paid no attention as eragon backed away nor when he lifted the latch to the door and slipped out of edur ithindra
shouldering his pack eragon trotted down through the crown of oak trees and away from the cluster of five hills and the demented spellcaster who resided among them
that practice however forced him to spend the night in the village of eastcroft twenty miles north of melian
he had intended to abandon the road long before he arrived at eastcroft and find a sheltered hollow or cave where he might rest until morn but because of his relative unfamiliarity with the land he misjudged the distance and came upon the village while in the company of three men at arms
leaving then less than an hour from the safety of eastcroft is walls and gates and the comfort of a warm bed would have inspired even the slowest dullard to ask why he was trying to avoid the village
so eragon set his teeth and silently rehearsed the stories he had concocted to explain his trip
the bloated sun was two fingers above the horizon when eragon first beheld eastcroft a medium sized village enclosed by a tall palisade
it was almost dark by the time he finally arrived at the village and entered through the gate
behind him he heard a sentry ask the men at arms if anyone else had been close behind them on the road
that is good enough for me replied the sentry
if there are laggards they will have to wait until tomorrow to get ** to another man on the opposite side of the gate he shouted close it ** together they pushed the fifteen foot tall ironbound doors shut and barred them with four oak beams as thick as eragon is chest
they must expect a siege thought eragon and then smiled at his own blindness
well who does not expect trouble in these times a few months ago he would have worried about being trapped in eastcroft but now he was confident he could scale the fortifications barehanded and if he concealed himself with magic escape unnoticed in the gloom of night
he chose to stay however for he was tired and casting a spell might attract the attention of nearby magicians if there were any
before he took more than a few steps down the muddy lane that led to the town square a watchman accosted him thrusting a lantern toward his face