for instance who are your friends and why were you hiding in carvahall i trust you with my life which is why i am still traveling with you but i need to know more about who you are and what you are doing
what did you steal in gil ead and what is the tuatha du orothrim that you re taking me through i think that after all that is happened i deserve an explanation
i see that you have yet to learn proper manners said brom grimly tugging on his beard
what makes you think that this concerns you
nothing really said eragon shrugging
just it is an odd coincidence that you happened to be hiding in carvahall when i found saphira is eggand that you also know so much dragonlore
the more i think about it the less likely it seems
there were other clues that i mostly ignored but they re obvious now that i look back
like how you knew of the ra zac in the first place and why they ran away when you approached
and i can not help but wonder if you had something to do with the appearance of saphira is egg
there is a lot you have not told us and saphira and i can not afford to ignore anything that might be dangerous
dark lines appeared on brom is forehead as he reined snowfire to a halt
you wo not wait he asked
eragon shook his head mulishly
brom sighed
this would not be a problem if you were not so suspicious but i suppose that you would not be worth my time if you were ** eragon was unsure if he should take that as a compliment
brom lit his pipe and slowly blew a plume of smoke into the air
i will tell you he said but you have to understand that i cannot reveal ** eragon started to protest but brom cut him off
it is not out of a desire to withhold information but because i wo not give away secrets that are not mine
there are other stories woven in with this narrative
you will have to talk with the others involved to find out the rest
very well
explain what you can said eragon
are you sure asked brom
there are reasons for my secretiveness
i ve tried to protect you by shielding you from forces that would tear you apart
once you know of them and their purposes you will never have the chance to live quietly
you will have to choose sides and make a stand
do you really want to know
i cannot live my life in ignorance said eragon quietly
a worthy goal
very well there is a war raging in alagaesia between the varden and the empire
their conflict however reaches far beyond any incidental armed clashes
they are locked in a titanic power struggle
centered around you
me said eragon disbelieving
that is impossible
i do not have anything to do with either of them
not yet said brom but your very existence is the focus of their battles
the varden and the empire are not fighting to control this land or its people
their goal is to control the next generation of riders of whom you are the first
whoever controls these riders will become the undisputed master of alagaesia
eragon tried to absorb brom is statements
it seemed incomprehensible that so many people would be interested in him and saphira
no one besides brom had thought he was that important
the whole concept of the empire and varden fighting over him was too abstract for him to grasp fully
objections quickly formed in his mind
but all the riders were killed except for the forsworn who joined galbatorix
as far as i know even those are now dead
and you told me in carvahall that no one knows if there are still dragons in alagaesia
i lied about the dragons said brom flatly
even though the riders are gone there are still three dragon eggs left all of them in galbatorix is possession
actually there are only two now since saphira hatched
the king salvaged the three during his last great battle with the riders
so there may soon be two new riders both of them loyal to the king asked eragon with a sinking feeling
exactly said brom
there is a deadly race in progress
galbatorix is desperately trying to find the people for whom his eggs will hatch while the varden are employing every means to kill his candidates or steal the eggs
but where did saphira is egg come from how could anyone have gotten it away from the king and why do you know all of this asked eragon bewildered