a shudder ran up the mast jarring roran is teeth and the crow is nest swung in the new direction making him giddy with vertigo
fear gripped roran when they continued to slow
he slashed off his bindings and with reckless disregard for his own safety swung himself over the edge of the basket grabbed the ropes underneath and shinnied down the rigging so quickly that he lost his grip once and fell several feet before he could catch himself
he jumped to the deck ran to the fore hatchway and descended to the first bank of oars where he joined baldor and albriech on an oak pole
they said not a word but labored to the sound of their own desperate breathing the frenzied beat of the drum bonden is hoarse shouts and the roar of the boar is eye
roran could feel the mighty whirlpool resisting with every stroke of the oar
and yet their efforts could not keep thedragon wing from coming to a virtual ** re not going to make it thought roran
his back and legs burned from the exertion
his lungs stabbed
between the drumbeats he heard uthar ordering the hands above deck to trim the sails to take full advantage of the fickle wind
two places ahead of roran darmmen and hamund surrendered their oar to thane and ridley then lay in the middle of the aisle their limbs trembling
less than a minute later someone else collapsed farther down the gallery and was immediately replaced by birgit and another woman
if we survive thought roran it will only be because we have enough people to sustain this pace however long is necessary
it seemed an eternity that he worked the oar in the murky smoky room first pushing then pulling doing his best to ignore the pain mounting within his body
his neck ached from hunching underneath the low ceiling
the dark wood of the pole was streaked with blood where his skin had blistered and torn
he ripped off his shirt dropping the spyglass to the floor wrapped the cloth around the oar and continued rowing
at last roran could do no more
his legs gave way and he fell on his side slipping across the aisle because he was so sweaty
orval took his place
roran lay still until his breath returned then pushed himself onto his hands and knees and crawled to the hatchway
like a fever mad drunk he pulled himself up the ladder swaying with the motion of the ship and often slumping against the wall to rest
when he came out on deck he took a brief moment to appreciate the fresh air then staggered aft to the helm his legs threatening to cramp with every step
how goes it he gasped to uthar who manned the wheel
peering over the gunwale roran espied the three sloops perhaps a half mile away and slightly more to the west closer to the center of the eye
the sloops appeared motionless in relation to thedragon wing
at first as roran watched the positions of the four ships remained unchanged
then he sensed a shift in thedragon wing is speed as if the ship had crossed some crucial point and the forces restraining her had diminished
it was a subtle difference and amounted to little more than a few additional feet per minute but it was enough that the distance between thedragon wing and the sloops began to increase
with every stroke of the oars thedragon wing gained momentum
the sloops however could not overcome the whirlpool is dreadful strength
their oars gradually slowed until one by one the ships drifted backward and were drawn toward the veil of mist beyond which waited the gyrating walls of ebony water and the gnashing rocks at the bottom of the ocean floor
they can not keep rowing realized ** crews are too small and they re too tired
he could not help but feel a pang of sympathy for the fate of the men on the sloops
at that precise instant an arrow sprang from the nearest sloop and burst into green flame as it raced toward thedragon wing
the dart must have been sustained by magic to have flown so far
it struck the mizzen sail and exploded into globules of liquid fire that stuck to whatever they touched
within seconds twenty small fires burned along the mizzenmast the mizzen sail and the deck below