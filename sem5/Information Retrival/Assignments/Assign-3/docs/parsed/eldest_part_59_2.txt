an this d be our destination since food wo not last us to reavstone
how we get there though without being overtaken is beyond me
without our mizzen topgallant those accursed sloops will catch us by noon tomorrow evening if we manage the sails well
can we replace the mast asked jeod
vessels of this size carry spars to make just such repairs
uthar shrugged
we could provided we had a proper ship is carpenter among us
seeing as we do not i d rather not let inexperienced hands mount a spar only to have it crash down on deck and perhaps injure somebody
roran said if it were not for the magician or magicians i d say we should stand and fight since we far outnumber the crews of the sloops
as it is i am chary of battle
it seems unlikely that we could prevail considering how many ships sent to help the varden have disappeared
grunting uthar drew a circle around their current position
this d be how far we can sail by tomorrow evening assuming the wind stays with us
we could make landfall somewhere on beirland or nia if we wanted but i can not see how that d help us
we d be trapped
the soldiers on those sloops or the ra zac or galbatorix himself could hunt us at his leisure
roran scowled as he considered their options a fight with the sloops appeared inevitable
for several minutes the cabin was silent except for the slap of waves against the hull
then jeod placed his finger on the map between beirland and nia looked at uthar and asked what about the boar is eye
to roran is amazement the scarred sailor actually blanched
i d not risk that master jeod not on my life
i d rather face the sloops an die in the open sea than go to that doomed place
there has consumed twice as many ships as in galbatorix is fleet
i seem to recall reading said jeod leaning back in his chair that the passage is perfectly safe at high tide and low tide
is that not so
with great and evident reluctance uthar admitted aye
but the eye is so wide it requires the most precise timing to cross without being destroyed
we d be hard pressed to accomplish that with the sloops near on our tail
if we could though pressed jeod if we could time it right the sloops would be wrecked or if their nerve failed them forced to circumvent nia
it would give us time to find a place to hide along beirland
if if
you d send us to the crushing deep you would
come now uthar your fear is unreasoning
what i propose is dangerous i admit but no more than fleeing teirm was
or do you doubt your ability to sail the gap are you not man enough to do it
uthar crossed his bare arms
you ve never seen the eye have you sir
it is not that i am not man enough but that the eye far exceeds the strength of men it puts to shame our biggest ships our grandest buildings an anything else you d care to name
tempting it would be like trying to outrun an avalanche you might succeed but then you just as well might be ground into dust
what asked roran is this boar is eye
the all devouring maw of the ocean proclaimed uthar
in a milder tone jeod said it is a whirlpool roran
the eye forms as the result of tidal currents that collide between beirland and nia
when the tide waxes the eye rotates north to west
when the tide wanes it rotates north to east
uthar shook his head queue whipping the sides of his wind burned neck and laughed
not so dangerous he ** **
what you fail to comprehend continued jeod is the size of the vortex
on average the center of the eye is a league in diameter while the arms of the pool can be anywhere from ten to fifteen miles across
ships unlucky enough to be snared by the eye are borne down to the floor of the ocean and dashed against the jagged rocks therein
remnants of the vessels are often found as flotsam on the beaches of the two islands
would anyone expect us to take this route roran queried
no an for good reason growled uthar
jeod shook his head at the same time
is it even possible for us to cross the eye
it d be a blasted fool thing to do
roran nodded
i know it is not something you want to risk uthar but our options are limited
i am no seaman so i must rely upon your judgment can we cross the eye