eragon laughed as he watched
jeod entertained the crowd with a song he had learned from a book long ago
tara danced a jig
nasuada is teeth flashed as she tossed her head back
and eragon by popular request recounted several of his adventures including a detailed description of his flight from carvahall with brom which was of special interest to his listeners
to think said gertrude the round faced healer tugging on her shawl we had a dragon in our valley and we never even knew ** with a pair of knitting needles produced from within her sleeves she pointed at eragon
to think i nursed you when your legs had been scraped from flying on saphira and i never suspected the ** shaking her head and clucking her tongue she cast on with brown wool yarn and began to knit with speed born of decades of practice
elain was the first to leave the party pleading exhaustion brought on by her advanced stage of pregnancy one of her sons baldor went with her
half an hour later nasuada also made to leave explaining that the demands of her position prevented her from staying as long as she would like but that she wished them health and happiness and hoped they would continue to support her in her fight against the empire
as she moved away from the table nasuada beckoned to eragon
he joined her by the entrance
turning her shoulder to the rest of the tent she said eragon i know that you need time to recover from your journey and that you have affairs of your own that you must tend to
therefore tomorrow and the day after are yours to spend as you will
but on the morning of the third day present yourself at my pavilion and we shall talk about your future
i have a most important mission for you
my ** then he said you keep elva close at hand wherever you go do you not
aye she is my safeguard against any danger that might slip past the nighthawks
also her ability to divine what it is that pains people has proved enormously helpful
it is so much easier to obtain someone is cooperation when you are privy to all of their secret hurts
are you willing to give that up
she studied him with a piercing gaze
you intend to remove your curse from elva
i intend to try
remember i promised her i would
yes i was ** the crash of a falling chair distracted her for an instant then she said your promises will be the death of us
elva is irreplaceable no one else has her skill
and the service she provides as i just testified is worth more than a mountain of gold
i have even thought that of all of us she alone might be able to defeat galbatorix
she would be able to anticipate his every attack and your spell would show her how to counter them and as long as countering them did not require her to sacrifice her life she would prevail
for the good of the varden eragon for the good of everyone in alagaesia could not you feign your attempt to cure elva
no he said biting off the word as if it offended him
i would not do it even if i could
it would be wrong
if we force elva to remain as she is she will turn against us and i do not want her as an ** he paused then at nasuada is expression added besides there is a good chance i may not succeed
removing such a vaguely worded spell is a difficult prospect at best
if i may make a suggestion
be honest with elva
explain to her what she means to the varden and ask her if she will continue to carry her burden for the sake of all free people
she may refuse she has every right to but if she does her character is not one we would want to rely upon anyway
and if she accepts then it shall be of her own free will
with a slight frown nasuada nodded
i shall speak with her tomorrow
you should be present as well to help me persuade her and to lift your curse if we fail
be at my pavilion three hours after ** and with that she swept into the torch lit night outside
much later when the candles guttered in their sockets and the villagers began to disperse in twos and threes roran grasped eragon is arm by the elbow and drew him through the back of the tent to stand by saphira is side where the others could not hear