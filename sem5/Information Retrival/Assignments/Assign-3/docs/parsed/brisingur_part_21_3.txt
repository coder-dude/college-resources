a quick word on the part of eragon with the one armed veteran who was leading the drills was sufficient to secure gedric is temporary release
the tanner ran over to eragon and stood before him his gaze lowered
he was short and swarthy with a jaw like a mastiff is heavy eyebrows and arms thick and gnarled from stirring the foul smelling vats where he had cured his hides
although he was far from handsome eragon knew him to be a kind and honest man
what can i do for you shadeslayer gedric mumbled
you have already done it
and i have come here to thank and repay you
i how have i helped you shadeslayer he spoke slowly cautiously as if afraid eragon were setting a trap for him
soon after i ran away from carvahall you discovered that someone had stolen three ox hides from the drying hut by the vats
am i right
gedric is face darkened with embarrassment and he shuffled his feet
ah well now i did not lock that hut you know
anyone might have snuck in and carried those hides off
besides given what is happened since i can not see as it is much important
i destroyed most of my stock before we trooped into the spine to keep the empire and those filthy ra zac from getting their claws on anything of use
whoever took those hides saved me from having to destroy three more
so let bygones be bygones i say
perhaps said eragon but i still feel honor bound to tell you that it was i who stole your hides
gedric met his gaze then looking at him as if he were an ordinary person without fear awe or undue respect as if the tanner were reevaluating his opinion of eragon
i stole them and i am not proud of it but i needed the hides
without them i doubt i would have survived long enough to reach the elves in du weldenvarden
i always preferred to think that i had borrowed the hides but the truth is i stole them for i had no intention of returning them
therefore you have my apologies
and since i am keeping the hides or what is left of them it seems only right to pay you for ** from within his belt eragon removed one of the spheres of gold hard round and warm from the heat of his flesh and handed it to gedric
gedric stared at the shiny metal pearl his massive jaw clamped shut the lines around his thin lipped mouth harsh and unyielding
he did not insult eragon by weighing the gold in his hand nor by biting it but when he spoke he said i cannot accept this eragon
i was a good tanner but the leather i made was not worth this much
your generosity does you credit but it would bother me to keep this gold
i would feel as if i had not earned it
unsurprised eragon said you would not deny another man the opportunity to haggle for a fair price would you
good
then you cannot deny me this
most people haggle downward
in this case i have chosen to haggle upward but i will still haggle as fiercely as if i were trying to save myself a handful of coins
to me the hides are worth every ounce of that gold and i would not pay you a copper less not even if you held a knife to my throat
gedric is thick fingers closed around the gold orb
since you insist i will not be so churlish as to keep refusing you
no one can say that gedric ostvensson allowed good fortune to pass him by because he was too busy protesting his own unworthiness
my thanks ** he placed the orb in a pouch on his belt wrapping the gold in a patch of wool cloth to protect it from scratches
garrow did right by you eragon
he did right by both you and roran
he may have been sharp as vinegar and as hard and dry as a winter rutabaga but he raised the two of you well
he would be proud of you i think
as gedric turned to rejoin the other villagers he paused
if i may ask eragon why were those hides worth so much to you what did you use them for
eragon chuckled
use them for why with brom is help i made a saddle for saphira out of them
she does not wear it as often as she used to not since the elves gave us a proper dragon is saddle but it served us well through many a scrape and fight and even the battle of farthen dur
astonishment raised gedric is eyebrows exposing pale skin that normally lay hidden in deep folds