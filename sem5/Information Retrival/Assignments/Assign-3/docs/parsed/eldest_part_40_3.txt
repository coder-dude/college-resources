we will try again
the pressure returned to eragon is legs as soon as oromis mouthed his inaudible invocation
eragon was so tired he doubted that he could provide much opposition
nevertheless he reached for the magic
before the ancient language left eragon is mouth he became aware of a curious sensation as the weight constraining his legs lessened at a steady rate
it tickled and felt like he was being pulled out of a mire of cold slick mud
he glanced at oromis and saw the elf is face scribed by passion as if he clung to something precious that he could not bear to lose
a vein throbbed at one of oromis is temples
when eragon is arcane fetters ceased to exist oromis recoiled as if he had been pricked by a wasp and stood with his gaze fixed on his two hands his thin chest heaving
for perhaps a minute he remained thus then he drew himself upright and walked to the very edge of the crags of tel naeir a lone figure outlined against the pale sky
regret and sorrow welled in eragon the same emotions that had gripped him when he first saw glaedr is mutilated foreleg
he cursed himself for being so arrogant with oromis so oblivious to his infirmities and for not placing more confidence in the elf is ** am not the only one who must deal with past injuries
eragon had not fully comprehended what it meant when oromis said that all but the slightest magic escaped his grasp
now he appreciated the depths of oromis is situation and the pain that it must cause him especially for one of his race who was born and bred with magic
eragon went to oromis knelt and bowed in the fashion of the dwarves pressing his bruised forehead against the ground
ebrithil i beg your pardon
the elf gave no indication that he had heard
the two of them lingered in their respective positions while the sun declined before them the birds sang their evening songs and the air grew cool and moist
from the north came the faint offbeat thumps of saphira and glaedr is wing strokes as they returned for the day
in a low distant voice oromis said we will begin anew tomorrow with this and other ** from his profile eragon could tell that oromis had regained his customary expression of impassive reserve
is that agreeable to you
yes master said eragon grateful for the question
i think it best if from now on you endeavor to speak only in the ancient language
we have little time at our disposal and this is the fastest way for you to learn
even when i talk to saphira
adopting the elven tongue eragon vowed then i will work ceaselessly until i not only think but dream in your language
if you achieve that said oromis replying in kind our venture may yet ** he paused
instead of flying directly here in the morning you will accompany the elf i send to guide you
he will take you to where those of ellesmera practice swordplay
stay for an hour then continue on as normal
wo not you teach me yourself asked eragon feeling slighted
i have naught to teach
you are as good a swordsman as ever i have met
i know no more of fighting than you and that which i possess and you do not i cannot give you
all that remains for you is to preserve your current level of skill
why can not i do that with you
master
because i do not appreciate beginning the day with alarum and ** he looked at eragon then relented and added and because it will be good for you to become acquainted with others who live here
i am not representative of my race
but enough of that
look they approach
the two dragons glided across the flat disk of the sun
first came glaedr with a roar of wind blotting out the sky with his massive bulk before he settled on the grass and folded his golden wings then saphira as quick and agile as a sparrow beside an eagle
as they had that morning oromis and glaedr asked a number of questions to ensure that eragon and saphira had paid attention to each other is lessons
they had not always but by cooperating and sharing information between themselves they were able to answer all of the questions
their only stumbling block was the foreign language they were required to communicate in