and then there is the desert itself
what do you know of it
only that it is hot dry and full of sand confessed eragon
that about sums it up replied murtagh
it is filled with poisonous and inedible plants venomous snakes scorpions and a blistering sun
you saw the great plain on our way to gil ead
it was a rhetorical question but eragon answered anyway yes and once before
then you are familiar with its immense range
it fills the heart of the empire
now imagine something two or three times its size and you will understand the vastness of the hadarac desert
that is what you re proposing to cross
eragon tried to envision a piece of land that gigantic but was unable to grasp the distances involved
he retrieved the map of alagaesia from his saddlebags
the parchment smelled musty as he unrolled it on the ground
he inspected the plains and shook his head in amazement
no wonder the empire ends at the desert
everything on the other side is too far away for galbatorix to control
murtagh swept his hand over the right side of the parchment
all the land beyond the desert which is blank on this map was under one rule when the riders lived
if the king were to raise up new riders under his command it would allow him to expand the empire to an unprecedented size
but that was not the point i was trying to make
the hadarac desert is so huge and contains so many dangers the chances are slim that we can cross it unscathed
it is a desperate path to take
weare desperate said eragon firmly
he studied the map carefully
if we rode through the belly of the desert it would take well over a month perhaps even two to cross it
but if we angle southeast toward the beor mountains we could cut through much faster
then we can either follow the beor mountains farther east into the wilderness or go west to surda
if this map is accurate the distance between here and the beors is roughly equal to what we covered on our way to gil ead
but that took us nearly a **
eragon shook his head impatiently
our ride to gil ead was slow on account of my injuries
if we press ourselves it will take only a fraction of that time to reach the beor mountains
enough
you made your point acknowledged murtagh
before i consent however something must be solved
as i am sure you noticed i bought supplies for us and the horses while i was in gil ead
but how can we get enough water the roving tribes who live in the hadarac usually disguise their wells and oases so no one can steal their water
and carrying enough for more than a day is impractical
just think about how much saphira ** she and the horses consume more water at one time than we do in a week
unless you can make it rain whenever we need i do not see how we can go the direction you propose
eragon rocked back on his heels
making rain was well beyond his power
he suspected that not even the strongest rider could have done it
moving that much air was like trying to lift a mountain
he needed a solution that would not drain all of his ** wonder if it is possible to convert sand into water that would solve our problem but only if it does not take too much energy
i have an idea he said
let me experiment then i will give you an ** eragon strode out of the camp with saphira following closely
what are you going to try she asked
i do not know he ** could you carry enough water for us
she shook her enormous ** i would not even be able to lift that much weight let alone fly with it
too ** knelt and picked up a stone with a cavity large enough for a mouthful of water
he pressed a clump of dirt into the hollow and studied it thoughtfully
now came the hard part
somehow he had to convert the dirt into ** what words should i use he puzzled over it for a moment then picked two he hoped would work
the icy magic rushed through him as he breached the familiar barrier in his mind and commanded deloi **
immediately the dirt began to absorb his strength at a prodigious rate
eragon is mind flashed back to brom is warning that certain tasks could consume all of his power and take his life
panic blossomed in his chest
he tried to release the magic but could not