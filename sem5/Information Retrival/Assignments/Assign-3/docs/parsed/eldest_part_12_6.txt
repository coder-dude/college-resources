and that one he asked indicating a mushroom with a lightning blue stem molten orange gills and a glossy black two tiered cap
she looked at it fondly
fricai andlat as the elves might say
the stalk is instant death while the cap can cure most poisons
it is what tunivor is nectar is extracted from
fricai andlat only grows in caves in du weldenvarden and farthen dur and it would die out here if the dwarves started carting their dung elsewhere
eragon looked back at the hill and realized that was exactly what it was a dung heap
hello saphira said angela reaching past him to pat saphira on the nose
saphira blinked and looked pleased tail twitching
at the same time solembum padded into sight his mouth clamped firmly around a limp rat
without so much as a flick of his whiskers the werecat settled on the ground and began to nibble on the rodent studiously ignoring the three of them
so said angela tucking back a curl of her enormous hair off to ellesmera eragon nodded
he did not bother asking how she had found out she always seemed to know what was going on
when he remained silent she scowled
well do not act so morose
it is not as if it is your **
thensmile because if it is not your execution you should be ** you re as flaccid as solembum is **
what a wonderful word do not you think
that wrung a grin out of him and saphira chortled with amusement deep in her throat
i am not sure it is quite as wonderful as you think but yes i understand your point
i am glad you understand
understanding is ** with arched eyebrows she hooked a fingernail underneath a mushroom and flipped it over inspecting its gills as she said it is fortuitous we met tonight as you are about to leave and i
i will accompany the varden to surda
as i told you before i like to be where things are happening and that is the place
eragon grinned even more
well then that must mean we will have a safe journey else you d be with us
angela shrugged then said seriously be careful in du weldenvarden
just because elves do not display their emotions does not mean they are not subject to rage and passion like the rest of us mortals
what can make them so deadly though is how they conceal it sometimes for years
after a pause he asked what do you think of nasuada is plans
mmm
she is ** you re ** they re all ** she cackled doubling over then straightened abruptly
notice i did not specify what kind of doom so no matter what happens i predicted it
how verywise of ** she lifted the basket again setting it on one hip
i suppose i wo not see you for a while so farewell best of luck avoid roasted cabbage do not eat earwax and look on the bright side of ** and with a cheery wink she strolled off leaving eragon blinking and nonplussed
after an appropriate pause solembum picked up his dinner and followed ever so dignified
dawn was a half hour away when eragon and saphira arrived at tronjheim is north gate
the gate was raised just enough to let saphira pass so they hurried underneath it then waited in the recessed area beyond where red jasper pillars loomed above and carved beasts snarled between the bloody piers
past those at the very edge of tronjheim sat two thirty foot high gold griffins
identical pairs guarded each of the city mountain is gates
no one was in sight
eragon held snowfire is reins
the stallion was brushed reshod and saddled his saddlebags bulging with goods
he pawed the floor impatiently eragon had not ridden him for over a week
before long orik ambled up bearing a large pack on his back and a bundle in his arms
no horse asked eragon somewhat ** we supposed to walk all the way to du weldenvarden
orik grunted
we will be stopping at tarnag just north of here
from there we take rafts along the az ragni to hedarth an outpost for trading with the elves
we wo not need steeds before hedarth so i will use my own feet till then
he set the bundle down with a clang then unwrapped it revealing eragon is armor
the shield had been repainted so the oak tree stood clearly in the center and all the dings and scrapes removed
beneath it was the long mail shirt burnished and oiled until the steel gleamed brilliantly