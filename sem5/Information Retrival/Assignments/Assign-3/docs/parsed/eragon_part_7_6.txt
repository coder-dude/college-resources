the group had garnered much sympathy as they eluded galbatorix is efforts to destroy them
little was known about the varden except that if you were a fugitive and had to hide or if you hated the empire they would accept you
the only problem was finding them
morn leaned over the bar and said incredible is not it they re worse than vultures circling a dying animal
there is going to be trouble if they stay much longer
them said morn as angry voices filled the tavern
eragon left when the argument threatened to become violent
the door thudded shut behind him cutting off the voices
it was early evening and the sun was sinking rapidly the houses cast long shadows on the ground
as eragon headed down the street he noticed roran and katrina standing in an alley
roran said something eragon could not hear
katrina looked down at her hands and answered in an undertone then leaned up on her tiptoes and kissed him before darting away
eragon trotted to roran and teased having a good time roran grunted noncommittally as he paced away
have you heard the traders news asked eragon following
most of the villagers were indoors talking to traders or waiting until it was dark enough for the troubadours to perform
** roran seemed distracted
what do you think of sloan
there will be blood between us when he finds out about katrina and me stated roran
a snowflake landed on eragon is nose and he looked up
the sky had turned gray
he could think of nothing appropriate to say roran was right
he clasped his cousin on the shoulder as they continued down the byway
dinner at horst is was hearty
the room was full of conversation and laughter
sweet cordials and heavy ales were consumed in copious amounts adding to the boisterous atmosphere
when the plates were empty horst is guests left the house and strolled to the field where the traders were camped
a ring of poles topped with candles had been stuck into the ground around a large clearing
bonfires blazed in the background painting the ground with dancing shadows
the villagers slowly gathered around the circle and waited expectantly in the cold
the troubadours came tumbling out of their tents dressed in tasseled clothing followed by older and more stately minstrels
the minstrels provided music and narration as their younger counterparts acted out the stories
the first plays were pure entertainment bawdy and full of jokes pratfalls and ridiculous characters
later however when the candles sputtered in their sockets and everyone was drawn together into a tight circle the old storyteller brom stepped forward
a knotted white beard rippled over his chest and a long black cape was wrapped around his bent shoulders obscuring his body
he spread his arms with hands that reached out like talons and recited thus
the sands of time cannot be stopped
years pass whether we will them or not
but we can remember
what has been lost may yet live on in memories
that which you will hear is imperfect and fragmented yet treasure it for without you it does not exist
i give you now a memory that has been forgotten hidden in the dreamy haze that lies behind us
his keen eyes inspected their interested faces
his gaze lingered on eragon last of all
before your grandfathers fathers were born and yea even before their fathers the dragon riders were formed
to protect and guard was their mission and for thousands of years they succeeded
their prowess in battle was unmatched for each had the strength of ten men
they were immortal unless blade or poison took them
for good only were their powers used and under their tutelage tall cities and towers were built out of the living stone
while they kept peace the land flourished
it was a golden time
the elves were our allies the dwarves our friends
wealth flowed into our cities and men prospered
but weep
for it could not last
brom looked down silently
infinite sadness resonated in his voice
though no enemy could destroy them they could not guard against themselves
and it came to pass at the height of their power that a boy galbatorix by name was born in the province of inzilbeth which is no more