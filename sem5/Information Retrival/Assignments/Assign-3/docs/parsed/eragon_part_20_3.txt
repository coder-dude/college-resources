with his mind eragon instructed the horse to return to brom
he then got onto saphira
she crept up the road fighting the gale while he clung to her back and kept his head down
when they reached brom he shouted over the storm is she hurt
eragon shook his head and dismounted
cadoc trotted over to him nickering
as he stroked the horse is long cheek brom pointed at a dark curtain of rain sweeping toward them in rippling gray sheets
what else cried eragon pulling his clothes tighter
he winced as the torrent reached them
the stinging rain was cold as ice before long they were drenched and shivering
lightning lanced through the sky flickering in and out of existence
mile high blue bolts streaked across the horizon followed by peals of thunder that shook the ground below
it was beautiful but dangerously so
here and there grass fires were ignited by strikes only to be extinguished by the rain
the wild elements were slow to abate but as the day passed they wandered elsewhere
once again the sky was revealed and the setting sun glowed with brilliance
as beams of light tinted the clouds with blazing colors everything gained a sharp contrast brightly lit on one side deeply shadowed on the other
objects had a unique sense of mass grass stalks seemed sturdy as marble pillars
ordinary things took on an unearthly beauty eragon felt as if he were sitting inside a painting
the rejuvenated earth smelled fresh clearing their minds and raising their spirits
saphira stretched craning her neck and roared happily
the horses skittered away from her but eragon and brom smiled at her exuberance
before the light faded they stopped for the night in a shallow depression
too exhausted to spar they went straight to sleep