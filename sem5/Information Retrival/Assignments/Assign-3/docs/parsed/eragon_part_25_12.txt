countless possible futures await you all of them filled with blood and conflict but only one will bring you happiness and peace
beware of losing your way for you are one of the few who are truly free to choose their own fate
that freedom is a gift but it is also a responsibility more binding than chains
then her face grew sad
and yet as if to counteract that here is the lightning bolt
it is a terrible omen
there is a doom upon you but of what sort i know not
part of it lies in a death one that rapidly approaches and will cause you much grief
but the rest awaits in a great journey
look closely at this bone
you can see how its end rests on that of the sailing ship
that is impossible to misunderstand
your fate will be to leave this land forever
where you will end up i know not but you will never again stand in alagaesia
this is inescapable
it will come to pass even if you try to avoid it
her words frightened ** death
who must i lose now his thoughts immediately went to roran
then he thought about his ** could ever force me to leave and where would i go if there are lands across the sea or to the east only the elves know of them
angela rubbed her temples and breathed deeply
the next bone is easier to read and perhaps a bit more ** eragon examined it and saw a rose blossom inscribed between the horns of a crescent moon
angela smiled and said an epic romance is in your future extraordinary as the moon indicates for that is a magical symbol and strong enough to outlast empires
i cannot say if this passion will end happily but your love is of noble birth and heritage
she is powerful wise and beautiful beyond compare
of noble birth thought eragon in ** could that ever happen i have no more standing than the poorest of farmers
now for the last two bones the tree and the hawthorn root which cross each other strongly
i wish that this were not so it can only mean more trouble but betrayal is clear
and it will come from within your family
roran would not do ** objected eragon abruptly
i would not know said angela carefully
but the bones have never lied and that is what they say
doubt wormed into eragon is mind but he tried to ignore it
what reason would there ever be for roran to turn on him angela put a comforting hand on his shoulder and offered him the wineskin again
this time eragon accepted the drink and it made him feel better
after all that death might be welcome he joked ** from roran it could not ** it wo not
it might be said angela solemnly then laughed slightly
but you should not fret about what has yet to occur
the only way the future can harm us is by causing worry
i guarantee that you will feel better once you re out in the sun
** unfortunately he reflected wryly nothing she said will make sense until it has already happened
if it really does he amended himself
you used words of power he noted quietly
angela is eyes flashed
what i would not give to see how the rest of your life plays out
you can speak to werecats know of the ancient language and have a most interesting future
also few young men with empty pockets and rough traveling clothes can expect to be loved by a noblewoman
who are you
eragon realized that the werecat must not have told angela that he was a rider
he almost said evan but then changed his mind and simply stated i am eragon
angela arched her eyebrows
is that who you are or your name she asked
both said eragon with a small smile thinking of his namesake the first rider
now i am all the more interested in seeing how your life will unfold
who was the ragged man with you yesterday
eragon decided that one more name could not hurt
his name is brom
a guffaw suddenly burst out of angela doubling her over in mirth
she wiped her eyes and took a sip of wine then fought off another attack of merriment
finally gasping for breath she forced out oh
that ** i had no **
no no do not be upset said angela hiding a smile
it is only that well he is known by those in my profession
i am afraid that the poor man is doom or future if you will is something of a joke with us
do not insult ** he is a better man than any you could ** snapped eragon