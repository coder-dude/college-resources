the moon floated high among the stars when roran left the makeshift tent he shared with baldor padded to the edge of the camp and replaced albriech on watch
nothing to report whispered albriech then slipped off
roran strung his bow and planted three goose feather arrows upright in the loam within easy reach then wrapped himself in a blanket and curled against the rockface to his left
his position afforded him a good view down and across the dark foothills
as was his habit roran divided the landscape into quadrants examining each one for a full minute always alert for the flash of movement or the hint of light that might betray the approach of enemies
his mind soon began to wander drifting from subject to subject with the hazy logic of dreams distracting him from his task
he bit the inside of his cheek to force himself to concentrate
staying awake was difficult in such mild weather
roran was just glad that he had escaped drawing lots for the two watches preceding dawn because they gave you no opportunity to catch up on lost sleep afterward and you felt tired for the rest of the day
a breath of wind ghosted past him tickling his ear and making the skin on the back of his neck prickle with an apprehension of evil
the intrusive touch frightened roran obliterating everything but the conviction that he and the rest of the villagers were in mortal danger
he quaked as if with the ague his heart pounded and he had to struggle to resist the urge to break cover and flee
what is wrong with me it required an effort for him to even nock an arrow
to the east a shadow detached itself from the horizon
visible only as a void among the stars it drifted like a torn veil across the sky until it covered the moon where it remained hovering
illuminated from behind roran could see the translucent wings of one of the ra zac is mounts
the black creature opened its beak and uttered a long piercing shriek
roran grimaced with pain at the cry is pitch and frequency
it stabbed at his eardrums turned his blood to ice and replaced hope and joy with despair
the ululation woke the entire forest
birds and beasts for miles around exploded into a yammering chorus of panic including to roran is alarm what remained of the villagers herds
staggering from tree to tree roran returned to the camp whispering the ra zac are here
be quiet and stay where you are to everyone he encountered
he saw the other sentries moving among the frightened villagers spreading the same message
fisk emerged from his tent with a spear in hand and roared are we under attack what is set off those blasted roran tackled the carpenter to silence him uttering a muffled bellow as he landed on his right shoulder and pained his old injury
fisk went still and in an undertone asked what should i do
help me to calm the animals
together they picked their way through the camp to the adjacent meadow where the goats sheep donkeys and horses were bedded
the farmers who owned the bulk of the herds slept with their charges and were already awake and working to soothe the beasts
roran thanked his paranoia that he had insisted on having the animals scattered along the edge of the meadow where the trees and brush helped to camouflage them from unfriendly eyes
as he tried to pacify a clump of sheep roran glanced up at the terrible black shadow that still obscured the moon like a giant bat
to his horror it began to move toward their hiding ** that creature screams again we re doomed
by the time the ra zac circled overhead most of the animals had quieted except for one donkey who insisted upon loosing a gratinghee haw
without hesitation roran dropped to one knee fit arrow to string and shot the ass between the ribs
his aim was true and the animal dropped without a sound
he was too late though the braying had already alerted the ra zac
the monster swung its head in the direction of the clearing and descended toward it with outstretched claws preceded by its fetid stench
now the time has come to see if we can slay a nightmare thought roran
fisk who was crouched beside him in the grass hefted his spear preparing to hurl it once the brute was in range