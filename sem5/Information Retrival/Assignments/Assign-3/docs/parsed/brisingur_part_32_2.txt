roran covered himself with his shield and squinted at his saddle as a pure white light bright as the noonday sun illuminated the landscape
the stark glow originated from a point somewhere above the camp roran resisted the temptation to see exactly where
shouting he kicked snowfire in the ribs and hunched over the horse is neck as his steed leaped forward
on either side carn and the other warriors did the same brandishing their weapons
branches tore at roran is head and shoulders and then snowfire broke free of the trees and raced toward the camp at full gallop
two other groups of horsemen also thundered toward the camp one led by martland the other ulhart
the soldiers and drivers cried out in alarm and covered their eyes
staggering about like blind men they scrabbled after their weapons while trying to position themselves to repel the attack
roran made no attempt to slow snowfire
spurring the stallion once more he rose high in the stirrups and held on with all his strength as snowfire jumped over the slight gap between two wagons
his teeth clattered as they landed
snowfire kicked dirt into one of the fires sending up a burst of sparks
the rest of roran is group jumped the wagons as well
knowing they would attend to the soldiers behind him roran concentrated on those in front
aiming snowfire at one of the men he jabbed at the soldier with the end of his hammer and broke the man is nose splashing crimson blood across his face
roran dispatched the man with a second blow to the head then parried a sword from another soldier
farther down the curved line of wagons martland ulhart and their men also jumped into the camp alighting with a clack of hooves and a jangle of armor and weapons
a horse screamed and fell as a soldier wounded it with a spear
roran blocked the soldier is sword a second time then rapped the man is sword hand breaking bones and forcing the man to drop his weapon
without pause roran struck the man in the center of his red tunic cracking his sternum and felling the gasping mortally wounded soldier
roran twisted in the saddle searching the camp for his next opponent
his muscles vibrated with frantic excitement every detail around him was as sharp and clear as if it were etched in glass
he felt invincible invulnerable
time itself seemed to stretch and slow so that a confused moth that fluttered past him appeared to be flying through honey instead of air
then a pair of hands clamped down on the back of his mail hauberk and yanked him off snowfire and slammed him into the hard ground knocking the breath out of him
roran is sight flickered and went black for a moment
when he recovered he saw that the first soldier he had attacked was sitting on his chest choking him
the soldier blotted out the source of light carn had created in the sky
a white halo surrounded his head and shoulders casting his features in such deep shadow roran could make out nothing of his face but the flash of bared teeth
the soldier tightened his fingers around roran is throat as roran gasped for air
roran groped after his hammer which he had dropped but it was not within reach
tensing his neck to keep the soldier from crushing the life out of him he drew his dagger from his belt and drove it through the soldier is hauberk through his gambeson and between the ribs on the soldier is left side
the soldier did not even flinch nor did his grip relax
a continuous stream of gurgling laughter emanated from the soldier
the lurching heart stopping chuckle hideous in the extreme turned roran is stomach cold with fear
he remembered the sound from before he had heard it while watching the varden fight the men who felt no pain on the grassy field beside the jiet river
in a flash he understood why the soldiers had chosen such a poor campsite they do not care if they are trapped or not for we cannot hurt them
roran is vision turned red and yellow stars danced before his eyes
teetering on the edge of unconsciousness he yanked the dagger free and stabbed upward into the soldier is armpit twisting the blade in the wound