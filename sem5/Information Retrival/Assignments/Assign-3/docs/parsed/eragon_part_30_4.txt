the air was chill and dry
bare walls extended to a vaulted ceiling that was so high eragon felt no taller than an ant
stained glass windows depicting scenes of anger hate and remorse pierced the walls while spectral beams of light washed sections of the granite pews with transparent hues leaving the rest in shadow
his hands were shaded a deep blue
between the windows stood statues with rigid pale eyes
he returned their stern gazes then slowly trod up the center row afraid to break the quiet
his leather boots padded noiselessly on the polished stone floor
the altar was a great slab of stone devoid of adornment
a solitary finger of light fell upon it illuminating motes of golden dust floating in the air
behind the altar the pipes of a wind organ pierced the ceiling and opened themselves to the elements
the instrument would play its music only when a gale rocked dras leona
out of respect eragon knelt before the altar and bowed his head
he did not pray but paid homage to the cathedral itself
the sorrows of the lives it had witnessed as well as the unpleasantness of the elaborate pageantry that played out between its walls emanated from the stones
it was a forbidding place bare and cold
in that chilling touch though came a glimpse of eternity and perhaps the powers that lay there
finally eragon inclined his head and rose
calm and grave he whispered words to himself in the ancient language then turned to leave
he froze
his heart jumped hammering like a drum
the ra zac stood at the cathedral is entrance watching him
their swords were drawn keen edges bloody in a crimson light
a sibilant hiss came from the smaller ra zac
neither of them moved
rage welled up in eragon
he had chased the ra zac for so many weeks that the pain of their murderous deed had dulled within him
but his vengeance was at hand
his wrath exploded like a volcano fueled even more by his pent up fury at the slaves plight
a roar broke from his lips echoing like a thunderstorm as he snatched his bow from his back
deftly he fit an arrow to the string and loosed it
two more followed an instant later
the ra zac leapt away from the arrows with inhuman swiftness
they hissed as they ran up the aisle between the pews cloaks flapping like raven wings
eragon reached for another arrow but caution stayed his ** they knew where to find me brom is in danger as ** i must warn ** then to eragon is horror a line of soldiers filed into the cathedral and he glimpsed a field of uniforms jostling outside the doorway
eragon gazed hungrily at the charging ra zac then swept around searching for means of escape
a vestibule to the left of the altar caught his attention
he bounded through the archway and dashed down a corridor that led to a priory with a belfry
the patter of the ra zac is feet behind him made him quicken his pace until the hall abruptly ended with a closed door
he pounded against it trying to break it open but the wood was too strong
the ra zac were nearly upon him
frantic he sucked in his breath and barked ** with a flash the door splintered into pieces and fell to the floor
eragon jumped into the small room and continued running
he sped through several chambers startling a group of priests
shouts and curses followed him
the priory bell tolled an alarm
eragon dodged through a kitchen passed a pair of monks then slipped through a side door
he skidded to stop in a garden surrounded by a high brick wall devoid of handholds
there were no other exits
eragon turned to leave but there was a low hiss as the ra zac shouldered aside the door
desperate he rushed at the wall arms pumping
magic could not help him here if he used it to break through the wall he would be too tired to run
he jumped
even with his arms outstretched only his fingertips cleared the edge of the wall
the rest of his body smashed against the bricks driving out his breath
eragon gasped and hung there struggling not to fall
the ra zac prowled into the garden swinging their heads from side to side like wolfhounds sniffing for prey
eragon sensed their approach and heaved with his arms