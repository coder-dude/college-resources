that angela had chosen to do this for the handsome woman with the scars on her wrists and the teenage girl with the forearms of a swordfighter told him they were people of note people who had had and would have important roles in shaping the alagaesia to be
as if to confirm his suspicions he spotted solembum in his usual form of a cat with large tufted ears lurking behind the corner of a nearby tent watching the proceedings with enigmatic yellow eyes
and yet eragon still hesitated haunted by the memory of the first and last blessing he had bestowed how because of his relative unfamiliarity with the ancient language he had distorted the life of an innocent child
her tail whipped through the air
do not be so reluctant
you hav
learned from your mistake and you shall not make it again
why then should you withhold your blessing from those who may benefit from it bless them i say and do it properly this time
what are your names he asked
if it please you shadeslayer said the tall black haired woman with a hint of an accent he could not place names have power and we would prefer ours remain ** she kept her gaze angled slightly downward but her tone was firm and unyielding
the girl uttered a small gasp as if shocked by the woman is effrontery
eragon nodded neither upset nor surprised although the woman is reticence had piqued his curiosity even more
he would have liked to know their names but they were not essential for what he was about to do
pulling the glove off his right hand he placed his palm on the middle of the woman is warm forehead
she flinched at the contact but did not retreat
her nostrils flared the corners of her mouth thinned a crease appeared between her eyebrows and he felt her tremble as if his touch pained her and she were fighting the urge to knock aside his arm
in the background eragon was vaguely aware of blodhgarm stalking closer ready to pounce on the woman should she prove to be hostile
disconcerted by her reaction eragon broached the barrier in his mind immersed himself in the flow of magic and with the full power of the ancient language said atra gulia un ilian tauthr ono un atra ono waise skoliro fra ** by imbuing the phrase with energy as he would the words of a spell he ensured that it would shape the course of events and thereby improve the woman is lot in life
he was careful to limit the amount of energy he transferred into the blessing for unless he put checks on it a spell of that sort would feed off his body until it absorbed all of his vitality leaving him an empty husk
despite his caution the drop in his strength was more than he expected his vision dimmed and his legs wobbled and threatened to collapse underneath him
it was with a sense of relief that he lifted his hand from the woman is brow a sentiment that she seemed to share for she stepped back and rubbed her arms
she looked to him like a person trying to cleanse herself of some foul substance
moving on eragon repeated the procedure with the teenage girl
her face widened as he released the spell as if she could feel it becoming part of her body
she curtsied
thank you shadeslayer
we are in your debt
i hope that you succeed in defeating galbatorix and the empire
she turned to leave but stopped when saphira snorted and snaked her head past eragon and angela so she loomed above the two women
bending her neck saphira breathed first upon the face of the older woman and then upon the face of the younger and projecting her thoughts with such force as to overwhelm all but the thickest defenses for she and eragon had noticed that the black haired woman had a well armored mind she said good hunting o wild ones
may the wind rise under your wings may the sun always be at your backs and may you catch your prey napping
and wolf eyes i hope that when you find the one who left your paws in his traps you do not kill him too quickly
both women stiffened when saphira began to speak
afterward the elder clapped her fists against her chest and said that i shall not o beautiful ** then she bowed to angela saying train hard strike first seer