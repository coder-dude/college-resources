after two days of traveling north toward the ocean saphira sighted teirm
a heavy fog clung to the ground obscuring brom is and eragon is sight until a breeze from the west blew the mist away
eragon gaped as teirm was suddenly revealed before them nestled by the edge of the shimmering sea where proud ships were docked with furled sails
the surf is dull thunder could be heard in the distance
the city was contained behind a white wall a hundred feet tall and thirty feet thick with rows of rectangular arrow slits lining it and a walkway on top for soldiers and watchmen
the wall is smooth surface was broken by two iron portcullises one facing the western sea the other opening south to the road
above the wall and set against its northeast section rose a huge citadel built of giant stones and turrets
in the highest tower a lighthouse lantern gleamed brilliantly
the castle was the only thing visible over the fortifications
soldiers guarded the southern gate but held their pikes carelessly
this is our first test said brom
let is hope they have not received reports of us from the empire and wo not detain us
whatever happens do not panic or act suspiciously
eragon told saphira you should land somewhere now and hide
we re going in
sticking your nose where it does not belong
again she said sourly
i know
but brom and i do have some advantages most people do ** will be all right
if anything happens i am going to pin you to my back and never let you off
then i will bind you all the tighter
eragon and brom rode toward the gate trying to appear casual
a yellow pennant bearing the outline of a roaring lion and an arm holding a lily blossom waved over the entrance
as they neared the wall eragon asked in amazement how big is this place
larger than any city you have ever seen said brom
at the entrance to teirm the guards stood straighter and blocked the gate with their pikes
wha is yer name asked one of them in a bored tone
i am called neal said brom in a wheezy voice slouching to one side an expression of happy idiocy on his face
and who is th other one asked the guard
well i wus gettin to that
this ed be m nephew evan
he is m isister is boy not a
the guard nodded impatiently
yeah yeah
and yer business here
he is visitin an old friend supplied eragon dropping his voice into a thick accent
i am along t make sure he do not get lost if y get m ameaning
he ai not as young as he used to be had a bit too much sun when he was young r
touch o the brain fever y ** brom bobbed his head pleasantly
right
go on through said the guard waving his hand and dropping the pike
just make sure he does not cause any trouble
oh he wo not promised eragon
he urged cadoc forward and they rode into teirm
the cobblestone street clacked under the horses hooves
once they were away from the guards brom sat up and growled touch of brain fever eh
i could not let you have all the fun teased eragon
the houses were grim and foreboding
small deep windows let in only sparse rays of light
narrow doors were recessed into the buildings
the tops of the roofs were flat except for metal railings and all were covered with slate shingles
eragon noticed that the houses closest to teirm is outer wall were no more than one story but the buildings got progressively higher as they went in
those next to the citadel were tallest of all though insignificant compared to the fortress
this place looks ready for war said eragon
brom nodded
teirm has a history of being attacked by pirates urgals and other enemies
it has long been a center of commerce
there will always be conflict where riches gather in such abundance
the people here have been forced to take extraordinary measures to keep themselves from being overrun
it also helps that galbatorix gives them soldiers to defend their city
why are some houses higher than others
look at the citadel said brom pointing
it has an unobstructed view of teirm
if the outer wall were breached archers would be posted on all the roofs
because the houses in the front by the outer wall are lower the men farther back could shoot over them without fear of hitting their comrades