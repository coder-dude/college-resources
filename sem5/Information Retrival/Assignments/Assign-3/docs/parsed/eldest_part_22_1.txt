on the morning of the fourth day when eragon rode alongside shrrgnien the dwarf said so tell me do men really have ten toes as is said for truly i have never traveled beyond our borders before
of course we have ten ** said eragon astonished
he shifted in snowfire is saddle lifted his foot removed his right boot and sock and wiggled his toes under shrrgnien is amazed eyes
do not you
shrrgnien shook his head
nay we have seven on each foot
it is how helzvog made us
five is too few and six is the wrong number but seven
seven is just ** he glanced at eragon is foot again then spurred his donkey ahead and began speaking animatedly to ama and hedin who eventually handed him several silver coins
i think said eragon as he pulled the boot back on that i was just the source of a bet
for some reason saphira found that immensely amusing
as dusk fell and the full moon rose the edda river drew ever closer to the fringe of du weldenvarden
they rode down a narrow trail through tangled dogwood and rosebushes in full bloom which filled the evening air with the flowers warm scent
eager anticipation swelled within eragon as he gazed into the dark forest knowing they had already entered the elves domain and were close to ceris
he leaned forward in snowfire is saddle the reins pulled tight between his hands
saphira is excitement was as great as his own she ranged overhead flicking her tail back and forth with impatience
eragon felt as if they had wandered into a ** does not seem real he said
aye
here the legends of old still bestride the earth
at last they came upon a small meadow set between the river and forest
stop here said arya in a low voice
she walked forward until she stood alone in the midst of the lush grass then cried in the ancient language come forth my ** you have nothing to fear
tis i arya of ellesmera
my companions are friends and allies they mean us no ** she added other words as well ones alien to eragon
for several minutes the only sound was the river rushing behind them until from underneath the still leaves came a line of elvish so quick and fleeting that eragon missed the meaning
arya responded i do
with a rustle two elves stood on the edge of the forest and two ran lightly out on the boughs of a gnarled oak
those on the ground bore long spears with white blades while the others held bows
all were garbed in tunics the color of moss and bark underneath flowing cloaks clasped at the shoulder with ivory brooches
one had tresses as black as arya is
three had hair like starlight
the elves dropped from the trees and embraced arya laughing in their clear pure voices
they joined hands and danced in a circle around her like children singing merrily as they spun through the grass
eragon watched in amazement
arya had never given him reason to suspect that elves liked to or evencould laugh
it was a wondrous sound like flutes and harps trilling with delight at their own music
he wished that he could listen to it forever
then saphira drifted over the river and settled beside eragon
at her approach the elves cried out in alarm and aimed their weapons toward her
arya spoke quickly in soothing tones motioning first at saphira then at eragon
when she paused for breath eragon drew back the glove on his right hand tilted his palm so that the gedwey ignasia caught the moonlight and said as he once had to arya so long ago eka fricai un shur ** i am a rider and friend
remembering his lesson from yesterday he touched his lips adding atra esterni ono thelduin
the elves lowered their weapons as their angled faces lit up with radiant joy
they pressed their forefingers to their lips and bowed to saphira and him murmuring their reply in the ancient language
then they rose pointed at the dwarves and laughed as if at a hidden joke
drifting back into the forest they waved their hands and called come **
eragon followed arya with saphira and the dwarves who were grumbling among themselves
as they passed between the trees the canopy overhead plunged them into velvet darkness except where fragments of moonlight gleamed through chinks in the shell of overlapping leaves