an air of power lay over it as if an unstoppable force resided in its core
it had been created for the violent convulsions of battle to end men is lives yet it held a terrible beauty
this was once a rider is blade said brom gravely
when a rider finished his training the elves would present him with a sword
their methods of forging have always remained secret
however their swords are eternally sharp and will never stain
the custom was to have the blade is color match that of the rider is dragon but i think we can make an exception in this case
this sword is named zar roc
i do not know what it means probably something personal to the rider who owned ** he watched eragon swing the sword
where did you get it asked eragon
he reluctantly slipped the blade back into the sheath and attempted to hand the sword back but brom made no move to take it
it does not matter said brom
i will only say that it took me a series of nasty and dangerous adventures to attain it
consider it yours
you have more of a claim to it than i do and before all is done i think you will need it
the offer caught eragon off guard
it is a princely gift thank ** unsure of what else to say he slid his hand down the sheath
what is this symbol he asked
that was the rider is personal ** eragon tried to interrupt but brom glared at him until he was quiet
now if you must know anyone can learn how to speak to a dragon if they have the proper training
and he raised a finger for emphasis it does not mean anything if they can
i know more about the dragons and their abilities than almost anyone else alive
on your own it might take years to learn what i can teach you
i am offering my knowledge as a shortcut
as for how i know so much i will keepthat to myself
saphira pulled herself up as he finished speaking and prowled over to eragon
he pulled out the blade and showed her the ** has power she said touching the point with her nose
the metal is iridescent color rippled like water as it met her scales
she lifted her head with a satisfied snort and the sword resumed its normal appearance
eragon sheathed it troubled
brom raised an eyebrow
that is the sort of thing i am talking about
dragons will constantly amaze you
things
happen around them mysterious things that are impossible anywhere else
even though the riders worked with dragons for centuries they never completely understood their abilities
some say that even the dragons do not know the full extent of their own powers
they are linked with this land in a way that lets them overcome great obstacles
what saphira just did illustrates my earlier point there is much you do not know
there was a long pause
that may be said eragon but i can learn
and the strangers are the most important thing i need to know about right now
do you have any idea who they are
brom took a deep breath
they are called the ra zac
no one knows if that is the name of their race or what they have chosen to call themselves
either way if they have individual names they keep them hidden
the ra zac were never seen before galbatorix came to power
he must have found them during his travels and enlisted them in his service
little or nothing is known about them
however i can tell you this they are not human
when i glimpsed one is head it appeared to have something resembling a beak and black eyes as large as my fist though how they manage our speech is a mystery to me
doubtless the rest of their bodies are just as twisted
that is why they cover themselves with cloaks at all times regardless of the weather
as for their powers they are stronger than any man and can jump incredible heights but they cannot use magic
be thankful for that because if they could you would already be in their grasp
i also know they have a strong aversion to sunlight though it wo not stop them if they re determined
do not make the mistake of underestimating a ra zac for they are cunning and full of guile
how many of them are there asked eragon wondering how brom could possibly know so much
as far as i know only the two you saw
there might be more but i ve never heard of them