though he was tired from the previous day eragon forced himself to rise before dawn in an attempt to catch one of the elves asleep
it had become a game with him to discover when the elves got up or if they slept at all as he had yet to see any of them with their eyes closed
today was no exception
good morning said nari and lifaen from above him
eragon craned back his head and saw that they each stood on the bough of a pine tree over fifty feet in the air
jumping from branch to branch with feline grace the elves dropped to the ground alongside him
we have been keeping watch explained lifaen
arya stepped around a tree and said for my fears
du weldenvarden has many mysteries and dangers especially for a rider
we have lived here for thousands of years and old spells still linger in unexpected places magic permeates the air the water and the earth
in places it has affected the animals
sometimes strange creatures are found roaming the forest and not all of them friendly
are they eragon stopped as his gedwey ignasia tingled
the silver hammer on the necklace gannel had given him grew hot on his chest and he felt the amulet is spell draw upon his strength
someone was trying to scry him
is it galbatorix he wondered frightened
he clutched the necklace and pulled it out of his tunic ready to yank it off should he become too weak
from the other side of the camp saphira rushed to his side bolstering him with her own reserves of energy
a moment later the heat leached out of the hammer leaving it cold against eragon is skin
he bounced it on his palm then tucked it back under his clothes whereupon saphira said our enemies are searching for us
enemies could not it be someone in du vrangr gata
i think hrothgar would have told nasuada that he ordered gannel to enchant you this necklace
she might have even come up with the idea in the first place
arya frowned when eragon explained what had occurred
this makes it all the more important we reach ellesmera quickly so your training can resume
events in alagaesia move apace and i fear you wo not have adequate time for your studies
eragon wanted to discuss it further but lost the opportunity in the rush to leave camp
once the canoes were loaded and the fire tamped out they continued to forge up the gaena river
they had only been on the water for an hour when eragon noticed that the river was growing wider and deeper
a few minutes later they came upon a waterfall that filled du weldenvarden with its throbbing rumble
the cataract was about a hundred feet tall and streamed down a stone face with an overhang that made it impossible to climb
how do we get past that he could already feel cool spray on his face
lifaen pointed at the left shore some distance from the falls where a trail had been worn up the steep ridge
we have to portage our canoes and supplies for half a league before the river clears
the five of them untied the bundles wedged between the seats of the canoes and divided the supplies into piles that they stuffed into their packs
ugh said eragon hefting his load
it was twice as heavy as what he usually carried when traveling on foot
i could fly it upstream for you
all of it offered saphira crawling onto the muddy bank and shaking herself dry
when eragon repeated her suggestion lifaen looked horrified
we would never dream of using a dragon as a beast of burden
it would dishonor you saphira and eragon as shur tugal and it would shame our hospitality
saphira snorted and a plume of flame erupted from her nostrils vaporizing the surface of the river and creating a cloud of ** is nonsense
reaching past eragon with one scaly leg she hooked her talons through the packs shoulder straps then took off over their ** me if you can
a peal of clear laughter broke the silence like the trill of a mockingbird
amazed eragon turned and looked at arya
it was the first time he had ever heard her laugh he loved the sound
she smiled at lifaen
you have much to learn if you presume to tell a dragon what she may or may not do
it is no dishonor if saphira does it of her free will asserted arya