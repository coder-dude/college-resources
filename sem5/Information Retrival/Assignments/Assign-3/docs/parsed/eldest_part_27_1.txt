every day since leaving the outpost of ceris was a hazy dream of warm afternoons spent paddling up eldor lake and then the gaena river
all around them water gurgled through the tunnel of verdant pines that wound ever deeper into du weldenvarden
eragon found traveling with the elves delightful
nari and lifaen were perpetually smiling laughing and singing songs especially when saphira was around
they rarely looked elsewhere or spoke of another subject but her in her presence
however the elves were not human no matter the similarity of appearance
they moved too quickly too fluidly for creatures born of simple flesh and blood
and when they spoke they often used roundabout expressions and aphorisms that left eragon more confused than when they began
in between their bursts of merriment lifaen and nari would remain silent for hours observing their surroundings with a glow of peaceful rapture on their faces
if eragon or orik attempted to talk with them during their contemplation they would receive only a word or two in response
it made eragon appreciate how direct and forthright arya was by comparison
in fact she seemed uneasy around lifaen and nari as if she were no longer sure how to behave with her own kind
from the prow of the canoe lifaen looked over his shoulder and said tell me eragon finiarel
what do your people sing about in these dark days i remember the epics and lays i heard in ilirea sagas of your proud kings and earls but it was long long ago and the memories are like withered flowers in my mind
what new works have your people created eragon frowned as he tried to recall the names of stories brom had recited
when lifaen heard them he shook his head sorrowfully and said so much has been lost
no court ballads survive and if you speak truly nor does most of your history or art except for fanciful tales galbatorix has allowed to thrive
brom once told us about the fall of the riders said eragon defensively
an image of a deer bounding over rotting logs flashed behind his eyes from saphira who was off hunting
ah a brave ** for a minute lifaen paddled silently
we too sing about the fall
but rarely
most of us were alive when vrael entered the void and we still grieve for our burned cities the red lilies of ewayena the crystals of luthivira and for our slain families
time cannot dull the pain of those wounds not if a thousand thousand years pass and the sun itself dies leaving the world to float in eternal night
orik grunted in the back
as it is with the dwarves
remember elf we lost an entire clan to galbatorix
and we lost our king evandar
i never heard that said eragon surprised
lifaen nodded as he guided them around a submerged rock
few have
brom could have told you about it he was there when the fatal blow was struck
before vrael is death the elves faced galbatorix on the plains of ilirea in our final attempt to defeat him
there evandar
it is uru baen boy said orik
used to be an elf city
unperturbed by the interruption lifaen continued as you say ilirea was one of our cities
we abandoned it during our war with the dragons and then centuries later humans adopted it as their capital after king palancar was exiled
eragon said king palancar who was he is that how palancar valley got its name
this time the elf turned and looked at him with amusement
you have as many questions as leaves on a tree argetlam
brom was of the same opinion
lifaen smiled then paused as if to gather his thoughts
when your ancestors arrived in alagaesia eight hundred years ago they roamed far across it seeking a suitable place to live
eventually they settled in palancar valley though it was not called such then as it was one of the few defendable locations that we or the dwarves had not claimed
there your king palancar began to build a mighty state
in an attempt to expand his borders he declared war against us though we had offered no provocation
three times he attacked and three times we prevailed
our strength frightened palancar is nobles and they pled with their liege for peace
he ignored their counsel
then the lords approached us with a treaty which we signed without the king is knowledge