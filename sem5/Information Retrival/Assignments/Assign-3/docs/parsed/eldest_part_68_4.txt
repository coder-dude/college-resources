roran bowed even lower
thank you lady nasuada
if i were not so pressed for time i would insist upon knowing how and why you and your village evaded galbatorix is men traveled to surda and then found us
even just the bare facts of your trek make an extraordinary tale
i still intend to learn the specifics especially since i suspect it concerns eragon but i must deal with other more urgent matters at the moment
please said eragon let him stay
he should be here for this
nasuada gave him a quizzical look
very well
if you want
but enough of this dawdling
jump to the meat of the matter and tell us about the **
eragon began with a quick history of the three remaining dragon eggs two of which had now hatched as well as morzan and murtagh so that roran would understand the significance of his news
then he proceeded to describe his and saphira is fight with thorn and the mysterious rider paying special attention to his extraordinary powers
as soon as he spun his sword around i realized wehad dueled before so i threw myself at him and tore off his ** eragon paused
it was murtagh was not it asked nasuada quietly
she sighed
if the twins survived it only made sense that murtagh had as well
did he tell you what really happened that day in farthen dur so eragon recounted how the twins betrayed the varden recruited the urgals and kidnapped murtagh
a tear rolled down nasuada is cheek
it is a pity that this befell murtagh when he has already endured so much hardship
i enjoyed his company in tronjheim and believed he was our ally despite his upbringing
i find it hard to think of him as our ** turning to roran she said it seems i am also personally in your debt for slaying the traitors who murdered my father
fathers mothers brothers cousins thought ** all comes down to family
summoning his courage he completed his report with murtagh is theft of zar roc and then his final terrible secret
eragon saw shock and revulsion cross roran is face before he managed to conceal his reactions
that more than anything else hurt eragon
could murtagh have been lying asked arya
i do not see how
when i questioned him he told me the same thing in the ancient language
a long uncomfortable silence filled the pavilion
then arya said no one else can know about this
the varden are demoralized enough by the presence of a new rider
and they will be even more upset when they learn it is murtagh whom they fought alongside and came to trust in farthen dur
if word spreads that eragon shadeslayer is morzan is son the men will grow disillusioned and few people will want to join us
not even king orrin should be told
nasuada rubbed her temples
i fear you re right
a new rider
she shook her head
i knew it was possible for this to occur but i did not really believe it would since galbatorix is remaining eggs had gone so long without hatching
it has a certain symmetry said eragon
our task is doubly hard now
we may have held our own today but the empire still far outnumbers us and now we face not one but two riders both of whom are stronger than you eragon
do you think you could defeat murtagh with the help of the elves spellcasters
maybe
but i doubt he d be foolish enough to fight them and me together
for several minutes they discussed the effect murtagh could have on their campaign and strategies to minimize or eliminate it
at last nasuada said enough
we cannot decide this when we are bloody and tired and our minds are clouded from fighting
go rest and we shall take this up again tomorrow
as eragon turned to leave arya approached and looked him straight in the eye
do not allow this to trouble you overmuch eragon elda
you are not your father nor your brother
their shame is not yours
aye agreed nasuada
nor imagine that it has lowered our opinion of ** she reached out and cupped his face
i know you eragon
you have a good heart
the name of your father cannot change that
warmth blossomed inside eragon
he looked from one woman to the next then twisted his hand over his chest overwhelmed by their friendship
thank you
once they were back out in the open eragon put his hands on his hips and took a deep breath of the smoky air