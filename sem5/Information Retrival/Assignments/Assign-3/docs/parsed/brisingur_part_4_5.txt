you had to do it
when you must fight roran does not the fierce joy of combat lend wings to your feet do you not know the pleasure of pitting yourself against a worthy opponent and the satisfaction of seeing the bodies of your enemies piled before you eragon you have experienced this
help me explain it to your cousin
eragon stared at the coals
she had stated a truth that he was reluctant to acknowledge lest by agreeing that one could enjoy violence he would become a man he would despise
so he was mute
across from him roran appeared similarly affected
in a softer voice saphira said do not be angry
i did not intend to upset you
i forget sometimes that you are still unaccustomed to these emotions while i have fought tooth and nail for survival since the day i hatched
rising to his feet eragon walked to their saddlebags and retrieved the small earthenware jar orik had given him before they parted then poured two large mouthfuls of raspberry mead down his gullet
warmth bloomed in his stomach
grimacing eragon passed the jar to roran who also partook of the concoction
several drinks later when the mead had succeeded in tempering his black mood eragon said we may have a problem tomorrow
eragon directed his words toward saphira as well
remember how i said that we saphira and i could easily handle the ra zac
and so we can said saphira
well i was thinking about it while we spied on helgrind and i am not so sure anymore
there are almost an infinite number of ways to do something with magic
for example if i want to light a fire i could light it with heat gathered from the air or the ground i could create a flame out of pure energy i could summon a bolt of lightning i could concentrate a raft of sunbeams into a single point i could use friction and so forth
the problem is even though i can devise numerous spells to perform this one action blocking those spells might require but a single counterspell
if you prevent the action itself from taking place then you do not have to tailor your counterspell to address the unique properties of each individual spell
i still do not understand what this has to do with tomorrow
i do said saphira to both of them
she had immediately grasped the implications
it means that over the past century galbatorix
may have placed wards around the ra zac
a whole range of spells
i probably wo not
be able to kill them with any
of the words of death i was taught nor any
attacks that we can invent now or then
we may
** exclaimed roran
he gave a pained smile
stop please
my head hurts when you do that
eragon paused with his mouth open until that moment he had been unaware that he and saphira were speaking in turn
the knowledge pleased him it signified that they had achieved new heights of cooperation and were acting together as a single entity which made them far more powerful than either would be on their own
it also troubled him when he contemplated how such a partnership must by its very nature reduce the individuality of those involved
he closed his mouth and chuckled
sorry
what i am worried about is this if galbatorix has had the foresight to take certain precautions then force of arms may be the only means by which we can slay the ra zac
if that is true
i will just be in your way tomorrow
nonsense
you may be slower than the ra zac but i have no doubt you will give them cause to fear your weapon roran ** the compliment seemed to please roran
the greatest danger for you is that the ra zac or the lethrblaka will manage to separate you from saphira and me
the closer we stay together the safer we will all be
saphira and i will try to keep the ra zac and lethrblaka occupied but some of them may slip past us
four against two are only good odds if you re among the four
to saphira eragon said if i had a sword i am sure i could slay the ra zac by myself but i do not know if i can beat two creatures who are quick as elves using nothing but this staff
you were the one who insisted on carrying that dry twig instead of a proper weapon she said
remember i told you it might not suffice against enemies as dangerous as the ra zac