she tried to soothe him with her own mind but was unable to alleviate his suffering
her tail instinctually lifted as if to fight
it took minutes before the fit subsided and the last throb faded away leaving eragon gasping
sweat drenched his face making his hair stick and his eyes sting
he reached back and gingerly fingered the top of his scar
it was hot and inflamed and sensitive to touch
saphira lowered her nose and touched him on the ** little one
it was worse this time he said staggering upright
she let him lean against her as he wiped off the sweat with a rag then he tentatively stepped toward the door
are you strong enough to go
we have to
we re obliged as dragon and rider to make a public choice regarding the next head of the varden and perhaps even influence the selection
i wo not ignore the strength of our position we now wield great authority within the varden
at least the twins are not here to grab the position for themselves
that is the only good in the situation
very well but durza should suffer a thousand years of torture for what he did to you
he ** stay close to me
together they made their way through tronjheim toward the nearest kitchen
in the corridors and hallways people stopped and bowed to them murmuring argetlam or ** even dwarves made the motions though not as often
eragon was struck by the somber haunted expressions of the humans and the dark clothing they wore to display their sadness
many women were dressed entirely in black lace veils covering their faces
in the kitchen eragon brought a stone platter of food to a low table
saphira watched him carefully in case he should have another attack
several people tried to approach him but she lifted a lip and growled sending them scurrying away
eragon picked at his food and pretended to ignore the disturbances
finally trying to divert his thoughts from murtagh he asked who do you think has the means to take control of the varden now that ajihad and the twins are gone
she ** is possible you could if ajihad is last words were interpreted as a blessing to secure the leadership
almost no one would oppose you
however that does not seem a wise path to take
i see only trouble in that direction
i agree
besides arya would not approve and she could be a dangerous enemy
elves can not lie in the ancient language but they have no such inhibition in ours she could deny that ajihad ever uttered those words if it served her purposes
no i do not want the position
what about jormundur
ajihad called him his right hand man
unfortunately we know little about him or the varden is other leaders
such a short time has passed since we came here
we will have to make our judgment on our feelings and impressions without the benefit of history
eragon pushed his fish around a lump of mashed ** not forget hrothgar and the dwarf clans they wo not be quiet in this
except for arya the elves have no say in the succession a decision will be made before word of this even reaches them
but the dwarves can not be wo not be ignored
hrothgar favors the varden but if enough clans oppose him he might be maneuvered into backing someone unsuited for the command
a person easily ** closed his eyes and leaned ** could be anyone in farthen dur anyone at all
for a long while they both considered the issues facing them
then saphira said eragon there is someone here to see you
i can not scare him away
eh he cracked his eyes open squinting as they adjusted to the light
a pale looking youth stood by the table
the boy eyed saphira like he was afraid she would try to eat him
what is it asked eragon not unkindly
the boy started flustered then bowed
you have been summoned argetlam to speak before the council of elders
the question confused the boy even more
the the council is
are
people we that is the varden choose to speak on our behalf to ajihad
they were his trusted advisers and now they wish to see you
it is a great ** he finished with a quick smile
are you to lead me to them
saphira looked at eragon questioningly
he shrugged and left the uneaten food motioning for the boy to show the way