the outcome is yours alone to decide
saphira you gave me the star upon my brow and you have always been kind to me
i am and shall always remain your faithful servant
lifting her chin to maximize her three and a half foot height elva surveyed the interior of the pavilion
eragon saphira nasuada
angela
good ** and with that she swept off toward the entrance
the nighthawks parted ranks as she passed between them and went outside
eragon stood feeling unsteady
what sort of monster have i created the two urgal nighthawks touched the tip of each of their horns which he knew was how they warded off evil
to nasuada he said i am sorry
i seem to have only made things worse for you for all of us
calm as a mountain lake nasuada arranged her robes before answering no matter
the game has gotten a little more complicated that is all
it is to be expected the closer we get to uru baen and galbatorix
a moment later eragon heard the sound of an object rushing through the air toward him
he flinched but fast as he was he was too slow to avoid a stinging slap that knocked his head to one side and sent him staggering against a chair
he rolled across the seat of the chair and sprang upright his left arm lifted to ward off an oncoming blow his right arm pulled back ready to stab with the hunting knife he had snatched from his belt during the maneuver
to his astonishment he saw that it was angela who had struck him
the elves were gathered inches behind the fortuneteller ready to subdue her if she should attack him again or to escort her away should eragon order it
solembum was at her feet teeth and claws bared and his hair standing on end
right then eragon could care less about the elves
what did you do that for he demanded
he winced as his split lower lip stretched tearing the flesh farther apart
warm metallic tasting blood trickled down his throat
angela tossed her head
now i am going to have to spend the next ten years teaching elva how to ** that is not what i had in mind for the next **
teach her exclaimed eragon
she wo not let you
she will stop you as easily as she stopped me
humph
not likely
she does not know what bothers me nor what might be about to hurt me
i saw to that the day she and i first met
would you share this spell with us nasuada asked
after how this has turned out it seems prudent for us to have a means of protecting ourselves from elva
no i do not think i will said angela
then she too marched out of the pavilion and solembum stalked after her waving his tail ever so gracefully
the elves sheathed their blades and retreated to a discreet distance from the tent
nasuada rubbed her temples with a circular motion
magic she cursed
the pair of them started as greta cast herself upon the ground and began to weep and wail while pulling at her thin hair beating herself on the face and ripping at her bodice
oh my poor ** i ve lost my ** ** what will become of her all alone oh woe is me my own little blossom rejecting me
it is a shameful reward it is for the work i ve done bending my back like a slave i have
what a cruel hard world always stealing your happiness from ** she groaned
my plum
my rose
my pretty sweet pea
gone
and no one to look after her
** will you watch over her
eragon grasped her by the arm and helped her to her feet consoling her with assurances that he and saphira would keep a close eye on elva
if only as saphira said to eragon because she might attempt to slip a knife between our ribs