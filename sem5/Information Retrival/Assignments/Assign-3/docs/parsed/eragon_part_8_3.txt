summoning his strength he focused on the dragon and tried to impress on it one idea stay here
the dragon stopped moving and cocked its head at him
he pushed harder stay here
a dim acknowledgment came tentatively through the link but eragon wondered if it really ** all it is only an animal
he retreated from the contact with relief and felt the safety of his own mind envelop him
eragon left the tree casting glances backward
the dragon stuck its head out of the shelter and watched with large eyes as he left
after a hurried walk home he sneaked back into his room to dispose of the egg fragments
he was sure garrow and roran would not notice the egg is absence it had faded from their thoughts after they learned it could not be sold
when his family got up roran mentioned that he had heard some noises during the night but to eragon is relief did not pursue the issue
eragon is enthusiasm made the day go by quickly
the mark on his hand proved easy to hide so he soon stopped worrying about it
before long he headed back to the rowan carrying sausages he had pilfered from the cellar
with apprehension he approached the ** the dragon able to survive outside in winter
his fears were groundless
the dragon was perched on a branch gnawing on something between its front legs
it started squeaking excitedly when it saw him
he was pleased to see that it had remained in the tree above the reach of large predators
as soon as he dropped the sausages at the base of the trunk the dragon glided down
while it voraciously tore apart the food eragon examined the shelter
all the meat he had left was gone but the hut was intact and tufts of feathers littered the **
it can get its own food
it struck him that he did not know if the dragon was a he or a she
he lifted and turned it over ignoring its squeals of displeasure but was unable to find any distinguishing ** seems like it wo not give up any secrets without a struggle
he spent a long time with the dragon
he untied it set it on his shoulder and went to explore the woods
the snow laden trees watched over them like solemn pillars of a great cathedral
in that isolation eragon showed the dragon what he knew about the forest not caring if it understood his meaning
it was the simple act of sharing that mattered
he talked to it continuously
the dragon gazed back at him with bright eyes drinking in his words
for a while he just sat with it resting in his arms and watched it with wonder still stunned by recent events
eragon started for home at sunset conscious of two hard blue eyes drilling into his back indignant at being left behind
that night he brooded about all the things that could happen to a small and unprotected animal
thoughts of ice storms and vicious animals tormented him
it took hours for him to find sleep
his dreams were of foxes and black wolves tearing at the dragon with bloody teeth
in the sunrise glow eragon ran from the house with food and scraps of cloth extra insulation for the shelter
he found the dragon awake and safe watching the sunrise from high in the tree
he fervently thanked all the gods known and unknown
the dragon came down to the ground as he approached and leapt into his arms huddling close to his chest
the cold had not harmed it but it seemed frightened
a puff of dark smoke blew out of its nostrils
he stroked it comfortingly and sat with his back to the rowan murmuring softly
he kept still as the dragon buried its head in his coat
after a while it crawled out of his embrace and onto his shoulder
he fed it then wrapped the new rags around the hut
they played together for a time but eragon had to return to the house before long
a smooth routine was quickly established
every morning eragon ran out to the tree and gave the dragon breakfast before hurrying back
during the day he attacked his chores until they were finished and he could visit the dragon again
both garrow and roran noted his behavior and asked why he spent so much time outside
eragon just shrugged and started checking to make sure he was not followed to the tree
after the first few days he stopped worrying that a mishap would befall the dragon