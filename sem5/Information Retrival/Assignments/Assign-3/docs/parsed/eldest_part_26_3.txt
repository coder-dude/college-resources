how are you asked baldor coming alongside him
roran forced a smile
it did not turn out quite how i hoped
sloan is beyond reason when it comes to the spine
that too
i roran fell silent as loring stopped before them
that was a blastedfool thing to ** growled the shoemaker wrinkling his nose
then he stuck out his chin grinned and bared his stumps of teeth
but i ope you and the girl have the best of ** he shook his head
heh you re going to need it **
we re all going to need it snapped thane as he walked past
loring waved a hand
bah sourpuss
listen roran i ve lived in carvahall for many many years and in my experience it is better that this happenednow instead of when we re all warm and cozy
baldor nodded but roran asked why so
is not it obvious normally you and katrina would be the meat of gossip for the next nine ** loring put a finger on the side of his nose
ah but this way you will soon be forgotten amid everything else that is going on and then the two of you might even have some peace
roran frowned
i d rather be talked about than have those desecrators camped on the road
so would we all
still it is something to be grateful for and we all need something to be grateful for ispecially once you re ** loring cackled and pointed at roran
your face just turned purple **
roran grunted and set about gathering katrina is possessions off the ground
as he did he was interrupted by comments from whoever happened to be nearby none of which helped to settle his nerves
rotgut he muttered to himself after a particularly invidious remark
although the expedition into the spine was delayed by the unusual scene the villagers had just witnessed it was only slightly after midmorning when the caravan of people and donkeys began to ascend the bare trail scratched into the side of narnmor mountain to the crest of the igualda falls
it was a steep climb and had to be taken slowly on account of the children and the size of the burdens everyone carried
roran spent most of his time caught behind calitha thane is wife and her five children
he did not mind as it gave him an opportunity to indulge his injured calf and to consider recent events at length
he was disturbed by his confrontation with ** least he consoled himself katrina wo not remain in carvahall much longer
for roran was convinced in his heart of hearts that the village would soon be defeated
it was a sobering yet unavoidable realization
he paused to rest three quarters of the way up the mountain and leaned against a tree as he admired the elevated view of palancar valley
he tried to spot the ra zac is camp which he knew was just to the left of the anora river and the road south but was unable to discern even a wisp of smoke
roran heard the roar of the igualda falls long before they came into sight
the falls appeared for all the world like a great snowy mane that billowed and drifted off narnmor is craggy head to the valley floor a half mile below
the massive stream curved in several directions as it fell the result of different layers of wind
past the slate ledge where the anora river became airborne down a glen filled with thimbleberries and then finally into a large clearing guarded on one side by a pile of boulders roran found that those at the head of the procession had already begun setting up camp
the forest rang with the children is shouts and cries
removing his pack roran untied an ax from the top then set about clearing the underbrush from the site along with several other men
when they finished they began chopping down enough trees to encircle the camp
the aroma of pine sap filled the air
roran worked quickly the wood chips flying in unison with his rhythmic swings
by the time the fortifications were complete the camp had already been erected with seventeen wool tents four small cookfires and glum expressions from people and donkeys alike
no one wanted to leave and no one wanted to stay
roran surveyed the assortment of boys and old men clutching spears and thought too much experience and too little
the grandfathers know how to deal with bears and the like but will the grandsons have the strength to actually do it then he noticed the hard glint in the women is eyes and realized that while they might hold a babe or be busy tending a scraped arm their own shields and spears were never far from reach