she still feels the pain of those around her
but now it is by her own choice said oromis
no longer does your magic force it upon her
you did not come here to seek my opinion concerning elva
what is it that weighs upon your heart eragon ask what you will and i promise i shall answer all of your questions to the best of my knowledge
what said eragon if i do not know the right questions to ask
a twinkle appeared in oromis is gray eyes
ah you begin to think like an elf
you must trust us as your mentors to teach you and saphira those things of which you are ignorant
and you must also trust us to decide when it is appropriate to broach those subjects for there are many elements of your training that should not be spoken of out of turn
eragon placed the blueberry in the precise center of the tray then in a quiet but firm voice said it seems as if there is much you have not spoken of
for a moment the only sounds were the rustle of branches and the burble of the stream and the chatter of distant squirrels
if you have a quarrel with us eragon said glaedr then give voice to it and do not gnaw on your anger like a dry old bone
saphira shifted her position and eragon imagined he heard a growl from her
he glanced at her and then fighting to control the emotions coursing through him he asked when i was last here did you know who my father was
and did you know that murtagh was my brother
oromis nodded once more
we did but
then why did not you tell ** exclaimed eragon and jumped to his feet knocking over his chair
he pounded a fist against his hip strode several feet away and stared at the shadows within the tangled forest
whirling around eragon is anger swelled as he saw that oromis appeared as calm as before
were you ever going to tell me did you keep the truth about my family a secret because you were afraid it would distract me from my training or was it that you were afraid i would become like my father a worse thought occurred to eragon
or did you not even consider it important enough to mention and what of brom did he know did he choose carvahall to hide in because of me because i was the son of his enemy you can not expect me to believe it was coincidence he and i happened to be living only a few miles apart and that arya just happened to send saphira is egg to me in the spine
what arya did was an accident asserted oromis
she had no knowledge of you then
eragon gripped the pommel of his dwarf sword every muscle in his body as hard as iron
when brom first saw saphira i remember he said something to himself about being unsure whether this was a farce or a tragedy
at the time i thought he was referring to the fact that a common farmer like myself had become the first new rider in over a hundred years
but that is not what he meant was it he was wondering whether it was a farce or a tragedy that morzan is youngest son should be the one to take up the riders mantle
is that why you and brom trained me to be nothing more than a weapon against galbatorix so that i may atone for the villainy of my father is that all i am to you a balancing of the scales before oromis could respond eragon swore and said my whole life has been a ** since the moment i was born no one but saphira has wanted me not my mother not garrow not aunt marian not even brom
brom showed interest in me only because of morzan and saphira
i have always been an inconvenience
whatever you think of me though i am not my father nor my brother and i refuse to follow in their ** placing his hands on the edge of the table eragon leaned forward
i am not about to betray the elves or the dwarves or the varden to galbatorix if that is what you are worried about
i will do what i must but from now on you have neither my loyalty nor my trust
i will not
the ground and the air shook as glaedr growled his upper lip pulling back to reveal the full length of his fangs
you have more reason to trust us than anyone else hatchling he said his voice thundering in eragon is mind
if not for our efforts you would be long dead
then to eragon is surprise saphira said to oromis and glaedr tell him and it alarmed him to feel the distress in her thoughts