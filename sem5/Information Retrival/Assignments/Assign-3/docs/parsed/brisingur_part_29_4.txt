you have killed many urgralgra firesword
it is why we must be allies or my race will not survive
eragon crossed his arms
when brom and i were tracking the ra zac we passed through yazuac a village by the ninor river
we found all of the people piled in the center of the village dead with a baby stuck on a spear at the top of the pile
it was the worst thing i ve ever seen
and it was urgals who killed them
before i got my horns said garzhvog my father took me to visit one of our villages along the western fringes of the spine
we found our people tortured burnt and slaughtered
the men of narda had learned of our presence and they had surprised the village with many soldiers
not one of our tribe escaped
it is true we love war more than other races firesword and that has been our downfall many times before
our women will not consider a ram for a mate unless he has proven himself in battle and killed at least three foes himself
and there is a joy in battle unlike any other joy
but though we love feats of arms that does not mean we are not aware of our faults
if our race cannot change galbatorix will kill us all if he defeats the varden and you and nasuada will kill us all if you overthrow that snake tongued betrayer
am i not right firesword
eragon jerked his chin in a nod
aye
it does no good then to dwell upon past wrongs
if we cannot overlook what each of our races has done there will never be peace between humans and the urgralgra
how should we treat you though if we defeat galbatorix and nasuada gives your race the land you have asked for and twenty years from now your children begin to kill and plunder so they can win mates if you know your own history garzhvog then you know it has always been so when urgals sign peace accords
with a thick sigh garzhvog said then we will hope that there are still urgralgra across the sea and that they are wiser than us for we will be no more in this land
neither of them spoke again that night
garzhvog curled up on his side and slept with his massive head resting on the ground while eragon wrapped himself in his cloak and sat against the stump and gazed at the slowly turning stars drifting in and out of his waking dreams
eragon and garzhvog did not stop that night but continued running through the hours of darkness and through the day thereafter
when morning arrived the sky grew bright but because of the beor mountains it was almost noon before the sun burst forth between two peaks and rays of light as wide as the mountains themselves streamed out over the land that was still caught in the strange twilight of shadow
eragon paused then on the bank of a brook and contemplated the sight in silent wonderment for several minutes
as they skirted the vast range of mountains their journey began to seem to eragon uncomfortably similar to his flight from gil ead to farthen dur with murtagh saphira and arya
he even thought he recognized the place where they had camped after crossing the hadarac desert
when he and garzhvog arrived at the mouth of the great rift that split the range of mountains for many leagues from north to south they turned to their right and passed between the cold and indifferent peaks
arriving at the beartooth river which flowed out of the narrow valley that led to farthen dur they forded the frigid waters and continued southward
that night before they ventured east into the mountains proper they camped by a small pond and rested their limbs
garzhvog killed another deer with his sling this time a buck and they both ate their fill
his hunger sated eragon was hunched over mending a hole in the side of his boot when he heard an eerie howl that set his pulse racing
he glanced around the darkened landscape and to his alarm he saw the silhouette of a huge beast loping around the pebble lined shore of the pond
garzhvog said eragon in a low voice and reached over to his pack and drew his falchion
taking a fist sized rock from the ground the kull placed it in the leather pocket of his sling and then rising to his full height he opened his maw and bellowed into the night until the land rang with echoes of his defiant challenge