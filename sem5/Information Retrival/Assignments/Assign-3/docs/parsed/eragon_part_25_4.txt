somehow the empire at least i think it is them has discovered those of us who have been helping to support tronjheim
but i am still not convinced that it is the empire
no one sees any soldiers
i do not understand it
perhaps galbatorix hired mercenaries to harass us
i heard that you lost a ship recently
the last one i owned answered jeod bitterly
every man on it was loyal and brave
i doubt i will ever see them again
the only option i have left is to send caravans to surda or gil ead which i know wo not get there no matter how many guards i hire or charter someone else is ship to carry the goods
but no one will take them now
how many merchants have been helping you asked brom
oh a good number up and down the seaboard
all of them have been plagued by the same troubles
i know what you are thinking i ve pondered it many a night myself but i cannot bear the thought of a traitor with that much knowledge and power
if there is one we re all in jeopardy
you should return to tronjheim
and take eragon there interrupted brom
they d tear him apart
it is the worst place he could be right now
maybe in a few months or even better a year
can you imagine how the dwarves will react everyone will be trying to influence him especially islanzadi
he and saphira wo not be safe in tronjheim until i at least get them through tuatha du orothrim
** eragon ** is this tronjheim and why did he tell jeod about saphira he should not have done that without asking me
still i have a feeling that they are in need of your power and wisdom
wisdom snorted brom
i am just what you said earlier a crotchety old man
let them
i ve no need to explain myself
no ajihad will have to get along without me
what i am doing now is much more important
but the prospect of a traitor raises troubling questions
i wonder if that is how the empire knew where to be
his voice trailed off
and i wonder why i have not been contacted about this said jeod
maybe they tried
but if there is a traitor
brom paused
i have to send word to ajihad
do you have a messenger you can trust
i think so said jeod
it depends on where he would have to go
i do not know said brom
i ve been isolated so long my contacts have probably died or forgotten me
could you send him to whoever receives your shipments
what is not these days how soon can he leave
he can go in the morning
i will send him to gil ead
it will be faster said jeod
what can he take to convince ajihad the message comes from you
here give your man my ring
and tell him that if he loses it i will personally tear his liver out
it was given to me by the queen
brom grunted
after a long silence he said we d better go out and join eragon
i get worried when he is alone
that boy has an unnatural propensity for being wherever there is trouble
eragon heard chairs being pushed back
he quickly pulled his mind away and opened his eyes
what is going on he muttered to ** and other traders are in trouble for helping people the empire does not favor
brom found something in gil ead and went to carvahall to hide
what could be so important that he would let his own friend think he was dead for nearly twenty years he mentioned a queen when there are not any queens in the known kingdoms and dwarves who as he himself told me disappeared underground long ago
he wanted ** but he would not confront brom now and risk jeopardizing their mission
no he would wait until they left teirm and then he would persist until the old man explained his secrets
eragon is thoughts were still whirling when the door opened
were the horses all right asked brom
fine said eragon
they untied the horses and left the castle
as they reentered the main body of teirm brom said so jeod you finally got married
and he winked slyly to a lovely young woman
congratulations
jeod did not seem happy with the compliment
he hunched his shoulders and stared down at the street
whether congratulations are in order is debatable right now
helen is not very happy
why what does she want asked brom
the usual said jeod with a resigned shrug
a good home happy children food on the table and pleasant company