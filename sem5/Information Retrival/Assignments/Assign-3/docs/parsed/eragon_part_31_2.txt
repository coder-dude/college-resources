murtagh watched the entire process
it was over quickly
as the light vanished eragon sat feeling ** ve never done that before he said
saphira ** we can cast spells that are beyond either of us
murtagh examined brom is side and asked is he completely healed
i can only mend what is on the surface
i do not know enough to fix whatever is damaged inside
it is up to him now
i ve done all i ** eragon closed his eyes for a moment utterly weary
my
my head seems to be floating in clouds
you probably need to eat said murtagh
i will make soup
while murtagh fixed the meal eragon wondered who this stranger was
his sword and bow were of the finest make as was his horn
either he was a thief or accustomed to money and lots of ** was he hunting the ra zac what have they done to make him an enemy i wonder if he works for the varden
murtagh handed him a bowl of broth
eragon spooned it down and asked how long has it been since the ra zac fled
we have to go before they return with reinforcements
you might be able to travel said murtagh then gestured at brom but he can not
you do not get up and ride away after being stabbed between the ribs
if we make a litter can you carry brom with your claws like you did with garrow eragon asked saphira
yes but landing will be awkward
as long as it can be ** said to murtagh saphira can carry him but we need a litter
can you make one i do not have the strength
wait ** murtagh left the camp sword drawn
eragon hobbled to his bags and picked up his bow from where it had been thrown by the ra zac
he strung it found his quiver then retrieved zar roc which lay hidden in shadow
last he got a blanket for the litter
murtagh returned with two saplings
he laid them parallel on the ground then lashed the blanket between the poles
after he carefully tied brom to the makeshift litter saphira grasped the saplings and laboriously took flight
i never thought i would see a sight like that murtagh said an odd note in his voice
as saphira disappeared into the dark sky eragon limped to cadoc and hoisted himself painfully into the saddle
thanks for helping us
you should leave now
ride as far away from us as you can
you will be in danger if the empire finds you with us
we can not protect you and i would not see harm come to you on our account
a pretty speech said murtagh grinding out the fire but where will you go is there a place nearby that you can rest in safety
murtagh is eyes glinted as he fingered the hilt of his sword
in that case i think i will accompany you until you re out of danger
i ve no better place to be
besides if i stay with you i might get another shot at the ra zac sooner than if i were on my own
interesting things are bound to happen around a rider
eragon wavered unsure if he should accept help from a complete stranger
yet he was unpleasantly aware that he was too weak to force the issue either ** murtagh proves untrustworthy saphira can always chase him away
join us if you ** he shrugged
murtagh nodded and mounted his gray war horse
eragon grabbed snowfire is reins and rode away from the camp into the wilderness
an oxbow moon provided wan light but he knew that it would only make it easier for the ra zac to track them
though eragon wanted to question murtagh further he kept silent conserving his energy for riding
near dawn saphira said i must stop
my wings are tired and brom needs attention
i discovered a good place to stay about two miles ahead of where you are
they found her sitting at the base of a broad sandstone formation that curved out of the ground like a great hill
its sides were pocked with caves of varying sizes
similar domes were scattered across the land
saphira looked pleased with ** found a cave that can not be seen from the ground
it is large enough for all of us including the horses
follow me
she turned and climbed up the sandstone her sharp claws digging into the rock
the horses had difficulty as their shod hooves could not grip the sandstone
eragon and murtagh had to pull and shove the animals for almost an hour before they managed to reach the cave
the cavern was a good hundred feet long and more than twenty feet wide yet it had a small opening that would protect them from bad weather and prying eyes