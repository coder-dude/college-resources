to the casual observer the various names an intrepid traveler will encounter throughout alagaesia might seem but a random collection of labels with no inherent integrity culture or history
however as with any land that has been repeatedly colonized by different cultures and in this case different races alagaesia quickly accumulated layers of names from the elves dwarves humans and even urgals
thus we can have palancar valley ** human ** the anora river and ristvak baen ** ** and utgard mountain ** dwarf ** all within a few square miles of each other
while this is of great historical interest practically it often leads to confusion as to the correct pronunciation
unfortunately there are no set rules for the neophyte
each name must be learned upon its own terms unless you can immediately place its language of origin
the matter grows even more confusing when you realize that in many places the spelling and pronunciation of foreign words were altered by the resident population to conform to their own language
the anora river is a prime example
originallyanora was spelledaenora which meansbroad in the ancient language
in their writings the humans simplified the word toanora and this combined with a vowel shift whereinae ** ** was said as the easiera ** created the name as it appears in eragon is time
to spare readers as much difficulty as possible the following list is provided with the understanding that these are only rough guidelines to the actual pronunciation
the enthusiast is encouraged to study the source languages in order to master their true intricacies