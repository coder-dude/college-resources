saphira glided down while eragon searched for the farm
when he spotted it fear jolted him
a black plume with orange flames dancing at its base rose from the farm
** ** me down there
now
she locked her wings and tilted into a steep dive hurtling groundward at a frightening rate
then she altered her dive slightly so they sped toward the forest
he yelled over the screaming air land in the ** he held on tighter as they plummeted
saphira waited until they were only a hundred feet off the ground before driving her wings downward in several powerful strokes
she landed heavily breaking his grip
he crashed to the ground then staggered upright gasping for breath
the house had been blasted apart
timbers and boards that had been walls and roof were strewn across a wide area
the wood was pulverized as if a giant hammer had smashed it
sooty shingles lay everywhere
a few twisted metal plates were all that remained of the stove
the snow was perforated with smashed white crockery and chunks of bricks from the chimney
thick oily smoke billowed from the barn which burned fiercely
the farm animals were gone either killed or frightened away
** eragon ran to the wreckage hunting through the destroyed rooms for garrow
there was no sign of him
** eragon cried again
saphira walked around the house and came to his side
this would not have happened if you had not run away with **
you would not be alive if we had stayed
look at ** he screamed
we could ve warned ** it is your fault he did not get ** he slammed his fist against a pole splitting the skin on his knuckles
blood dripped down his fingers as he stalked out of the house
he stumbled to the path that led to the road and bent down to examine the snow
several tracks were before him but his vision was blurry and he could barely ** i going blind he wondered
with a shaking hand he touched his cheeks and found them wet
a shadow fell on him as saphira loomed overhead sheltering him with her ** comfort all may not be lost
he looked up at her searching for ** the trail my eyes see only two sets of prints
garrow could not have been taken from here
he focused on the trampled snow
the faint imprints of two pairs of leather boots headed toward the house
on top of those were traces of the same two sets of boots leaving
and whoever had made the departing tracks had been carrying the same weight as when they ** re right garrow has to be ** he leapt to his feet and hurried back to the house
i will search around the buildings and in the forest said saphira
eragon scrambled into the remains of the kitchen and frantically started digging through a pile of rubble
pieces of debris that he could not have moved normally now seemed to shift on their own accord
a cupboard mostly intact stymied him for a second then he heaved and sent it flying
as he pulled on a board something rattled behind him
he spun around ready for an attack
a hand extended from under a section of collapsed roof
it moved weakly and he grasped it with a cry
uncle can you hear me there was no response
eragon tore at pieces of wood heedless of the splinters that pierced his hands
he quickly exposed an arm and shoulder but was barred by a heavy beam
he threw his shoulder at it and shoved with every fiber of his being but it defied his efforts
** i need **
she came immediately
wood cracked under her feet as she crawled over the ruined walls
without a word she nosed past him and set her side against the beam
her claws sank into what was left of the floor her muscles strained
with a grating sound the beam lifted and eragon rushed under it
garrow lay on his stomach his clothes mostly torn off
eragon pulled him out of the rubble
as soon as they were clear saphira released the beam leaving it to crash to the floor
eragon dragged garrow out of the destroyed house and eased him to the ground
dismayed he touched his uncle gently
his skin was gray lifeless and dry as if a fever had burned off any sweat
his lip was split and there was a long scrape on his cheekbone but that was not the worst
deep ragged burns covered most of his body