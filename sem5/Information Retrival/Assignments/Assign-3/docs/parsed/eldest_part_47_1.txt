concentrate eragon said oromis though not unkindly
eragon blinked and rubbed his eyes in an attempt to focus on the glyphs that decorated the curling parchment paper before him
sorry ** weariness dragged upon him like lead weights tied to his limbs
he squinted at the curved and spiked glyphs raised his goose feather quill and began to copy them again
through the window behind oromis the green shelf on top of the crags of tel naeir was streaked with shadows from the descending sun
beyond feathery clouds banded the sky
eragon is hand jerked as a line of pain shot up his leg and he broke the nib of the quill and sprayed ink across the paper ruining it
across from him oromis also started clutching his right arm
** eragon
he reached for her with his mind and to his bewilderment was deflected by impenetrable barriers that she had erected around herself
he could barely feel her
it was as if he were trying to grasp an orb of polished granite coated with oil
she kept slipping away from him
he looked at oromis
something is happened to them has not it
i know not
glaedr returns but he refuses to talk to ** taking his blade naegling from the wall oromis strode outside and stood upon the edge of the crags head uplifted as he waited for the gold dragon to appear
eragon joined him thinking of everything probable and improbable that might have befallen saphira
the two dragons had left at noon flying north to a place called the stone of broken eggs where the wild dragons had nested in ages past
it was an easy ** could not be urgals the elves do not allow them into du weldenvarden he told himself
at last glaedr came into view high above as a winking speck among the darkening clouds
as he descended to land eragon saw a wound on the back of the dragon is right foreleg a tear in his lapped scales as wide as eragon is hand
scarlet blood laced the grooves between the surrounding scales
the moment glaedr touched the ground oromis rushed toward him only to stop when the dragon growled at him
hopping on his injured leg glaedr crawled to the edge of the forest where he curled up beneath the outstretched boughs his back to eragon and set about licking clean his wound
oromis went and knelt in the clover by glaedr keeping his distance with calm patience
it was obvious that he would wait as long as need be
eragon fidgeted as the minutes elapsed
finally by some unspoken signal glaedr allowed oromis to draw near and inspect his leg
magic glowed from oromis is gedwey ignasia as he placed his hand over the rent in glaedr is scales
how is he asked eragon when oromis withdrew
it looks a fearsome wound but it is no more than a scratch for one so large as glaedr
what about saphira though i still can not contact her
you must go to her said oromis
she is hurt in more ways than one
glaedr said little of what transpired but i have guessed much and you would do well to hurry
eragon glanced about for any means of transportation and groaned with anguish when he confirmed that none existed
how can i reach her it is too far to run there is no trail and i can not
calm thyself eragon
what was the name of the steed who bore you hence from silthrim
it took eragon a moment to recall
folkvir
then summon him with your skill at gramarye
name him and your need in this the most powerful of languages and he will come to your assistance
letting the magic suffuse his voice eragon cried out for folkvir sending his plea echoing over the forested hills toward ellesmera with all the urgency he could muster
twelve minutes later folkvir emerged like a silver ghost from the dark shadows among the trees tossing his mane and snorting with excitement
the stallion is sides heaved from the speed of his journey
throwing a leg over the small elven horse eragon said i will return as soon as i can
do what you must said oromis
then eragon touched his heels to folkvir is ribs and shouted run ** ** the horse leaped forward and bounded into du weldenvarden threading his way with incredible dexterity between the gnarled pines
eragon guided him toward saphira with images from his mind