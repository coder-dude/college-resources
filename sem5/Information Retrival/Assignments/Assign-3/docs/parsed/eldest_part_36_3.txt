the heavy odor of metallic vapors made nasuada is eyes water
lifting their hems off the floor she and farica wended their way in single file toward the back of the room past hourglasses and scales arcane tomes bound with black iron dwarven astrolabes and piles of phosphorescent crystal prisms that produced fitful blue flashes
they met orrin by a marble topped bench where he stirred a crucible of quicksilver with a glass tube that was closed at one end open at the other and must have measured at least three feet in length although it was only a quarter of an inch thick
sire said nasuada
as befitted one of equal rank to the king she remained upright while farica curtsied
you seem to have recovered from the explosion last week
orrin grimaced good naturedly
i learned that it is not wise to combine phosphorus and water in an enclosed space
the result can be quite violent
has all of your hearing returned
not entirely but
grinning like a boy with his first dagger he lit a taper with the coals from a brazier which she could not fathom how he endured in the stifling weather carried the flaming brand back to the bench and used it to start a pipe packed with cardus weed
i did not know that you smoked
i do not really he confessed except that i found that since my eardrum has not completely sealed up yet i can do this
drawing on the pipe he puffed out his cheeks until a tendril of smoke issued from his left ear like a snake leaving its den and coiled up the side of his head
it was so unexpected nasuada burst out laughing and after a moment orrin joined her releasing a plume of smoke from his mouth
it is the most peculiar sensation he confided
tickles like crazy on the way out
growing serious again nasuada asked was there something else that you wished to discuss with me sire
he snapped his fingers
of ** dipping his long glass tube in the crucible he filled it with quicksilver then capped the open end with one finger and showed the whole thing to her
would you agree that the only thing in this tube is quicksilver
i ** is this why he wanted to see me
and what about now with a quick movement he inverted the tube and planted the open end inside the crucible removing his finger
instead of all pouring out as nasuada expected the quicksilver in the tube dropped about halfway then stopped and held its position
orrin pointed to the empty section above the suspended metal
he asked what occupies that space
it must be air asserted nasuada
orrin grinned and shook his head
if that were true how would the air bypass the quicksilver or diffuse through the glass no routes are available by which the atmosphere can gain ** he gestured at farica
what is your opinion maid
farica stared at the tube then shrugged and said it can not be nothing sire
ah but that is exactly what i think it is nothing
i believe that i ve solved one of the oldest conundrums of natural philosophy by creating and proving the existence of a ** it completely invalidates vacher is theories and means that ladin was actually a genius
blasted elves always seem to be right
nasuada struggled to remain cordial as she asked what purpose does it serve though
purpose orrin looked at her with genuine astonishment
none of course
at least not that i can think of
however this will help us to understand the mechanics of our world how and why things happen
it is a wondrous discovery
who knows what else it might lead to while he spoke he emptied the tube and carefully placed it in a velvet padded box that held similar delicate instruments
the prospect that truly excites me though is of using magic to ferret out nature is secrets
why just yesterday with a single spell trianna helped me to discover two entirely new gases
imagine what could be learned if magic were systematically applied to the disciplines of natural philosophy
i am considering learning magic myself if i have the talent for it and if i can convince some magic users to divulge their knowledge
it is a pity that your dragon rider eragon did not accompany you here i am sure that he could help me