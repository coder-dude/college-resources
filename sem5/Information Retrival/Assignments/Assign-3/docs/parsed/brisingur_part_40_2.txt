this i swear upon mine family is honor
aye said orik
i suppose it would have been too much to ask for nado to vote for anyone but himself
setting aside his knife and wood freowin of durgrimst gedthrall heaved his bulk halfway out of his chair and keeping his gaze angled downward said in his whispering baritone on behalf of mine clan i vote for grimstborith nado as our new ** then he lowered himself back into his seat and resumed carving his raven ignoring the stir of astonishment that swept through the room
nado is expression changed from pleased to smug
barzul growled orik his scowl deepening
his chair creaked as he pressed his forearms down against the armrests the tendons in his hands rigid with strain
that false faced traitor
he promised his vote to **
eragon is stomach sank
why would he betray you
he visits sindri is temple twice a day
i should have known he would not go against gannel is wishes
** gannel is been playing me this whole time
i at that moment the attention of the clanmeet turned to orik
concealing his anger orik got to his feet and looked around the table at each of the other clan chiefs and in his own language he said on behalf of mine clan i vote for myself as our new king
if you will have me i promise to bring our people gold and glory and the freedom to live above the ground without fear of galbatorix destroying our homes
this i swear upon mine family is honor
five to four eragon said to orik as he returned to his seat
and not in our favor
orik grunted
i can count eragon
eragon rested his elbows on his knees his eyes darting from one dwarf to another
the desire to act gnawed at him
how he knew not but with so much at stake he felt that he ought to find a way to ensure orik would become king and thus that the dwarves would continue to aid the varden in their struggle against the empire
for all he tried however eragon could think of nothing to do but sit and wait
the next dwarf to rise was havard of durgrimst fanghur
with his chin tucked against his breastbone havard pushed out his lips and tapped the table with the two fingers he still had on his right hand appearing thoughtful
eragon inched forward on his seat his heart pounding
will he uphold his bargain with orik eragon wondered
havard tapped the table once more then slapped the stone with the flat of his hand
lifting his chin he said on behalf of mine clan i vote for grimstborith orik as our new king
it gave eragon immense satisfaction to watch as nado is eyes widened and then the dwarf gnashed his teeth together a muscle in his cheek twitching
** muttered orik
that put a burr in his beard
the only two clan chiefs who had yet to vote were hreidamar and iorunn
hreidamar the compact muscular grimstborith of the urzhad appeared uneasy with the situation while iorunn she of durgrimst vrenshrrgn the war wolves traced the crescent shaped scar on her left cheekbone with the tip of a pointed fingernail and smiled like a self satisfied cat
eragon held his breath as he waited to hear what the two of them would say
if iorunn votes for herself he thought and if hreidamar is still loyal to her then the election will have to proceed to a second round
there is no reason for her to do that however other than to delay events and so far as i know she would not profit from a delay
she cannot hope to become queen at this point her name would be eliminated from consideration before the beginning of the second round and i doubt she would be so foolish as to squander the power she has now merely so she can boast to her grandchildren that she was once a candidate for the throne
but if hreidamar does part ways with her then the vote will remain tied and we will continue on to a second round regardless
** if only i could scry into the ** what if orik loses should i seize control of the clanmeet then i could seal the chamber so no one could enter or leave and then
but no that would be
iorunn interrupted eragon is thoughts by nodding at hreidamar and then directing her heavy lidded gaze toward eragon which made him feel as if he were a prize ox she was examining