if this dormnad can take you to the varden then he needs to be found as quickly as possible
neither of us should remain near gil ead longer than a few days
again wisdom flies from his mouth commented saphira dryly
she told eragon what should be said to dormnad and he relayed the information to murtagh
very well said murtagh adjusting his sword
unless there is trouble i will be back within a couple of hours
make sure there is some food left for ** with a wave of his hand he jumped onto tornac and rode away
eragon sat by the fire tapping zar roc is pommel apprehensively
hours passed but murtagh did not return
eragon paced around the fire zar roc in hand while saphira watched gil ead attentively
only her eyes moved
neither of them voiced their worries though eragon unobtrusively prepared to leave in case a detachment of soldiers left the city and headed toward their camp
eragon swiveled toward gil ead alert
he saw a distant horseman exit the city and ride furiously toward their ** do not like this he said as he climbed onto ** ready to fly
i am prepared for more than that
as the rider approached eragon recognized murtagh bent low over tornac
no one seemed to be pursuing him but he did not slow his reckless pace
he galloped into the camp and jumped to the ground drawing his sword
what is wrong asked eragon
murtagh scowled
did anyone follow me from gil ead
good
then let me eat before i explain
i am ** he seized a bowl and began eating with gusto
after a few sloppy bites he said through a full mouth dormnad has agreed to meet us outside gil ead at sunrise tomorrow
if he is satisfied you really are a rider and that it is not a trap he will take you to the varden
where are we supposed to meet him asked eragon
murtagh pointed west
on a small hill across the road
murtagh spooned more food into his bowl
it is a rather simple thing but all the more deadly because of it i was seen in the street by someone who knows me
i did the only thing i could and ran away
it was too late though he recognized me
it was unfortunate but eragon was unsure how bad it really was
since i do not know your friend i have to ask will he tell anyone
murtagh gave a strained laugh
if youhad met him that would not need answering
his mouth is loosely hinged and hangs open all the time vomiting whatever happens to be in his mind
the question is notwhether he will tell people butwhom he will tell
if word of this reaches the wrong ears we will be in trouble
i doubt that soldiers will be sent to search for you in the dark eragon pointed out
we can at least count on being safe until morning and by then if all goes well we will be leaving with dormnad
murtagh shook his head
no only you will accompany him
as i said before i wo not go to the varden
eragon stared at him unhappily
he wanted murtagh to stay
they had become friends during their travels and he was loath to tear that apart
he started to protest but saphira hushed him and said gently wait until tomorrow
now is not the time
very well he said glumly
they talked until the stars were bright in the sky then slept as saphira took the first watch
eragon woke two hours before dawn his palm tingling
everything was still and quiet but something sought his attention like an itch in his mind
he buckled on zar roc and stood careful not to make a sound
saphira looked at him curiously her large eyes ** is it she asked
i do not know said eragon
he saw nothing amiss
saphira sniffed the air curiously
she hissed a little and lifted her head
i smell horses nearby but they re not moving
they reek with an unfamiliar stench
eragon crept to murtagh and shook his shoulder
murtagh woke with a start yanked a dagger from under his blankets then looked at eragon quizzically
eragon motioned for him to be silent whispering there are horses close by
murtagh wordlessly drew his sword
they quietly stationed themselves on either side of saphira prepared for an attack
as they waited the morning star rose in the east
a squirrel chattered
then an angry snarl from behind made eragon spin around sword held high
a broad urgal stood at the edge of the camp carrying a mattock with a nasty ** did he come from we have not seen their tracks ** thought eragon