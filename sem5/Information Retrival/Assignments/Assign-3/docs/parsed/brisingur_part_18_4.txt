what you said earlier about helgrind was that all of it asked roran
his grip was like a pair of iron pincers clamped around eragon is flesh
his eyes were hard and questioning and also unusually vulnerable
eragon held his gaze
if you trust me roran never ask me that question again
it is not something you want to ** even as he spoke eragon felt a deep sense of unease over having to conceal sloan is existence from roran and katrina
he knew the deception was necessary but it still made him uncomfortable to lie to his family
for a moment eragon considered telling roran the truth but then he remembered all the reasons he had decided not to and held his tongue
roran hesitated his face troubled then he set his jaw and released eragon
i trust you
that is what family is for after all eh trust
roran laughed and rubbed his nose with a thumb
that ** he rolled his thick round shoulders and reached up to massage his right one a habit he had fallen into since the ra zac had bitten him
i have another question
it is a boon
a favor i seek of ** a wry smile touched his lips and he shrugged
i never thought i would speak to you of this
you re younger than i you ve barely reached your manhood and you re my cousin to boot
speak of what stop beating around the bush
of marriage said roran and lifted his chin
will you marry katrina and me it would please me if you would and while i have refrained from mentioning it to her until i had your answer i know katrina would be honored and delighted if you would consent to join us as man and wife
astonished eragon was at a loss for words
at last he managed to stammer me then he hastened to say i would be happy to do it of course but
me is that really what you want i am sure nasuada would agree to marry the two of you
you could have king orrin a real ** he would leap at the chance to preside over the ceremony if it would help him earn my favor
i want you eragon said roran and clapped him on the shoulder
you are a rider and you are the only other living person who shares my blood murtagh does not count
i cannot think of anyone else i would rather have tie the knot around my wrist and hers
then said eragon i ** the air whooshed out of him as roran embraced him and squeezed with all of his prodigious strength
he gasped slightly when roran released him and then once his breath had returned said when nasuada has a mission planned for me
i do not know what it is yet but i am guessing it will keep me busy for some time
so
maybe early next month if events allow
roran is shoulders bunched and knotted
he shook his head like a bull sweeping its horns through a clump of brambles
what about the day after tomorrow
so soon is not that rushing it a bit there would hardly be any time to prepare
people will think it is unseemly
roran is shoulders rose and the veins on his hands bulged as he opened and closed his fists
it can not wait
if we re not married and quick the old women will have something far more interesting to gossip about than my impatience
do you understand
it took eragon a moment to grasp roran is meaning but once he did eragon could not stop a broad smile from spreading across his face
roran is going to be a ** he thought
still smiling he said i think so
the day after tomorrow it ** eragon grunted as roran hugged him again pounding him on the back
with some difficulty he freed himself
grinning roran said i am in your debt
thank you
now i must go share the news with katrina and we must do what we can to ready a wedding feast
i will let you know the exact hour once we decide on it
roran began walking toward the tent then he spun around and threw his arms out in the air as if he would gather the entire world to his breast
eragon i am going to be **
with a laugh eragon waved his hand
go on you fool
she is waiting for you
eragon climbed onto saphira as the flaps of the tent closed over roran
blodhgarm he called
quiet as a shadow the elf glided into the light his yellow eyes glowing like coals
saphira and i are going to fly for a little while
we will meet you at my tent
shadeslayer said blodhgarm and tilted his head