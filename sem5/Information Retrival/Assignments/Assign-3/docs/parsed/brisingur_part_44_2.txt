the only piece of furniture inside was a battered wooden stool
he sat and concentrated upon his breathing determined to remain calm
as the minutes passed roran began to hear the tromp of boots and the clink of mail as the varden assembled around the whipping post
roran imagined the thousands of men and women staring at him including the villagers from carvahall
his pulse quickened and sweat sprang up upon his brow
after about half an hour the sorceress trianna entered the tent and had him strip down to his trousers which embarrassed roran although the woman seemed to take no notice
trianna examined him all over and even cast an additional spell of healing on his left shoulder where the soldier had stabbed him with the bolt of a crossbow
then she declared him fit to continue and gave him a shirt made of sackcloth to wear in place of his own
roran had just pulled the shirt over his head when katrina pushed her way into the tent
as he beheld her an equal measure of joy and dread filled roran
katrina glanced between him and trianna then curtsied to the sorceress
may i please speak with my husband alone
of course
i shall wait outside
once trianna had departed katrina rushed to roran and threw her arms around him
he hugged her just as fiercely as she hugged him for he had not seen her since he had returned to the varden
oh how i ve missed you katrina whispered in his right ear
they drew apart just far enough so that they could gaze into each other is eyes and then katrina scowled
this is ** i went to nasuada and i begged her to pardon you or at least to reduce the number of lashes but she refused to grant my request
running his hands up and down katrina is back roran said i wish that you had not
because i said that i would remain with the varden and i will not go back on my word
but this is ** said katrina gripping him by his shoulders
carn told me what you did roran you slew almost two hundred soldiers by yourself and if not for your heroism none of the men with you would have survived
nasuada ought to be plying you with gifts and praise not having you whipped like a common **
it does not matter whether this is right or wrong he told her
it is necessary
if i were in nasuada is position i would have given the same order myself
katrina shuddered
fifty lashes though
why does it have to be so many men have died from being whipped that many times
only if they had weak hearts
do not be so worried it will take more than that to kill me
a false smile flickered across katrina is lips and then a sob escaped her and she pressed her face against his chest
he cradled her in his arms stroking her hair and reassuring her as best he could even though he felt no better than she
after several minutes roran heard a horn being winded outside the tent and he knew that their time together was drawing to a close
extricating himself from katrina is embrace he said there is something i want you to do for me
what she asked dabbing at her eyes
go back to our tent and do not leave it until after my flogging
katrina appeared shocked by his request
** i shall not leave you
not now
please he said you should not have to see this
and you should not have to endure it she retorted
leave that
i know you wish to stay by my side but i can bear this better if i know that you are not here watching me
i brought this upon myself katrina and i do not want you to suffer because of it as well
her expression became strained
the knowledge of your fate shall pain me regardless of where i am standing
however
i shall do as you ask but only because it will help you through this ordeal
you know that i would have the whip fall upon my own body instead of yours if i could
and you know he said kissing her on both cheeks that i would refuse to let you take my place
tears welled up in her eyes again and she pulled him closer hugging him so tightly he had difficulty breathing
they were still wrapped in each other is arms when the entrance flap to the tent was swept back and jormundur entered along with two of the nighthawks
katrina disengaged herself from roran curtsied to jormundur and then without a word slipped out of the tent