did you arrange that eragon murmured to arya
at the top of the hill roran and katrina stood motionless before eragon while they waited for the villagers to finish singing
as the final refrain faded into oblivion eragon raised his hands and said welcome one and all
today we have come together to celebrate the union between the families of roran garrowsson and katrina ismirasdaughter
they are both of good reputation and to the best of my knowledge no one else has a claim upon their hands
if that not be the case however or if any other reason exists that they should not become man and wife then make your objections known before these witnesses that we may judge the merit of your ** eragon paused for an appropriate interval then continued
who here speaks for roran garrowsson
horst stepped forward
roran has neither father nor uncle so i horst ostrecsson speak for him as my blood
and who here speaks for katrina ismirasdaughter
birgit stepped forward
katrina has neither mother nor aunt so i birgit mardrasdaughter speak for her as my ** despite her vendetta against roran by tradition it was birgit is right and responsibility to represent katrina as she had been a close friend of katrina is mother
it is right and proper
what then does roran garrowsson bring to this marriage that both he and his wife may prosper
he brings his name said horst
he brings his hammer
he brings the strength of his hands
and he brings the promise of a farm in carvahall where they may both live in peace
astonishment rippled through the crowd as people realized what roran was doing he was declaring in the most public and binding way possible that the empire would not stop him from returning home with katrina and providing her with the life she would have had if not for galbatorix is murderous interference
roran was staking his honor as a man and a husband on the downfall of the empire
do you accept this offer birgit mardrasdaughter eragon asked
and what does katrina ismirasdaughter bring to this marriage that both she and her husband may prosper
she brings her love and devotion with which she shall serve roran garrowsson
she brings her skills at running a household
and she brings a ** surprised eragon watched as birgit motioned and two men who were standing next to nasuada came forward carrying a metal casket between them
birgit undid the clasp to the casket then lifted open the lid and showed eragon the contents
he gaped as he beheld the mound of jewelry inside
she brings with her a gold necklace studded with diamonds
she brings a brooch set with red coral from the southern sea and a pearl net to hold her hair
she brings five rings of gold and electrum
the first ring as birgit described each item she lifted it from the casket so all might see she spoke the truth
bewildered eragon glanced at nasuada and noted the pleased smile she wore
after birgit had finished her litany and closed the casket and fastened the lock again eragon asked do you accept this offer horst ostrecsson
thus your families become one in accordance with the law of the ** then for the first time eragon addressed roran and katrina directly those who speak for you have agreed upon the terms of your marriage
roran are you pleased with how horst ostrecsson has negotiated on your behalf
and katrina are you pleased with how birgit mardrasdaughter has negotiated on your behalf
roran stronghammer son of garrow do you swear then by your name and by your lineage that you shall protect and provide for katrina ismirasdaughter while you both yet live
i roran stronghammer son of garrow do swear by my name and by my lineage that i shall protect and provide for katrina ismirasdaughter while we both yet live
do you swear to uphold her honor to remain steadfast and faithful to her in the years to come and to treat her with the proper respect dignity and gentleness
i swear i shall uphold her honor remain steadfast and faithful to her in the years to come and treat her with the proper respect dignity and gentleness
and do you swear to give her the keys to your holdings such as they may be and to your strongbox where you keep your coin by sunset tomorrow so she may tend to your affairs as a wife should