between the pillars hulked statues of outlandish creatures captured forever by the sculptor is chisel
the heavy gate rumbled open before them as hidden chains slowly raised the mammoth beams
a four story high passageway extended straight toward the center of tronjheim
the top three levels were pierced by rows of archways that revealed gray tunnels curving off into the distance
clumps of people filled the arches eagerly watching eragon and saphira
on ground level however the archways were barred by stout doors
rich tapestries hung between the different levels embroidered with heroic figures and tumultuous battle scenes
a cheer rang in their ears as saphira stepped into the hall and paraded down it
eragon raised his hand eliciting another roar from the throng though many of the dwarves did not join the welcoming shout
the mile long hall ended in an arch flanked by black onyx pillars
yellow zircons three times the size of a man capped the dark columns coruscating piercing gold beams along the hall
saphira stepped through the opening then stopped and craned back her neck humming deeply in her chest
they were in a circular room perhaps a thousand feet across that reached up to tronjheim is peak a mile overhead narrowing as it rose
the walls were lined with arches one row for each level of the city mountain and the floor was made of polished carnelian upon which was etched a hammer girdled by twelve silver pentacles like on orik is helm
the room was a nexus for four hallways including the one they had just exited that divided tronjheim into quarters
the halls were identical except for the one opposite eragon
to the right and left of that hall were tall arches that opened to descending stairs which mirrored each other as they curved underground
the ceiling was capped by a dawn red star sapphire of monstrous size
the jewel was twenty yards across and nearly as thick
its face had been carved to resemble a rose in full bloom and so skilled was the craftsmanship the flower almost seemed to be real
a wide belt of lanterns wrapped around the edge of the sapphire which cast striated bands of blushing light over everything below
the flashing rays of the star within the gem made it appear as if a giant eye gazed down at them
eragon could only gape with wonder
nothing had prepared him for this
it seemed impossible that tronjheim had been built by mortal beings
the city mountain shamed everything he had seen in the empire
he doubted if even uru baen could match the wealth and grandeur displayed here
tronjheim was a stunning monument to the dwarves power and perseverance
the bald man walked in front of saphira and said you must go on foot from ** there was scattered booing from the crowd as he spoke
a dwarf took tornac and snowfire away
eragon dismounted saphira but stayed by her side as the bald man led them across the carnelian floor to the right hand hallway
they followed it for several hundred feet then entered a smaller corridor
their guards remained despite the cramped space
after four sharp turns they came to a massive cedar door stained black with age
the bald man pulled it open and conducted everyone but the guards inside