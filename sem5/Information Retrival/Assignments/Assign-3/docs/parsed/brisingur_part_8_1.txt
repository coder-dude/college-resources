he was lying on his back legs folded under at the knees stretching his thighs after running farther and with more weight than he ever had before when the loud liquid rumble erupted from his innards
the sound was so unexpected eragon bolted upright groping for his staff
wind whistled across the empty land
the sun had set and in its absence everything was blue and purple
nothing moved save for the blades of grass that fluttered and sloan whose fingers slowly opened and closed in response to some vision in his enchanted slumber
a bone biting cold heralded the arrival of true night
eragon relaxed and allowed himself a small smile
his amusement soon vanished as he considered the source of his discomfort
battling the ra zac casting numerous spells and bearing sloan upon his shoulders for most of the day had left eragon so ravenous he imagined that if he could travel back in time he could eat the entire feast the dwarves had cooked in his honor during his visit to tarnag
the memory of how the roast nagra the giant boar had smelled hot pungent seasoned with honey and spices and dripping with lard was enough to make his mouth water
the problem was he had no supplies
water was easy enough to come by he could draw moisture from the soil whenever he wanted
finding food in that desolate place however was not only far more difficult it presented him with a moral dilemma that he had hoped to avoid
oromis had devoted many of his lessons to the various climates and geographic regions that existed throughout alagaesia
thus when eragon left their camp to investigate the surrounding area he was able to identify most of the plants he encountered
few were edible and of those none were large or bountiful enough for him to gather a meal for two grown men in a reasonable amount of time
the local animals were sure to have hidden away caches of seeds and fruit but he had no idea where to begin searching for them
nor did he think it was likely that a desert mouse would have amassed more than a few mouthfuls of food
that left him with two options neither of which appealed to him
he could as he had before drain the energy from the plants and insects around their camp
the price of doing so would be to leave a death spot upon the earth a blight where nothing not even the tiny organisms in the soil still lived
and while it might keep him and sloan on their feet transfusions of energy were far from satisfying as they did nothing to fill one is stomach
eragon scowled and twisted the butt of his staff into the ground
after sharing the thoughts and desires of numerous animals it revolted him to consider eating one
nevertheless he was not about to weaken himself and perhaps allow the empire to capture him just because he went without supper in order to spare the life of a rabbit
as both saphira and roran had pointed out every living thing survived by eating something else
ours is a cruel world he thought and i cannot change how it is made
the elves may be right to avoid flesh but at the moment my need is great
i refuse to feel guilty if circumstances drive me to this
it is not a crime to enjoy some bacon or a trout or what have you
he continued to reassure himself with various arguments yet disgust at the concept still squirmed within his gut
for almost half an hour he remained rooted to the spot unable to do what logic told him was necessary
then he became aware of how late it was and swore at himself for wasting time he needed every minute of rest he could get
steeling himself eragon sent out tendrils from his mind and probed the land until he located two large lizards and curled in a sandy den a colony of rodents that reminded him of a cross between a rat a rabbit and a squirrel
deyja said eragon and killed the lizards and one of the rodents
they died instantly and without pain but he still gritted his teeth as he extinguished the bright flames of their minds
the lizards he retrieved by hand flipping over the rocks they had been hiding underneath
the rodent however he extracted from the den with magic