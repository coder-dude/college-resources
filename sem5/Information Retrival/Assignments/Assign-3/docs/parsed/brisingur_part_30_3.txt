on three occasions roran was sure the soldier was about to wound him but the man is saber twisted at the last moment and missed roran diverted by an unseen force
roran was thankful for eragon is wards then
having no other recourse roran resorted to the unexpected he stuck his head and neck out and shouted ** just as he would if he were trying to scare someone in a dark hallway
the soldier flinched and as he flinched roran leaned over and brought his hammer down on the man is left knee
the man is face went white with pain
before he could recover roran struck him in the small of his back and then as the soldier screamed and arched his spine roran ended his misery with a quick blow to the head
roran sat panting for a moment then tugged on snowfire is reins and spurred him into a canter as they returned to the convoy
his eyes darting from place to place drawn by any flicker of motion roran took stock of the battle
most of the soldiers were already dead as were the men who had been driving the wagons
by the lead wagon carn stood facing a tall man in robes the two of them rigid except for occasional twitches the only sign of their invisible duel
even as roran watched carn is opponent pitched forward and lay motionless on the ground
by the middle of the convoy however five enterprising soldiers had cut the oxen loose from three wagons and had pulled the wagons into a triangle from within which they were able to hold off martland redbeard and ten other varden
four of the soldiers poked spears between the wagons while the fifth fired arrows at the varden forcing them to retreat behind the nearest wagon for cover
the archer had already wounded several of the varden some of whom had fallen off their horses others of whom had kept their saddles long enough to find cover
roran frowned
they could not afford to linger out in the open on one of the empire is main roads while they slowly picked off the entrenched soldiers
time was against them
all the soldiers were facing west the direction from which the varden had attacked
aside from roran none of the varden had crossed to the other side of the convoy
thus the soldiers were unaware that he was bearing down on them from the east
a plan occurred to roran
in any other circumstances he would have dismissed it as ludicrous and impractical but as it was he accepted the plan as the only course of action that could resolve the standoff without further delay
he did not bother to consider the danger to himself he had abandoned all fear of death and injury the moment their charge had begun
roran urged snowfire into a full gallop
he placed his left hand on the front of his saddle edged his boots almost out of the stirrups and gathered his muscles in preparation
when snowfire was fifty feet away from the triangle of wagons he pressed downward with his hand and lifting himself placed his feet on the saddle and stood crouched on snowfire
it took all his skill and concentration to maintain his balance
as roran had expected snowfire lessened his speed and started to veer to the side as the cluster of wagons loomed large before them
roran released the reins just as snowfire turned and jumped off the horse is back leaping high over the east facing wagon of the triangle
his stomach lurched
he caught a glimpse of the archer is upturned face the soldier is eyes round and edged with white then slammed into the man and they both crashed to the ground
roran landed on top the soldier is body cushioning his fall
pushing himself onto his knees roran raised his shield and drove its rim through the gap between the soldier is helm and his tunic breaking his neck
then roran shoved himself upright
the other four soldiers were slow to react
the one to roran is left made the mistake of trying to pull his spear inside the triangle of wagons but in his haste he wedged the spear between the rear of one wagon and the front wheel of another and the shaft splintered in his hands
roran lunged toward him
the soldier tried to retreat but the wagons blocked his way
swinging the hammer in an underhand blow roran caught the soldier beneath his chin