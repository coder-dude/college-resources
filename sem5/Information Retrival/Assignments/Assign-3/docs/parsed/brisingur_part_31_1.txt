eragon was sitting in the wooden stands that the dwarves had built along the base of the outer ramparts of bregan hold
the hold sat on a rounded shoulder of thardur mountain over a mile above the floor of the mist laden valley and from it one could see for leagues in either direction or until the ridged mountains obscured the view
like tronjheim and the other dwarf cities eragon had visited bregan hold was made entirely of quarried stone in this case a reddish granite that lent a sense of warmth to the rooms and corridors within
the hold itself was a thick solid building that rose five stories to an open bell tower which was topped by a teardrop of glass that was as large around as two dwarves and was held in place by four granite ribs that joined together to form a pointed capstone
the teardrop as orik had told eragon was a larger version of the dwarves flameless lanterns and during notable occasions or emergencies it could be used to illuminate the entire valley with a golden light
the dwarves called it az sindriznarrvel or the gem of sindri
clustered around the flanks of the hold were numerous outbuildings living quarters for the servants and warriors of durgrimst ingeitum as well as other structures such as stables forges and a church devoted to morgothal the dwarves god of fire and their patron god of smiths
below the high smooth walls of bregan hold were dozens of farms scattered about clearings in the forest coils of smoke drifting up from the stone houses
all that and more orik had shown and explained to eragon after the three dwarf children had escorted eragon into the courtyard of bregan hold shouting ** to everyone within earshot
orik had greeted eragon like a brother and then had taken him to the baths and when he was clean saw to it that he was garbed in a robe of deep purple with a gold circlet for his brow
afterward orik surprised eragon by introducing him to hvedra a bright eyed apple faced dwarf woman with long hair and proudly announcing that they had been married but two days past
while eragon expressed his astonishment and congratulations orik shifted from foot to foot before replying it pained me that you were not able to attend the ceremony eragon
i had one of our spellcasters contact nasuada and i asked her if she would give you and saphira my invitation but she refused to mention it to you she feared the offer might distract you from the task at hand
i cannot blame her but i wish that this war would have allowed you to be at our wedding and us at your cousin is for we are all related now by law if not by blood
in her thick accent hvedra said please consider me as your kin now shadeslayer
so long as it is within mine power you shall always be treated as family at bregan hold and you may claim sanctuary of us whenever you need even if it is galbatorix who hunts you
eragon bowed touched by her offer
you are most ** then he asked if you do not mind my curiosity why did you and orik choose to marry now
we had planned to join hands this spring but
but orik continued in his gruff manner the urgals attacked farthen dur and then hrothgar sent me traipsing off with you to ellesmera
when i returned here and the families of the clan accepted me as their new grimstborith we thought it the perfect time to consummate our betrothal and become husband and wife
none of us may survive the year so why tarry
so you did become clan chief eragon said
aye
choosing the next leader of durgrimst ingeitum was a contentious business we were hard at it for over a week but in the end most of the families agreed that i should follow in hrothgar is footsteps and inherit his position since i was his only named heir
now eragon sat next to orik and hvedra devouring the bread and mutton the dwarves had brought him and watching the contest taking place in front of the stands
it was customary orik had said for a dwarf family if they had the gold to stage games for the entertainment of their wedding guests
hrothgar is family was so wealthy the current games had already lasted for three days and were scheduled to continue for another four