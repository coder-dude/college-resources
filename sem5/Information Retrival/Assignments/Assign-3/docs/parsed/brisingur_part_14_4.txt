how is it asked arya you could kill that man but you could not bring yourself to lay a finger on sloan she stood and faced him her gaze frank
devoid of emotion he shrugged
he was a threat
sloan was not
is not it obvious
arya was quiet for a while
it ought to be but it is not
i am ashamed to be instructed in morality by one with so much less experience
perhaps i have been too certain too confident of my own choices
eragon heard her speak but the words meant nothing to him as his gaze drifted over the corpses
is this all my life has become he wondered
a never ending series of battles i feel like a murderer
i understand how difficult this is said arya
remember eragon you have experienced only a small part of what it means to be a dragon rider
eventually this war will end and you will see that your duties encompass more than violence
the riders were not just warriors they were teachers healers and scholars
his jaw muscles knotted for a moment
why are we fighting these men arya
because they stand between us and galbatorix
then we should find a way to strike at galbatorix directly
none exist
we cannot march to uru baen until we defeat his forces
and we cannot enter his castle until we disarm almost a century is worth of traps magical and otherwise
there has to be a way he muttered
he remained where he was as arya strode forward and picked up a spear
but when she placed the tip of the spear under the chin of a slain soldier and thrust it into his skull eragon sprang toward her and pushed her away from the body
what are you doing he shouted
anger flashed across arya is face
i will forgive that only because you are distraught and not of your right mind
think ** it is too late in the day for anyone to be coddling you
why is this necessary
the answer presented itself to him and he grudgingly said if we do not the empire will notice that most of the men were killed by hand
** the only ones capable of such a feat are elves riders and kull
and since even an imbecile could figure out a kull was not responsible for this they will soon know we are in the area and in less than a day thorn and murtagh will be flying overhead searching for ** there was a wet squelch as she pulled the spear out of the body
she held it out to him until he accepted it
i find this as repulsive as you do so you might as well make yourself useful and help
eragon nodded
then arya scavenged a sword and together they set out to make it appear as if a troop of ordinary warriors had killed the soldiers
it was grisly work but it went quickly for they both knew exactly what kinds of wounds the soldiers should have to ensure the success of the deception and neither of them wished to linger
when they came to the man whose chest eragon had destroyed arya said there is little we can do to disguise an injury like that
we will have to leave it as is and hope people assume a horse stepped on ** they moved on
the last soldier they dealt with was the commander of the patrol
his mustache was now limp and torn and had lost most of its former splendor
after enlarging the pebble hole so it more closely resembled the triangular pit left by the spike of a war hammer eragon rested for a moment contemplating the commander is sad mustache then said he was right you know
i need a weapon a proper weapon
i need a ** wiping his palms on the edge of his tunic he surveyed the plain around them counting the bodies
that is it then is not it we re ** he went and collected his scattered armor rewrapped it in cloth and returned it to the bottom of his pack
then he joined arya on the low hillock she had climbed
we had best avoid the roads from now on she said
we cannot risk another encounter with galbatorix is ** indicating his deformed right hand which stained his tunic with blood she said you should tend to that before we set ** she gave him no time to respond but grasped his paralyzed fingers and said waise heill
an involuntary groan escaped him as his fingers popped back into their sockets and as his abraded tendons and crushed cartilage regained the fullness of their proper shapes and as the flaps of skin hanging from his knuckles again covered the raw flesh below