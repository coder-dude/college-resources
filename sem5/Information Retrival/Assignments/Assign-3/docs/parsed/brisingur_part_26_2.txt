and if aberon becomes too dangerous then you will go to the beor mountains and live with the dwarves
and if galbatorix strikes at the dwarves then you will go to the elves in du weldenvarden
and if galbatorix attacks du weldenvarden i will fly to the moon and raise our child among the spirits who inhabit the heavens
and they will bow down to you and make you their queen as you deserve
together they sat and watched as one by one the stars vanished from the sky obscured by the glow spreading in the east
when only the morning star remained roran said you know what this means do not you
i will just have to ensure we kill every last one of galbatorix is soldiers capture all the cities in the empire defeat murtagh and thorn and behead galbatorix and his turncoat dragon before your time comes
that way there will be no need for you to go away
she was silent for a moment then said if you could i would be very happy
they were about to return to their cot when out of the glimmering sky there sailed a miniature ship woven of dry strips of grass
the ship hovered in front of their tent rocking upon invisible waves of air and almost seemed to be looking at them with its dragon head shaped prow
like a living creature the ship darted across the path before their tent then it swooped up and around chasing an errant moth
when the moth escaped the ship glided back toward the tent stopping only inches from katrina is face
before roran could decide if he should snatch the ship out of the air it turned and flew off toward the morning star vanishing once more into the endless ocean of the sky leaving them to gaze after it in wonder