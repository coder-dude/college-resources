close upon the heels of the second impact came a third thud and with it a raw throated yell that roran recognized for he had heard it many times in his childhood
he looked up and beheld a gigantic sapphire dragon diving out of the shifting clouds
and on the dragon is back at the juncture between its neck and shoulders sat his cousin eragon
it was not the eragon he remembered but rather as if an artist had taken his cousin is base features and enhanced them streamlined them making them both more noble and more feline
this eragon was garbed like a prince in fine cloth and armor though tarnished by the grime of war and in his right hand he wielded a blade of iridescent red
this eragon roran knew could kill without hesitation
this eragon was powerful and implacable
this eragon could slay the ra zac and their mounts and help him to rescue katrina
flaring its translucent wings the dragon pulled up sharply and hung before the ship
then eragon met roran is eyes
until that moment roran had not completely believed jeod is story about eragon and brom
now as he stared at his cousin a wave of confused emotions washed over ** is a ** it seemed inconceivable that the slight moody overeager boy he grew up with had turned into this fearsome warrior
seeing him alive again filled roran with unexpected joy
yet at the same time a terrible familiar anger welled up inside him over eragon is role in garrow is death and the siege of carvahall
in those few seconds roran knew not whether he loved or hated eragon
he stiffened with alarm as a vast and alien being touched his mind
from that consciousness emanated eragon is voice roran
think your answers and i will hear them
is everyone from carvahall with you
how did you
no we can not go into it there is no time
stay where you are until the battle is decided
better yet go back farther down the river where the empire can not attack you
we have to talk eragon
you have much to answer for
eragon hesitated with a troubled expression then said i know
but not now later
with no visible prompting the dragon veered away from the ship and flew off to the east vanishing in the haze over the burning plains
in an awed voice horst said a ** a real ** i never thought i d see the day much less that it would be ** he shook his head
i guess you told us the truth eh longshanks jeod grinned in response looking like a delighted child
their words sounded muted to roran as he stared at the deck feeling like he was about to explode with tension
a host of unanswerable questions assailed him
he forced himself to ignore ** can not think about eragon now
we have to fight
the varden mustdefeat the empire
a rising tide of fury consumed him
he had experienced this before a berserk frenzy that allowed him to overcome nearly any obstacle to move objects he could not shift ordinarily to face an enemy in combat and feel no fear
it gripped him now a fever in his veins quickening his breath and setting his heart a pounding
he pushed himself off the railing ran the length of the ship to the quarterdeck where uthar stood by the wheel and said ground the ship
ground the ship i ** stay here with the rest of the soldiers and use the ballistae to wreak what havoc you can keep thedragon wing from being boarded and guard our families with your lives
understand
uthar stared at him with flat eyes and roran feared he would not accept the orders
then the scarred sailor grunted and said aye aye stronghammer
horst is heavy tread preceded his arrival at the quarterdeck
what do you intend to do roran
do roran laughed and spun widdershins to stand toe to toe with the smith
do why i intend to alter the fate of **