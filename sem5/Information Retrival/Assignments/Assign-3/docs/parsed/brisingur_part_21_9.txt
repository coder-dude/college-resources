they have dedicated themselves to collecting every piece of information in the world and preserving it against a time when they believe an unspecified catastrophe will destroy all the civilizations in alagaesia
it seems a strange religion eragon said
are not all religions strange to those who stand outside of them countered jeod
eragon said i have a gift for you as well or rather for you ** she tilted her head a quizzical frown on her face
your family was a merchant family yes she jerked her chin in an affirmative
were you very familiar with the business yourself
lightning sparked in helen is eyes
if i had not married him she motioned with a shoulder i would have taken over the family affairs when my father died
i was an only child and my father taught me everything he knew
that was what eragon had hoped to hear
to jeod he said you claimed that you are content with your lot here with the varden
i understand
however you risked a great deal to help brom and me and you risked even more to help roran and everyone else from carvahall
eragon chuckled and continued
without your assistance the empire would surely have captured them
and because of your act of rebellion you both lost all that was dear to you in teirm
we would have lost it anyway
i was bankrupt and the twins had betrayed me to the empire
it was only a matter of time before lord risthart had me arrested
maybe but you still helped roran
who can blame you if you were protecting your own necks at the same time the fact remains that you abandoned your lives in teirm in order to steal the dragon wing along with roran and the villagers
and for your sacrifice i will always be grateful
so this is part of my thanks
sliding a finger underneath his belt eragon removed the second of the three gold orbs and presented it to helen
she cradled it as gently as if it were a baby robin
while she gazed at it with wonder and jeod craned his neck to see over the edge of her hand eragon said it is not a fortune but if you are clever you should be able to make it grow
what nasuada did with lace taught me that there is a great deal of opportunity for a person to prosper in war
oh yes breathed helen
war is a merchant is delight
for one nasuada mentioned to me last night at dinner that the dwarves are running low on mead and as you might suspect they have the means to buy as many casks as they want even if the price were a thousandfold of what it was before the war
but then that is just a suggestion
you may find others who are more desperate to trade if you look for yourself
eragon staggered back a step as helen rushed at him and embraced him
her hair tickled his chin
she released him suddenly shy then her excitement burst forth again and she lifted the honey colored globe in front of her nose and said thank you ** oh thank ** she pointed at the gold
this i can use
i know i can
with it i will build an empire even larger than my father ** the shiny orb disappeared within her clenched fist
you believe my ambition exceeds my abilities it shall be as i have said
i shall not **
eragon bowed to her
i hope that you succeed and that your success benefits us all
eragon noticed that hard cords stood out in helen is neck as she curtsied and said you are most generous shadeslayer
again i thank you
yes thank you said jeod rising from the bed
i cannot think that we deserve this helen shot him a furious look which he ignored but it is most welcome nevertheless
improvising eragon added and for you jeod your gift is not from me but saphira
she has agreed to let you fly on her when you both have a spare hour or ** it pained eragon to share saphira and he knew that she would be upset he had not consulted her before volunteering her services but after giving helen the gold he would have felt guilty about not giving jeod something of equal value
a film of tears glazed jeod is eyes
he grasped eragon is hand and shook it and still holding it said i cannot imagine a higher honor
thank you
you do not know how much you have done for us
extricating himself from jeod is grip eragon edged toward the entrance to the tent while excusing himself as gracefully as he could and making his farewells