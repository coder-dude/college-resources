the foreign presence groped after his thoughts seeking to grab ahold and subdue them and subject them to murtagh is approval
as on the burning plains eragon noticed that murtagh is mind felt as if it contained multitudes as if a confused chorus of voices was murmuring beneath the turmoil of murtagh is own thoughts
eragon wondered if murtagh had a group of magicians assisting him even as the elves were him
difficult as it was eragon emptied his mind of everything but an image of zar roc
he concentrated on the sword with all his might smoothing the plane of his consciousness into the calm of meditation so murtagh would find no purchase with which to establish a foothold in eragon is being
and when thorn flailed underneath them and murtagh is attention wavered for an instant eragon launched a furious counterattack clutching at murtagh is consciousness
the two of them strove against each other in grim silence while they fell wrestling back and forth in the confines of their minds
sometimes eragon seemed to gain the upper hand sometimes murtagh but neither could defeat the other
eragon glanced at the ground rushing up at them and realized that their contest would have to be decided by other means
lowering the falchion so it was level with murtagh eragon shouted ** the same spell murtagh had used on him during their previous confrontation
it was a simple piece of magic it would do nothing more than hold murtagh is arms and torso in place but it would allow them to test themselves directly against one another and determine which of them had the most energy at their disposal
murtagh mouthed a counterspell the words lost in thorn is snarling and in the howling of the wind
eragon is pulse raced as the strength ebbed from his limbs
when he had nearly depleted his reserves and was faint from the effort saphira and the elves poured the energy from their bodies into his maintaining the spell for him
across from him murtagh had originally appeared smug and confident but as eragon continued to restrain him murtagh is scowl deepened and he pulled back his lips baring his teeth
and the whole while they besieged each other is minds
eragon felt the energy arya was funneling into him decrease once then twice and he assumed that two of the spellweavers under blodhgarm is command had fainted
murtagh can not hold out much longer he thought and then had to struggle to regain control of his mind for his lapse of concentration had granted murtagh entry
the force from arya and the other elves declined by half and even saphira began to shake with exhaustion
just as eragon became convinced murtagh would prevail murtagh uttered an anguished shout and a great weight seemed to lift off eragon as murtagh is resistance vanished
murtagh appeared astonished by eragon is success
what now eragon asked arya and saphira
do we take them as hostages can we
now said saphira i must fly
she released thorn and pushed herself away from him raising her wings and laboriously flapping as she endeavored to keep them aloft
eragon looked over her shoulder and had a brief impression of horses and sun streaked grass hurtling toward them then it was as if a giant struck him from underneath and his sight went black
with a jolt eragon sat upright in the saddle barely noticing that saphira was crouched amid a circle of king orrin is horsemen
arya was nowhere to be seen
now that he was alert again eragon could feel the spell he had cast on murtagh still draining his strength and in ever increasing amounts
if not for the aid of saphira and arya and the other elves he would have already died
eragon released the magic then looked for thorn and murtagh on the ground
there said saphira and motioned with her snout
low in the northwestern sky eragon saw thorn is glittering shape the dragon winging his way up the jiet river fleeing toward galbatorix is army some miles distant
murtagh healed thorn again and thorn was lucky enough to land on the slope of a hill
he ran down it then took off before you regained consciousness
from across the rolling landscape murtagh is magnified voice boomed do not think you have won eragon saphira