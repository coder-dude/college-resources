then islanzadi bade them a safe journey
sweeping around her red cape billowing from her shoulders she made to leave the gardens only to stop at the edge of the pleasance and say and eragon
when you meet with arya please express my affection to her and tell her that she is sorely missed in ** the words were stiff and formal
without waiting for a reply she strode away and disappeared among the shadowed boles that guarded the interior of tialdari hall followed by the elf lords and ladies
it took saphira less than a minute to fly to the sparring field where orik sat on his bulging pack tossing his war ax from one hand to the other and scowling ferociously
about time you got here he grumbled
he stood and slipped the ax back under his belt
eragon apologized for the delay then tied orik is pack onto the back of his saddle
the dwarf eyed saphira is shoulder which loomed high above him
and how by morgothal is black beard am i supposed to get up there a cliff has more handholds than you saphira
here she said
she lay flat on her belly and pushed her right hind leg out as far as she could forming a knobby ramp
pulling himself onto her shin with a loudhuff orik crawled up her leg on hands and knees
a small jet of flame burst from saphira is nostrils as she ** up that tickles
orik paused on the ledge of her haunches then placed one foot on either side of saphira is spine and carefully walked his way up her back toward the saddle
he tapped one of the ivory spikes between his legs and said there be as good a way to lose your manhood as ever i ve seen
eragon grinned
do not ** when orik lowered himself onto the front of the saddle eragon mounted saphira and sat behind the dwarf
to hold orik in place when saphira turned or inverted eragon loosened the thongs that were meant to secure his arms and had orik put his legs through them
as saphira rose to her full height orik swayed then clutched the spike in front of him
** eragon do not let me open my eyes until we re in the air else i fear i will be sick
this is unnatural it is
dwarves are not meant to ride dragons
it is never been done before
orik shook his head without answering
clusters of elves drifted out of du weldenvarden gathered along the edge of the field and with solemn expressions watched saphira lift her translucent wings in preparation to take off
eragon tightened his grip as he felt her mighty thews bunch underneath his legs
with a rush of acceleration saphira launched herself into the azure sky flapping swift and hard to rise above the giant trees
she wheeled over the vast forest spiraling upward as she gained altitude and then aimed herself south toward the hadarac desert
though the wind was loud in eragon is ears he heard an elf woman in ellesmera raise her clear voice in song as he had when they first arrived
she sang
away away you shall fly away
away away you shall fly away
and never return to me