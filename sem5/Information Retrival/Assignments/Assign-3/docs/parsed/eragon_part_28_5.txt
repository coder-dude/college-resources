my wrist is broken he said swaying
brom cursed and saddled cadoc for him
he helped eragon onto the horse and said we have to put a splint on your arm as soon as possible
try not to move your wrist until ** eragon gripped the reins tightly with his left hand
brom said to saphira it is almost dark you might as well fly right overhead
if urgals show up they will think twice about attacking with you nearby
they d better or else they wo not think again remarked saphira as she took off
the light was disappearing quickly and the horses were tired but they spurred them on without respite
eragon is wrist swollen and red continued to throb
a mile from the camp brom halted
listen he said
eragon heard the faint call of a hunting horn behind them
as it fell silent panic gripped him
they must have found where we were said brom and probably saphira is tracks
they will chase us now
it is not in their nature to let prey ** then two horns winded
they were closer
a chill ran through eragon
our only chance is to run said brom
he raised his head to the sky and his face blanked as he called saphira
she rushed out of the night sky and landed
leave cadoc
go with her
you will be safer commanded brom
i will be fine
now ** unable to muster the energy to argue eragon climbed onto saphira while brom lashed snowfire and rode away with cadoc
saphira flew after him flapping above the galloping horses
eragon clung to saphira as best he could he winced whenever her movements jostled his wrist
the horns blared nearby bringing a fresh wave of terror
brom crashed through the underbrush forcing the horses to their limits
the horns trumpeted in unison close behind him then were quiet
minutes ** are the urgals wondered eragon
a horn sounded this time in the distance
he sighed in relief resting against saphira is neck while on the ground brom slowed his headlong ** was close said eragon
yes but we cannot stop until saphira was interrupted as a horn blasted directly underneath them
eragon jerked in surprise and brom resumed his frenzied retreat
horned urgals shouting with coarse voices barreled along the trail on horses swiftly gaining ground
they were almost in sight of brom the old man could not outrun ** have to do ** exclaimed eragon
land in front of the **
** i know what i am doing said ** is not time for anything else
they re going to overtake brom
very ** pulled ahead of the urgals then turned preparing to drop onto the trail
eragon reached for his power and felt the familiar resistance in his mind that separated him from the magic
he did not try to breach it yet
a muscle twitched in his neck
as the urgals pounded up the trail he shouted ** saphira abruptly folded her wings and dropped straight down from above the trees landing on the trail in a spray of dirt and rocks
the urgals shouted with alarm and yanked on their horses reins
the animals went stiff legged and collided into each other but the urgals quickly untangled themselves to face saphira with bared weapons
hate crossed their faces as they glared at her
there were twelve of them all ugly jeering brutes
eragon wondered why they did not flee
he had thought that the sight of saphira would frighten them ** are they waiting are they going to attack us or not
he was shocked when the largest urgal advanced and spat our master wishes to speak with you ** the monster spoke in deep rolling gutturals
it is a trap warned saphira before eragon could say ** not listen to him
at least let is find out what he has to say he reasoned curious but extremely wary
who is your master he asked
the urgal sneered
his name does not deserve to be given to one as low as yourself
he rules the sky and holds dominance over the earth
you are no more than a stray ant to him
yet he has decreed that you shall be brought before him alive
take heart that you have become worthy of such **
i will never go with you nor any of my ** declared eragon thinking of yazuac
whether you serve shade urgal or some twisted fiend i ve not heard of i have no wish to parley with him
that is a grave mistake growled the urgal showing his fangs