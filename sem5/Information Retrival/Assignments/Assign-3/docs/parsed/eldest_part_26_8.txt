a foul wind rushed across the land bringing with it a sulfurous miasma that made roran cough and gag
the soldiers were likewise afflicted their curses echoed as they pressed sleeves and scarves over their noses
above them the shadows paused and then began to drift downward enclosing the camp in a dome of menacing darkness
the sickly torches flickered and threatened to extinguish themselves yet they still provided sufficient light to reveal the two beasts descending among the tents
their bodies were naked and hairless like newborn mice with leathery gray skin pulled tight across their corded chests and bellies
in form they resembled starved dogs except that their hind legs bulged with enough muscle to crush a boulder
a narrow crest extended from the back of each of their attenuated heads opposite a long straight ebony beak made for spearing prey and cold bulbous eyes identical to the ra zac is
from their shoulders and backs sprang huge wings that made the air moan under their weight
flinging themselves to the ground the soldiers cowered and hid their faces from the monsters
a terrible alien intelligence emanated from the creatures bespeaking a race far older and far more powerful than humans
roran was suddenly afraid that his mission might fail
behind him horst whispered to the men urging them to hold their ground and remain hidden else they would be slain
the ra zac bowed to the beasts then slipped into a tent and returned carrying katrina who was bound with ropes and leading sloan
the butcher walked freely
roran stared unable to comprehend how sloan had been ** house is not anywhere near horst is
then it struck him
he betrayed us said roran with wonder
his fist slowly tightened on his hammer as the true horror of the situation exploded within ** he killed byrd and he betrayed ** tears of rage streamed down his face
roran murmured horst crouching beside him
we can not attack now they d slaughter us
roran
do you hear me
he heard but a whisper in the distance as he watched the smaller ra zac jump onto one beast above the shoulders then catch katrina as the other ra zac tossed her up
sloan seemed upset and frightened now
he began arguing with the ra zac shaking his head and pointing at the ground
finally the ra zac struck him across the mouth knocking him unconscious
mounting the second beast with the butcher slung over its shoulder the largest ra zac declared we will return once it isss sssafe again
kill the boy and your livesss are ** then the steeds flexed their massive thighs and leaped into the sky once again shadows upon the field of stars
no words or emotions were left to roran
he was utterly destroyed
all that remained was to kill the soldiers
he stood and raised his hammer in preparation to charge but as he stepped forward his head throbbed in unison with his wounded shoulder the ground vanished in a burst of light and he toppled into oblivion