that evening as eragon returned to his quarters from bathing he was surprised to find a tall woman waiting for him in the hall
she had dark hair startling blue eyes and a wry mouth
wound around her wrist was a gold bracelet shaped like a hissing snake
eragon hoped that she was not there to ask him for advice like so many of the varden
he inclined his head in return
can i help you
i hope so
i am trianna sorceress of du vrangr gata
really a sorceress he asked intrigued
and battle mage and spy and anything else the varden deem necessary
there are not enough magic users so we each end up with a half dozen ** she smiled displaying even white teeth
that is why i came today
we would be honored to have you take charge of our group
you re the only one who can replace the twins
almost without realizing it he smiled back
she was so friendly and charming he hated to say no
i am afraid i can not saphira and i are leaving tronjheim soon
besides i d have to consult with nasuada first ** and i do not want to be entangled in any more politics
especially not where the twins used to lead
trianna bit her lip
i am sorry to hear ** she moved a step closer
perhaps we can spend some time together before you have to go
i could show you how to summon and control spirits
it would beeducational for both of us
eragon felt a hot flush warm his face
i appreciate the offer but i am really too busy at the moment
a spark of anger flared within trianna is eyes then vanished so quickly he wondered whether he had seen it at all
she sighed delicately
i understand
she sounded so disappointed and looked so forlorn eragon felt guilty for rebuffing ** can not hurt to talk with her for a few minutes he told himself
i am curious how did you learn magic
trianna brightened
my mother was a healer in surda
she had a bit of power and was able to instruct me in the old ways
of course i am nowhere near as powerful as a rider
none of du vrangr gata could have defeated durza alone like you did
that was a heroic deed
embarrassed eragon scuffed his boots against the ground
i would not have survived if not for arya
you are too modest argetlam she admonished
it wasyou who struck the final blow
you should be proud of your accomplishment
it is a feat worthy of vrael ** she leaned toward him
his heart quickened as he smelled her perfume which was rich and musky with a hint of an exotic spice
have you heard the songs composed about you the varden sing them every night around their fires
they say you ve come to take the throne from **
no said eragon quick and sharp
that was one rumor he would not tolerate
they might but i do not
whatever my fate may be i do not aspire to rule
and it is wise of you not to
what is a king after all but a man imprisoned by his duties that would be a poor reward indeed for the last free rider and his dragon
no for you the ability to go and do what you will and by extension to shape the future of ** she paused
do you have any family left in the empire
the question caught him off guard
he had never been asked that before
no i am not betrothed
surely there must be someone you care ** she came another step closer and her ribboned sleeve brushed his arm
i was not close to anyone in carvahall he faltered and i ve been traveling since then
trianna drew back slightly then lifted her wrist so the serpent bracelet was at eye level
do you like him she inquired
eragon blinked and nodded though it was actually rather disconcerting
i call him lorga
he is my familiar and ** bending forward she blew upon the bracelet then murmured se orum thornessa havr sharjalvi lifs
with a dry rustle the snake stirred to life
eragon watched fascinated as the creature writhed around trianna is pale arm then lifted itself and fixed its whirling ruby eyes upon him wire tongue whipping in and out
its eyes seemed to expand until they were each as large as eragon is fist
he felt as if he were tumbling into their fiery depths he could not look away no matter how hard he tried
then at a short command the serpent stiffened and resumed its former position
with a tired sigh trianna leaned against the wall