taking care not to attract undue attention roran went through the camp and gathered the villagers he trusted the most and had them accompany him to horst is tent where he said the five we agreed upon will leave now before it gets much later
horst will take my place while i am gone
remember that your most important task is to ensure clovis does not leave with the barges or damage them in any way
they may be our only means to reach surda
that and make sure we are not discovered commented orval
exactly
if none of us have returned by nightfall day after tomorrow assume we were captured
take the barges and set sail for surda but do not stop in kuasta to buy provisions the empire will probably be lying in wait there
you will have to find food elsewhere
while his companions readied themselves roran went to clovis is cabin on thered boar
just the five of you be going demanded clovis after roran explained their plan
that is ** roran let his iron gaze bore into clovis until the man fidgeted with unease
and when i get back i expect you these barges and every one of your men to still be here
you dare impugn my honor after how i ve kept our bargain
i impugn nothing only tell you what i expect
too much is at stake
if you commit treachery now you condemn our entire village to death
that i know muttered clovis avoiding his eyes
my people will defend themselves during my absence
so long as breath remains in their lungs they will not be taken tricked or abandoned
and if misfortunewere to befall them i d avenge them even if i had to walk a thousand leagues and fight galbatorix himself
heed my words master clovis for i speak the truth
we re not so fond of the empire as you seem to believe protested clovis
i would not do them a favor more than the next man
roran smiled with grim amusement
men will do anything to protect their families and homes
as roran lifted the door latch clovis asked and what will you do once you reach surda
not we you
what will you do i ve watched you roran
i ve listened to you
an you seem a good enough sort even if i do not care for how you dealt with me
but i cannot fit it in my head you dropping that hammer of yours and taking up the plow again just because you ve arrived in surda
roran gripped the latch until his knuckles turned white
when i have delivered the village to surda he said in a voice as empty as a blackened desert then i shall go hunting
ah
after that redheaded lass of yours i heard some talk of that but i did not put
the door slammed behind roran as he left the cabin
he let his anger burn hot and fast for a moment enjoying the freedom of the emotion before he began to subdue his unruly passions
he marched to felda is tent where mandel was throwing a hunting knife at a stump
felda is right someone has to talk some sense into ** you re wasting your time said roran
mandel whirled around with surprise
why do you say that
in a real fight you re more likely to put out your own eye than injure your enemy
if you do not know the exact distance between you and your target
roran shrugged
you might as well throw rocks
he watched with detached interest as the younger man bristled with pride
gunnar told me about a man he knew in cithri who could hit a flying crow with his knife eight times out of ten
and the other two times you get killed
it is usually a bad idea to throw away your weapon in ** roran waved a hand forestalling mandel is objections
get your kit together and meet me on the hill past the stream in fifteen minutes
i ve decided you should come with us to teirm
yes ** with an enthusiastic grin mandel dove into the tent and began packing
as roran left he encountered felda her youngest daughter balanced on one hip
felda glanced between him and mandel is activity in the tent and her expression tightened
keep him safe ** she set her daughter on the ground and then bustled about helping to gather the items mandel would need
roran was the first to arrive at the designated hill
he squatted on a white boulder and watched the sea while he readied himself for the task ahead
when loring gertrude birgit and nolfavrell birgit is son arrived roran jumped off the boulder and said we have to wait for mandel he will be joining us