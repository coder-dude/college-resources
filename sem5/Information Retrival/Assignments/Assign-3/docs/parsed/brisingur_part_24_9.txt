we shall meet again i promise and thorn and i shall defeat you then for we shall be even stronger than we are **
eragon clenched his shield and his falchion so tightly he bled from underneath his fingernails
do you think you can overtake him
i could but the elves would not be able to help you from so far away and i doubt we could prevail without their support
we might be able eragon stopped and pounded his leg in frustration
blast it i am an ** i forgot about aren
we could have used the energy in brom is ring to help defeat them
you had other things on your mind
anyone might have made the same mistake
maybe but i still wish i had thought of aren sooner
we could still use it to capture thorn and murtagh
and then what asked saphira
how could we keep them as prisoners would you drug them like durza drugged you in gil ead or do you just want to kill them
i do not ** we could help them to change their true names to break their oaths to galbatorix
letting them wander around unchecked though is too dangerous
arya said in theory you are right eragon but you are tired saphira is tired and i would rather thorn and murtagh escape than we lose the two of you because you were not at your best
but we do not have the capabilities to safely detain a dragon and rider for an extended period and i do not think killing thorn and murtagh would be as easy as you assume eragon
be grateful we have driven them off and rest easy knowing we can do so again when next they dare to confront us
so saying she withdrew from his mind
eragon watched until thorn and murtagh had vanished from sight then he sighed and rubbed saphira is neck
i could sleep for a fortnight
you should be proud you outflew thorn at nearly every turn
yes i did did not i she preened
it was hardly a fair competition
thorn does not have my experience
nor your talent i should think
twisting her neck she licked the upper part of his right arm the mail hauberk tinkling and then gazed down at him with sparkling eyes
he managed a ghost of a smile
i suppose i should have expected it but it still surprised me that murtagh was as fast as me
more magic on the part of galbatorix no doubt
why did your wards fail to deflect zar roc though they saved you from worse blows when we fought the ra zac
i am not sure
murtagh or galbatorix might have invented a spell i had not thought to guard against
or it could just be that zar roc is a rider is blade and as glaedr said
the swords rhunon forged excel at
cutting through enchantments of every kind and
it is only rarely they are
affected by magic
exactly
eragon stared at the streaks of dragon blood on the flat of the falchion weary
when will we be able to defeat our enemies on our own i could not have killed durza if arya had not broken the star sapphire
and we were only able to prevail over murtagh and thorn with the help of arya and twelve others
yes but how how has galbatorix amassed his strength has he found a way to feed off the bodies of his slaves even when he is hundreds of miles away ** i do not know
a runnel of sweat coursed down eragon is brow and into the corner of his right eye
he wiped off the perspiration with the palm of his hand then blinked and again noticed the horsemen gathered around him and saphira
what are they doing here looking beyond he realized saphira had landed close to where king orrin had intercepted the soldiers from the boats
not far off to her left hundreds of men urgals and horses milled about in panic and confusion
occasionally the clatter of swords or the scream of a wounded man broke through the uproar accompanied by snatches of demented laughter
i think they are here to protect us said saphira
** from what why have not they killed the soldiers yet where eragon abandoned his question as arya blodhgarm and four other haggard looking elves sprinted up to saphira from the direction of the camp
raising a hand in greeting eragon called ** what is happened no one seems to be in command
to eragon is alarm arya was breathing so hard she was unable to speak for a few moments
then the soldiers proved more dangerous than we anticipated