soon after my companions and i left the beor mountains we encountered a band of roving urgals
we were content to keep our swords in their sheaths and continue on our way but as is their wont the urgals insisted on trying to win honor and glory to better their standing within their tribes
our force was larger than theirs for weldon the man who succeeded brom as leader of the varden was with us and it was easy for us to drive them off
that day was the first time i took a life
it troubled me for weeks afterward until i realized i would go mad if i continued to dwell upon it
many do and they become so angry so grief ridden they can no longer be relied upon or their hearts turn to stone and they lose the ability to distinguish right from wrong
how did you come to terms with what you had done
i examined my reasons for killing to determine if they were just
satisfied they were i asked myself if our cause was important enough to continue supporting it even though it would probably require me to kill again
then i decided that whenever i began to think of the dead i would picture myself in the gardens of tialdari hall
brushing her hair out of her face she tucked it behind one round ear
it did
the only antidote for the corrosive poison of violence is finding peace within yourself
it is a difficult cure to obtain but well worth the ** she paused and then added breathing helps too
slow regular breathing as if you were meditating
it is one of the most effective methods for calming yourself
following her advice eragon began to consciously inhale and exhale taking care to maintain a steady tempo and to expel all the air from his lungs with each breath
within a minute the knot inside his gut loosened his frown eased and the presence of his fallen enemies no longer seemed quite so tangible
the wolves howled again and after an initial burst of trepidation he listened without fear for their baying had lost the power to unsettle him
thank you he said
arya responded with a gracious tilt of her chin
silence reigned for a quarter of an hour until eragon said ** he let the statement stand for a while a verbal monolith of ambivalence
what do you think about nasuada allowing them to join the varden
arya picked up a twig by the edge of her splayed dress and rolled it between her aquiline fingers studying the crooked piece of wood as if it contained a secret
it was a courageous decision and i admire her for it
she always acts in the best interests of the varden no matter what the cost may be
she upset many of the varden when she accepted nar garzhvog is offer of support
and she won back their loyalty with the trial of the long knives
nasuada is very clever when it comes to maintaining her ** arya flicked the twig into the fire
i have no love for urgals but neither do i hate them
unlike the ra zac they are not inherently evil merely overfond of war
it is an important distinction even if it can provide no consolation to the families of their victims
we elves have treated with urgals before and we shall again when the need arises
it is a futile prospect however
she did not have to explain why
many of the scrolls oromis had assigned eragon to read were devoted to the subject of urgals and one in particular the travels of gnaevaldrskald had taught him that the urgals entire culture was based upon feats of combat
male urgals could only improve their standing by raiding another village whether urgal human elf or dwarf mattered little or by fighting their rivals one on one sometimes to the death
and when it came to picking a mate urgal females refused to consider a ram eligible unless he had defeated at least three opponents
as a result each new generation of urgals had no choice but to challenge their peers challenge their elders and scour the land for opportunities to prove their valor
the tradition was so deeply ingrained every attempt to suppress it had failed
at least they are true to who they are mused eragon
that is more than most humans can claim
how is it he asked that durza was able to ambush you glenwing and faolin with urgals did not you have wards to protect yourself against physical attacks