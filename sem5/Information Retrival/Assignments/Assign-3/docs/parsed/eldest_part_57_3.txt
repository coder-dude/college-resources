still eragon remained convinced that otherworldly forces influenced the world in mysterious ways a belief that his exposure to the dwarves religion had bolstered
he said where do you think the world came from then if it was not created by the gods
your gods the dwarf gods our gods
someone must have created it
oromis raised an eyebrow
i would not necessarily agree with you
but be as that may i cannot prove that gods donot exist
nor can i prove that the world and everything in it was not created by an entity or entities in the distant past
but i can tell you that in the millennia we elves have studied nature we have never witnessed an instance where the rules that govern the world have been broken
that is we have never seen a miracle
many events have defied our ability to explain but we are convinced that we failed because we are still woefully ignorant about the universe and not because a deity altered the workings of nature
a god would not have to alter nature to accomplish his will asserted eragon
he could do it within the system that already exists
he could use magic to affect events
oromis smiled
very true
but ask yourself this eragon if gods exist have they been good custodians of alagaesia death sickness poverty tyranny and countless other miseries stalk the land
if this is the handiwork of divine beings then they are to be rebelled against and overthrown not given obeisance obedience and reverence
** the dwarvesbelieve
when it comes to certain matters they rely upon faith rather than reason
they have even been known to ignore proven facts that contradict their dogma
dwarf priests use coral as proof that stone is alive and can grow which also corroborates their story that helzvog formed the race of dwarves out of granite
but we elves discovered that coral is actually an exoskeleton secreted by minuscule animals that live inside the coral
any magician can sense the animals if he opens his mind
we explained this to the dwarves but they refused to listen saying that the life we felt resides in every kind of stone although their priests are the only ones who are supposed to be able to detect the life in landlocked stones
for a long time eragon stared out the window turning oromis is words over in his mind
you do not believe in an afterlife then
from what glaedr said you already knew that
and you do not put stock in gods
we give credence only to that which we can prove exists
since we cannot find evidence that gods miracles and other supernatural things are real we do not trouble ourselves about them
if that were to change if helzvog were to reveal himself to us then we would accept the new information and revise our position
it seems a cold world without something
more
on the contrary said oromis it is a better world
a place where we are responsible for our own actions where we can be kind to one another because we want to and because it is the right thing to do instead of being frightened into behaving by the threat of divine punishment
i wo not tell you what to believe eragon
it is far better to be taught to think critically and then be allowed to make your own decisions than to have someone else is notions thrust upon you
you asked after our religion and i have answered you true
make of it what you will
their discussion coupled with his previous worries left eragon so disturbed that he had difficulty concentrating on his studies in the following days even when oromis began to show him how to sing to plants which eragon had been eager to learn
eragon recognized that his own experiences had already led him to adopt a more skeptical attitude in principle he agreed with much of what oromis had said
the problem he struggled with though was that if the elves were right it meant that nearly all the humans and dwarves were deluded something eragon found difficult to ** many people can not be mistaken he insisted to himself
when he asked saphira about it she said it matters little to me eragon
dragons have never believed in higher powers
why should we when deer and other prey consider usto be a higher power he laughed at ** do not ignore reality in order to comfort yourself for once you do you make it easy for others to deceive you