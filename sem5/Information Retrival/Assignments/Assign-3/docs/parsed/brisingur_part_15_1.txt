it was late and they would have to get an early start the next morning but he made no move to retire nor did arya
she was situated at right angles to him her legs pulled up with her arms wrapped around them and her chin resting on her knees
the skirt of her dress spread outward like the wind battered petals of a flower
his chin sunk low against his chest eragon massaged his right hand with his left trying to dispel a deep seated ache
i need a sword he thought
short of that i could use some sort of protection for my hands so i do not cripple myself whenever i hit something
the problem is i am so strong now i would have to wear gloves with several inches of padding which is ridiculous
they would be too bulky too hot and what is more i can not go around with gloves on for the rest of my life
he frowned
pushing the bones of his hand out of their normal positions he studied how they altered the play of light over his skin fascinated by the malleability of his body
and what happens if i get in a fight while i am wearing brom is ring it is of elvish make so i probably do not have to worry about breaking the sapphire
but if i hit anything with the ring on my finger i wo not just dislocate a few joints i will splinter every bone in my hand
i might not even be able to repair the damage
he tightened his hands into fists and slowly turned them from side to side watching the shadows deepen and fade between his knuckles
i could invent a spell that would stop any object that was moving at a dangerous speed from touching my hands
no wait that is no good
what if it was a boulder what if it was a mountain i d kill myself trying to stop it
well if gloves and magic wo not work i d like to have a set of the dwarves ascudgamln their fists of ** with a smile he remembered how the dwarf shrrgnien had a steel spike threaded into a metal base that was embedded in each of his knuckles excluding those on his thumbs
the spikes allowed shrrgnien to hit whatever he wanted with little fear of pain and they were convenient too for he could remove them at will
the concept appealed to eragon but he was not about to start drilling holes in his knuckles
besides he thought my bones are thinner than dwarf bones too thin perhaps to attach the base and still have the joints function as they should
so ascudgamln are a bad idea but maybe instead i can
bending low over his hands he whispered thaefathan
the backs of his hands began to crawl and prickle as if he had fallen into a patch of stinging nettles
the sensation was so intense and so unpleasant he longed to jump up and scratch himself as hard as he could
with an effort of will he stayed where he was and watched as the skin on his knuckles bulged forming a flat whitish callus half an inch thick over each joint
they reminded him of the hornlike deposits that appear on the inside of horses legs
when he was pleased with the size and density of the knobs he released the flow of magic and set about exploring by touch and sight the mountainous new terrain that loomed over his fingers
his hands were heavier and stiffer than before but he could still move his fingers through their full range of motion
it may be ugly he thought rubbing the rough protuberances on his right hand against the palm of his left and people may laugh and sneer if they notice but i do not care for it will serve its purpose and may keep me alive
brimming with silent excitement he struck the top of a domed rock that rose out of the ground between his legs
the impact jarred his arm and produced a muted thud but caused him no more discomfort than it would have to punch a board covered with several layers of cloth
emboldened he retrieved brom is ring from his pack and slipped on the cool gold band checking that the adjacent callus was higher than the face of the ring
he tested his observation by again ramming his fist against the rock
the only resulting sound was that of dry compacted skin colliding with unyielding stone
what are you doing asked arya peering at him through a veil of her black hair