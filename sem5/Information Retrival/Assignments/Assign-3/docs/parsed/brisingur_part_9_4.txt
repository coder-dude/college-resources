she cast a frank gaze at nasuada is guests then curtsied to the lot of them and said yes mistress
nasuada gave fadawar a nod
we may ** then she addressed her handmaid help me out of my dress i do not want to ruin it
the older woman looked shocked by the request
here ma am in front of these
men
yes here
and be quick about it ** i should not have to argue with my own ** nasuada was harsher than she meant to be but her heart was racing and her skin was incredibly terribly sensitive the soft linen of her undergarments seemed as abrasive as canvas
patience and courtesy were beyond her now
all she could concentrate on was her upcoming ordeal
nasuada stood motionless as farica picked and pulled at the laces to her dress which extended from her shoulder blades to the base of her spine
when the cords were loose enough farica lifted nasuada is arms out of the sleeves and the shell of bunched fabric dropped in a pile around nasuada is feet leaving her standing almost naked in her white chemise
she fought back a shiver as the four warriors examined her feeling vulnerable beneath their covetous looks
ignoring them she stepped forward out of the dress and farica snatched the garment out of the dirt
across from nasuada fadawar had been busy removing the bangles from his forearms revealing the embroidered sleeves of his robes underneath
finished he lifted off his massive crown and handed it to one of his retainers
the sound of voices outside the pavilion delayed further progress
marching through the entrance a message boy jarsha was his name nasuada remembered planted himself a foot or two inside and proclaimed king orrin of surda jormundur of the varden trianna of du vrangr gata and naako and ramusewa of the inapashunna ** jarsha very pointedly kept his eyes fixed on the ceiling while he spoke
snapping about jarsha departed and the congregation he had announced entered with orrin at the vanguard
the king saw fadawar first and greeted him saying ah warlord this is unexpected
i trust you and astonishment suffused his youthful face as he beheld nasuada
why nasuada what is the meaning of this
i should like to know that as well rumbled jormundur
he gripped the hilt of his sword and glowered at anyone who dared stare at her too openly
i have summoned you here she said to witness the trial of the long knives between fadawar and myself and to afterward speak the truth of the outcome to everyone who asks
the two gray haired tribesmen naako and ramusewa appeared alarmed by her revelation they leaned close together and began to whisper
trianna crossed her arms baring the snake bracelet coiled around one slim wrist but otherwise betrayed no reaction
jormundur swore and said have you taken leave of your senses my lady this is madness
you cannot
my lady if you do i
your concern is noted but my decision is final
and i forbid anyone from ** she could tell he longed to disobey her order but as much as he wanted to shield her from harm loyalty had ever been jormundur is predominant trait
but nasuada said king orrin
this trial is not it where
blast it then why do not you give up this mad venture you would have to be addled to carry it out
i have already given my word to fadawar
the mood in the pavilion became even more somber
that she had given her word meant she could not rescind her promise without revealing herself to be an honorless oath breaker that fairminded men would have no choice but to curse and shun
orrin faltered for a moment but he persisted with his questions to what end that is if you should lose
if i should lose the varden shall no longer answer to me but to fadawar
nasuada had expected a storm of protest
instead there came a silence wherein the hot anger that animated king orrin is visage cooled and sharpened and acquired a brittle temper
i do not appreciate your choice to endanger our entire ** to fadawar he said will you not be reasonable and release nasuada from her obligation i will reward you richly if you agree to abandon this illconceived ambition of yours
i am rich already said fadawar