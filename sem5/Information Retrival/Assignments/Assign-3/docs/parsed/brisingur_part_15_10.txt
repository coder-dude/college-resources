resting on them was an exquisite ship made of green and white grass
it was no more than four inches long but so detailed eragon descried benches for rowers tiny railings along the edge of the deck and portholes the size of raspberry seeds
the curved prow was shaped somewhat like the head and neck of a rearing dragon
there was a single mast
arya leaned forward and murmured ** she gently blew upon the ship and it rose from her hands and sailed around the fire and then gathering speed slanted upward and glided off into the sparkling depths of the night sky
forever she said
it takes the energy to stay aloft from the plants below
wherever there are plants it can fly
the idea bemused eragon but he also found it rather sad to think of the pretty grass ship wandering among the clouds for the rest of eternity with none but birds for company
imagine the stories people will tell about it in years to come
arya knit her long fingers together as if to keep them from making something else
many such oddities exist in the world
the longer you live and the farther you travel the more of them you will see
eragon gazed at the pulsing fire for a while then said if it is so important to protect your true name should i cast a spell to keep galbatorix from using my true name against me
you can if you wish to said arya but i doubt it is necessary
true names are not so easy to find as you think
galbatorix does not know you well enough to guess your name and if he were inside your mind and able to examine your every thought and memory you would be already lost to him true name or no
if it is any comfort i doubt that even i could divine your true name
could not you he asked
he was both pleased and displeased that she believed any part of him was a mystery to her
she glanced at him and then lowered her eyes
no i do not think so
could you guess mine
silence enveloped their camp
above the stars gleamed cold and white
a wind sprang up from the east and raced across the plains battering the grass and wailing with a long thin voice as if lamenting the loss of a loved one
as it struck the coals burst into flame again and a twisting mane of sparks trailed off to the west
eragon hunched his shoulders and pulled the collar of his tunic close around his neck
there was something unfriendly about the wind it bit at him with unusual ferocity and it seemed to isolate him and arya from the rest of the world
they sat motionless marooned on their tiny island of light and heat while the massive river of air rushed past howling its angry sorrows into the empty expanse of land
when the gusts became more violent and began to carry the sparks farther away from the bare patch where eragon had built the fire arya poured a handful of dirt over the wood
moving forward onto his knees eragon joined her scooping the dirt with both hands to speed the process
with the fire extinguished he had difficulty seeing the countryside had become a ghost of itself full of writhing shadows indistinct shapes and silvery leaves
arya made as if to stand then stopped in a half crouch arms outstretched for balance her expression alert
eragon felt it as well the air prickled and hummed as if a bolt of lightning were about to strike
the hair on the back of his hands rose from his skin and waved freely in the wind
we are being watched
whatever happens do not use magic or you may get us killed
casting about he found a fist sized rock pried it out of the ground and hefted it testing its weight
in the distance a cluster of glowing multicolored lights appeared
they darted toward the camp flying low over the grass
as they drew near he saw that the lights were constantly changing in size ranging from an orb no larger than a pearl to one several feet in diameter and that their colors also varied cycling through every hue in the rainbow
a crackling nimbus surrounded each orb a halo of liquid tendrils that whipped and lashed as if hungry to entangle something in their grasp
the lights moved so fast he could not determine exactly how many there were but he guessed it was about two dozen