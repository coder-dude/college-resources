brom scratched a rune on parchment with charcoal then showed it to eragon
this is the lettera he said
learn it
with that eragon began the task of becoming literate
it was difficult and strange and pushed his intellect to its limits but he enjoyed it
without anything else to do and with a good if sometimes impatient teacher he advanced rapidly
a routine was soon established
every day eragon got up ate in the kitchen then went to the study for his lessons where he labored to memorize the sounds of the letters and the rules of writing
it got so that when he closed his eyes letters and words danced in his mind
he thought of little else during that time
before dinner he and brom would go behind jeod is house and spar
the servants along with a small crowd of wide eyed children would come and watch
if there was any time afterward eragon would practice magic in his room with the curtains securely closed
his only worry was saphira
he visited her every evening but it was not enough time together for either of them
during the day saphira spent most of her time leagues away searching for food she could not hunt near teirm without arousing suspicion
eragon did what he could to help her but he knew that the only solution for both her hunger and loneliness was to leave the city far behind
every day more grim news poured into teirm
arriving merchants told of horrific attacks along the coast
there were reports of powerful people disappearing from their houses in the night and their mangled corpses being discovered in the morning
eragon often heard brom and jeod discussing the events in an undertone but they always stopped when he came near
the days passed quickly and soon a week had gone by
eragon is skills were rudimentary but he could now read whole pages without asking brom is help
he read slowly but he knew that speed would come with time
brom encouraged him no matter you will do fine for what i have planned
it was afternoon when brom summoned both jeod and eragon to the study
brom gestured at eragon
now that you can help us i think it is time to move ahead
what do you have in mind asked eragon
a fierce smile danced on brom is face
jeod groaned
i know that look it is what got us into trouble in the first place
a slight exaggeration said brom but not unwarranted
very well this is what we will do
we leave tonight or tomorrow eragon told saphira from within his room
this is unexpected
will you be safe during this venture
eragon ** do not know
we may end up fleeing teirm with soldiers on our heels
he felt her worry and tried to reassure ** will be all right
brom and i can use magic and we re good fighters
he lay on the bed and stared at the ceiling
his hands shook slightly and there was a lump in his throat
as sleep overcame him he felt a wave of ** do not want to leave teirm he suddenly ** time i ve spent here has been almost normal
what i would give not to keep uprooting myself
to stay here and be like everyone else would be wonderful
then another thought raged through him but i will never be able to while saphira is around
never
dreams owned his consciousness twisting and directing it to their whims
at times he quaked with fear at others he laughed with pleasure
then something changed it was as though his eyes had been opened for the first time and a dream came to him that was clearer than any before
he saw a young woman bent over by sorrow chained in a cold hard cell
a beam of moonlight shone through a barred window set high in the wall and fell on her face
a single tear rolled down her cheek like a liquid diamond
eragon rose with a start and found himself crying uncontrollably before sinking back into a fitful sleep