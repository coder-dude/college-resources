in that area the coastline was composed of low rolling hills verdant with lush grass and occasional briars willows and poplars
the soft muddy ground gave under their feet and made walking difficult
to their right lay the glittering sea
to their left ran the purple outline of the spine
the ranks of snowcapped mountains were laced with clouds and mist
as roran is company wended past the properties surrounding teirm some freehold farms others massive estates they made every effort to go undetected
when they encountered the road that connected narda to teirm they darted across it and continued farther east toward the mountains for several more miles before turning south again
once they were confident they had circumnavigated the city they angled back toward the ocean until they found the southern road in
during his time on thered boar it had occurred to roran that officials in narda might have deduced that whoever killed the two guards was among the men who left upon clovis is barges
if so messengers would have warned teirm is soldiers to watch for anyone matching the villagers descriptions
and if the ra zac had visited narda then the soldiers would also know that they were looking not just for a handful of murderers but roran stronghammer and the refugees from carvahall
teirm could be one huge trap
yet they could not bypass the city for the villagers needed supplies and a new mode of transportation
roran had decided that their best precaution against capture was to send no one into teirm who had been seen in narda except for gertrude and himself gertrude because only she understood the ingredients for her medicines and roran because though he was the most likely to be recognized he trusted no one else to do what was required
he knew he possessed the will to act when others hesitated like the time he slew the guards
the rest of the group was chosen to minimize suspicion
loring was old but a tough fighter and an excellent liar
birgit had proven herself canny and strong and her son nolfavrell had already killed a soldier in combat despite his tender age
hopefully they would appear as nothing more than an extended family traveling ** is if mandel does not throw the scheme awry thought roran
it was also roran is idea to enter teirm from the south and thus make it seem even more unlikely that they had come from narda
evening was nigh when teirm came into view white and ghostly in the gloaming
roran stopped to inspect what lay before them
the walled city stood alone upon the edge of a large bay self contained and impregnable to any conceivable attack
torches glowed between the merlons on the battlements where soldiers with bows patrolled their endless circuits
above the walls rose a citadel and then a faceted lighthouse which swept its hazy beam across the dark waters
loring bobbed his head without taking his eyes off teirm
aye that it is
roran is attention was caught by a ship moored at one of the stone piers jutting from the city
the three masted vessel was larger than any he had seen in narda with a high forecastle two banks of oarlocks and twelve powerful ballistae mounted along each side of the deck for shooting javelins
the magnificent craft appeared equally suited for either commerce or war
even more importantly roran thought that it might might be able to hold the entire village
that is what we need he said pointing
birgit uttered a sour grunt
we d have to sell ourselves into slavery to afford passage on that monster
clovis had warned them that teirm is portcullis closed at sunset so they quickened their pace to avoid spending the night in the countryside
as they neared the pale walls the road filled with a double stream of people hurrying to and from teirm
roran had not anticipated so much traffic but he soon realized that it could help shield his party from unwanted attention
beckoning to mandel roran said drop back a ways and follow someone else through the gate so the guards do not think you re with us
we will wait for you on the other side
if they ask you ve come here seeking employment as a seaman