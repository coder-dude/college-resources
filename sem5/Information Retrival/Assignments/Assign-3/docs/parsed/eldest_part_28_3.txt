as the gloom deepened a cluster of white lights placed at every conceivable height among the trees sprang into existence a mile ahead
the sparks glowed with the silver radiance of the full moon eerie and mysterious in the night
with a faint splash a dark boat passed them from the opposite direction accompanied by a murmur of kvetha fricai from the elf steering
arya brought her canoe alongside eragon is
we will stop here tonight
they made camp a ways from ardwen lake where the ground was dry enough to sleep on
the ferocious droves of mosquitoes forced arya to cast a protective spell so that they could eat dinner in relative comfort
afterward the five of them sat around the fire staring at the gold flames
eragon leaned his head against a tree and watched a meteor streak across the sky
his eyelids were about to sink shut when a woman is voice drifted through the woods from silthrim a faint susurration that brushed the inside of his ear like a down feather
he frowned and straightened trying to better hear the tenuous whisper
like a thread of smoke that thickens as a newborn fire blazes to life so the voice rose in strength until the forest sighed with a teasing twisting melody that leaped and fell with wild abandon
more voices joined the unearthly song embroidering the original theme with a hundred variations
the air itself seemed to shimmer with the fabric of the tempestuous music
the fey strains sent jolts of elation and fear down eragon is spine they clouded his senses drawing him into the velvet night
seduced by the haunting notes he jumped to his feet ready to dash through the forest until he found the source of the voices ready to dance among the trees and moss anything so that he could join the elves revels
but before he could move arya caught his arm and yanked him around to face her
** clear your ** he struggled in a futile attempt to break her grip
eyddr eyreya ** empty your ** everything fell silent then as if he had gone deaf
he stopped fighting and looked around wondering what had just occurred
on the other side of the fire lifaen and nari wrestled noiselessly with orik
eragon watched arya is mouth move as she spoke then sound returned to the world with apop though he could no longer hear the music
what
he asked dazed
gerr off me growled orik
lifaen and nari lifted their hands and backed away
arya gazed toward silthrim
i miscounted the days i did not want to be anywhere near a city during dagshelgr
our saturnalias our celebrations are perilous for mortals
we sing in the ancient language and the lyrics weave spells of passion and longing that are difficult to resist even for us
nari stirred restlessly
we should be at a grove
we should agreed arya but we will do our duty and wait
shaken eragon sat closer to the fire wishing for saphira he was sure she could have protected his mind from the music is influence
what is the point of dagshelgr he asked
arya joined him on the ground crossing her long legs
it is to keep the forest healthy and fertile
every spring we sing for the trees we sing for the plants and we sing for the animals
without us du weldenvarden would be half its ** as if to emphasize her point birds deer squirrels red and gray striped badgers foxes rabbits wolves frogs toads tortoises and every other nearby animal forsook their hiding and began to rush madly about with a cacophony of yelps and cries
they are searching for mates explained arya
all across du weldenvarden in each of our cities elves are singing this song
the more who participate the stronger the spell and the greater du weldenvarden will be this year
eragon snatched back his hand as a trio of hedgehogs trundled past his thigh
the entire forest yammered with ** ve stepped into fairyland he thought hugging himself
orik came around the fire and raised his voice above the clamor by my beard and my ax i will not be controlled against my will by magic
if it happens again arya i swear on helzvog is stone girdle that i will return to farthen dur and you will have the wrath of durgrimst ingeitum to deal with