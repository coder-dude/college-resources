see there if you can find the weapon the werecat enticed you with
when you have satisfied your curiosity retire to the quarters of your tree house which islanzadi is servants keep in readiness for you and saphira
tomorrow we shall do what we can
but master we have so little time
and the pair of you are far too tired for any more excitement today
trust me eragon you will do better for the rest
i think the hours between will help you to digest all we have spoken of
even by the measure of kings queens and dragons this conversation of ours has been no light exchange
despite oromis is assurances eragon felt uneasy about spending the remainder of the day in leisure
his sense of urgency was so great he wanted to continue working even when he knew he ought to be recuperating
eragon shifted in his chair and by the motion he must have revealed something of his ambivalence for oromis smiled and said if it will help you relax eragon i promise you this before you and saphira leave for the varden you may pick any use of magic and in the brief while we have i will teach you everything i can concerning it
with his thumb eragon pushed his ring around his right index finger and considered oromis is offer trying to decide what of all areas of magic he would most like to learn
at last he said i would like to know how to summon spirits
a shadow passed over oromis is face
i shall keep my word eragon but sorcery is a dark and unseemly art
you should not seek to control other beings for your own gain
even if you ignore the immorality of sorcery it is an exceptionally dangerous and fiendishly complicated discipline
a magician requires at least three years of intensive study before he can hope to summon spirits and not have them possess him
sorcery is not like other magics eragon by it you attempt to force incredibly powerful and hostile beings to obey your commands beings who devote every moment of their captivity to finding a flaw in their bonds so that they can turn on you and subjugate you in revenge
throughout history never has there been a shade who was also a rider and of all the horrors that have stalked this fair land such an abomination could easily be the worst worse even than galbatorix
please choose another subject eragon one less perilous for you and for our cause
then said eragon could you teach me my true name
your requests said oromis grow ever more difficult eragon finiarel
i might be able to guess your true name if i so ** the silver haired elf studied eragon with increased intensity his eyes heavy upon him
yes i believe i could
but i will not
a true name can be of great importance magically but it is not a spell in and of itself and so it is exempt from my promise
if your desire is to better understand yourself eragon then seek to discover your true name on your own
if i gave you it you might profit thereof but you would do so without the wisdom you would otherwise acquire during the journey to find your true name
a person must earn enlightenment eragon
it is not handed down to you by others regardless of how revered they be
eragon fiddled with his ring for another moment then made a noise in his throat and shook his head
i do not know
my questions have run dry
that i very much doubt said oromis
eragon found it difficult to concentrate upon the matter at hand his thoughts kept returning to the eldunari and to brom
again eragon marveled at the strange series of events that had led brom to settle in carvahall and eventually to eragon himself becoming a dragon rider
if arya had not
eragon stopped and smiled as a thought occurred to him
will you teach me how to move an object from place to place without delay just as arya did with saphira is egg
oromis nodded
an excellent choice
the spell is costly but it has many uses
i am sure it will prove most helpful to you in your dealings with galbatorix and the empire
arya for one can attest to its effectiveness
lifting his goblet from the table oromis held it up to the sun and the radiance from above rendered the wine transparent
he studied the liquid for a long while then lowered the goblet and said before you venture into the city you should know that he whom you sent to live among us arrived here some time ago