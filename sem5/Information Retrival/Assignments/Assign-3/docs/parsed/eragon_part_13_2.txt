you must be eragon
i would offer you a job too but roran got the only one
maybe in a year or two eh
eragon smiled uneasily and shook his hand
the man was friendly
under other circumstances eragon would have liked him but right then he sourly wished that the miller had never come to carvahall
dempton huffed
good very ** he returned his attention to roran and started to explain how a mill worked
they re ready to go interrupted horst gesturing at the table where several bundles rested
you can take them whenever you want ** they shook hands then horst left the smithy beckoning to eragon on the way out
interested eragon followed
he found the smith standing in the street with his arms crossed
eragon thrust his thumb back toward the miller and asked what do you think of him
horst rumbled a good man
he will do fine with ** he absently brushed metal filings off his apron then put a massive hand on eragon is shoulder
lad do you remember the fight you had with sloan
if you re asking about payment for the meat i have not forgotten
no i trust you lad
what i wanted to know is if you still have that blue stone
eragon is heart ** does he want to know maybe someone saw ** struggling not to panic he said i do but why do you ask
as soon as you return home get rid of ** horst overrode eragon is exclamation
two men arrived here yesterday
strange fellows dressed in black and carrying swords
it made my skin crawl just to look at them
last evening they started asking people if a stone like yours had been found
they re at it again ** eragon blanched
no one with any sense said anything
they know trouble when they see it but i could name a few people who will talk
dread filled eragon is heart
whoever had sent the stone into the spine had finally tracked it down
or perhaps the empire had learned of saphira
he did not know which would be ** ** the egg is gone
it is impossible for them to find it now
but if they know what it was it will be obvious what happened
saphira might be in ** it took all of his self control to retain a casual air
thanks for telling me
do you know where they are he was proud that his voice barely trembled
i did not warn you because i thought you needed to meet those ** leave carvahall
go home
all right said eragon to placate the smith if you think i should
i ** horst is face softened
i may be overreacting but these strangers give me a bad feeling
it would be better if you stay home until they leave
i will try to keep them away from your farm though it may not do any good
eragon looked at him gratefully
he wished he could tell him about saphira
i will leave now he said and hurried back to roran
eragon clasped his cousin is arm and bade him farewell
are not you going to stay awhile roran asked with surprise
eragon almost laughed
for some reason the question struck him as funny
there is nothing for me to do and i am not going to stand around until you go
well said roran doubtfully i guess this is the last time we will see each other for a few months
i am sure it wo not seem that long said eragon hastily
take care and come back ** he hugged roran then left
horst was still in the street
aware that the smith was watching eragon headed to the outskirts of carvahall
once the smithy was out of sight he ducked behind a house and sneaked back through the village
eragon kept to the shadows as he searched each street listening for the slightest noise
his thoughts flashed to his room where his bow hung he wished that it was in his hand
he prowled across carvahall avoiding everyone until he heard a sibilant voice from around a house
although his ears were keen he had to strain to hear what was being said
when did this happen the words were smooth like oiled glass and seemed to worm their way through the air
underlying the speech was a strange hiss that made his scalp prickle
about three months ago someone else answered
eragon identified him as sloan
shade is blood he is telling them
he resolved to punch sloan the next time they met
a third person spoke
the voice was deep and moist
it conjured up images of creeping decay mold and other things best left untouched