the snow came down in great sheets blanketing the countryside in white
they only dared leave the house for firewood and to feed the animals for they feared getting lost in the howling wind and featureless landscape
they spent their time huddled over the stove as gusts rattled the heavy window shutters
days later the storm finally passed revealing an alien world of soft white drifts
i am afraid the traders may not come this year with conditions this bad said garrow
they re late as it is
we will give them a chance and wait before going to carvahall
but if they do not show soon we will have to buy any spare supplies from the ** his countenance was resigned
they grew anxious as the days crept by without sign of the traders
talk was sparse and depression hung over the house
on the eighth morning roran walked to the road and confirmed that the traders had not yet passed
the day was spent readying for the trip into carvahall scrounging with grim expressions for saleable items
that evening out of desperation eragon checked the road again
he found deep ruts cut into the snow with numerous hoofprints between them
elated he ran back to the house whooping bringing new life to their preparations
they packed their surplus produce into the wagon before sunrise
garrow put the year is money in a leather pouch that he carefully fastened to his belt
eragon set the wrapped stone between bags of grain so it would not roll when the wagon hit bumps
after a hasty breakfast they harnessed the horses and cleared a path to the road
the traders wagons had already broken the drifts which sped their progress
by noon they could see carvahall
in daylight it was a small earthy village filled with shouts and laughter
the traders had made camp in an empty field on the outskirts of town
groups of wagons tents and fires were randomly spread across it spots of color against the snow
the troubadours four tents were garishly decorated
a steady stream of people linked the camp to the village
crowds churned around a line of bright tents and booths clogging the main street
horses whinnied at the noise
the snow had been pounded flat giving it a glassy surface elsewhere bonfires had melted it
roasted hazelnuts added a rich aroma to the smells wafting around them
garrow parked the wagon and picketed the horses then drew coins from his pouch
get yourselves some treats
roran do what you want only be at horst is in time for supper
eragon bring that stone and come with ** eragon grinned at roran and pocketed the money already planning how to spend it
roran departed immediately with a determined expression on his face
garrow led eragon into the throng shouldering his way through the bustle
women were buying cloth while nearby their husbands examined a new latch hook or tool
children ran up and down the road shrieking with excitement
knives were displayed here spices there and pots were laid out in shiny rows next to leather harnesses
eragon stared at the traders curiously
they seemed less prosperous than last year
their children had a frightened wary look and their clothes were patched
the gaunt men carried swords and daggers with a new familiarity and even the women had poniards belted at their waists
what could have happened to make them like this and why are they so late wondered eragon
he remembered the traders as being full of good cheer but there was none of that now
garrow pushed down the street searching for merlock a trader who specialized in odd trinkets and pieces of jewelry
they found him behind a booth displaying brooches to a group of women
as each new piece was revealed exclamations of admiration followed
eragon guessed that more than a few purses would soon be depleted
merlock seemed to flourish and grow every time his wares were complimented
he wore a goatee held himself with ease and seemed to regard the rest of the world with slight contempt
the excited group prevented garrow and eragon from getting near the trader so they settled on a step and waited
as soon as merlock was unoccupied they hurried over
and what might you sirs want to look at asked merlock