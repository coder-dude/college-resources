after a final nearly instantaneous circuit through his body the creature withdrew
the contact between them broke like a twisted cable under too much tension
the panoply of rays outlining eragon is hand faded into oblivion leaving behind lurid pink afterimages streaked across his field of vision
again changing colors the orb in front of eragon shrank to the size of an apple and rejoined its companions in the swirling vortex of light that encircled him and arya
the humming increased to an almost unbearable pitch and then the vortex exploded outward as the blazing orbs scattered in every direction
they regrouped a hundred feet or so from the dim camp tumbling over each other like wrestling kittens then raced off to the south and disappeared as if they had never existed in the first place
the wind subsided to a gentle breeze
eragon fell to his knees arm outstretched toward where the orbs had gone feeling empty without the bliss they had given him
what he asked and then had to cough and start over again his throat was so dry
what are they
they did not look like the ones that came out of durza when i killed him
spirits can assume many different guises dictated by their whim
he blinked several times and wiped the corners of his eyes with the back of a finger
how can anyone bear to enslave them with magic it is monstrous
i would be ashamed to call myself a sorcerer
** and trianna boasts of being one
i will have her stop using spirits or i will expel her from du vrangr gata and ask nasuada to banish her from the varden
i would not be so hasty
surely you do not think it is right for magicians to force spirits to obey their will
they are so beautiful that he broke off and shook his head overcome with emotion
anyone who harms them ought to be thrashed within an inch of their life
with a hint of a smile arya said i take it oromis had yet to address the topic when you and saphira left ellesmera
if you mean spirits he mentioned them several times
but not in any great detail i dare say
in the darkness the outline of her shape moved as she leaned to one side
spirits always induce a sense of rapture when they choose to communicate with we who are made of matter but do not allow them to deceive you
they are not as benevolent content or cheerful as they would have you believe
pleasing those they interact with is their way of defending themselves
they hate to be bound in one place and they realized long ago that if the person they are dealing with is happy then he or she will be less likely to detain the spirits and keep them as servants
i do not know said eragon
they make you feel so good i can understand why someone would want to keep them nearby instead of releasing them
her shoulders rose and fell
spirits have as much difficulty predicting our behavior as we do theirs
they share so little in common with the other races of alagaesia conversing with them in even the simplest terms is a challenging prospect and any meeting is fraught with peril for one never knows how they will react
none of which explains why i should not order trianna to abandon sorcery
have you ever seen her summon spirits to do her bidding
i thought not
trianna has been with the varden for nigh on six years and in that time she has demonstrated her mastery of sorcery exactly once and that after much coaxing on ajihad is part and much consternation and preparation on trianna is
she has the necessary skills she is no charlatan but summoning spirits is exceedingly dangerous and one does not embark upon it lightly
eragon rubbed his shining palm with his left thumb
the hue of light changed as blood rushed to the surface of his skin but his efforts did nothing to reduce the amount of light radiating from his hand
he scratched at the gedwey ignasia with his fingernails
this had better not last more than a few hours
i can not go around shining like a lantern
it could get me killed
and it is silly too
whoever heard of a dragon rider with a glowing body part
eragon considered what brom had told him
they are not human spirits are they nor elf nor dwarf nor those of any other creature