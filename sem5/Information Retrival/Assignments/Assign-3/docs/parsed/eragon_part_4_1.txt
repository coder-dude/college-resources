wind howled through the night carrying a scent that would change the world
a tall shade lifted his head and sniffed the air
he looked human except for his crimson hair and maroon eyes
he blinked in surprise
the message had been correct they were here
or was it a trap he weighed the odds then said icily spread out hide behind trees and bushes
stop whoever is coming
or die
around him shuffled twelve urgals with short swords and round iron shields painted with black symbols
they resembled men with bowed legs and thick brutish arms made for crushing
a pair of twisted horns grew above their small ears
the monsters hurried into the brush grunting as they hid
soon the rustling quieted and the forest was silent again
the shade peered around a thick tree and looked up the trail
it was too dark for any human to see but for him the faint moonlight was like sunshine streaming between the trees every detail was clear and sharp to his searching gaze
he remained unnaturally quiet a long pale sword in his hand
a wire thin scratch curved down the blade
the weapon was thin enough to slip between a pair of ribs yet stout enough to hack through the hardest armor
the urgals could not see as well as the shade they groped like blind beggars fumbling with their weapons
an owl screeched cutting through the silence
no one relaxed until the bird flew past
then the monsters shivered in the cold night one snapped a twig with his heavy boot
the shade hissed in anger and the urgals shrank back motionless
he suppressed his distaste they smelled like fetid meat and turned away
they were tools nothing more
the shade forced back his impatience as the minutes became hours
the scent must have wafted far ahead of its owners
he did not let the urgals get up or warm themselves
he denied himself those luxuries too and stayed behind the tree watching the trail
another gust of wind rushed through the forest
the smell was stronger this time
excited he lifted a thin lip in a snarl
get ready he whispered his whole body vibrating
the tip of his sword moved in small circles
it had taken many plots and much pain to bring himself to this moment
it would not do to lose control now
eyes brightened under the urgals thick brows and the creatures gripped their weapons tighter
ahead of them the shade heard a clink as something hard struck a loose stone
faint smudges emerged from the darkness and advanced down the trail
three white horses with riders cantered toward the ambush their heads held high and proud their coats rippling in the moonlight like liquid silver
on the first horse was an elf with pointed ears and elegantly slanted eyebrows
his build was slim but strong like a rapier
a powerful bow was slung on his back
a sword pressed against his side opposite a quiver of arrows fletched with swan feathers
the last rider had the same fair face and angled features as the other
he carried a long spear in his right hand and a white dagger at his belt
a helm of extraordinary craftsmanship wrought with amber and gold rested on his head
between these two rode a raven haired elven lady who surveyed her surroundings with poise
framed by long black locks her deep eyes shone with a driving force
her clothes were unadorned yet her beauty was undiminished
at her side was a sword and on her back a long bow with a quiver
she carried in her lap a pouch that she frequently looked at as if to reassure herself that it was still there
one of the elves spoke quietly but the shade could not hear what was said
the lady answered with obvious authority and her guards switched places
the one wearing the helm took the lead shifting his spear to a readier grip
they passed the shade is hiding place and the first few urgals without suspicion
the shade was already savoring his victory when the wind changed direction and swept toward the elves heavy with the urgals stench
the horses snorted with alarm and tossed their heads
the riders stiffened eyes flashing from side to side then wheeled their mounts around and galloped away
the lady is horse surged forward leaving her guards far behind