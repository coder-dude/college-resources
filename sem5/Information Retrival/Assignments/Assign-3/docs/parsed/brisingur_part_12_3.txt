it made him want to resurrect the ra zac so they could endure the same suffering they had inflicted upon her and his father
every day you ask me and every day i tell you ** be patient i will recover but it will take time
the best remedy for what ails me is being with you here under the sun
it does me more good than i can tell you
that was not all i was asking
crimson spots appeared on katrina is cheeks and she tilted her head back her lips curving in a mischievous smile
my you are bold dear sir
most bold indeed
i am not sure i should be alone with you for fear you might take liberties with me
the spirit of her reply set his concern to rest
liberties eh well since you already consider me a scoundrel i might as well enjoy some of these ** and he kissed her until she broke the contact although she remained in his embrace
oh she said out of breath
you re a hard man to argue with roran stronghammer
that i ** nodding toward the tent behind her he lowered his voice and asked does elain know
she would if she were not so preoccupied with her pregnancy
i think the stress of the trip from carvahall may cause her to lose the child
she is sick a good part of the day and she has pains that
well of an unfortunate nature
gertrude has been tending her but she can not do much to ease her discomfort
all the same the sooner eragon returns the better
i am not sure how long i can keep this secret
you will do fine i am ** he released her then and tugged on the hem of his tunic to smooth out the wrinkles
how do i look
katrina studied him with a critical eye and then wet the tips of her fingers and ran them through his hair pushing it back off his forehead
spotting the knot at his collar she began to pick at it saying you ought to pay closer attention to your clothes
clothes have not been trying to kill me
well things are different now
you re the cousin of a dragon rider and you should look the part
people expect it of you
he allowed her to continue fussing with him until she was pleased with his appearance
kissing her goodbye he walked the half mile to the center of the varden is massive camp where nasuada is red command pavilion stood
the pennant mounted on the top bore a black shield and two parallel swords slanting underneath and it whipped and snapped in a warm wind from the east
the six guards outside the pavilion two humans two dwarves and two urgals lowered their weapons as roran approached and one of the urgals a thickset brute with yellow teeth challenged him saying who goes there his accent was nearly unintelligible
roran stronghammer son of garrow
nasuada sent for me
pounding his breastplate with one fist which produced a loud crash the urgal announced roran stronghammer requests an audience with you lady nightstalker
you may admit him came the answer from inside
the warriors lifted their blades and roran carefully made his way past
they watched him and he them with the detached air of men who might have to fight each other at a moment is notice
inside the pavilion roran was alarmed to see that most of the furniture was broken and overturned
the only pieces that seemed unharmed were a mirror mounted on a pole and the grand chair in which nasuada was sitting
ignoring their surroundings he knelt and bowed to her
nasuada is features and bearing were so different from those of the women roran had grown up with he was not sure how to act
she appeared strange and imperious with her embroidered dress and the gold chains in her hair and her dusky skin which at the moment had a reddish cast due to the color of the fabric walls
in stark contrast to the rest of her apparel linen bandages encased her forearms a testament to her astounding courage during the trial of the long knives
her feat had been a topic of constant discussion among the varden ever since roran had returned with katrina
it was the one aspect of her he felt as if he understood for he too would make any sacrifice in order to protect those he cared about
it just so happened that she cared about a group of thousands while he was committed to his family and his village