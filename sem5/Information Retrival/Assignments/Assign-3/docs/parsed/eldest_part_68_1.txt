eragon and saphira picked their way between the corpses that littered the burning plains moving slowly on account of their wounds and their exhaustion
they encountered other survivors staggering through the scorched battlefield hollow eyed men who looked without truly seeing their gazes focused somewhere in the distance
now that his bloodlust had subsided eragon felt nothing but sorrow
the fighting seemed so pointless to ** a tragedy that so many must die to thwart a single madman
he paused to sidestep a thicket of arrows planted in the mud and noticed the gash on saphira is tail where thorn had bitten her as well as her other ** lend me your strength i will heal you
tend to those in mortal danger first
acquiescing he bent down and mended a soldier is torn neck before moving on to one of the varden
he made no distinction between friend and foe treating both to the limit of his abilities
eragon was so preoccupied with his thoughts he paid little attention to his work
he wished he could repudiate murtagh is claim but everything murtagh had said about his mother theirmother coincided with the few things eragon knew about her selena left carvahall twenty some years ago returned once to give birth to eragon and was never seen again
his mind darted back to when he and murtagh first arrived in farthen dur
murtagh had discussed how his mother had vanished from morzan is castle while morzan was hunting brom jeod and saphira is ** morzan threw zar roc at murtagh and nearly killed him mother must have hidden her pregnancy and then gone back to carvahall in order to protect me from morzan and galbatorix
it heartened eragon to know that selena had cared for him so deeply
it also grieved him to know she was dead and they would never meet for he had nurtured the hope faint as it was that his parents might still be alive
he no longer harbored any desire to be acquainted with his father but he bitterly resented that he had been deprived of the chance to have a relationship with his mother
ever since he was old enough to understand that he was a fosterling eragon had wondered who his father was and why his mother left him to be raised by her brother garrow and his wife marian
those answers had been thrust upon him from such an unexpected source and in such an unpropitious setting it was more than he could make sense of at the moment
it would take months if not years to come to terms with the revelation
eragon always assumed he would be glad to learn the identity of his father
now that he had the knowledge revolted him
when he was younger he often entertained himself by imagining that his father was someone grand and important though eragon knew the opposite was far more likely
still it never occurred to him even in his most extravagant daydreams that he might be the son of a rider much less one of the forsworn
it turned a daydream into a nightmare
i was sired by a monster
my father was the one who betrayed the riders to ** left eragon feeling sullied
but no
as he healed a man is broken spine a new way of viewing the situation occurred to him one that restored a measure of his self confidence morzan may be my parent but he is not my father
garrow was my father
he raised me
he taught me how to live well and honorably with integrity
i am who i am because of him
even brom and oromis are more my father than morzan
and roran is my brother not murtagh
eragon nodded determined to maintain that outlook
until then he had refused to completely accept garrow as his father
and even though garrow was dead doing so relieved eragon gave him a sense of closure and helped to ameliorate his distress over morzan
wise he shook his ** i ve just learned how to think
that much at least oromis gave me
eragon wiped a layer of dirt off the face of a fallen banner boy making sure he really was dead then straightened wincing as his muscles spasmed in ** realize do not you that brom musthave known about this
why else would he choose to hide in carvahall while he waited for you to hatch
he wanted to keep an eye upon his enemy is son