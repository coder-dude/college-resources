flecks of purple light danced around the interior of the pavilion as she twisted her neck and fixed her eyes upon eragon is
little one
he pressed his lips together in a rigid line
and what of you
you know i hate to be separated from you but nasuada is arguments are well reasoned
if i can help keep murtagh and thorn away by remaining with the varden then perhaps i should
his emotions and hers washed between their minds tidal surges in a shared pool of anger anticipation reluctance and tenderness
from him flowed the anger and reluctance from her other gentler sentiments as rich in scope as his own that moderated his choleric passion and lent him perspectives he would not otherwise have
nevertheless he clung with stubborn insistence to his opposition to nasuada is scheme
if you flew me to farthen dur i would not be gone for as long meaning galbatorix would have less of an opportunity to mount a new assault
but his spies would tell him the varden were vulnerable the moment we left
i do not want to part with you again so soon after helgrind
our own desires cannot take precedence over the needs of the varden but no i do not want to part with you either
still remember what oromis said that the prowess of a dragon and rider is measured not only by how well they work together but also by how well they can function when apart
we are both mature enough to operate independently of each other eragon however much we may dislike the prospect
you proved that yourself during your trip from helgrind
would it bother you fighting with arya on your back as nasuada mentioned
her i would mind least of all
we have fought together before and it was she who ferried me across alagaesia for nigh on twenty years when i was in my egg
you know that little one
why pose this question are you jealous
an amused twinkle lit her sapphire eyes
she flicked her tongue at him
then it is very sweet of you
would you i should stay or go
it is your choice to make not mine
eragon dug at the ground with the tip of his boot
then he said if we must participate in this mad scheme we should do everything we can to help it succeed
stay and see if you can keep nasuada from losing her head over this thrice blasted plan of hers
be of good cheer little one
run fast and we shall be reunited in short order
eragon looked up at nasuada
very well he said i will go
nasuada is posture relaxed somewhat
thank you
and you saphira will you stay or go
projecting her thoughts to include nasuada as well as eragon saphira said i will stay nightstalker
nasuada inclined her head
thank you saphira
i am most grateful for your support
have you spoken to blodhgarm of this asked eragon
has he agreed to it
no i assumed you would inform him of the details
eragon doubted the elves would be pleased by the prospect of him traveling to farthen dur with only an urgal for company
he said if i might make a suggestion
you know i welcome your suggestions
that stopped him for a moment
a suggestion and a request ** nasuada lifted a finger motioning for him to continue
when the dwarves have chosen their new king or queen saphira should join me in farthen dur both to honor the dwarves new ruler and to fulfill the promise she made to king hrothgar after the battle for tronjheim
nasuada is expression sharpened into that of a hunting wildcat
what promise was this she asked
you have not told me of this before
that saphira would mend the star sapphire isidar mithrim as recompense for arya breaking it
her eyes wide with astonishment nasuada looked at saphira and said you are capable of such a feat
i am but i do not know if i will be able to summon the magic i will need when i am standing before isidar mithrim
my ability to cast spells is not subject to my own desires
at times it is as if i have gained a new sense and i can feel the pulse of energy within my own flesh and by directing it with my will i can reshape the world as i wish
the rest of my life however i can no more cast a spell than a fish can fly
if i could mend isidar mithrim though it would go a long way toward earning us the goodwill of all the dwarves not just a select few who have the breadth of knowledge to appreciate the importance of their cooperation with us