if you break the rules the penalty is death without exception
your deeds are limited by your strength the words you know and your imagination
what do you mean by words asked eragon
more ** cried brom
for a moment i had hoped you were empty of them
but you are quite right in asking
when you shot the urgals did not you say something
yes brisingr
the fire flared and a shiver ran through eragon
something about the word made him feel incredibly alive
i thought ** is from an ancient language that all living things used to speak
however it was forgotten over time and went unspoken for eons in alagaesia until the elves brought it back over the sea
they taught it to the other races who used it for making and doing powerful things
the language has a name for everything if you can find it
but what does that have to do with magic interrupted eragon
** it is the basis for all power
the language describes the true nature of things not the superficial aspects that everyone sees
for example fire is calledbrisingr
not only is thata name for fire it isthe name for fire
if you are strong enough you can usebrisingr to direct fire to do whatever you will
and that is what happened today
eragon thought about it for a moment
why was the fire blue how come it did exactly what i wanted if all i said wasfire
the color varies from person to person
it depends on who says the word
as to why the fire did what you wanted that is a matter of practice
most beginners have to spell out exactly what they want to happen
as they gain more experience it is not as necessary
a true master could just saywater and create something totally unrelated like a gemstone
you would not be able to understand how he had done it but the master would have seen the connection betweenwater and the gem and would have used that as the focal point for his power
the practice is more of an art than anything else
what you did was extremely difficult
saphira interrupted eragon is ** is a ** that is how he was able to light the fire on the plains
he does not just know about magic he can use it himself
ask him about this power but be careful of what you say
it is unwise to trifle with those who have such abilities
if he is a wizard or sorcerer who knows what his motives might have been for settling in carvahall
eragon kept that in mind as he said carefully saphira and i just realized something
you can use this magic can not you that is how you started the fire our first day on the plains
brom inclined his head slightly
i am proficient to some degree
then why did not you fight the urgals with it in fact i can think of many times when it would have been useful you could have shielded us from the storm and kept the dirt out of our eyes
after refilling his pipe brom said some simple reasons really
i am not a rider which means that even at your weakest moment you are stronger than i
and i have outlived my youth i am not as strong as i used to be
every time i reach for magic it gets a little harder
eragon dropped his eyes abashed
i am sorry
do not be said brom as he shifted his arm
it happens to everyone
where did you learn to use magic
that is one fact i will keep to myself
suffice it to say it was in a remote area and from a very good teacher
i can at the very least pass on his ** brom snuffed his pipe with a small rock
i know that you have more questions and i will answer them but they must wait until morning
he leaned forward eyes gleaming
until then i will say this to discourage any experiments magic takes just as much energy as if you used your arms and back
that is why you felt tired after destroying the urgals
and that is why i was angry
it was a dreadful risk on your part
if the magic had used more energy than was in your body it would have killed you
you should use magic only for tasks that can not be accomplished the mundane way
how do you know if a spell will use all your energy asked eragon frightened
brom raised his hands
most of the time you do not
that is why magicians have to know their limits well and even then they are cautious
once you commit to a task and release the magic you can not pull it back even if it is going to kill you