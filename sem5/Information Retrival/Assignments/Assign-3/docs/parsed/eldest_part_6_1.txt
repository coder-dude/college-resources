the council of elders beamed with triumph pleased that nasuada had done what they wanted
we do insist said jormundur for your own good and the good of the ** the rest of the elders added their expressions of support which nasuada accepted with sad smiles
sabrae threw an angry glance at eragon when he did not join in
throughout the exchange eragon watched arya for any reaction to either his news or the council is announcement
neither revelation caused her inscrutable expression to change
however saphira told him she wishes to talk with us afterward
before eragon could reply falberd turned to arya
will the elves find this agreeable
she stared at falberd until the man fidgeted under her piercing gaze then lifted an eyebrow
i cannot speak for my queen but i find nothing objectionable to it
nasuada has my blessing
how could she find it otherwise knowing what we ve told her thought eragon ** re all backed into corners
arya is remark obviously pleased the council
nasuada thanked her and asked jormundur is there anything else that must be discussed for i am weary
jormundur shook his head
we will make all the arrangements
i promise you wo not be troubled until the funeral
again thank you
would you leave me now i need time to consider how best to honor my father and serve the varden
you have given me much to ** nasuada splayed her delicate fingers on the dark cloth on her lap
umerth looked like he was going to protest at the council being dismissed but falberd waved a hand silencing him
of course whatever will give you peace
if you need help we are ready and willing to ** gesturing for the rest of them to follow he swept past arya to the door
startled eragon lowered himself back into his chair ignoring alert looks from the councilors
falberd lingered by the door suddenly reluctant to depart then slowly went out
arya was the last to go
before she closed the door she looked at eragon her eyes revealing worry and apprehension that had been concealed before
nasuada sat partially turned away from eragon and saphira
so we meet again rider
you have not greeted me
have i offended you
no nasuada i was reluctant to speak for fear of being rude or foolish
current circumstances are unkind to hasty ** paranoia that they might be eavesdropped on gripped him
reaching through the barrier in his mind he delved into the magic and intoned atra nosu waise vardo fra eld hornya
there now we may speak without being overheard by man dwarf or elf
nasuada is posture softened
thank you eragon
you do not know what a gift that ** her words were stronger and more self assured than before
behind eragon is chair saphira stirred then carefully made her way around the table to stand before nasuada
she lowered her great head until one sapphire eye met nasuada is black ones
the dragon stared at her for a full minute before snorting softly and ** her said saphira that i grieve for her and her loss
also that her strength must become the varden is when she assumes ajihad is mantle
they will need a sure guide
eragon repeated the words adding ajihad was a great man his name will always be remembered
there is something i must tell you
before ajihad died he charged me commanded me to keep the varden from falling into chaos
those were his last words
arya heard them as well
i was going to keep what he said a secret because of the implications but you have a right to know
i am not sure what ajihad meant nor exactly what he wanted but i am certain of this i will always defend the varden with my powers
i wanted you to understand that and that i ve no desire to usurp the varden is leadership
nasuada laughed brittlely
but that leadership is not to be me is it her reserve had vanished leaving behind only composure and determination
i know why you were here before me and what the council is trying to do
do you think that in the years i served my father we never planned for this eventuality i expected the council to do exactly what it did
and now everything is in place for me to take command of the varden
you have no intention of letting them rule you said eragon with wonder