the following morn eragon went looking for arya in order to apologize
he searched for over an hour without success
it seemed as if she had vanished among the many hidden nooks within ellesmera
he caught a glimpse of her once as he paused by the entrance to tialdari hall and called out to her but she slipped away before he could reach her ** is avoiding me he finally realized
as the days rolled by eragon embraced oromis is training with a zeal that the elder rider praised devoting himself to his studies in order to distract himself from thoughts of arya
night and day eragon strove to master his lessons
he memorized the words of making binding and summoning learned the true names of plants and animals and studied the perils of transmutation how to call upon the wind and the sea and the myriad skills needed to understand the forces of the world
at spells that dealt with the great energies such as light heat and magnetism he excelled for he possessed the talent to judge nigh exactly how much strength a task required and whether it would exceed that of his body
occasionally orik would come and watch standing without comment by the edge of the clearing while oromis tutored eragon or while eragon struggled alone with a particularly difficult spell
oromis set many challenges before him
he had eragon cook meals with magic in order to teach him finer control of his gramarye eragon is first attempts resulted in a blackened mess
the elf showed eragon how to detect and neutralize poisons of every sort and from then on eragon had to inspect his food for the different venoms oromis was liable to slip into it
more than once eragon went hungry when he could not find the poison or was unable to counteract it
twice he became so sick oromis had to heal him
and oromis had eragon cast multiple spells simultaneously which required tremendous concentration to keep the spells directed at their intended targets and prevent them from shifting among the items eragon wanted to affect
oromis devoted long hours to the craft of imbuing matter with energy either to be released at a later time or to give an object certain attributes
he said this is how rhunon charmed the riders swords so they never break or dull how we sing plants into growing as we desire how a trap might be set in a box only to be triggered when the box is opened how we and the dwarves make the erisdar our lanterns and how you may heal one who is injured to name but a few uses
these are the most potent of spells for they can lie dormant for a thousand years or more and are difficult to perceive or avert
they permeate much of alagaesia shaping the land and the destiny of those who live here
eragon asked you could use this technique to alter your body could not you or is that too dangerous
oromis is lips quirked in a faint smile
alas you have stumbled upon elves greatest weakness our vanity
we love beauty in all its forms and we seek to represent that ideal in our appearance
that is why we are known as the fair folk
every elf looks exactly as he or she wishes to
when elves learn the spells for growing and molding living things they often choose to modify their appearance to better reflect their personalities
a few elves have gone beyond mere aesthetic changes and altered their anatomy to adapt to various environments as you will see during the blood oath celebration
oftentimes they are more animal than elf
however transferring power to a living creature is different from transferring power to an inanimate object
very few materials are suitable for storing energy most either allow it to dissipate or become so charged with force that when you touch the object a bolt of lightning drives through you
the best materials we have found for this purpose are gemstones
quartz agates and other lesser stones are not as efficient as say a diamond but any gem will suffice
that is why riders swords always have a jewel set in their pommels
it is also why your dwarf necklace which is entirely metal must sap your strength to fuel its spell since it can hold no energy of its own