the raven is narrow shoulders and crooked neck gave him the appearance of a miser basking in the radiance of a pile of gold
the raven lifted his pallid head and uttered his ominous cry **
this is what happened
once there lived a woman linnea in the years of spice and wine before our war with the dragons and before we became as immortal as any beings still composed of vulnerable flesh can be
linnea had grown old without the comfort of a mate or children nor did she feel the need to seek them out preferring to occupy herself with the art of singing to plants of which she was a master
that is she did until a young man came to her door and beguiled her with words of love
his affections woke a part of linnea that she had never suspected existed a craving to experience the things that she had unknowingly sacrificed
the offer of a second chance was too great an opportunity for her to ignore
she deserted her work and devoted herself to the young man and for a time they were happy
but the young man was young and he began to long for a mate closer to his own age
his eye fell upon a young woman and he wooed and won her
and for a time they too were happy
when linnea discovered that she had been spurned scorned and abandoned she went mad with grief
the young man had done the worst possible thing he had given her a taste of the fullness of life then torn it away with no more thought than a rooster flitting from one hen to the next
she found him with the woman and in her fury she stabbed him to death
linnea knew that what she had done was evil
she also knew that even if she was exonerated of the murder she could not return to her previous existence
life had lost all joy for her
so she went to the oldest tree in du weldenvarden pressed herself against it and sang herself into the tree abandoning all allegiance to her own race
for three days and three nights she sang and when she finished she had become one with her beloved plants
and through all the millennia since has she kept watch over the forest
thus was the menoa tree created
at the conclusion of her tale arya and eragon sat side by side on the crest of a huge root twelve feet off the ground
eragon bounced his heels against the tree and wondered if arya had intended the story as a warning to him or if it was merely an innocent piece of history
his doubt hardened into certainty when she asked do you think that the young man was to blame for the tragedy
i think he said knowing that a clumsy reply could turn her against him that what he did was cruel
and that linnea overreacted
they were both at fault
arya stared at him until he was forced to avert his gaze
they were not suited for each other
eragon began to deny it but then stopped himself
she was right
and she had maneuvered him so that he had to say it out loud so that he had to say it toher
perhaps he admitted
silence accumulated between them like sand piling into a wall that neither of them was willing to breach
the high pitched hum of cicadas echoed from the edge of the clearing
at last he said being home seems to agree with you
it ** with unconscious ease she leaned over and picked up a thin branch that had fallen from the menoa tree and began to weave the clumps of needles into a small basket
hot blood rushed to eragon is face as he watched her
he hoped that the moon was not bright enough to reveal that his cheeks had turned mottled red
where
where do you live do you and islanzadi have a palace or castle
we live in tialdari hall our family is ancestral buildings in the western part of ellesmera
i would enjoy showing our home to you
** a practical question suddenly intruded in eragon is muddled thoughts driving away his embarrassment
arya do you have any siblings she shook her head
then you are the sole heir to the elven throne
of course
why do you ask she sounded bemused by his curiosity
i do not understand why you were allowed to become an ambassador to the varden and dwarves as well as ferry saphira is egg from here to tronjheim
it is too dangerous an errand for a princess much less the queen in waiting