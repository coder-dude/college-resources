around the tree is lowest branch coiled a morning glory with three velvety black blossoms that were clenched shut
blowing on them arya whispered open
the petals rustled as they unfurled fanning their inky robes to expose the hoard of nectar in their centers
a starburst of royal blue filled the flowers throats diffusing into the sable corolla like the vestiges of day into night
is it not the most perfect and lovely flower asked arya
eragon gazed at her exquisitely aware of how close they were and said yes
it ** before his courage deserted him he added as are you
arya fixed her eyes upon him studying him until he was forced to look away
when he dared face her again he was mortified to see her wearing a faint smile as if amused by his reaction
you are too kind she murmured
reaching up she touched the rim of a blossom and glanced from it to him
faolin created this especially for me one summer solstice long ago
he shuffled his feet and responded with a few unintelligible words hurt and offended that she did not take his compliment more seriously
he wished he could turn invisible and even considered trying to cast a spell that would allow him to do just that
in the end he drew himself upright and said please excuse us arya svit kona but it is late and we must return to our tree
her smile deepened
of course eragon
i ** she accompanied them to the main archway opened the doors for them and said good night saphira
good night eragon
despite his embarrassment eragon could not help asking will we see you tomorrow
arya tilted her head
i think i shall be busy ** then the doors closed cutting off his view of her as she returned to the main compound
crouching low on the path saphira nudged eragon in the ** daydreaming and get on my back
climbing up her left foreleg he took his usual place then clutched the neck spike in front of him as saphira rose to her full height
after a few steps how can you criticize my behavior with glaedr and then go and do something like that what were you thinking
you know how i feel about her he grumbled
** if you are my conscience and i am yours then it is my duty to tell you when you re acting like a deluded popinjay
you re not using logic like oromis keeps telling us to
what do you really expect to happen between you and arya she is a princess
she is an elf you re a human
i look more like an elf every day
eragon she is over a hundred years old
i will live as long as her or any elf
ah but you have not yet and that is the problem
you can not overcome such a vast difference
she is a grown woman with a century of experience while you re
what what am i he ** child is that what you mean
no not a child
not after what you have seen and done since we were joined
but you are young even by the reckoning of your short lived race much less by that of the dwarves dragons and elves
his retort silenced her for a minute
then i am just trying to protect you eragon
that is all
i want you to be happy and i am afraid you wo not be if you insist on pursuing arya
the two of them were about to retire when they heard the trapdoor in the vestibule bang open and the jingle of mail as someone climbed inside
zar roc in hand eragon threw back the screen door ready to confront the intruder
his hand dropped as he saw orik on the floor
the dwarf took a hearty draught from the bottle he wielded in his left hand then squinted at eragon
bricks and bones where be you ah there you shtand
i wondered where you were
could not find you so i thought that given this fine dolorous night i might go find you
and here you ** what shall we talk about you and i now that we re together in this delectable bird is nest
taking hold of the dwarf is free arm eragon pulled him upright surprised as he always was by how dense orik was like a miniature boulder
when eragon removed his support orik swayed from one side to the other achieving such precarious angles that he threatened to topple at the slightest provocation
come on in said eragon in his own language
he closed the trapdoor
you will catch cold out here
orik blinked his round deep set eyes at eragon