he struggled to digest her words then finally said will you be back soon
returning his attention to his surroundings eragon found that both the elf and the dwarf were watching him
i understand your concern
and i d still like my question answered
lifaen hesitated briefly
arya is quite young
she was born a year before the destruction of the riders
a ** he had expected such a figure eragon was still shocked
he concealed it behind a blank face thinking she could have great grandchildren older than ** he brooded on the subject for several minutes and then to distract himself said you mentioned that humans discovered alagaesia eight hundred years ago
yet brom said that we arrived three centuries after the riders were formed which was thousands of years ago
two thousand seven hundred and four years by our reckoning declared orik
brom was right if you consider a single ship with twenty warriors the arrival of humans in alagaesia
they landed in the south where surda is now
we met while they were exploring and exchanged gifts but then they departed and we did not see another human for almost two millennia or until king palancar arrived with a fleet in tow
the humans had completely forgotten us by then except for vague stories about hairy men of the mountains that preyed on children in the night
**
do you know where palancar came from asked eragon
orik frowned and gnawed the tip of his mustache then shook his head
our histories only say that his homeland was far to the south beyond the beors and that his exodus was the result of war and famine
excited by an idea eragon blurted so there might be countries elsewhere that could help us against galbatorix
possibly said orik
but they would be difficult to find even on dragonback and i doubt that you d speak the same language
who would want to help us though the varden have little to offer another country and it is hard enough to get an army from farthen dur to uru baen much less bring forces from hundreds if not thousands of miles away
we could not spare you anyway said lifaen to eragon
i still eragon broke off as saphira soared over the river followed by a furious crowd of sparrows and blackbirds intent on driving her away from their nests
at the same time a chorus of squeaks and chatters burst from the armies of squirrels hidden among the branches
lifaen beamed and cried is not she glorious see how her scales catch the ** no treasure in the world can match this ** similar exclamations floated across the river from nari
bloody unbearable that is what it is muttered orik into his beard
eragon hid a smile though he agreed with the dwarf
the elves never seemed to tire of praising saphira
nothing is wrong with a few compliments said saphira
she landed with a gigantic splash and submerged her head to escape a diving sparrow
saphira eyed him from ** that sarcasm
he chuckled and let it pass
glancing at the other boat eragon watched arya paddle her back perfectly straight her face inscrutable as she floated through webs of mottled light beneath the mossy trees
she seemed so dark and somber it made him want to comfort her
lifaen he asked softly so that orik would not hear why is arya so
unhappy you and
lifaen is shoulders stiffened underneath his russet tunic and he whispered so low that eragon could barely hear we are honored to serve arya drottningu
she has suffered more than you can imagine for our people
we celebrate out of joy for what she has achieved with saphira and we weep in our dreams for her sacrifice
and her loss
her sorrows are her own though and i cannot reveal them without her permission
as eragon sat by their nightly campfire petting a swatch of moss that felt like rabbit fur he heard a commotion deeper in the forest
exchanging glances with saphira and orik he crept toward the sound drawing zar roc
eragon stopped at the lip of a small ravine and looked across to the other side where a gyrfalcon with a broken wing thrashed in a bed of snowberries
the raptor froze when it saw him then opened its beak and uttered a piercing screech