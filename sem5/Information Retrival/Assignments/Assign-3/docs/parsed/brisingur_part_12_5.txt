i am sending out a contingent under the command of martland redbeard the earl of thun to destroy them and to do some scouting besides
if you are agreeable you will serve under martland
you will listen to and obey him and hopefully learn from him
he in turn will watch you and report to me whether he believes you are suitable for advancement
martland is very experienced and i have every confidence in his opinion
does this strike you as fair roran stronghammer
it does
only when would i leave and how long would i be gone
you would leave today and return within a fortnight
then i must ask could you wait and send me on a different expedition in a few days i would like to be here when eragon returns
your concern for your cousin is admirable but events move apace and we cannot delay
as soon as i know eragon is fate i will have one of du vrangr gata contact you with the tidings whether they be good or ill
roran rubbed his thumb along the sharp edges of his hammer as he tried to compose a reply that would convince nasuada to change her mind and yet would not betray the secret he held
at last he abandoned the task as impossible and resigned himself to revealing the truth
you re right
i am worried about eragon but of all people he can fend for himself
seeing him safe and sound is not why i want to stay
because katrina and i wish to be married and we would like eragon to perform the ceremony
there was a cascade of sharp clicks as nasuada tapped her fingernails against the arms of her chair
if you believe i will allow you to loll about when you could be helping the varden just so you and katrina can enjoy your wedding night a few days earlier then you are sorely mistaken
it is a matter of some urgency lady nightstalker
nasuada is fingers paused in midair and her eyes narrowed
how urgent
the sooner we are wed the better it will be for katrina is honor
if you understand me at all know that i would never ask favors for myself
light shifted on nasuada is skin as she tilted her head
i see
why eragon why do you want him to perform the ceremony why not someone else an elder from your village perhaps
because he is my cousin and i care for him and because he is a rider
katrina lost nearly everything on my account her home her father and her dowry
i cannot replace those things but i at least want to give her a wedding worth remembering
without gold or livestock i cannot pay for a lavish ceremony so i must find some other means besides wealth to make our wedding memorable and it seems to me nothing could be more grand than having a dragon rider marry us
nasuada held her peace for so long roran began to wonder if she expected him to leave
then it would indeed be an honor to have a dragon rider marry you but it would be a sorry day if katrina had to accept your hand without a proper dowry
the dwarves furnished me with many presents of gold and jewelry when i lived in tronjheim
some i have already sold to fund the varden but what i have left would still keep a woman clothed in mink and satin for many years to come
they shall be katrina is if you are amenable
startled roran bowed again
thank you
your generosity is overwhelming
i do not know how i can ever repay you
repay me by fighting for the varden as you fought for carvahall
i will i swear it
galbatorix will curse the day he ever sent the ra zac after me
i am sure he already does
now go
you may remain in camp until eragon returns and marries you to katrina but then i expect you to be in the saddle the following morning