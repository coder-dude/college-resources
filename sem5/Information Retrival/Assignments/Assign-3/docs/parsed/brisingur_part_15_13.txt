that is they are not ghosts
we do not become them after we die
no
and please do not ask me as i know you are about to what then they really are
it is a question for oromis to answer not me
the study of sorcery if properly conducted is long and arduous and should be approached with care
i do not want to say anything that may interfere with the lessons oromis has planned for you and i certainly do not want you to hurt yourself trying something i mentioned when you lack the proper instruction
and when am i supposed to return to ellesmera he demanded
i can not leave the varden again not like this not while thorn and murtagh are still alive
until we defeat the empire or the empire defeats us saphira and i have to support nasuada
if oromis and glaedr really want to finish our training they should join us and galbatorix be **
please eragon she said
this war shall not end as quickly as you think
the empire is large and we have but pricked its hide
as long as galbatorix does not know about oromis and glaedr we have an advantage
is it an advantage if they never make full use of themselves he grumbled
she did not answer and after a moment he felt childish for complaining
oromis and glaedr wanted more than anyone else to destroy galbatorix and if they chose to bide their time in ellesmera it was because they had excellent reasons for doing so
eragon could even name several of them if he was so inclined the most prominent being oromis is inability to cast spells that required large amounts of energy
cold eragon pulled his sleeves down over his hands and crossed his arms
what was it you said to the spirit
it was curious why we had been using magic that was what brought us to their attention
i explained and i also explained that you were the one who freed the spirits trapped inside of durza
that seemed to please them a great ** silence crept between them and then she sidled toward the lily and touched it again
** she said
they were indeed grateful
**
at her command a wash of soft light illuminated the camp
by it he saw that the leaf and stem of the lily were solid gold the petals were a whitish metal he failed to recognize and the heart of the flower as arya revealed by tilting the blossom upward appeared to have been carved out of rubies and diamonds
amazed eragon ran a finger over the curved leaf the tiny wire hairs on it tickling him
bending forward he discerned the same collection of bumps grooves pits veins and other minute details with which he had adorned the original version of the plant the only difference was they were now made of gold
it is a perfect ** he said
** concentrating he searched for the faint signs of warmth and movement that would indicate the lily was more than an inanimate object
he located them strong as they ever were in a plant during the night
fingering the leaf again he said this is beyond everything i know of magic
by all rights this lily ought to be dead
instead it is thriving
i cannot even imagine what would be involved in turning a plant into living metal
perhaps saphira could do it but she would never be able to teach the spell to anyone else
the real question said arya is whether this flower will produce seeds that are fertile
i would not be surprised if it does
numerous examples of selfperpetuating magic exist throughout alagaesia such as the floating crystal on the island of eoam and the dream well in mani is caves
this would be no more improbable than either of those phenomena
unfortunately if anyone discovers this flower or the offspring it may have they will dig them all up
every fortune hunter in the land would come here to pick the golden lilies
they will not be so easy to destroy i think but only time will tell for sure
a laugh bubbled up inside of eragon
with barely contained glee he said i ve heard the expression to gild the lily before but the spirits actually did ** they gilded the ** and he fell to laughing letting his voice boom across the empty plain
arya is lips twitched
well their intentions were noble
we cannot fault them for being ignorant of human sayings