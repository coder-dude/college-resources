what a terrible fate to be unable to fly said saphira
when arya arrived she eyed the gyrfalcon then strung her bow and with unerring aim shot it through the breast
at first eragon thought that she had done it for food but she made no move to retrieve either the bird or her arrow
with a hard expression arya unstrung her bow
it was too injured for me to heal and would have died tonight or tomorrow
such is the nature of things
i saved it hours of suffering
saphira lowered her head and touched arya on the shoulder with her snout then returned to their camp her tail scraping bark off the trees
as eragon started to follow he felt orik tug his sleeve and bent down to hear the dwarf say in an undertone never ask an elf for help they might decide that you re better off dead eh