he could see vanir standing over him with a derisive sneer
it occurred to eragon that vanir was very young
after the seizure eragon wiped the blood from his mouth with his hand and showed it to vanir asking thin enough vanir did not deign to respond but rather sheathed his sword and walked away
where are you going demanded eragon
we have unfinished business you and i
you are in no fit condition to spar scoffed the elf
try ** eragon might be inferior to the elves but he refused to give them the satisfaction of fulfilling their low expectations of him
he would earn their respect through sheer persistence if nothing else
he insisted on completing oromis is assigned hour after which saphira marched up to vanir and touched him on the chest with the point of one of her ivory ** she said
vanir paled
the other elves edged away from him
once they were in the air saphira said oromis was right
you give more of yourself when you have an opponent
at oromis is hut the day resumed its usual pattern saphira accompanied glaedr for her instruction while eragon remained with oromis
eragon was horrified when he discovered that oromis expected him to do the rimgar in addition to his earlier exercises
it took all of his courage to obey
his apprehension proved groundless though for the dance of snake and crane was too gentle to injure him
that coupled with his meditation in the secluded glade provided eragon with his first opportunity since the previous day to order his thoughts and consider the question that oromis had posed him
while he did he observed his red ants invade a smaller rival anthill overrunning the inhabitants and stealing their resources
by the end of the massacre only a handful of the rival ants were left alive alone and purposeless in the vast and hostile pine needle barrens
like the dragons in alagaesia thought eragon
his connection to the ants vanished as he considered the dragons unhappy fate
bit by bit an answer to his problem revealed itself to him an answer that he could live with and believe in
he finished his meditations and returned to the hut
this time oromis seemed reasonably satisfied with what eragon had accomplished
as oromis served the midday meal eragon said i know why fighting galbatorix is worth it though thousands of people may die
oh oromis seated himself
do tell me
because galbatorix has already caused more suffering over the past hundred years than we ever could in a single generation
and unlike a normal tyrant we cannot wait for him to die
he could rule for centuries or millennia persecuting and tormenting people the entire time unless we stop him
if he became strong enough he would march on the dwarves and you here in du weldenvarden and kill or enslave both races
and
eragon rubbed the heel of his palm against the edge of the table
because rescuing the two eggs from galbatorix is the only way to save the dragons
the strident warble of oromis is teakettle intruded escalating in volume until eragon is ears rang
standing oromis hooked the kettle off the cookfire and poured the water for blueberry tea
the creases around his eyes softened
now he said you understand
i understand but i take no pleasure in it
nor should you
but now we can be confident that you wo not shrink from the path when you are confronted by the injustices and atrocities that the varden will inevitably commit
we cannot afford to have you consumed by doubts when your strength and focus are most ** oromis steepled his fingers and gazed into the dark mirror of his tea contemplating whatever he saw in its tenebrous reflection
do you believe that galbatorix is evil
do you believe that he considers himself evil
oromis tapped his forefingers against each other
then you must also believe that durza was evil
the fragmented memories eragon had gleaned from durza when they fought in tronjheim returned to him now reminding him how the young shade carsaib then had been enslaved by the wraiths he had summoned to avenge the death of his mentor haeg
he was not evil himself but the spirits that controlled him were