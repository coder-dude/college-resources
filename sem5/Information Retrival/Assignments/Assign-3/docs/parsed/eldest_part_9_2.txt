upon his head was a helm strewn with precious stones
his hands were clasped beneath his collarbone over the ivory hilt of his bare sword which extended from underneath the shield covering his chest and legs
silver mail like circlets of moonbeams weighed down his limbs and fell onto the bier
close behind the body stood nasuada grave sable cloaked and strong in stature though tears adorned her countenance
to the side was hrothgar in dark robes then arya the council of elders all with suitably remorseful expressions and finally a stream of mourners that extended a mile from tronjheim
every door and archway of the four story high hall that led to the central chamber of tronjheim half a mile away was thrown open and crowded with humans and dwarves alike
between the gray bands of faces the long tapestries swayed as they were brushed with hundreds of sighs and whispers when saphira and eragon came into view
jormundur beckoned for them to join him
trying not to disturb the formation eragon and saphira picked through the column to the space by his side earning a disapproving glare from sabrae
orik went to stand behind hrothgar
together they waited though for what eragon knew not
all the lanterns were shuttered halfway so that a cool twilight suffused the air lending an ethereal feel to the event
no one seemed to move or breathe for a brief moment eragon fancied that they were all statues frozen for eternity
a single plume of incense drifted from the bier winding toward the hazy ceiling as it spread the scent of cedar and juniper
it was the only motion in the hall a whiplash line undulating sinuously from side to side
deep in tronjheim a drum **
the sonorous bass note resonated through their bones vibrating the city mountain and causing it to echo like a great stone bell
** the second note another lower drum melded with the first each beat rolling inexorably through the hall
the force of the sound propelled them along at a majestic pace
it gave each step significance a purpose and gravity suited to the occasion
no thought could exist in the throbbing that surrounded them only an upwelling of emotion that the drums expertly beguiled summoning tears and bittersweet joy at the same time
when the tunnel ended ajihad is bearers paused between the onyx pillars before gliding into the central chamber
there eragon saw the dwarves grow even more solemn upon beholding isidar mithrim
they walked through a crystal graveyard
a circle of towering shards lay in the center of the great chamber surrounding the inlaid hammer and pentacles
many pieces were larger than saphira
the rays of the star sapphire still shimmered in the fragments and on some petals of the carved rose were visible
the bearers continued forward between the countless razor edges
then the procession turned and descended broad flights of stairs to the tunnels below
through many caverns they marched passing stone huts where dwarven children clutched their mothers and stared with wide eyes
and with that final crescendo they halted under ribbed stalactites that branched over a great catacomb lined with alcoves
in each alcove lay a tomb carved with a name and clan crest
thousands hundreds of thousands were buried here
the only light came from sparsely placed red lanterns pale in the shadows
after a moment the bearers strode to a small room annexed to the main chamber
in the center on a raised platform was a great crypt open to waiting darkness
on the top was carved in runes
may all knurlan humans and elves
for he was noble strong and wise
when the mourners were gathered around ajihad was lowered into the crypt and those who had known him personally were allowed to approach
eragon and saphira were fifth in line behind arya
as they ascended the marble steps to view the body eragon was gripped by an overwhelming sense of sorrow his anguish compounded by the fact that he considered this as much murtagh is funeral as ajihad is
stopping alongside the tomb eragon gazed down at ajihad
he appeared far more calm and tranquil than he ever did in life as if death had recognized his greatness and honored him by removing all traces of his worldly cares