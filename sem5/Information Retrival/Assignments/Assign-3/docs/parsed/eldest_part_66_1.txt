eragon barely noticed as saphira carried him back into the swirling confusion of the battle
he had known that roran was at sea but it never occurred to him that roran might be heading for surda nor that they would reunite in this manner
and roran is ** his eyes seemed to bore into eragon questioning relieved enraged
accusing
in them eragon saw that his cousin had learned of eragon is role in garrow is death and had not yet forgiven him
it was only when a sword bounced off his greaves that eragon returned his attention to his surroundings
he unleashed a hoarse shout and slashed downward cutting away the soldier who struck him
cursing himself for being so careless eragon reached out to trianna and said no one on that ship is an enemy
spread the word that they re not to be attacked
ask nasuada if as a favor to us she can send a herald to explain the situation to them and see that they stay away from the fighting
from the western flank of the battle where she alighted saphira traversed the burning plains in a few giant leaps stopping before hrothgar and his dwarves
dismounting eragon went to the king who said hail ** hail ** the elves seem to have done more for you than they ** beside him stood orik
no sir it was the dragons
really i must hear your adventures once our bloody work here is done
i am glad you accepted my offer to become durgrimst ingeitum
it is an honor to have you as mine kin
hrothgar laughed then turned to saphira and said i still have not forgotten your vow to mend isidar mithrim dragon
even now our artisans are assembling the star sapphire in the center of tronjheim
i look forward to seeing it whole once again
she bowed her ** i promised so it shall be
after eragon repeated her words hrothgar reached out with a gnarled finger and tapped one of the metal plates on her side
i see you wear our armor
i hope it has served you well
very well king hrothgar said saphira through ** has saved me many an injury
hrothgar straightened and lifted volund a twinkle in his deep set eyes
well then shall we march out and test it once again in the forge of war he looked back at his warriors and shouted akh sartos oen **
vor hrothgarz ** vor hrothgarz **
eragon looked at orik who translated with a mighty yell by hrothgar is ** joining the chant eragon ran with the dwarf king toward the crimson ranks of soldiers saphira by his side
now at last with the help of the dwarves the battle turned in favor of the varden
together they pushed back the empire dividing them crushing them forcing galbatorix is vast army to abandon positions they had held since morn
their efforts were helped by the fact that more of angela is poisons had taken effect
many of the empire is officers behaved irrationally giving orders that made it easier for the varden to penetrate deeper into the army sowing chaos as they went
the soldiers seemed to realize that fortune no longer smiled upon them for hundreds surrendered or defected outright and turned on their former comrades or threw down their weapons and fled
and the day passed into the late afternoon
eragon was in the midst of fighting two soldiers when a flaming javelin roared past overhead and buried itself in one of the empire is command tents twenty yards away igniting the fabric
dispatching his opponents eragon glanced back and saw dozens of fiery missiles arcing out from the ship on the jiet ** are you playing at roran wondered eragon before charging the next batch of soldiers
soon afterward a horn echoed from the rear of the empire is army then another and another
someone began to pound a sonorous drum the peals of which stilled the field as everyone looked about for the source of the beat
even as eragon watched an ominous figure detached itself from the horizon in the north and rose up in the lurid sky over the burning plains
the gore crows scattered before the barbed black shadow which balanced motionless upon the thermals
at first eragon thought it a lethrblaka one of the ra zac is mounts
then a ray of light escaped the clouds and struck the figure crossways from the west