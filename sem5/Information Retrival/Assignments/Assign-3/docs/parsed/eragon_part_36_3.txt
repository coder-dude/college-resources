it was linked to him until the task was complete or he was dead
all he could do was remain motionless growing weaker every moment
just as he became convinced that he would die kneeling there the dirt shimmered and morphed into a thimbleful of water
relieved eragon sat back breathing hard
his heart pounded painfully and hunger gnawed at his innards
eragon shook his head still in shock from the drain on his body is reserves
he was glad that he had not tried to transmute anything **
this wo not work he ** do not even have the strength to give myself a drink
you should have been more careful she ** can yield unexpected results when the ancient words are combined in new ways
he glared at ** know that but this was the only way i could test my idea
i was not going to wait until we were in the ** he reminded himself that she was only trying to ** did you turn brom is grave into diamond without killing yourself i can barely handle a bit of dirt much less all that sandstone
i do not know how i did it she stated ** just happened
could you do it again but this time make water
eragon she said looking him squarely in the ** ve no more control over my abilities than a spider does
things like that occur whether i will them or not
brom told you that unusual events happen around dragons
he spoke truly
he gave no explanation for it nor do i have one
sometimes i can work changes just by feel almost without thought
the rest of the time like right now i am as powerless as snowfire
you re never powerless he said softly putting a hand on her neck
for a long period they were both quiet
eragon remembered the grave he had made and how brom lay within it
he could still see the sandstone flowing over the old man is face
at least we gave him a decent burial he whispered
he idly swirled a finger in the dirt making twisting ridges
two of the ridges formed a miniature valley so he added mountains around it
with his fingernail he scratched a river down the valley then deepened it because it seemed too shallow
he added a few more details until he found himself staring at a passable reproduction of palancar valley
homesickness welled up within him and he obliterated the valley with a swipe of his hand
i do not want to talk about it he muttered angrily staving off saphira is questions
he crossed his arms and glared at the ground
almost against his will his eyes flicked back to where he had gouged the earth
he straightened surprised
though the ground was dry the furrow he had made was lined with moisture
curious he scraped away more dirt and found a damp layer a few inches under the surface
look at ** he said excitedly
saphira lowered her nose to his ** does this help us water in the desert is sure to be buried so deeply we would have to dig for weeks to find it
yes said eragon delightedly but as long as it is there i can get it
** he deepened the hole then mentally accessed the magic
instead of changing the dirt into water he simply summoned forth the moisture that was already in the earth
with a faint trickle water rushed into the hole
he smiled and sipped from it
the liquid was cool and pure perfect for ** we can get all we need
saphira sniffed the ** yes
but in the desert there may not be enough water in the ground for you to bring to the surface
it will work eragon assured ** i am doing is lifting the water an easy enough task
as long as it is done slowly my strength will hold
even if i have to draw the water from fifty paces down it wo not be a problem
especially if you help me
saphira looked at him ** you sure think carefully upon your answer for it will mean our lives if you are wrong
eragon hesitated then said firmly i am sure
then go tell murtagh
i will keep watch while you sleep
but you ve stayed up all night like us he ** should rest
i will be fine i am stronger than you know she said gently
her scales rustled as she curled up with a watchful eye turned northward toward their pursuers
eragon hugged her and she hummed deeply sides **
he lingered then reluctantly returned to murtagh who asked well is the desert open to us