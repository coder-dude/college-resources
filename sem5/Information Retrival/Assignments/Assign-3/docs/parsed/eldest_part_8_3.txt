in short bursts he reiterated all they had seen and its possible implications most importantly that it was now clear the strangers were agents of the empire
horst fingered his beard
you have to leave carvahall
fetch some food from the house then take my mare ivor is pulling stumps with her and ride into the foothills
once we know what the soldiers want i will send albriech or baldor with word
what will you say if they ask for me
that you re out hunting and we do not know when you will return
it is true enough and i doubt they will chance blundering around in the trees for fear of missing you
assuming it is you they re really after
roran nodded then turned and ran to horst is house
inside he grabbed the mare is tack and bags from the wall quickly tied turnips beets jerky and a loaf of bread in a knot of blankets snatched up a tin pot and dashed out pausing only long enough to explain the situation to elain
the supplies were an awkward bundle in his arms as he jogged east from carvahall to ivor is farm
ivor himself stood behind the farmhouse flicking the mare with a willow wand as she strained to tear the hairy roots of an elm tree from the ground
come on ** shouted the farmer
put your back into ** the horse shuddered with effort her bit lathered then with a final surge tilted the stump on its side so the roots reached toward the sky like a cluster of gnarled fingers
ivor stopped her exertion with a twitch of the reins and patted her good naturedly
all right
there we go
roran hailed him from a distance and when they were close pointed to the horse
i need to borrow ** he gave his reasons
ivor swore and began unhitching the mare grumbling always the moment i get a bit of work done that is when the interruption comes
never ** he crossed his arms and frowned as roran cinched the saddle intent on his work
when he was ready roran swung onto the horse bow in hand
i am sorry for the trouble but it can not be helped
well do not worry about it
just make sure you are not caught
as he set heels to the mare is sides roran heard ivor call and do not be hiding up my **
roran grinned and shook his head bending low over the horse is neck
he soon reached the foothills of the spine and worked his way up to the mountains that formed the north end of palancar valley
from there he climbed to a point on the mountainside where he could observe carvahall without being seen
then he picketed his steed and settled down to wait
roran shivered eyeing the dark pines
he disliked being this close to the spine
hardly anyone from carvahall dared set foot in the mountain range and those who did often failed to return
before long roran saw the soldiers march up the road in a double line two ominous black figures at their head
they were stopped at the edge of carvahall by a ragged group of men some of them with picks in hand
the two sides spoke then simply faced each other like growling dogs waiting to see who would strike first
after a long moment the men of carvahall moved aside and let the intruders pass
what happens now wondered roran rocking back on his heels
by evening the soldiers had set up camp in a field adjacent to the village
their tents formed a low gray block that flickered with weird shadows as sentries patrolled the perimeter
in the center of the block a large fire sent billows of smoke into the air
roran had made his own camp and now he simply watched and thought
he always assumed that when the strangers destroyed his home they got what they wanted which was the stone eragon brought from the ** must not have found it he ** eragon managed to escape with the stone
perhaps he felt that he had to leave in order to protect it
he frowned
that would go a long way toward explaining why eragon fled but it still seemed far fetched to ** the reason that stone must be a fantastic treasure for the king to send so many men to retrieve it
i can not understand what would make it so valuable
maybe it is magic
he breathed deeply of the cool air listening to the hoot of an owl
a flicker of movement caught his attention