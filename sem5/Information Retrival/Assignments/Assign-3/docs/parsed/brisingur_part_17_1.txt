beckoning to eragon and arya nasuada placed her left hand on king orrin is arm and with him entered the pavilion
what about you eragon asked saphira as he followed
then he stepped inside the pavilion and saw that a panel at the back had been rolled up and tied to the wooden frame above so that saphira might insert her head and participate in the goings on
he had to wait but a moment before her glittering head and neck swung into view around the edge of the opening darkening the interior as she settled into place
purple flecks of light adorned the walls projected by her blue scales onto the red fabric
eragon examined the rest of the tent
it was barren compared with when he had last visited a result of the destruction saphira had caused when she crawled into the pavilion to see eragon in nasuada is mirror
with only four pieces of furniture the tent was austere even by military standards
there was the polished high backed chair where nasuada was sitting king orrin standing next to her the selfsame mirror which was mounted at eye level on a carved brass pole a folding chair and a low table strewn with maps and other documents of import
an intricately knotted dwarf rug covered the ground
besides arya and himself a score of people were already gathered before nasuada
they were all looking at him
among them he recognized narheim the current commander of the dwarf troops trianna and other spellcasters from du vrangr gata sabrae umerth and the rest of the council of elders save for jormundur and a random assortment of nobles and functionaries from king orrin is court
those who were strangers to him he assumed also held positions of distinction in one of the many factions that made up the varden is army
six of nasuada is guards were present two stationed by the entrance and four behind nasuada and eragon detected the convoluted pattern of elva is dark and twisted thoughts from where the witch child was hidden at the far end of the pavilion
eragon said nasuada you have not met before but let me introduce sagabato no inapashunna fadawar chief of the inapashunna tribe
he is a brave man
for the next hour eragon endured what seemed like an endless procession of introductions congratulations and questions that he could not answer forthrightly without revealing secrets that were better left unsaid
when all of the guests had conversed with him nasuada bade them take their leave
as they filed out of the pavilion she clapped her hands and the guards outside ushered in a second group and then when the second group had enjoyed the dubious fruits of their visitation with him a third
eragon smiled the whole while
he shook hand after hand
he exchanged meaningless pleasantries and strove to memorize the plethora of names and titles that besieged him and otherwise acted with perfect civility the role he was expected to play
he knew that they honored him not because he was their friend but because of the chance of victory he embodied for the free peoples of alagaesia because of his power and because of what they hoped to gain by him
in his heart he howled with frustration and longed to break free of the stifling constraints of good manners and polite conduct and to climb on saphira and fly away to somewhere peaceful
the one part of the process eragon enjoyed was watching how the supplicants reacted to the two urgals who loomed behind nasuada is chair
some pretended to ignore the horned warriors although from the quickness of their motions and the shrill tones of their voices eragon could tell that the creatures unnerved them while others glared at the urgals and kept their hands on the pommels of their swords or daggers and still others affected a false bravado and belittled the urgals notorious strength and boasted of their own
only a few people truly seemed unaffected by the sight of the urgals
foremost among them was nasuada but their number also included king orrin trianna and an earl who said he had seen morzan and his dragon lay waste to an entire town when he had been but a boy