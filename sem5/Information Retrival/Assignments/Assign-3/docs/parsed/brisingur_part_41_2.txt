you must be ** eragon exclaimed worried
he looked her over for signs of injury
to his relief he found none
i am tired she admitted but not hungry
not yet
once i have rested then i will need to eat
right now i do not think i could stomach so much as a rabbit
the earth is unsteady beneath me i feel as if i am still flying
if they had not been apart for so long eragon might have reproached her for being reckless but as it was he was touched and grateful that she had pushed herself
thank you he said
i would have hated to wait another day for us to be together again
as would have i
she closed her eyes and pressed her head against his hands as he continued to scratch behind her jaw
besides i could hardly be late for the coronation now could i who did the clanmeet
before she could finish the question eragon sent her an image of orik
ah she sighed her satisfaction flowing through him
he will make a fine king
is the star sapphire ready for me to mend
if the dwarves have not already finished piecing it together i am sure they will have by tomorrow
that is good
cracking open an eyelid she fixed him with her piercing gaze
nasuada told me of what az sweldn rak anhuin attempted
always you get into trouble when i am not with you
his smile widened
and when you are
i eat the trouble before it eats you
so you say
what about when the urgals ambushed us by gil ead and took me captive
a plume of smoke escaped from between saphira is fangs
that does not count
i was smaller then and not as experienced
it would not happen now
and you are not as helpless as you once were
i ve never been helpless he protested
i just have powerful enemies
for some reason saphira found his last statement enormously amusing she started laughing deep within her chest and soon eragon was laughing as well
neither of them was able to stop until eragon was lying on his back gasping for air and saphira was struggling to contain the darts of flame that kept shooting out of her nostrils
then saphira made a sound eragon had never heard before a strange jumping growl and he noticed the oddest feeling through their connection
saphira made the sound again then shook her head as if trying to rid herself of a swarm of flies
oh dear she said
i seem to have the hiccups
eragon is mouth dropped open
he held that pose for a moment then he doubled over laughing so hard tears streamed down his face
every time he was about to recover saphira would hiccup bobbing her head forward like a stork and he would go off into convulsions again
at last he plugged his ears with his fingers and stared at the ceiling and recited the true names of every metal and stone he could remember
when he finished he took a deep breath and stood
better saphira asked
her shoulders shook as another hiccup racked her
eragon bit his tongue
better
come on let is go to tronjheim
you should have some water
that might help
and then you should sleep
cannot you cure hiccups with a spell
maybe
probably
but neither brom nor oromis taught me how
saphira grunted her understanding and a hiccup followed an instant later
biting his tongue even harder eragon stared at the tips of his boots
shall we
saphira extended her right foreleg in invitation
eragon eagerly climbed up onto her back and settled into the saddle at the base of her neck
together they continued through the tunnel toward tronjheim both of them happy and both of them sharing in each other is happiness