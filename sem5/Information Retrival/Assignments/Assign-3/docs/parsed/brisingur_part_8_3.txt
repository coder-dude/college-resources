the elves and also the riders in days gone by called this place mirnathor
the dwarves refer to it as werghadn and humans as the gray heath
if that does not answer your question then perhaps it will if i say we are a number of leagues southeast of helgrind where you were imprisoned
sloan mouthed the word helgrind
you rescued me
leave your questions
eat this first
his harsh tone acted like a whip on the butcher sloan cringed and reached with fumbling fingers for the lizard
releasing it eragon retreated to his place next to the rock oven and scooped handfuls of dirt onto the coals blotting out the glow so that it would not betray their presence in the unlikely event that anyone else was in the vicinity
after an initial tentative lick to determine what it was eragon had given him sloan dug his teeth into the lizard and ripped a thick gobbet from the carcass
with each bite he crammed as much flesh into his mouth as he could and only chewed once or twice before swallowing and repeating the process
he stripped each bone clean with the efficiency of a man who possessed an intimate understanding of how animals were constructed and what was the quickest way to disassemble them
the bones he dropped into a neat pile on his left
as the final morsel of meat from the lizard is tail vanished down sloan is gullet eragon handed him the other reptile which was yet whole
sloan grunted in thanks and continued to gorge himself making no attempt to wipe the fat from his mouth and chin
the second lizard proved to be too large for sloan to finish
he stopped two ribs above the bottom of the chest cavity and placed what was left of the carcass on the cairn of bones
then he straightened his back drew his hand across his lips tucked his long hair behind his ears and said thank you strange sir for your hospitality
it has been so long since i had a proper meal i think i prize your food even above my own freedom
if i may ask do you know of my daughter katrina and what has happened to her she was imprisoned with me in ** his voice contained a complex mixture of emotions respect fear and submission in the presence of an unknown authority hope and trepidation as to his daughter is fate and determination as unyielding as the mountains of the spine
the one element eragon expected to hear but did not was the sneering disdain sloan had used with him during their encounters in carvahall
sloan gaped
** how did he get here did the ra zac capture him as well or did
the ra zac and their steeds are dead
you killed them how
who for an instant sloan froze as if he were stuttering with his entire body and then his cheeks and mouth went slack and his shoulders caved in and he clutched at a bush to steady himself
he shook his head
no no no
no
it can not be
the ra zac spoke of this they demanded answers i did not have but i thought
that is who would believe
his sides heaved with such violence eragon wondered if he would hurt himself
in a gasping whisper as if he were forced to speak after being punched in the middle sloan said you can not be eragon
a sense of doom and destiny descended upon eragon
he felt as if he were the instrument of those two merciless overlords and he replied in accordance slowing his speech so each word struck like a hammer blow and carried all the weight of his dignity station and anger
i am eragon and far more
i am argetlam and shadeslayer and firesword
my dragon is saphira she who is also known as bjartskular and flametongue
we were taught by brom who was a rider before us and by the dwarves and by the elves
we have fought the urgals and a shade and murtagh who is morzan is son
we serve the varden and the peoples of alagaesia
and i have brought you here sloan aldensson to pass judgment upon you for murdering byrd and for betraying carvahall to the empire
lie roared eragon
i do not ** thrusting out with his mind he engulfed sloan is consciousness in his own and forced the butcher to accept memories that confirmed the truth of his statements
he also wanted sloan to feel the power that was now his and to realize that he was no longer entirely human