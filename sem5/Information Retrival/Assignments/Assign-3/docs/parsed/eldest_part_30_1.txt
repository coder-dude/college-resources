eragon woke at dawn well rested
he tapped saphira is ribs and she lifted her wing
running his hands through his hair he walked to the room is precipice and leaned against one side bark rough against his shoulder
below the forest sparkled like a field of diamonds as each tree reflected the morning light with a thousand thousand drops of dew
he jumped with surprise as saphira dove past him twisting like an auger toward the canopy before she pulled up and circled through the sky roaring with ** little one
he smiled happy that she was happy
he opened the screen to their bedroom where he found two trays of food mostly fruit that had been placed by the lintel during the night
by the trays was a bundle of clothes with a paper note pinned to it
eragon had difficulty deciphering the flowing script since he had not read for over a month and had forgotten some of the letters but at last he understood that it said
greetings saphira bjartskular and eragon shadeslayer
i bellaen of house miolandra do humble myself and apologize to you saphira for this unsatisfactory meal
elves do not hunt and no meat is to be had in ellesmera nor in any of our cities
if you wish you can do as the dragons of old were wont and catch what you may in du weldenvarden
we only ask that you leave your kills in the forest so that our air and water remain untainted by blood
eragon these clothes are for you
they were woven by niduen of islanzadi is house and are her gift to you
may good fortune rule over you
and the stars watch over you
when eragon told saphira the message she said it does not matter i wo not need to eat for a while after yesterday is meal
however she did snap up a few seed ** so that i do not appear rude she explained
after eragon finished breakfast he hauled the bundle of clothes onto his bed and carefully unfolded them finding two full length tunics of russet trimmed with thimbleberry green a set of creamy leggings to wrap his calves in and three pairs of socks so soft they felt like liquid when he pulled them through his hands
the quality of the fabric shamed the weaving of the women of carvahall as well as the dwarf clothes he wore now
eragon was grateful for the new raiment
his own tunic and breeches were sadly travel worn from their weeks exposed to the rain and sun since farthen dur
stripping he donned one of the luxurious tunics savoring its downy texture
he had just laced on his boots when someone knocked on the screen to the bedroom
come in he said reaching for zar roc
orik poked his head inside then cautiously entered testing the floor with his feet
he eyed the ceiling
give me a cave any day instead of a bird is nest like this
how fared your night eragon saphira
well enough
and yours said eragon
i slept like a ** the dwarf chuckled at his own jest then his chin sank into his beard and he fingered the head of his ax
i see you ve eaten so i will ask you to accompany me
arya the queen and a host of other elves await you at the base of the ** he fixed eragon with a testy gaze
something is going on that they have not told us about
i am not sure what they want from you but it is important
islanzadi is as tense as a cornered wolf
i thought i d warn you beforehand
eragon thanked him then the two of them descended by way of the stairs while saphira glided to earth
they were met on the ground by islanzadi arrayed in a mantle of ruffled swan feathers which were like winter snow heaped upon a cardinal is breast
she greeted them and said follow me
her wending course took the group to the edge of ellesmera where the buildings were few and the paths were faint from disuse
at the base of a wooded knoll islanzadi stopped and said in a terrible voice before we go any farther the three of you must swear in the ancient language that you will never speak to outsiders of what you are about to see not without permission from me my daughter or whoever may succeed us to the throne
why should i gag myself demanded orik
why indeed asked ** you not trust us
it is not a matter of trust but of safety
we must protect this knowledge at all costs it is our greatest advantage over galbatorix and if you are bound by the ancient language you will never willingly reveal our secret