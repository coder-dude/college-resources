beyond them pale and wraithlike in the glittering shards of rain sat the ra zac on their black horses
without slowing roran fell upon the first man jabbing his spear
his first and second stabs were deflected by an upraised arm then roran caught the soldier on the hip and when he stumbled in his throat
sloan howled like an enraged beast threw his cleaver and split one of the men is helms crushing his skull
two soldiers charged him with drawn swords
sloan sidestepped laughing now and blocked their attacks with his shield
one soldier swung so hard his blade stuck in the shield is rim
sloan yanked him closer and gored him through the eye with a carving knife from his belt
drawing a second cleaver the butcher circled his other opponent with a maniacal grin
shall i gut and hamstring you he demanded almost prancing with a terrible bloody glee
roran lost his spear to the next two men he faced
he barely managed to drag out his hammer in time to stop a sword from shearing off his leg
the soldier who had torn the spear from roran is grip now cast the weapon at him aiming for his breast
roran dropped his hammer caught the shaft in midair which astounded him as much as the soldiers spun it around and drove the spear through the armor and ribs of the man who had launched it
left weaponless roran was forced to retreat before the remaining soldier
he stumbled over a corpse cutting his calf on a sword as he fell and rolled to avoid a two handed blow from the soldier scrabbling frantically in the ankle deep mud for something anything he could use for a weapon
a hilt bruised his fingers and he ripped it from the muck and slashed at the soldier is sword hand severing his thumb
the man stared dumbly at the glistening stump then said this is what comes from not shielding myself
aye agreed roran and beheaded him
the last soldier panicked and fled toward the impassive specters of the ra zac while sloan bombarded him with a stream of insults and foul names
when the soldier finally pierced the shining curtain of rain roran watched with a thrill of horror as the two black figures bent down from their steeds on either side of the man and gripped the nape of his neck with twisted hands
the cruel fingers tightened and the man shrieked desperately and convulsed then went limp
the ra zac placed the corpse behind one of their saddles before turning their horses and riding away
roran shuddered and looked at sloan who was cleaning his blades
you fought ** he had never suspected that the butcher contained such ferocity
sloan said in a low voice they will never get katrina
never even if i must skin the lot of them or fight a thousand urgals and the king to boot
i d tear the sky itself down and let the empire drown in its own blood before she suffers so much as a ** he clamped his mouth shut then jammed the last of his knives into his belt and began dragging the three broken trees back into position
while he did roran rolled the dead soldiers through the trampled mud away from the ** i have killed five
at the completion of his labor he straightened and glanced around puzzled for all he heard was silence and the hissing ** has no one come to help us
wondering what else might have occurred he returned with sloan to the scene of the first attack
two soldiers hung lifelessly on the slick branches of the tree wall but that was not what held their attention
horst and the other villagers knelt in a circle around a small body
roran caught his breath
it was elmund son of delwin
the ten year old boy had been struck in his side by a spear
his parents sat in the mud beside him their faces as blank as stone
something has to be done thought roran dropping to his knees and leaning against his spear
few children survived their first five or six years
but to lose your firstborn sonnow when everything indicated that he should grow tall and strong to take his father is place in carvahall it was enough to crush **
the children
they all have to be protected
but where
where
where
where