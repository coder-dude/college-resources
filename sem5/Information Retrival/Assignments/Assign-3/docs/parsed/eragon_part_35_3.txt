murtagh gave zar roc to eragon
i found the other sword and bow in the guardroom
i ve never seen weapons like them before so i assumed they were the elf is
let is find out said eragon through a mouthful of bread
the sword slim and light with a curved crossguard the ends of which narrowed into sharp points fit the elf is sheath perfectly
there was no way to tell if the bow was hers but it was shaped so gracefully he doubted it could be anyone else is
what now he asked cramming another bite of food into his mouth
we can not stay here forever
sooner or later the soldiers will find us
now said murtagh taking out his own bow and fitting an arrow to the string we wait
like i said our escape has been arranged
you do not understand there is a shade ** if he finds us we re doomed
a ** exclaimed murtagh
in that case tell saphira to come immediately
we were going to wait until the watch changed but delaying even that long is too dangerous ** eragon relayed the message succinctly refraining from distracting saphira with questions
you messed up my plans by escaping yourself groused murtagh watching the room is entrances for soldiers
eragon smiled
in that case perhaps i should have ** timing was perfect though
i would not have been able to even crawl if i had been forced to fight all those soldiers with magic
glad to be of some use remarked murtagh
he stiffened as they heard men running nearby
let is just hope the shade does not find us
a cold chuckle filled the banquet room
i am afraid it is far too late for that
murtagh and eragon spun around
the shade stood alone at the end of the room
in his hand was a pale sword with a thin scratch on the blade
he unclasped the brooch that held his cape in place and let the garment fall to the floor
his body was like a runner is thin and compact but eragon remembered brom is warning and knew that the shade is appearance was deceiving he was many times stronger than a normal human
so my youngrider do you wish to test yourself against me sneered the shade
i should not have trusted the captain when he said you ate all your food
i will not make that mistake again
i will take care of him said murtagh quietly putting down his bow and drawing his sword
no said eragon under his breath
he wants me alive not you
i can stall him for a short while but then you d better have a way out for us
fine go said murtagh
you wo not have to hold him off for long
i hope not said eragon grimly
he drew zar roc and slowly advanced
the red blade glinted with light from torches on the wall
the shade is maroon eyes burned like coals
he laughed softly
do you really think to defeat me du sundavar freohr what a pitiful name
i would have expected something more subtle from you but i suppose that is all you re capable of
eragon refused to let himself be goaded
he stared at the shade is face waiting for a flicker of his eyes or twitch of his lip anything that would betray his next ** can not use magic for fear of provoking him to do the same
he has to think that he can win without resorting to it which he probably can
before either of them moved the ceiling boomed and shook
dust billowed from it and turned the air gray while pieces of wood fell around them shattering on the floor
from the roof came screams and the sound of clashing metal
afraid of being brained by the falling timber eragon flicked his eyes upward
the shade took advantage of his distraction and attacked
eragon barely managed to get zar roc up in time to block a slash at his ribs
their blades met with a clang that jarred his teeth and numbed his ** he is ** he grasped zar roc with both hands and swung with all of his might at the shade is head
the shade blocked him with ease whipping his sword through the air faster than eragon had thought possible
terrible screeches sounded above them like iron spikes being drawn across rock
three long cracks split the ceiling
shingles from the slate roof fell through the fissures
eragon ignored them even when one smashed into the floor next to him
though he had trained with a master of the blade brom and with murtagh who was also a deadly swordsman he had never been this outclassed