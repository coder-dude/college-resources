eragon looked away studying the landscape
the cliff provided a wonderful view of their surroundings especially the foaming sea as well as protection against unwelcome eyes
only birds would see saphira here
it was an ideal location
i do not ** proceeded to recount the day is ** are forces circling us that we are not aware of
sometimes i wonder if we can ever understand the true motives of the people around us
they all seem to have secrets
it is the way of the world
ignore all the schemes and trust in the nature of each person
brom is good
he means us no harm
we do not have to fear his plans
i hope so he said looking down at his hands
this finding of the ra zac through writing is a strange way of tracking she ** there be a way to use magic to see the records without being inside the room
i am not sure
you would have to combine the word forseeingwithdistance
or ** way it seems rather difficult
i will ask brom
that would be ** lapsed into tranquil silence
you know we may have to stay here awhile
saphira is answer held a hard ** as always i will be left to wait outside
that is not how i want it
soon enough we will travel together again
eragon smiled and hugged her
he noticed then how rapidly the light was ** have to go now before i am locked out of teirm
hunt tomorrow and i will see you in the evening
she spread her ** i will take you down
he got onto her scaly back and held on tightly as she launched off the cliff glided over the trees then landed on a knoll
eragon thanked her and ran back to teirm
he came into sight of the portcullis just as it was beginning to lower
calling for them to wait he put on a burst of speed and slipped inside seconds before the gateway slammed closed
ya cut that a little close observed one of the guards
it wo not happen again assured eragon bending over to catch his breath
he wound his way through the darkened city to jeod is house
a lantern hung outside like a beacon
a plump butler answered his knock and ushered him inside without a word
tapestries covered the stone walls
elaborate rugs dotted the polished wood floor which glowed with the light from three gold candelabra hanging from the ceiling
smoke drifted through the air and collected above
this way sir
your friend is in the study
they passed scores of doorways until the butler opened one to reveal a study
books covered the room is walls
but unlike those in jeod is office these came in every size and shape
a fireplace filled with blazing logs warmed the room
brom and jeod sat before an oval writing desk talking amiably
brom raised his pipe and said in a jovial voice ah here you are
we were getting worried about you
how was your walk
i wonder what put him in such a good mood why does not he just come out and ask how saphira is pleasant but the guards almost locked me outside the city
and teirm is big
i had trouble finding this house
jeod chuckled
when you have seen dras leona gil ead or even kuasta you wo not be so easily impressed by this small ocean city
i like it here though
when it is not raining teirm is really quite beautiful
eragon turned to brom
do you have any idea how long we will be here
brom spread his palms upward
that is hard to tell
it depends on whether we can get to the records and how long it will take us to find what we need
we will all have to help it will be a huge job
i will talk with brand tomorrow and see if he will let us examine the records
i do not think i will be able to help eragon said shifting uneasily
why not asked brom
there will be plenty of work for you
eragon lowered his head
i can not read
brom straightened with disbelief
you mean garrow never taught you
he knew how to read asked eragon puzzled
jeod watched them with interest
of course he did snorted brom
the proud fool what was he thinking i should have realized that he would not have taught you
he probably considered it an unnecessary ** brom scowled and pulled at his beard angrily
this sets my plans back but not irreparably
i will just have to teach you how to read
it wo not take long if you put your mind to it
eragon winced
brom is lessons were usually intense and brutally ** much more can i learn at one time i suppose it is necessary he said ruefully