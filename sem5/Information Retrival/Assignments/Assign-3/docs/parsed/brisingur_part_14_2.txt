upon his upper lip there sprouted an enormous curly mustache that after descending to the corners of his mouth extended a good nine inches in either direction and was in stark contrast to the straight hair that fell to his shoulders
how such a massive piece of sculpted foliage supported its own weight puzzled eragon especially since it was dull and lusterless and obviously had not been impregnated with warm beeswax
the other soldiers held spears pointed at eragon and arya
so much dirt covered them it was impossible to see the flames stitched on their tunics
now then said the man and his mustache wobbled like an unbalanced set of scales
who are you where are you going and what is your business in the king is lands then he waved a hand
no do not bother answering
it does not matter
nothing matters nowadays
the world is coming to an end and we waste our days interrogating peasants
** superstitious vermin who scurry from place to place devouring all the food in the land and reproducing at a ghastly rate
at my family is estate near uru baen we would have the likes of you flogged if we caught you wandering around without permission and if we learned that you had stolen from your master why then we d hang you
whatever you want to tell me is lies
it always is
what have you got in that pack of yours eh food and blankets yes but maybe a pair of gold candlesticks eh silverware from the locked chest secret letters for the varden eh cat got your tongue well we will soon sort the matter out
langward why do not you see what treasures you can excavate from yonder knapsack there is a good boy
eragon staggered forward as one of the soldiers struck him across the back with the haft of a spear
he had wrapped his armor in rags to keep the pieces from rubbing against each other
the rags however were too thin to entirely absorb the force of the blow and muffle the clang of metal
** exclaimed the man with the mustache
grabbing eragon from behind the soldier unlaced the top of his pack and pulled out his hauberk saying look **
the man with the mustache broke out in a delighted grin
** and of fine make as well
very fine i should say
well you are full of surprises
going to join the varden were you intent on treason and sedition mmh his expression soured
or are you one of those who generally give honest soldiers a bad name if so you are a most incompetent mercenary you do not even have a weapon
was it too much trouble to cut yourself a staff or a club eh well how about it answer **
no sir did not occur to you i suppose
it is a pity we have to accept such slow minded wretches but that is what this blasted war has reduced us to scrounging for leftovers
silence you insolent ** no one gave you permission to ** his mustache quivering the man gestured
red lights exploded across eragon is field of vision as the soldier behind him bashed him on the head
whether you are a thief a traitor a mercenary or merely a fool your fate will be the same
once you swear the oath of service you will have no choice but to obey galbatorix and those who speak for him
we are the first army in history to be free of dissent
no mindless blathering about what we should do
only orders clear and direct
you too shall join our cause and you shall have the privilege of helping to make real the glorious future our great king has foreseen
as for your lovely companion there are other ways she can be of use to the empire eh now tie them **
eragon knew then what he had to do
glancing over he found arya already looking at him her eyes hard and bright
he blinked once
she blinked in return
his hand tightened around the pebble
most of the soldiers eragon had fought on the burning plains had possessed certain rudimentary wards intended to shield them from magical attacks and he suspected these men were likewise equipped
he was confident he could break or circumvent any spells galbatorix is magicians invented but it would require more time than he now had
instead he cocked his arm and with a flick of his wrist threw the pebble at the man with the mustache