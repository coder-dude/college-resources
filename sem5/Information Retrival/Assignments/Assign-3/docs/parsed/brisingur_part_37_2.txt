** exclaimed orik and spat on the floor
they are vargrimstn warriors who have disgraced themselves and are now clanless
no one deals with such filth unless they are engaged in villainy themselves and do not wish others to know of it
and so it was with those three
they took their orders directly from grimstborith vermund of az sweldn rak anhuin
orik shook his head
there is no doubt it is az sweldn rak anhuin who tried to kill you eragon
we will probably never know if any other clans joined them in the attempt but if we expose az sweldn rak anhuin is treachery it will force everyone else who might have been involved in the plot to disparage their former confederates to abandon or at least delay further attacks on durgrimst ingeitum and if this is handled properly to give me their vote for king
an image flashed in eragon is mind of the prismatic blade emerging from the back of kvistor is neck and of the dwarf is agonized expression as he had fallen to the floor dying
how will we punish az sweldn rak anhuin for this crime should we kill vermund
ah leave that to me said orik and tapped the side of his nose
i have a plan
but we must tread carefully for this is a situation of the utmost delicacy
such a betrayal has not occurred in many long years
as an outsider you cannot know how abhorrent we find it that one of our own should attack a guest
you being the only free rider left to oppose galbatorix only worsens the offense
further bloodshed may yet be necessary but at the moment it would only bring about another clan war
a clan war might be the only way to deal with az sweldn rak anhuin eragon pointed out
i think not but if i am mistaken and war is unavoidable we must ensure it is a war between the rest of the clans and az sweldn rak anhuin
that would not be so bad
together we could crush them inside of a week
a war with the clans split into two or three factions however would destroy our country
it is crucial then that before we draw our swords we convince the other clans of what az sweldn rak anhuin has done
toward that end will you allow magicians from different clans to examine your memories of the attack so they may see it happened as we shall say it did and that we did not stage it for our own benefit
eragon hesitated reluctant to open his mind to strangers then nodded toward the three dwarves stacked on top of one another
what about them wo not their memories be enough to convince the clans of az sweldn rak anhuin is guilt
orik grimaced
they ought to be but in order to be thorough the clan chiefs will insist upon verifying their memories against yours and if you refuse az sweldn rak anhuin will claim we are hiding something from the clanmeet and that our accusations are nothing more than slanderous fiction
very well said eragon
if i must i must
but if any of the magicians stray where they are not supposed to even if by accident i will have no choice but to burn what they have seen out of their minds
there are some things i cannot allow to become common knowledge
nodding orik said aye i can think of at least one three legged piece of information that would cause us some consternation if it were to be trumpeted throughout the land eh i am sure the clan chiefs will accept your conditions for they all have secrets of their own they would not want bandied about just as i am sure they will order their magicians to proceed regardless of the danger
this attack has the potential to incite such turmoil among our race the grimstborithn will feel compelled to determine the truth about it though it may cost them their most skilled spellcasters
drawing himself upright then to the full extent of his limited height orik ordered the prisoners removed from the ornate entryway and dismissed all of his vassals save for eragon and a contingent of twenty six of his finest warriors
with a graceful flourish orik grasped eragon is left elbow and conducted him toward the inner rooms of his chambers
tonight you must remain here with me where az sweldn rak anhuin will not dare to strike
if you intend to sleep said eragon i must warn you i cannot rest not tonight