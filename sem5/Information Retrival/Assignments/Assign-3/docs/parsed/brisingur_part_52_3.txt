understand me shadeslayer i would prefer it if you had kept hold of zar roc but it would please me even more if you had a sword that was made for you
zar roc may have served you well but it was the wrong shape for your body
and do not even speak to me of tamerlein
you would have to be a fool to think you could wield it
as you can see said eragon i did not bring it with me from lord fiolr
rhunon nodded and resumed chiseling
well then good
if zar roc is the right sword for murtagh said eragon would not brom is sword be the right weapon for me
a frown pinched rhunon is eyebrows together
undbitr why would you think of brom is blade
because brom was my father said eragon and felt a thrill at being able to say that
is that so now laying down her hammer and chisel rhunon walked out from under the roof of her forge until she stood opposite eragon
her posture was slightly stooped from the centuries she had spent hunched over her work and because of it she appeared an inch or two shorter than he
mmh yes i can see the similarity
he was a rude one he was brom he said what he meant and wasted no words
i rather liked it
i cannot abide how my race has become
they are too polite too refined too precious
** i remember when elves used to laugh and fight like normal creatures
now they have become so withdrawn some seem to have no more emotion than a marble **
saphira said are you referring to how elves were before our races joined themselves to one another
rhunon turned her scowl onto saphira
brightscales
welcome
yes i was speaking of a time before the bond between elves and dragons was sealed
the changes i have seen in our races since you would hardly credit as possible but so they are and here i am one of the few still alive who can remember what we were like before
then rhunon whipped her gaze back to eragon
undbitr is not the answer to your need
brom lost his sword during the fall of the riders
if it does not reside in galbatorix is collection then it may have been destroyed or it may be buried in the earth somewhere underneath the crumbling bones of a long forgotten battlefield
even if it could be found you could not retrieve it before you would have to face your enemies again
what then should i do rhunon elda asked eragon
and he told her of the falchion he had chosen when he was among the varden and of the spells he had reinforced the falchion with and of how it had failed him in the tunnels underneath farthen dur
rhunon snorted
no that would never work
once a blade has been forged and quenched you can protect it with an endless array of spells but the metal itself remains as weak as ever
a rider needs something more a blade that can survive the most violent of impacts and one that is unaffected by most any magic
no what you must do is sing spells over the hot metal while you are extracting it from the ore and also while you are forging it so as to alter and improve the structure of the metal
how can i get such a sword though eragon asked
would you make me one rhunon elda
the wire thin lines on rhunon is face deepened
she reached over and rubbed her left elbow the thick muscles in her bare forearm writhing
you know that i swore that i would never create another weapon so long as i live
my oath binds me i cannot break it no matter how much i might wish ** continuing to hold her elbow rhunon walked back to her bench and sat before her sculpture
and why should i dragon rider tell me that
why should i loose another soul reaver upon the world
choosing his words with care eragon said because if you did you could help put an end to galbatorix is reign
would not it be fitting if i killed him with a blade you forged when it was with your swords he and the forsworn slew so many dragons and riders you hate how they have used your weapons
how better to balance the scales then than by forging the instrument of galbatorix is doom
rhunon crossed her arms and looked up at the sky
a sword
a new sword
after so long to again ply my craft
lowering her gaze she jutted her chin out at eragon and said it is possible just possible that there might be a way i could help you but it is futile to speculate for i cannot try