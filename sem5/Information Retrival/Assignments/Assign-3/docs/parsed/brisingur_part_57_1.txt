they arrived at the edge of du weldenvarden by late afternoon of the first day
there above the shadowed boundary between the trees and the fields of grass beyond glaedr and saphira circled one another and glaedr said keep safe your heart saphira and mine as well
and oromis shouted from glaedr is back fair winds to you both eragon ** when next we meet let it be before the gates of uru baen
fair winds to you as ** eragon called in return
then glaedr turned and followed the line of the forest westward which would lead him to the northernmost tip of isenstar lake and the lake thence to gil ead while saphira continued in the same southwesterly direction as before
saphira flew all through that night landing only to drink and so eragon could stretch his legs and relieve himself
unlike during their flight to ellesmera they encountered no headwinds the air remained clear and smooth as if even nature were eager for them to return to the varden
when the sun rose on their second day it found them already deep within the hadarac desert and heading straight south so as to skirt the eastern border of the empire
and by the time darkness had again engulfed the land and sky and held them in its cold embrace saphira and eragon were beyond the confines of the sandy wastes and were again soaring over the verdant fields of the empire their course such that they would pass between uru baen and lake tudosten on their way to the city of feinster
after flying for two days and two nights without sleep saphira was unable to continue
swooping down to a small thicket of white birch trees by a pond she curled up in their shade and napped for a few hours while eragon kept watch and practiced his swordsmanship with brisingr
ever since they had parted with oromis and glaedr a sense of constant anxiety had troubled eragon as he pondered what awaited him and saphira at feinster
he knew that they were better protected than most from death and injury but when he thought back to the burning plains and to the battle of farthen dur and when he remembered the sight of blood spurting from severed limbs and the screams of wounded men and the white hot lash of a sword slicing through his own flesh then eragon is gut would roil and his muscles would shake with suppressed energy and he did not know whether he wished to fight every soldier in the land or flee in the opposite direction and hide in a deep dark hole
his dread only worsened when he and saphira resumed their journey and spotted lines of armed men marching over the fields below
here and there pillars of pale smoke rose from sacked villages
the sight of so much wanton destruction sickened him
averting his gaze he squeezed the neck spike in front of him and squinted until the only thing visible through the bars of his blurry eyelashes was the white calluses on his knuckles
little one said saphira her thoughts slow and tired
we have done this before
do not allow it to disturb you so
regretting that he had distracted her from flying he said i am sorry
i will be fine when we get there
i just want it to be over
eragon sniffed and wiped his cold nose on the cuff of his tunic
sometimes i wish i enjoyed fighting as much as you do
then this would be so much easier
if you did she said the entire world would cower before our feet including galbatorix
no it is good you do not share my love of blood
we balance each other out eragon
apart we are incomplete but together we are whole
now clear your mind of these poisonous thoughts and tell me a riddle that will keep me awake
very well he said after a moment
i am colored red and blue and yellow and every other hue of the rainbow
i am long and short thick and thin and i often rest coiled up
i can eat a hundred sheep in a row and still be hungry
what am i
a dragon of course she said without hesitation
hours after dusk saphira wobbled and dropped several feet in a single sickening lurch
eragon straightened alarmed and looked around for any clues as to what had caused the disturbance but saw only blackness below and the glittering stars above