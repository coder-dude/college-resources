a second later he uttered an involuntary groan as he died three times once each with two small birds roosting in a nearby juniper and also with a snake hidden among the rocks
across from him roran threw back his head and bared his teeth in a soundless howl as his shoulder muscle jumped and writhed beneath the surface of his shifting skin
eragon inhaled a shuddering breath and rested his head in his hands taking advantage of the concealment they provided to wipe away his tears before he examined the results of his labor
he saw roran shrug several times and then stretch and windmill his arms
roran is shoulder was large and round the result of years spent digging holes for fence posts hauling rocks and pitching hay
despite himself a needle of envy pricked eragon
he might be stronger but he had never been as muscular as his cousin
roran grinned
it is as good as ** better maybe
thank you
it was the strangest thing
i actually felt as if i was going to crawl out of my hide
and it itched something terrible i could barely keep from ripping
get me some bread from your saddlebag would you i am hungry
i need a bite to eat after using magic like ** eragon sniffed and then pulled out his kerchief and wiped his nose
he sniffed again
what he had said was not quite true
it was the toll his spell had exacted on the wildlife that disturbed him not the magic itself and he feared he might throw up unless he had something to settle his stomach
you re not ill are you asked roran
** with the memory of the deaths he had caused still heavy in his mind eragon reached for the jar of mead by his side hoping to fend off a tide of morbid thoughts
something very large heavy and sharp struck his hand and pinned it against the ground
he winced and looked over to see the tip of one of saphira is ivory claws digging into his flesh
her thick eyelid went snick as it flashed across the great big glittering iris she fixed upon him
after a long moment she lifted the claw as a person would a finger and eragon withdrew his hand
he gulped and gripped the hawthorn staff once more striving to ignore the mead and to concentrate upon what was immediate and tangible instead of wallowing in dismal introspection
roran removed a ragged half of sourdough bread from his bags then paused and with a hint of a smile said would not you rather have some venison i did not finish all of ** he held out the makeshift spit of seared juniper wood on which were impaled three clumps of golden brown meat
to eragon is sensitive nose the odor that wafted toward him was thick and pungent and reminded him of nights he had spent in the spine and of long winter dinners where he roran and garrow had gathered around their stove and enjoyed each other is company while a blizzard howled outside
his mouth watered
it is still warm said roran and waved the venison in front of eragon
with an effort of will eragon shook his head
just give me the bread
are you sure it is perfect not too tough not too tender and cooked with the perfect amount of seasoning
it is so juicy when you take a bite it is as if you swallowed a mouthful of elain is best stew
roran stop teasing me and hand over that **
ah now see you look better already
maybe what you need is not bread but someone to get your hackles up eh
eragon glowered at him then faster than the eye could see snatched the bread away from roran
that seemed to amuse roran even more
as eragon tore at the loaf he said i do not know how you can survive on nothing but fruit bread and vegetables
a man has to eat meat if he wants to keep his strength up
do not you miss it
then why do you insist on torturing yourself like this every creature in this world has to eat other living beings even if they are only plants in order to survive
that is how we are made
why attempt to defy the natural order of things
i said much the same in ellesmera observed saphira but he did not listen to me
eragon shrugged
we already had this discussion
you do what you want
i wo not tell you or anyone else how to live
however i cannot in good conscience eat a beast whose thoughts and feelings i ve shared