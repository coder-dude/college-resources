aside from saphira is heart of hearts and glaedr is are there any eldunari that galbatorix has not captured
faint lines appeared around the corners of oromis is down turned mouth
none that we know of
after the fall of the riders brom went searching for eldunari that galbatorix might have overlooked but without success
nor in all my years of scouring alagaesia with my mind have i detected so much as a whisper of a thought from an eldunari
every eldunari was well accounted for when galbatorix and morzan initiated their attack on us and none of them vanished without explanation
it is inconceivable that any great store of eldunari might be lying hidden somewhere ready to help us if we could but locate them
although eragon had expected no other answer he still found it disappointing
one last question
when either a rider or a rider is dragon dies the surviving member of the pair would often waste away or commit suicide soon afterward
and those that did not usually went mad from the loss
am i right
what would happen though if the dragon transferred their consciousness to their heart and then their body died
through the soles of his boots eragon felt a faint tremor shake the ground as glaedr shifted his position
the gold dragon said if a dragon experienced body death and yet their rider still lived together they became known as indlvarn
the transition would hardly be a pleasant one for the dragon but many riders and dragons successfully adapted to the change and continued to serve the riders with distinction
if however it was a dragon is rider who died then the dragon would often smash their eldunari or arrange for another to smash it for them if their body was no more thus killing themselves and following their rider into the void
but not all
some dragons were able to overcome their loss as were some riders such as brom and continue to serve our order for many years afterward either through their flesh or through their heart of hearts
you have given us much to think about oromis elda said saphira
eragon nodded but stayed silent for he was busy pondering all that had been said