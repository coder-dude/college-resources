the red dragon nipped saphira is tail and she and eragon yelped with shared pain
panting from the effort saphira executed a tight backward loop ending up behind the dragon who then pivoted to the left and tried to spiral up and over saphira
while the dragons dueled with increasingly complex acrobatics eragon became aware of a disturbance on the burning plains the spellcasters of du vrangr gata were beset by two new magicians from the empire
these magicians were far more powerful than those who had preceded them
they had already killed one of du vrangr gata and were battering past the barriers of a second
eragon heard trianna scream with her mind ** you have to help ** we can not stop them
they will kill all the varden
help us it is the
her voice was lost to him as the rider stabbed at his consciousness
this must end spat eragon between clenched teeth as he strove to withstand the onslaught
over saphira is neck he saw the red dragon dive toward them angling beneath saphira
eragon dared not open his mind enough to talk with saphira so he said out loud catch ** with two strokes of zar roc he severed the straps around his legs and jumped off saphira is back
this is insane thought eragon
he laughed with giddy exhilaration as the feeling of weightlessness took hold of him
the rush of air tore off his helm and made his eyes water and sting
releasing his shield eragon spread out his arms and legs as oromis had taught him in order to stabilize his flight
below the steel clad rider noticed eragon is action
the red dragon shied to eragon is left but could not evade him
eragon lashed out with zar roc as the dragon is flank flashed by and he felt the blade sink into the creature is hamstring before his momentum carried him past
the impact of the blow sent eragon spinning up down and around
by the time he managed to stop his rotation he had plummeted through the cloud cover and was heading toward a swift and fatal landing on the burning plains
he could stop himself with magic if he had to but it would drain his last reserves of energy
he glanced over both his ** on saphira where are you
as if in answer she dropped out of the foul smoke her wings pressed tight against her body
she swooped underneath him and opened her wings a bit to slow her fall
careful not to impale himself on one of her spikes eragon maneuvered himself back into the saddle welcoming the return of gravity as she pulled out of the dive
never do that to me again she snapped
he surveyed the steaming blood that laced zar roc is ** worked did not it
his satisfaction disappeared as he realized that his stunt had placed saphira at the mercy of the red dragon
he hurtled at her from above harrying her this way and that as he forced her toward the ground
saphira tried to maneuver out from under him but every time she did he dove at her biting and buffeting her with his wings in order to make her change course
the dragons twisted and lunged until their tongues lolled out of their mouths their tails drooped and they gave up flapping and merely glided
his mind once again closed to all contact friendly or not eragon said out loud land saphira it is no good
i will fight him on the ground
with a grunt of weary resignation saphira descended to the nearest flat open area a small stone plateau set along the western edge of the jiet river
the water had turned red from the blood pouring into it from the battle
eragon jumped off saphira once she alighted on the plateau and tested his footing
it was smooth and hard with nothing to trip on
he nodded pleased
a few seconds later the red dragon rushed by overhead and settled on the opposite side of the plateau
he held his left hind leg off the ground to avoid aggravating his wound a long gash that nearly severed the muscle
the dragon trembled his entire length like an injured dog
he tried to hop forward then stopped and snarled at eragon
the enemy rider unbuckled his legs and slid down the uninjured side of his dragon
then he walked around the dragon and examined his leg
eragon let him he knew how much pain it would cause the man to see the damage inflicted on his bonded partner