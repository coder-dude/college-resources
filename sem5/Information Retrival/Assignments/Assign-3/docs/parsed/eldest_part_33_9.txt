and said eragon unable to stop the flow of questions what was the broddring kingdom
oromis is eyes widened with dismay
you do not know eragon shook his head
how can you not considering your circumstances and the fear that galbatorix wields among your people i might understand that you were raised in darkness ignorant of your heritage
but i cannot credit brom with being so lax with your instruction as to neglect subjects that even the youngest elf or dwarf knows
the children of your varden could tell me more about the past
brom was more concerned with keeping me alive than teaching me about people who are already dead retorted eragon
this drew silence from oromis
finally he said forgive me
i did not mean to impugn brom is judgment only i am impatient beyond reason we have so little time and each new thing you must learn reduces that which you can master during your tenure ** he opened a series of cupboards hidden within the curved wall and removed bread rolls and bowls of fruit which he rowed out on the table
he paused for a moment over the food with his eyes closed before beginning to eat
the broddring kingdom was the human is country before the riders fell
after galbatorix killed vrael he flew on ilirea with the forsworn and deposed king angrenost taking his throne and titles for his own
the broddring kingdom then formed the core of galbatorix is conquests
he added vroengard and other lands to the east and south to his holdings creating the empire you are familiar with
technically the broddring kingdom still exists though at this point i doubt that it is much more than a name on royal decrees
afraid to pester the elf with further inquiries eragon concentrated on his food
his face must have betrayed him though because oromis said you remind me of brom when i chose him as my apprentice
he was younger than you only ten but his curiosity was just as great
i doubt i heard aught from him for a year buthow what when and above all else why
do not be shy to ask what lies in your heart
i want to know so much whispered eragon
who are you where do you come from
where did brom come from what was morzan like how what when why and i want to know everything about vroengard and the riders
maybe then my own path will be clearer
silence fell between them as oromis meticulously disassembled a blackberry prying out one plump segment at a time
when the last corpuscle vanished between his port red lips he rubbed his hands flat together polishing his palms as garrow used to say and said know this about me then i was born some centuries past in our city of luthivira which stood in the woods by lake tudosten
at the age of twenty like all elf children i was presented to the eggs that the dragons had given the riders and glaedr hatched for me
we were trained as riders and for near a century we traveled the world over doing vrael is will
eventually the day arrived when it was deemed appropriate for us to retire and pass on our experience to the next generation so we took a position in ilirea and taught new riders one or two at a time until galbatorix destroyed us
brom came from a family of illuminators in kuasta
his mother was nelda and his father holcomb
kuasta is so isolated by the spine from the rest of alagaesia it has become a peculiar place full of strange customs and superstitions
when he was still new to ilirea brom would knock on a door frame three times before entering or leaving a room
the human students teased him about it until he abandoned the practice along with some of his other habits
morzan was my greatest failure
brom idolized him
he never left his side never contradicted him and never believed that he could best morzan in any venture
morzan i am ashamed to admit for it was within my power to stop was aware of this and took advantage of brom is devotion in a hundred different ways
he grew so proud and cruel that i considered separating him from brom
but before i could morzan helped galbatorix to steal a dragon hatchling shruikan to replace the one galbatorix had lost killing the dragon is original rider in the process