eragon pushed aside the scrolls
i d be delighted to see them
arya looked surprised when both of them spoke in the ancient language so eragon explained oromis is command
an excellent idea said arya joining them in the same language
and it is more appropriate to speak thus while you stay here
when all three of them had descended from the tree arya directed them westward toward an unfamiliar quadrant of ellesmera
they encountered many elves on the path all of whom stopped to bow to saphira
eragon noticed once again that no elf children were to be seen
he mentioned this to arya and she said aye we have few children
only two are in ellesmera at the present dusan and alanna
we treasure children above all else because they are so rare
to have a child is the greatest honor and responsibility that can be bestowed upon any living being
at last they arrived at a ribbed lancet arch grown between two trees which served as the entrance for a wide compound
still in the ancient language arya chanted root of tree fruit of vine let me pass by this blood of mine
the two archway doors trembled then swung outward releasing five monarch butterflies that fluttered toward the dusky sky
through the archway lay a vast flower garden arranged to look as pristine and natural as a wild meadow
the one element that betrayed artifice was the sheer variety of plants many of the species were blooming out of season or came from hotter or colder climates and would never have flourished without the elves magic
the scene was lit with the gemlike flameless lanterns augmented by constellations of swirling fireflies
to saphira arya said mind your tail that it does not sweep across the beds
advancing they crossed the garden and pressed deep into a line of scattered trees
before eragon quite knew where he was the trees became more numerous and then thickened into a wall
he found himself standing on the threshold of a burnished wood hall without ever being conscious of having gone inside
the hall was warm and homey a place of peace reflection and comfort
its shape was determined by the tree trunks which on the inside of the hall had been stripped of their bark polished and rubbed with oil until the wood gleamed like amber
regular gaps between the trunks acted as windows
the scent of crushed pine needles perfumed the air
a number of elves occupied the hall reading writing and in one dark corner playing a set of reed pipes
they all paused and inclined their heads to acknowledge saphira is presence
here you would stay said arya were you not rider and dragon
arya guided him and saphira everywhere in the compound that was accessible to dragons
each new room was a surprise no two were alike and each chamber found different ways to incorporate the forest in its construction
in one room a silver brook trickled down the gnarled wall and flowed across the floor on a vein of pebbles and back out under the sky
in another creepers blanketed the entire room except for the floor in a leafy green pelt adorned with trumpet shaped flowers with the most delicate pink and white colors
arya called it the liani vine
they saw many great works of art from fairths and paintings to sculptures and radiant mosaics of stained glass all based on the curved shapes of plants and animals
islanzadi met with them for a short time in an open pavilion joined to two other buildings by covered pathways
she inquired about the progress of eragon is training and the state of his back both of which he described with brief polite phrases
this seemed to satisfy the queen who exchanged a few words with saphira and then departed
in the end they returned to the garden
eragon walked beside arya saphira trailing behind entranced by the sound of her voice as she told him about the different varieties of flowers where they originated how they were maintained and in many instances how they had been altered with magic
she also pointed out the flowers that only opened their petals during the night like a white datura
which one is your favorite he asked
arya smiled and escorted him to a tree on the edge of the garden by a pond lined with rushes