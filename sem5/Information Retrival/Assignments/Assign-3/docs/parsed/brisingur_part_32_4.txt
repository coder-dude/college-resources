he spat to clear his mouth of bile
one of the varden they had sought to rescue had died in the struggle slain by a knife in the kidneys but the two who were still standing joined forces with roran and carn and with them they charged the next batch of soldiers
drive them toward the ** roran shouted
the water and the mud would limit the soldiers movement and perhaps allow the varden to gain the upper hand
not far away martland had succeeded in rallying the twelve of the varden who were still on their horses and they were already doing what roran had suggested herding the soldiers back toward the shining water
the soldiers and the few drivers who were still alive resisted
they shoved their shields against the men on foot
they jabbed spears at the horses
but in spite of their violent opposition the varden forced them to retreat a step at a time until the men in the crimson tunics stood knee deep in the fast flowing water half blinded by the uncanny light shining down on them
hold the ** shouted martland dismounting and planting himself with spread legs on the edge of the riverbank
do not let them regain the **
roran dropped into a half crouch ground his heels into the soft earth until he was comfortable with his stance and waited for the large soldier standing in the cold water several feet in front of him to attack
with a roar the soldier splashed out of the shallows swinging his sword at roran which roran caught on his shield
roran retaliated with a stroke of his hammer but the soldier blocked him with his own shield and then cut at roran is legs
for several seconds they exchanged blows but neither wounded the other
then roran shattered the man is left forearm knocking him back several paces
the soldier merely smiled and uttered a mirthless soul chilling laugh
roran wondered whether he or any of his companions would survive the night
they re harder to kill than snakes
we can cut them to ribbons and they will still keep coming at us unless we hit something vital
his next thought vanished as the soldier rushed at him again his notched sword flickering in the pale light like a tongue of flame
thereafter the battle assumed a nightmarish quality for roran
the strange baleful light gave the water and the soldiers an unearthly aspect bleaching them of color and projecting long thin razor sharp shadows across the shifting water while beyond and all around the fullness of night prevailed
again and again he repelled the soldiers who stumbled out of the water to kill him hammering at them until they were barely recognizable as human and yet they would not die
with every blow medallions of black blood stained the surface of the river like blots of spilled ink and drifted away on the current
the deadly sameness of each clash numbed and horrified roran
no matter how hard he strove there was always another mutilated soldier there to slash and stab at him
and always the demented giggling of men who knew they were dead and yet continued to maintain a semblance of life even while the varden destroyed their bodies
roran remained crouched behind his shield with his hammer half raised gasping and drenched with sweat and blood
a minute passed before it dawned on him that no one stood in the water before him
he glanced left and right three times unable to grasp that the soldiers were finally blessedly irrevocably dead
a corpse floated past him in the glittering water
an inarticulate bellow escaped him as a hand gripped his right arm
he whipped around snarling and pulling away only to see carn next to him
the wan gore smeared spellcaster was speaking
we won ** eh they re ** we vanquished **
roran let his arms drop and tilted his head back too tired even to sit
he felt
he felt as if his senses were abnormally sharp and yet his emotions were dull muted things tamped down somewhere deep inside of himself
he was glad it was so otherwise he thought he would go mad
gather up and inspect the ** shouted martland
the sooner you bestir yourselves the sooner we can leave this accursed ** carn attend to welmar