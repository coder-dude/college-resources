before letting the water flow back into the ground eragon gulped down as much as he could then watched the last drops melt back into the dirt
holding the water on the surface was harder than he had ** at least it is within my abilities he reflected remembering with some amusement how he had once struggled to lift even a pebble
it was freezing when they rose the next day
the sand had a pink hue in the morning light and the sky was hazy concealing the horizon
murtagh is mood had not improved with sleep and eragon found his own rapidly deteriorating
during breakfast he asked do you think it will be long before we leave the desert
murtagh glowered
we re only crossing a small section of it so i can not imagine that it will take us more than two or three days
but look how far we ve already come
all right maybe it wo ** all i care about right now is getting out of the hadarac as quickly as possible
what we re doing is hard enough without having to pick sand from our eyes every few minutes
they finished eating then eragon went to the elf
she lay as one dead a corpse except for her measured breathing
where lies your injury whispered eragon brushing a strand of hair from her face
how can you sleep like this and yet live the image of her alert and poised in the prison cell was still vivid in his mind
troubled he prepared the elf for travel then saddled and mounted snowfire
as they left the camp a line of dark smudges became visible on the horizon indistinct in the hazy air
murtagh thought they were distant hills
eragon was not convinced but he could make out no details
the elf is plight filled his thoughts
he was sure that something had to be done to help her or she would die though he knew not what that might be
saphira was just as concerned
they talked about it for hours but neither of them knew enough about healing to solve the problem confronting them
at midday they stopped for a brief rest
when they resumed their journey eragon noticed that the haze had thinned since morning and the distant smudges had gained definition
no longer were they indistinct purple blue lumps but rather broad forest covered mounds with clear outlines
the air above them was pale white bleached of its usual hue all color seemed to have been leached out of a horizontal band of sky that lay on top of the hills and extended to the horizon is edges
he stared puzzled but the more he tried to make sense of it the more confused he became
he blinked and shook his head thinking that it must be some illusion of the desert air
yet when he opened his eyes the annoying incongruity was still there
indeed the whiteness blanketed half the sky before them
sure that something was terribly wrong he started to point this out to murtagh and saphira when he suddenly understood what he was seeing
what they had taken to be hills were actually the bases of gigantic mountains scores of miles wide
except for the dense forest along their lower regions the mountains were entirely covered with snow and ice
it was this that had deceived eragon into thinking the sky white
he craned back his neck searching for the peaks but they were not visible
the mountains stretched up into the sky until they faded from sight
narrow jagged valleys with ridges that nearly touched split the mountains like deep gorges
it was like a ragged toothy wall linking alagaesia with the heavens
there is no end to ** thought awestruck
stories that mentioned the beor mountains always noted their size but he had discounted such reports as fanciful embellishments
now however he was forced to acknowledge their authenticity
sensing his wonder and surprise saphira followed his gaze with her own
within a few seconds she recognized the mountains for what they ** feel like a hatchling again
compared to them even i feel small
we must be near the edge of the desert said ** is only taken two days and we can already see the far side and beyond
saphira spiraled above the ** but considering the size of those peaks they could still be fifty leagues from here
it is hard to gauge distances against something so immense