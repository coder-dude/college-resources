as they approached the turn the rise and fall of voices reached them muffled by the thick layer of morning fog over the valley
at the copse is fringe roran slowed to a stop
it was foolish to surprise people when they too might be out hunting
still something bothered him
perhaps it was the number of voices the group seemed bigger than any family in the valley
without thinking he stepped off the road and slipped behind the underbrush lining the copse
what are you doing whispered baldor
roran put a finger to his lips then crept along parallel to the road keeping his footsteps as quiet as possible
as they rounded the bend he froze
on the grass by the road was a camp of soldiers
thirty helmets gleamed in a shaft of morning light as their owners devoured fowl and stew cooked over several fires
the men were mud splattered and travel stained but galbatorix is symbol was still visible on their red tunics a twisting flame outlined in gold thread
underneath the tunics they wore leather brigandines heavy with riveted squares of steel mail shirts and then padded gambesons
most of the soldiers bore broadswords though half a dozen were archers and another half dozen carried wicked looking halberds
and hunched in their midst were two twisted black forms that roran recognized from the numerous descriptions the villagers provided upon his return from therinsford the strangers who had destroyed his farm
his blood ** re servants of the ** he began to step forward fingers already reaching for an arrow when baldor grabbed his jerkin and dragged him to the ground
do not
you will get us both killed
roran glared at him then snarled
that is
they re the bastards
he stopped noticing that his hands were ** they ve **
roran whispered baldor intently you can not do anything
look they work for the king
even if you managed to escape you d be an outlaw everywhere and you d bring disaster on carvahall
what do they want whatcan they want the king
why did galbatorix countenance my father is torture
if they did not get what they needed from garrow and eragon fled with brom then they must want ** baldor paused letting the words sink in
we have to get back and warn everyone
then you have to hide
the strangers are the only ones with horses
we can get there first if we run
roran stared through the brush at the oblivious soldiers
his heart pounded fiercely for revenge clamoring to attack and fight to see those two agents of misfortune pierced with arrows and brought to their own justice
it mattered not that he would die as long as he could wash clean his pain and sorrow in one fell moment
all he had to do was break cover
the rest would take care of itself
with a choked sob he clenched his fist and dropped his ** can not leave katrina
he remained rigid eyes squeezed shut then with agonizing slowness dragged himself back
home then
without waiting for baldor is reaction roran slipped through the trees as fast as he dared
once the camp was out of sight he broke out onto the road and ran down the dirt track channeling his frustration anger and even fear into speed
baldor scrambled behind him gaining on the open stretches
roran slowed to a comfortable trot and waited for him to draw level before saying you spread the word
i will talk with ** baldor nodded and they pushed on
after two miles they stopped to drink and rest briefly
when their panting subsided they continued through the low hills preceding carvahall
the rolling ground slowed them considerably but even so the village soon burst into view
roran immediately broke for the forge leaving baldor to make his way to the center of town
as he pounded past the houses roran wildly considered schemes to evade or kill the strangers without incurring the wrath of the empire
he burst into the forge to catch horst tapping a peg into the side of quimby is wagon singing
and a ringing and a dingin
rang from old ** wily old iron
with a beat and a bang on the bones of the land
horst stopped his mallet in midblow when he saw roran
what is the matter lad is baldor hurt
roran shook his head and leaned over gasping for air