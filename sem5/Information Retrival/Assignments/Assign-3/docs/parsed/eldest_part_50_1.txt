roran stood upon the poop deck of thered boar his arms crossed over his chest and his feet planted wide apart to steady himself on the rolling barge
the salty wind ruffled his hair and tugged at his thick beard and tickled the hairs on his bare forearms
beside him clovis manned the tiller
the weathered sailor pointed toward the coastline at a seagull covered rock silhouetted on the crest of a rolling hill that extended into the ocean
teirm be right on the far side of that peak
roran squinted into the afternoon sun which reflected off the ocean in a blindingly bright band
we will stop here for now then
you do not want to go on into the city yet
not all of us at once
call over torson and flint and have them run the barges up on that shore
it looks like a good place to camp
clovis grimaced
arrgh
i was hoping t get a hot meal ** roran understood the fresh food from narda had long since been eaten leaving them with naught but salt pork salted herring salted cabbage sea biscuits the villagers had made from their purchased flour pickled vegetables and the occasional fresh meat when the villagers slaughtered one of their few remaining animals or managed to catch game when they landed
clovis is rough voice echoed over the water as he shouted to the skippers of the other two barges
when they drew near he ordered them to pull ashore much to their vociferous displeasure
they and the other sailors had counted on reaching teirm that day and lavishing their pay on the city is delights
after the barges were beached roran walked among the villagers and helped them by pitching tents here and there unloading equipment fetching water from a nearby stream and otherwise lending his assistance until everyone was settled
he paused to give morn and tara a word of encouragement for they appeared despondent and received a guarded response in turn
the tavern owner and his wife had been aloof to him ever since they left palancar valley
on the whole the villagers were in better condition than when they arrived at narda due to the rest they had garnered on the barges but constant worry and exposure to the harsh elements had prevented them from recuperating as well as roran hoped
stronghammer will you sup at our tent tonight asked thane coming up to roran
roran declined with as much grace as he could and turned to find himself confronted by felda whose husband byrd had been murdered by sloan
she bobbed a quick curtsy then said may i speak with you roran garrowsson
he smiled at her
always felda
you know that
thank ** with a furtive expression she fingered the tassels that edged her shawl and glanced toward her tent
i would ask a favor of you
it is about mandel roran nodded he had chosen her eldest son to accompany him into narda on that fateful trip when he killed the two guards
mandel had performed admirably then as well as in the weeks since while he crewed theedeline and learned what he could about piloting the barges
he is become quite friendly with the sailors on our barge and he is started playing dice with those lawless men
not for money we have none but for small things
things we need
have you asked him to stop
felda twisted the tassels
i fear that since his father died he no longer respects me as he once did
he has grown wild and willful
we have all grown wild thought roran
and what would you have me do about it he asked gently
you have ever dealt generously with mandel
he admires you
if you talk with him he will listen
roran considered the request then said very well i will do what i ** felda sagged with relief
tell me though what has he lost at dice
food ** felda hesitated and then added but i know he once risked my grandmother is bracelet for a rabbit those men snared
roran frowned
put your heart at ease felda
i will tend to the matter as soon as i can
thank ** felda curtsied again then slipped away between the makeshift tents leaving roran to mull over what she had said
roran absently scratched his beard as he walked
the problem with mandel and the sailors was a problem that cut both ways roran had noticed that during the trip from narda one of torson is men frewin had become close to odele a young friend of ** could cause trouble when we leave clovis