arya spun away from the sight
nasuada your majesty she said her eyes flicking toward orrin you have to stop the soldiers before they reach the camp
you cannot allow them to attack our defenses
if they do they will sweep over these ramparts like a storm driven wave and wreak untold havoc in our midst among the tents where we cannot maneuver effectively
untold havoc orrin scoffed
have you so little confidence in our prowess ambassador humans and dwarves may not be as gifted as elves but we shall have no difficulty in disposing of these miserable wretches i can assure you
the lines of arya is face tightened
your prowess is without compare your majesty
i do not doubt it
but listen this is a trap set for eragon and saphira
they she flung an arm toward the rising figures of thorn and murtagh have come to capture eragon and saphira and spirit them away to uru baen
galbatorix would not have sent so few men unless he was confident they could keep the varden occupied long enough for murtagh to overwhelm eragon
galbatorix must have placed spells on those men spells to aid them in their mission
what those enchantments might be i do not know but of this i am certain the soldiers are more than they appear and we must prevent them from entering this camp
emerging from his initial shock eragon said you do not want to let thorn fly over the camp he could set fire to half of it with a single pass
nasuada clasped her hands over the pommel of her saddle seemingly oblivious to murtagh and thorn and to the soldiers who were now less than a mile away
but why not attack us while we were unawares she asked
why alert us to their presence
it was narheim who answered
because they would not want eragon and saphira to get caught up in the fighting on the ground
no unless i am mistaken their plan is for eragon and saphira to meet thorn and murtagh in the air while the soldiers assail our position here
is it wise then to accommodate their wishes to willingly send eragon and saphira into this trap nasuada raised an eyebrow
yes insisted arya for we have an advantage they could not ** she pointed at blodhgarm
this time eragon shall not face murtagh alone
he will have the combined strength of thirteen elves supporting him
murtagh will not be expecting that
stop the soldiers before they reach us and you will have frustrated part of galbatorix is design
send saphira and eragon up with the mightiest spellcasters of my race bolstering their efforts and you will disrupt the remainder of galbatorix is scheme
you have convinced me said nasuada
however the soldiers are too close for us to intercept them any distance from the camp with men on foot
orrin
before she finished the king had turned his horse around and was racing toward the north gate of the camp
one of his retinue winded a trumpet a signal for the rest of orrin is cavalry to assemble for a charge
to garzhvog nasuada said king orrin will require assistance
send your rams to join him
lady ** throwing back his massive horned head garzhvog loosed a wild wailing bellow
the skin on the back of eragon is arms and neck prickled as he listened to the urgal is savage howl
with a snap of his jaws garzhvog ceased his belling and then grunted they will ** the kull broke into an earth shattering trot and ran toward the gate where king orrin and his horsemen were gathered
four of the varden dragged open the gate
king orrin raised his sword shouted and galloped out of the camp leading his men toward the soldiers in their gold stitched tunics
a plume of cream colored dust billowed out from underneath the hooves of the horses obscuring the arrowhead shaped formation from view
order two hundred swordsmen and a hundred spearmen after them
and have fifty archers station themselves seventy to eighty yards away from the fighting
i want these soldiers crushed jormundur obliterated ground out of existence
the men are to understand that no quarter is to be given or accepted
and tell them that although i cannot join them in this battle on account of my arms my spirit marches with them