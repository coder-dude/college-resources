with his hands he scooped out a shallow hole a foot and a half wide
he summoned forth water to fill the hole then uttered a spell of scrying
the water shimmered and acquired a soft yellow glow as eragon beheld the interior of oromis is hut
the silver haired elf was sitting at his kitchen table reading a tattered scroll
oromis looked up at eragon and nodded with unsurprised recognition
master eragon said and twisted his hand over his chest
greetings eragon
i have been expecting you
where are you
saphira and i just reached du weldenvarden
master i know we promised to return to ellesmera but the varden are only a few days away from the city of feinster and they are vulnerable without us
we do not have the time to fly all the way to ellesmera
could you answer our questions here through the scrying pool
oromis leaned back in his chair his angled face grave and pensive
then he said i will not instruct you at a distance eragon
i can guess at some of the things you wish to ask me and they are subjects we must discuss in person
master please
if murtagh and thorn
no eragon
i understand the reason for your urgency but your studies are just as important as protecting the varden maybe even more so
we must do this properly or not at all
eragon sighed and slumped forward
yes master
oromis nodded
glaedr and i will be waiting for you
fly safe and fly fast
we have much to talk about
feeling numb and worn out eragon ended the spell
the water soaked back into the ground
he held his head in his hands staring at the patch of moist dirt between his feet
saphira is heavy breathing was loud beside him
i guess we have to keep going he said
i am sorry
her breathing paused for a moment as she licked her chops
it is all right
i am not about to collapse
he looked up at her
are you sure
eragon reluctantly hoisted himself upright and climbed onto her back
as long as we re going to ellesmera he said tightening the straps around his legs we should visit the menoa tree again
maybe we can finally figure out what solembum meant
i could certainly use a new sword
when eragon had first met solembum in teirm the werecat had told him when the time comes and you need a weapon look under the roots of the menoa tree
then when all seems lost and your power is insufficient go to the rock of kuthian and speak your name to open the vault of souls
eragon still did not know where the rock of kuthian was but during their first stay in ellesmera he and saphira had had several chances to examine the menoa tree
they had discovered no clue as to the exact whereabouts of the supposed weapon
moss dirt bark and the occasional ant were the only things they had seen among the roots of the menoa tree and none of them indicated where to excavate
solembum might not have meant a sword saphira pointed out
werecats love riddles nearly as much as dragons do
if it even exists this weapon might be a scrap of parchment with a spell inscribed on it or a book or a painting or a sharp piece of rock or any other dangerous thing
whatever it is i hope we can find it
who knows when we will have the chance to return to ellesmera again
saphira raked aside a fallen tree that lay before her then crouched and unfurled her velvety wings her massive shoulder muscles bunching
eragon yelped and grabbed the front of his saddle as she surged up and forward with unexpected force rising above the tops of the trees in a single vertiginous bound
wheeling over the sea of shifting branches saphira oriented herself in a northwesterly direction and then set out toward the elves capital the beats of her wings slow and heavy