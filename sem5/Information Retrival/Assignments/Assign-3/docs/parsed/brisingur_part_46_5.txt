surely there is no shame in ** when several minutes had passed and yarbog still had not replied roran yanked on yarbog is horns and growled well
raising his voice so that all of the men and urgals could hear yarbog said ** svarvok curse me i ** i should not have challenged you stronghammer
you are worthy to be chief and i am not
as one the men cheered and shouted banging the pommels of their swords on their shields
the urgals shifted in place and said nothing
satisfied roran released yarbog is horns and rolled away from the gray urgal
feeling almost as if he had endured another flogging roran slowly got to his feet and hobbled out of the square to where carn was waiting
roran winced as carn draped a blanket over his shoulders and the fabric rubbed against his abused skin
grinning carn handed him a wineskin
after he knocked you down i thought for sure he would kill you
i should have learned by now to never count you out eh roran ** that was just about the finest fight i ve ever seen
you must be the only man in history to have wrestled an urgal
maybe not roran said between sips of wine
but i might be the only man who has survived the ** he smiled as carn laughed
roran looked over at the urgals who were clustered around yarbog talking with him in low grunts while two of their brethren wiped the grease and grime from yarbog is limbs
although the urgals appeared subdued they did not seem angry or resentful so far as he was able to judge and he was confident that he would have no more trouble from them
despite the pain of his wounds roran felt pleased with the outcome of the match
this wo not be the last fight between our two races he thought but as long as we can return safely to the varden the urgals wo not break off our alliance at least not on account of me
after taking one last sip roran stoppered the wineskin and handed it back to carn then shouted right now stop standing around yammering like sheep and finish drawing up a list of what is in those ** loften round up the soldiers horses if they have not already wandered too far ** dazhgra see to the oxen
make ** thorn and murtagh could be flying here even now
go on snap to
and carn where the blazes are my clothes