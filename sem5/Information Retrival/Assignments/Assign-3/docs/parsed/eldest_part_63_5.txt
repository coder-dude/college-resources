displaying a surprising amount of alacrity strength and flexibility she clambered over the many rows of breastwork the dwarves had engineered swinging from pole to pole leaping over trenches and finally running helter skelter down the steep face of the last rampart to stop panting by saphira
throwing back the hood of her cloak angela flashed them a bright smile
a welcoming ** how thoughtful of ** as she spoke the werecat shivered along his length fur rippling
then his outline blurred as if seen through cloudy water resolving once more into the nude figure of a shaggy haired boy
angela dipped her hand into a leather purse at her belt and passed a child is tunic and breeches back to solembum along with the small black dagger he fought with
what were you doing out there asked orik peering at them with a suspicious gaze
i think you better tell us said eragon
her face hardened
is that so do not you trust solembum and me the werecat bared his pointed teeth
not really admitted eragon but with a small smile
that is good said angela
she patted him on the cheek
you will live longer
if you must know then i was doing my best to help defeat the empire onlymy methods do not involve yelling and running around with a sword
and what exactly are your methods growled orik
angela paused to roll up her cloak into a tight bundle which she stored in her purse
i d rather not say i want it to be a surprise
you wo not have to wait long to find out
it will start in a few hours
orik tugged on his beard
what will start if you can not give us a straight answer we will have to take you to nasuada
maybe she can wring some sense out of you
it is no use dragging me off to nasuada said angela
she gave me permission to cross lines
so you say challenged orik ever more belligerent
and soi say announced nasuada walking up to them from behind as eragon knew she would
he also sensed that she was accompanied by four kull one of whom was garzhvog
scowling he turned to face them making no attempt to hide his anger at the urgals presence
orik was not as composed he jumped back with a mighty oath grasping his war ax
he quickly realized that they were not under attack and gave nasuada a terse greeting
but his hand never left the haft of his weapon and his eyes never left the hulking urgals
angela seemed to have no such inhibitions
she paid nasuada the respect due to her then addressed the urgals in their own harsh language to which they answered with evident delight
nasuada drew eragon off to the side so they could have a measure of privacy
there she said i need you to put aside your feelings for a moment and judge what i am about to tell you with logic and reason
can you do that he nodded stiff faced
good
i am doing everything i can to ensure we do not lose tomorrow
it does not matter though how well we fight or how well i lead the varden or even if we rout the empire ifyou she poked him in the chest are killed
do you understand he nodded again
there is nothing i can do to protect you if galbatorix reveals himself if he does you will face him alone
du vrangr gata poses no more of a threat to him than they do to you and i will not have them eradicated without reason
i have always known said eragon that i would face galbatorix alone but for saphira
a sad smile touched nasuada is lips
she looked very tired in the flickering torchlight
well there is no reason to invent trouble where none exists
it is possible galbatorix is not even ** she did not seem to believe her own words though
in any case i can at least keep you from dying from a sword in the gut
i heard what the dwarves intend to do and i thought i could improve upon the concept
i asked garzhvog and three of his rams to be your guards so long as they agreed which they have to let you examine their minds for treachery
eragon went rigid
you can not expect me to fight with thosemonsters
besides i already accepted the dwarves offer to defend saphira and me
they would take it poorly if i rejected them in favor of urgals
then they can both guard you retorted nasuada
she searched his face for a long time looking for what he could not tell