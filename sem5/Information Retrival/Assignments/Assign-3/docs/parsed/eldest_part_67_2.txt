i want to see what he will do
a bleak smile crossed murtagh is face
the twins enjoyed tormenting me when i was their captive
eragon glanced at him suspicious
you wo not hurt him you wo not warn the twins
vel einradhin iet ai shur ** upon my word as a rider
together they watched as roran hid behind a mound of bodies
eragon stiffened as the twins looked toward the pile
for a moment it seemed they had spotted him then they turned away and roran jumped up
he swung his hammer and bashed one of the twins in the head cracking open his skull
the remaining twin fell to the ground convulsing and emitted a wordless scream until he too met his end under roran is hammer
then roran planted his foot upon the corpses of his foes lifted his hammer over his head and bellowed his victory
what now demanded eragon turning away from the battlefield
are you here to kill me
of course not
galbatorix wants you alive
murtagh is lips quirked
you do not know ** there is a fine jest
it is not because of you it is because ofher
he jabbed a finger at saphira
the dragon inside galbatorix is last egg the last dragon egg in the world is male
saphira is the only female dragon in existence
if she breeds she will be the mother of her entire race
do you see now galbatorix does not want to eradicate the dragons
he wants to use saphira to rebuild the riders
he can not kill you either of you if his vision is to become reality
and what a vision it is eragon
you should hear him describe it then you might not think so badly of him
is it evil that he wants to unite alagaesia under a single banner eliminate the need for war and restore the riders
he is the one who destroyed the riders in the first **
and for good reason asserted murtagh
they were old fat and corrupt
the elves controlled them and used them to subjugate humans
they had to be removed so that we could start anew
a furious scowl contorted eragon is features
he paced back and forth across the plateau his breathing heavy then gestured at the battle and said how can you justify causing so much suffering on the basis of a madman is ravings galbatorix has done nothing but burn and slaughter and amass power for himself
he lies
he murders
he manipulates
youknow ** it is why you refused to work for him in the first ** eragon paused then adopted a gentler tone i can understand that you were compelled to act against your will and that you are not responsible for killing hrothgar
you can try to escape though
i am sure that arya and i could devise a way to neutralize the bonds galbatorix has laid upon you
join me murtagh
you could do so much for the varden
with us you would be praised and admired instead of cursed feared and hated
for a moment as murtagh gazed down at his notched sword eragon hoped he would accept
then murtagh said in a low voice you cannot help me eragon
no one but galbatorix can release us from our oaths and he will never do that
he knows our true names eragon
we are his slaves forever
though he wanted to eragon could not deny the sympathy he felt for murtagh is plight
with the utmost gravity he said then let us kill the two of you
kill ** why should we allow that
eragon chose his words with care it would free you from galbatorix is control
and it would save the lives of hundreds if not thousands of people
is not that a noble enough cause to sacrifice yourself for
murtagh shook his head
maybe for you but life is still too sweet for me to part with it so easily
no stranger is life is more important than thorn is or my own
as much as he hated it hated the entire situation in fact eragon knew then what had to be done
renewing his attack on murtagh is mind he leaped forward both feet leaving the ground as he lunged toward murtagh intending to stab him through the heart
eragon dropped back to the ground as invisible bands clamped around his arms and legs immobilizing him
to his right saphira discharged a jet of rippling fire and sprang at murtagh like a cat pouncing on a mouse
** commanded murtagh extending a clawlike hand as if to catch her
saphira yelped with surprise as murtagh is incantation stopped her in midair and held her in place floating several feet above the plateau