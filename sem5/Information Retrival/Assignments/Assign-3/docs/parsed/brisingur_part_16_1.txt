a broad smile stretched eragon is face and he laughed relieved
we made ** he exclaimed
murtagh thorn hundreds of soldiers galbatorix is pet magicians the ra zac none of them could catch us
** how is that for taunting the king this will tweak his beard for sure when he hears of it
he will be twice as dangerous then warned arya
i know he said grinning even wider
maybe he will get so angry he will forget to pay his troops and they will all throw away their uniforms and join the varden
you are in fine fettle today
and why should not i be he demanded
bouncing on the tips of his toes he opened his mind as wide as he could and gathering his strength shouted ** sending the thought flying over the countryside like a spear
a response was not long in coming
they embraced with their minds smothering each other with warm waves of love joy and concern
they exchanged memories of their time apart and saphira comforted eragon over the soldiers he had killed drawing off the pain and anger that had accumulated within him since the incident
he smiled
with saphira so close everything seemed right in the world
and i you little one
then she sent him an image of the soldiers he and arya had fought and said without fail every time i leave you you get yourself in trouble
every ** i hate to so much as turn tail on you for fear you will be locked in mortal combat the moment i take my eyes off you
be fair i ve gotten into plenty of trouble when i am with you
it is not something that just happens when i am alone
we seem to be lodestones for unexpected events
no you are a lodestone for unexpected events she sniffed
nothing out of the ordinary ever occurs to me when i am by myself
but you attract duels ambushes immortal enemies obscure creatures such as the ra zac long lost family members and mysterious acts of magic as if they were starving weasels and you were a rabbit that wandered into their den
what about the time you spent as galbatorix is possession was that an ordinary event
i had not hatched yet she said
you cannot count that
the difference between you and me is that things happen to you whereas i cause things to happen
maybe but that is because i am still learning
give me a few years and i will be as good as brom at getting things done eh you can not say i did not seize the initiative with sloan
mmh
we still have to talk about that
if you ever surprise me like that again i will pin you on the ground and lick you from head to toe
eragon shivered
her tongue was covered with hooked barbs that could strip hair hide and meat off a deer with a single swipe
i know but i was not sure myself whether i was going to kill sloan or let him go free until i was standing in front of him
besides if i had told you i was going to stay behind you would have insisted on stopping me
he sensed a faint growl as it rumbled through her chest
she said you should have trusted me to do the right thing
if we cannot talk openly how are we supposed to function as dragon and rider
would doing the right thing have involved taking me from helgrind regardless of my wishes
it might not have she said with a hint of defensiveness
he smiled
you re right though
i should have discussed my plan with you
i am sorry
from now on i promise i will consult with you before i do anything you do not expect
is that acceptable
only if it involves weapons magic kings or family members she said
or flowers she agreed
i do not need to know if you decide to eat some bread and cheese in the middle of the night
unless a man with a very long knife is waiting for me outside of my tent
if you could not defeat a single man with a very long knife you would be a poor excuse for a rider indeed
by your own argument you should take comfort in the fact that while i may attract more trouble than most people i am perfectly capable of escaping from situations that would kill most anyone else
even the greatest warriors can fall prey to bad luck she said
remember the dwarf king kaga who was killed by a novice swordsman swordsdwarf when he tripped on a rock
you should always remain cautious for no matter your skills you cannot anticipate and prevent every misfortune fate directs your way