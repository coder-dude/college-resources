breakfast was cold but the tea was hot
ice inside the windows had melted with the morning fire and soaked into the wood floor staining it with dark puddles
eragon looked at garrow and roran by the kitchen stove and reflected that this would be the last time he saw them together for many months
roran sat in a chair lacing his boots
his full pack rested on the floor next to him
garrow stood between them with his hands stuck deep into his pockets
his shirt hung loosely his skin looked drawn
despite the young men is cajoling he refused to go with them
when pressed for a reason he only said that it was for the best
do you have everything garrow asked roran
he nodded and took a small pouch from his pocket
coins clinked as he handed it to roran
i ve been saving this for you
it is not much but if you wish to buy some bauble or trinket it will suffice
thank you but i wo not be spending my money on trifles said roran
do what you will it is yours said garrow
i ve nothing else to give you except a father is blessing
take it if you wish but it is worth little
roran is voice was thick with emotion
i would be honored to receive it
then do and go in peace said garrow and kissed him on the forehead
he turned and said in a louder voice do not think that i have forgotten you eragon
i have words for both of you
it is time i said them as you are entering the world
heed them and they will serve you ** he bent his gaze sternly on them
first let no one rule your mind or body
take special care that your thoughts remain unfettered
one may be a free man and yet be bound tighter than a slave
give men your ear but not your heart
show respect for those in power but do not follow them blindly
judge with logic and reason but comment not
consider none your superior whatever their rank or station in life
treat all fairly or they will seek revenge
be careful with your money
hold fast to your beliefs and others will ** he continued at a slower pace of the affairs of love
my only advice is to be honest
that is your most powerful tool to unlock a heart or gain forgiveness
that is all i have to ** he seemed slightly self conscious of his speech
he hoisted roran is pack
now you must go
dawn is approaching and dempton will be waiting
roran shouldered the pack and hugged garrow
i will return as soon as i can he said
** replied garrow
but now go and do not worry about us
they parted reluctantly
eragon and roran went outside then turned and waved
garrow raised a bony hand his eyes grave and watched as they trudged to the road
after a long moment he shut the door
as the sound carried through the morning air roran halted
eragon looked back and surveyed the land
his eyes lingered on the lone buildings
they looked pitifully small and fragile
a thin finger of smoke trailing up from the house was the only proof that the snowbound farm was inhabited
there is our whole world roran observed somberly
eragon shivered impatiently and grumbled a good one ** roran nodded then straightened his shoulders and headed into his new future
the house disappeared from view as they descended the hill
it was still early when they reached carvahall but they found the smithy doors already open
the air inside was pleasantly warm
baldor slowly worked two large bellows attached to the side of a stone forge filled with sparkling coals
before the forge stood a black anvil and an iron bound barrel filled with brine
from a line of neck high poles protruding from the walls hung rows of items giant tongs pliers hammers in every shape and weight chisels angles center punches files rasps lathes bars of iron and steel waiting to be shaped vises shears picks and shovels
horst and dempton stood next to a long table
dempton approached with a smile beneath his flamboyant red mustache
** i am glad you came
there is going to be more work than i can handle with my new grindstones
are you ready to go
roran hefted his pack
yes
do we leave soon
i ve a few things to take care of first but we will be off within the ** eragon shifted his feet as dempton turned to him tugging at the corner of his mustache