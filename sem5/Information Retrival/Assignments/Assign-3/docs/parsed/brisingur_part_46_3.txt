three slanting scars marked the left side of his waist where he had been clawed by an animal
sparse black bristles grew over the whole of his hide
at least he is not a kull thought roran
he was confident of his own strength but even so he did not believe that he could overpower yarbog with sheer force
rare was the man who could hope to match the physical prowess of a healthy urgal ram
also roran knew that yarbog is large black fingernails his fangs his horns and his leathery hide would all provide yarbog with considerable advantages in the unarmed combat they were about to engage in
if i can i will roran decided thinking of all the low tricks he could use against the urgal for fighting yarbog would not be like wrestling with eragon or baldor or any other man from carvahall rather roran was sure that it would be like the ferocious and unrestrained brawling between two wild beasts
again and again roran is eyes returned to yarbog is immense horns for those he knew were the most dangerous of the urgal is features
with them yarbog could butt and gore roran with impunity and they would also protect the sides of yarbog is head from any blows roran could deliver with his bare hands although they limited the urgal is peripheral vision
then it occurred to roran that just as the horns were yarbog is greatest natural gift so too they might be his undoing
roran rolled his shoulders and bounced on the balls of his feet eager for the contest to be over
when both roran and yarbog were completely covered with bear grease their seconds retreated and they stepped into the confines of the square pegged out on the ground
roran kept his knees partially flexed ready to leap in any direction at the slightest hint of movement from yarbog
the rocky soil was cold hard and rough beneath the soles of his bare feet
a slight gust stirred the branches of the nearby willow tree
one of the oxen harnessed to the wagons pawed at a clump of grass his tack creaking
with a rippling bellow yarbog charged roran covering the distance between them with three thundering steps
roran waited until yarbog was nearly upon him then jumped to the right
he underestimated yarbog is speed however
lowering his head the urgal rammed his horns into roran is left shoulder and tossed him sprawling across the square
sharp rocks poked into roran is side as he landed
lines of pain flashed across his back tracing the paths of his half healed wounds
he grunted and rolled upright feeling several scabs break open exposing his raw flesh to the stinging air
dirt and small pebbles clung to the film of grease on his body
keeping both feet on the ground he shuffled toward yarbog never taking his eyes off the snarling urgal
again yarbog charged him and again roran attempted to jump out of the way
this time his maneuver succeeded and he slipped past the urgal with inches to spare
whirling around yarbog ran at him for a third time and once more roran managed to evade him
then yarbog changed tactics
advancing sideways like a crab he thrust out his large hooked hands to catch roran and pull him into his deadly embrace
roran flinched and retreated
whatever happened he had to avoid falling into yarbog is clutches with his immense strength the urgal could soon dispatch him
the men and urgals gathered around the square were silent their faces impassive as they watched roran and yarbog scuffle back and forth in the dirt
for several minutes roran and yarbog exchanged quick glancing blows
roran avoided closing with the urgal wherever possible trying to wear him out from a distance but as the fight dragged on and yarbog seemed no more tired than when they had begun roran realized that time was not his friend
if he was going to win he had to end the fight without further delay
hoping to provoke yarbog into charging again for his strategy depended upon just that roran withdrew to the far corner of the square and began to taunt him saying ** you are as fat and slow as a milk ** ca not you catch me yarbog or are your legs made of lard you should cut off your horns in shame for letting a human make a fool of you