and also while katrina was galbatorix is hostage roran was vulnerable to his manipulation
the queen lifted one dagger sharp eyebrow
a vulnerability that you could have prevented galbatorix from exploiting by tutoring roran in certain oaths in this the language of magic
i do not counsel you to cast away your friends or family
that would be folly indeed
but keep you firmly in mind what is at stake the entirety of alagaesia
if we fail now then galbatorix is tyranny will extend over all the races and his reign shall have no conceivable end
you are the tip of the spear that is our effort and if the tip should break and be lost then our spear shall bounce off the armor of our foe and we too shall be lost
folds of lichen cracked underneath eragon is fingers as he gripped the edge of the rock basin and suppressed the urge to make an impertinent remark about how any well equipped warrior ought to have a sword or another weapon to rely upon besides a spear
he was frustrated by the direction the conversation had taken and eager to change the topic as quickly as he could he had not contacted the queen so she could berate him as if he were a mere child
nevertheless allowing his impatience to dictate his actions would do nothing to further his cause so he remained calm and replied please believe me your majesty i take your concerns very very seriously
i can only say that if i had not helped roran i would have been as miserable as he and more so if he attempted to rescue katrina by himself and died as a result
in either case i would have been too upset to be of any use to you or anyone
cannot we at least agree to differ on the subject neither of us shall convince the other
very well said islanzadi
we shall lay the matter to rest
for the present
but do not think you have escaped a proper investigation of your decision eragon dragon rider
it seems to me you display a frivolous attitude toward your larger responsibilities and that is a serious matter
i shall discuss it with oromis he will decide what is to be done about you
now tell me why did you seek this audience
eragon clenched his teeth several times before he could bring himself to in a civil tone explain the day is events the reasons for his actions in regard to sloan and the punishment he envisioned for the butcher
when he finished islanzadi whirled around and paced the circumference of the tent her movements as lithe as a cat is then stopped and said you chose to stay behind in the middle of the empire to save the life of a murderer and a traitor
you are alone with this man on foot without supplies or weapons save for magic and your enemies are close behind
i see my earlier admonishments were more than justified
you
your majesty if you must be angry with me be angry with me later
i want to resolve this quickly so i can get some rest before dawn i have many miles to cover tomorrow
the queen nodded
your survival is all that matters
i shall be furious after we are done speaking
as for your request such a thing is unprecedented in our history
if i had been in your place i would have killed sloan and rid myself of the problem then and there
i know you would have
i once watched arya slay a gyrfalcon who was injured for she said its death was inevitable and by killing it she saved the bird hours of suffering
perhaps i should have done the same with sloan but i could not
i think it would have been a choice i would have regretted for the rest of my life or worse one that would have made it easier for me to kill in the future
islanzadi sighed and suddenly she appeared tired
eragon reminded himself that she too had been fighting that day
oromis may have been your proper teacher but you have proved yourself brom is heir not oromis is
brom is the only other person who managed to entangle himself in as many predicaments as you
like him you seem compelled to find the deepest patch of quicksand and then dive into it
eragon hid a smile pleased by the comparison
what of sloan he asked
his fate rests with you now
slowly islanzadi sat upon a stool next to the folding table placed her hands in her lap and gazed to one side of the seeing glass