she crouched several yards away her eyes fixed upon the molten heart of the fire
i could help with this you know she said
it would take me but a minute to melt the ore
yes said rhunon but if we melt it too quickly the metal will not combine with the charcoal and become hard and flexible enough for a sword
save your fire dragon
we shall need it later
the heat from the smelter and the effort of pumping the bellows soon had eragon covered in a sheen of sweat his bare arms shone in the light from the fire
every now and then he or rhunon would abandon their bellows to shovel a new layer of charcoal over the fire
the work was monotonous and as a result eragon soon lost track of the time
the constant roar of the fire the feel of the bellows handle in his hands the whoosh of rushing air and saphira is vigilant presence were the only things he was aware of
it came as a surprise to him then when rhunon said that should be sufficient
leave the bellows
wiping his brow eragon helped as she shoveled the incandescent coals out of the smelter and into a barrel filled with water
the coals sizzled and emitted an acrid smell as they struck the liquid
when they finally exposed the glowing pool of white hot metal at the bottom of the trough the slag and other impurities having run off during the process rhunon covered the metal with an inch of fine white ash then leaned her shovel against the side of the smelter and went to sit on the bench by her forge
what now eragon asked as he joined her
rhunon gestured toward the sky where the light from the setting sun painted a tattered array of clouds red and purple and gold
it must be dark when we work the metal if we are to correctly judge its color
also the brightsteel needs time to cool so that it will be soft and easy to shape
reaching around behind her head rhunon undid the cord that held back her hair then gathered up her hair again and retied the cord
in the meantime let us talk about your sword
how do you fight with one hand or two
eragon thought for a minute then said it varies
if i have a choice i prefer to wield a sword with one hand and carry a shield with my other
however circumstances have not always been favorable to me and i have often had to fight without a shield
then i like being able to grip the hilt with both hands so i can deliver a more powerful stroke
the pommel on zar roc was large enough to grasp with my left hand if i had to but the ridges around the ruby were uncomfortable and they did not afford me a secure hold
it would be nice to have a slightly longer hilt
i take it you do not want a true two handed sword said rhunon
eragon shook his head
no it would be too big for fighting indoors
that depends upon the size of the hilt and the blade combined but in general you are correct
would you be amenable to a hand and a half sword instead
an image flashed in eragon is mind of murtagh is original sword and he smiled
why not thought eragon
yes a hand and a half sword would be perfect i think
and how long would you like the blade
mmh
do you want a straight blade or a curved blade
have you any preferences as to the guard
crossing her arms rhunon sat with her chin touching her breastbone her eyes heavy lidded
her lips twitched
what of the width of the blade remember no matter how narrow it is the sword shall not break
perhaps it could be a little wider at the guard than zar roc was
i think it might look better
a harsh cracked laugh broke from rhunon is throat
but how would that improve the use of the sword
embarrassed eragon shifted on the bench at a loss for words
never ask me to alter a weapon merely in order to improve its appearance admonished rhunon
a weapon is a tool and if it is beautiful then it is beautiful because it is useful
a sword that could not fulfill its function would be ugly to my eyes no matter how fair its shape not even if it were adorned with the finest jewels and the most intricate ** the elf woman pursed her lips pushing them out as she thought
so a sword equally suited for the unrestrained bloodshed of a battlefield as it is for defending yourself in the narrow tunnels under farthen dur