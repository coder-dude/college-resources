i am not questioning your friendship eragon protested
but if what i said came to pass and my support might ensure that such a clan chief won the throne for the good of your people and for the good of the rest of alagaesia should not i back the dwarf who has the best chance of succeeding
in a deadly quiet voice orik said you swore a blood oath on the knurlnien eragon
by every law of our realm you are a member of durgrimst ingeitum no matter how greatly others may disapprove
what hrothgar did by adopting you has no precedent in all of our history and it cannot be undone unless as grimstborith i banish you from our clan
if you turn against me eragon you will shame me in front of our entire race and none will ever trust my leadership again
moreover you will prove to your detractors that we cannot trust a dragon rider
clan members do not betray each other to other clans eragon
it is not done not unless you wish to wake up one night with a dagger buried in your heart
are you threatening me asked eragon just as quietly
orik swore and banged his ax against the granite again
** i would never lift a hand against you ** you are my foster brother you are the only rider free of galbatorix is influence and blast it if i have not become fond of you during our travels together
but even though i would not harm you that does not mean the rest of the ingeitum would be so forbearing
i say that not as a threat but as a statement of fact
you must understand this eragon
if the clan hears you have given your support to another i may not be able to restrain them
even though you are our guest and the rules of hospitality protect you if you speak out against the ingeitum the clan will see you as having betrayed them and it is not our custom to allow traitors to remain within our midst
do you understand me eragon
what do you expect of me shouted eragon
he flung his arms outward and paced back and forth in front of orik
i swore an oath to nasuada as well and those were the orders she gave me
and you also pledged yourself to durgrimst ** roared orik
eragon stopped and stared at the dwarf
would you have me doom all of alagaesia just so you can maintain your standing among the clans
then do not ask the impossible of ** i will back you if it seems likely you can ascend to the throne and if not then i wo not
you worry about durgrimst ingeitum and your race as a whole while it is my duty to worry about them and all of alagaesia as ** eragon slumped against the cold trunk of a tree
and i cannot afford to offend you or your i mean our clan or the rest of dwarfdom
in a kinder tone orik said there is another way eragon
it would be more difficult for you but it would resolve your quandary
oh what wondrous solution would this be
sliding his ax back under his belt orik walked over to eragon grasped him by the forearms and gazed up at him through bushy eyebrows
trust me to do the right thing eragon shadeslayer
give me the same loyalty you would if you were indeed born of durgrimst ingeitum
those under me would never presume to speak out against their own grimstborith in favor of another clan
if a grimstborith strikes the rock wrong it is his responsibility alone but that does not mean i am oblivious to your ** he glanced down for a moment then said if i cannot be king trust me not to be so blinded by the prospect of power that i cannot recognize when my bid has failed
if that should happen not that i believe it shall then i will of my own volition lend my support to one of the other candidates for i have no more desire than you to see a grimstnzborith elected who is hostile to the varden
and if i should help promote another to the throne the status and prestige i will place at the service of that clan chief shall of its very nature include your own since you are ingeitum
will you trust me eragon will you accept me as your grimstborith as the rest of my hall sworn subjects do
eragon groaned and leaned his head against the rough tree and peered up at the crooked bone white branches wreathed in mist
trust
of all the things orik could have asked of him that was the most difficult to grant