late in the morning after they circumnavigated an especially broad mountain eragon saw a narrow valley tucked against its far side
the valley was so restricted it could easily be overlooked
the beartooth river which arya had mentioned flowed out of it and looped carelessly across the land
he smiled with relief that was where they needed to go
looking back eragon was alarmed to see that the distance between them and the urgals had shrunk to little more than a league
he pointed out the valley to murtagh
if we can slip in there without being seen it might confuse them
murtagh looked skeptical
it is worth a try
but they ve followed us easily enough so far
as they approached the valley they passed under the knotted branches of the beor mountains forest
the trees were tall with creviced bark that was almost black dull needles of the same color and knobby roots that rose from the soil like bare knees
cones littered the ground each the size of a horse is head
sable squirrels chattered from the treetops and eyes gleamed from holes in the trunks
green beards of tangled wolfsbane hung from the gnarled branches
the forest gave eragon an uneasy feeling the hair on the back of his neck prickled
there was something hostile in the air as if the trees resented their ** are very old said saphira touching a trunk with her nose
yes said eragon but not friendly
the forest grew denser the farther in they traveled
the lack of space forced saphira to take off with arya
without a clear trail to follow the tough underbrush slowed eragon and murtagh
the beartooth river wound next to them filling the air with the sound of gurgling water
a nearby peak obscured the sun casting them into premature dusk
at the valley is mouth eragon realized that although it looked like a slim gash between the peaks the valley was really as wide as many of the spine is vales
it was only the enormous size of the ridged and shadowy mountains that made it appear so confined
waterfalls dotted its sheer sides
the sky was reduced to a thin strip winding overhead mostly hidden by gray clouds
from the dank ground rose a clinging fog that chilled the air until their breath was visible
wild strawberries crawled among a carpet of mosses and ferns fighting for the meager sunlight
sprouting on piles of rotting wood were red and yellow toadstools
all was hushed and quiet sounds dampened by the heavy air
saphira landed by them in a nearby glade the rush of her wings strangely muted
she took in the view with a swing of her ** just passed a flock of birds that were black and green with red markings on their wings
i ve never seen birds like that before
everything in these mountains seems unusual replied ** you mind if i ride you awhile i want to keep an eye on the urgals
he turned to murtagh
the varden are hidden at the end of this valley
if we hurry we might get there before nightfall
murtagh grunted hands on his hips
how am i going to get out of here i do not see any valleys joining this one and the urgals are going to hem us in pretty soon
i need an escape route
do not worry about it said eragon impatiently
this is a long valley there is sure to be an exit further ** he released arya from saphira and lifted the elf onto snowfire
watch arya i am going to fly with saphira
we will meet you up ** he scrambled onto saphira is back and strapped himself onto her saddle
be careful murtagh warned his brow furrowed in thought then clucked to the horses and hurried back into the forest
as saphira jumped toward the sky eragon said do you think you could fly up to one of those peaks we might be able to spot our destination as well as a passage for murtagh
i do not want to listen to him griping through the entire valley
we can try agreed saphira but it will get much colder
hold on ** suddenly swooped straight up throwing him back in the saddle
her wings flapped strongly driving their weight upward
the valley shrank to a green line below them
the beartooth river shimmered like braided silver where light struck it
they rose to the cloud layer and icy moisture saturated the air