the tally astounded roran
he had not suspected the total was quite so large
a hoarse chuckle escaped him
a pity there are no more of them
another seven and i would have an even two hundred
the other men laughed as well
his thin face furrowed with concern carn reached for the bolt sticking out of roran is left shoulder saying here let me see to your wounds
** said roran and brushed him away
there may be others who are more seriously injured than i am
tend to them first
roran several of those cuts could prove fatal unless i stanch the bleeding
it wo not take but a
i am fine he growled
leave me alone
he did and averted his gaze
be quick about it ** roran stared into the featureless sky his mind empty of thought while carn pulled the bolt out of his shoulder and muttered various spells
in every spot where the magic took effect roran felt his skin itch and crawl followed by a blessed cessation of pain
when carn had finished roran still hurt but he did not hurt quite so badly and his mind was clearer than before
the healing left carn gray faced and shaking
he leaned against his knees until his tremors stopped
i will go
he paused for breath
go help the rest of the wounded ** he straightened and picked his way down the mound lurching from side to side as if he were drunk
roran watched him go concerned
then it occurred to him to wonder about the fate of the rest of their expedition
he looked toward the far side of the village and saw nothing but scattered bodies some clad in the red of the empire others in the brown wool favored by the varden
what of edric and sand he asked harald
i am sorry stronghammer but i saw nothing beyond the reach of my sword
calling to the few men who still stood on the roofs of the houses roran asked what of edric and sand
we do not know ** they replied
steadying himself with his hammer roran slowly picked his way down the tumbled ramp of bodies and with harald and three other men by his side crossed the clearing in the center of the village executing every soldier they found still alive
when they arrived at the edge of the clearing where the number of slain varden surpassed the number of slain soldiers harald banged his sword on his shield and shouted is anyone still alive
after a moment a voice came back at them from among the houses name **
harald and roran stronghammer and others of the varden
if you serve the empire then surrender for your comrades are dead and you cannot defeat **
from somewhere between the houses came a crash of falling metal and then in ones and twos warriors of the varden emerged from hiding and limped toward the clearing many of them supporting their wounded comrades
they appeared dazed and some were stained with so much blood roran at first mistook them for captured soldiers
he counted four and twenty men
among the final group of stragglers was edric helping along a man who had lost his right arm during the fighting
roran motioned and two of his men hurried to relieve edric of his burden
the captain straightened from under the weight
with slow steps he walked over to roran and looked him straight in the eye his expression unreadable
neither he nor roran moved and roran was aware that the clearing had grown exceptionally quiet
edric was the first to speak
how many of your men survived
he lives
what of sand
a soldier shot him during his charge
he died but a few minutes ** edric looked past roran then toward the mound of bodies
you defied my orders stronghammer
edric held out an open hand toward him
captain ** exclaimed harald stepping forward
if it were not for roran none of us would be standing here
and you should have seen what he did he slew nearly two hundred by **
harald is pleas made no impression on edric who continued to hold out his hand
roran remained impassive as well
turning to him then harald said roran you know the men are yours
just say the word and we will
roran silenced him with a glare
do not be a fool
between thin lips edric said at least you are not completely devoid of sense
harald keep your teeth shut unless you want to lead the packhorses the whole way back