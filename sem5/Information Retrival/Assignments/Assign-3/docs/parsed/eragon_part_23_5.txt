he could think of no rebuttal so he concentrated on controlling his stomach
saphira angled into a shallow dive and slowly approached the ground
although eragon is stomach lurched with every wobble he began to enjoy himself
he relaxed his arms a bit and stretched his neck back taking in the scenery
saphira let him enjoy the sights awhile then said let me show you what flying is really like
relax and do not be afraid she said
her mind tugged at his pulling him away from his body
eragon fought for a moment then surrendered control
his vision blurred and he found himself looking through saphira is eyes
everything was distorted colors had weird exotic tints blues were more prominent now while greens and reds were subdued
eragon tried to turn his head and body but could not
he felt like a ghost who had slipped out of the ether
pure joy radiated from saphira as she climbed into the sky
she loved this freedom to go anywhere
when they were high above the ground she looked back at eragon
he saw himself as she did hanging on to her with a blank look
he could feel her body strain against the air using updrafts to rise
all her muscles were like his own
he felt her tail swinging through the air like a giant rudder to correct her course
it surprised him how much she depended on it
their connection grew stronger until there was no distinction between their identities
they clasped their wings together and dived straight down like a spear thrown from on high
no terror of falling touched eragon engulfed as he was in saphira is exhilaration
the air rushed past their face
their tail whipped in the air and their joined minds reveled in the experience
even as they plummeted toward the ground there was no fear of collision
they snapped open their wings at just the right moment pulling out of the dive with their combined strength
slanting toward the sky they shot up and continued back over into a giant loop
as they leveled out their minds began to diverge becoming distinct personalities again
for a split second eragon felt both his body and saphira is
then his vision blurred and he again sat on her back
he gasped and collapsed on the saddle
it was minutes before his heart stopped hammering and his breathing calmed
once he had recovered he exclaimed that was ** how can you bear to land when you enjoy flying so much
i must eat she said with some ** i am glad that you took pleasure in it
those are spare words for such an experience
i am sorry i have not flown with you more i never thought it could be like that
do you always see so much blue
it is the way i am
we will fly together more often now
good she replied in a contented tone
they exchanged many thoughts as she flew talking as they had not for weeks
saphira showed eragon how she used hills and trees to hide and how she could conceal herself in the shadow of a cloud
they scouted the trail for brom which proved to be more arduous than eragon expected
they could not see the path unless saphira flew very close to it in which case she risked being detected
near midday an annoying buzz filled eragon is ears and he became aware of a strange pressure on his mind
he shook his head trying to get rid of it but the tension only grew stronger
brom is words about how people could break into others minds flashed through eragon is head and he frantically tried to clear his thoughts
he concentrated on one of saphira is scales and forced himself to ignore everything else
the pressure faded for a moment and then returned greater than ever
a sudden gust rocked saphira and eragon is concentration slipped
before he could marshal any defenses the force broke through
but instead of the invasive presence of another mind there were only the words what do you think you re doing get down here
i found something important
yes the old man said ** get that oversized lizard of yours to land
i am here
he sent a picture of his location
eragon quickly told saphira where to go and she banked toward the river below
meanwhile he strung his bow and drew several arrows
if there is trouble i will be ready for it