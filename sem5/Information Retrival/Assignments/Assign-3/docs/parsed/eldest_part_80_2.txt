brom also explains that twenty years ago he and jeod stole saphira is egg from galbatorix
in the process brom killed morzan first and last of the forsworn
only two other dragon eggs still exist both of which remain in galbatorix is possession
near dras leona the ra zac waylay eragon and his companions and brom is mortally wounded while protecting eragon
the ra zac are driven away by a mysterious young man named murtagh who says he is been tracking the ra zac
brom dies the following night
with his last breath he confesses that he was once a rider and his slain dragon was also named saphira
eragon buries brom in a tomb of sandstone which saphira transmutes into pure diamond
without brom eragon and saphira decide to join the varden
by ill chance eragon is captured at the city of gil ead and brought to the shade durza galbatorix is right hand man
with murtagh is help eragon escapes from prison bringing along with him the unconscious elf arya another captive
by this point eragon and murtagh have become great friends
with her mind arya tells eragon that she has been ferrying saphira is egg between the elves and the varden in the hopes that it might hatch for one of their children
however during her last trip she was ambushed by durza and forced to send the egg elsewhere with magic which is how it came to eragon
now arya is seriously wounded and requires the varden is medical help
using mental images she shows eragon how to find the rebels
an epic chase ensues
eragon and his friends traverse almost four hundred miles in eight days
they are pursued by a contingent of urgals who trap them in the towering beor mountains
murtagh who had not wanted to go to the varden is forced to tell eragon that he is the son of morzan
murtagh however has denounced his father is deeds and fled galbatorix is patronage to seek his own destiny
he shows eragon a great scar across his back inflicted when morzan threw his sword zar roc at him when he was just a child
thus eragon learns his sword once belonged to murtagh is father he who betrayed the riders to galbatorix and slaughtered many of his former comrades
just before they are overwhelmed by the urgals eragon and his friends are rescued by the varden who seem to appear out of the very stone
it turns out that the rebels are based in farthen dur a hollow mountain ten miles high and ten miles across
it is also home to the dwarves capital tronjheim
once inside eragon is taken to ajihad leader of the varden while murtagh is imprisoned because of his parentage
ajihad explains many things to eragon including that the varden elves and dwarves had agreed that when a new rider appeared he or she would initially be trained by brom and then sent to the elves to complete the instruction
eragon must now decide whether to follow this course
eragon meets with the dwarf king hrothgar and ajihad is daughter nasuada is tested by the twins two bald and rather nasty magicians who serve ajihad spars with arya once she has recovered and again encounters angela and solembum who have joined the varden
eragon and saphira also bless one of the varden is orphan babies
eragon is stay is disrupted by news of an urgal army approaching through the dwarves tunnels
in the battle that follows eragon is separated from saphira and forced to fight durza alone
far stronger than any human durza easily defeats eragon slashing open his back from shoulder to hip
at that moment saphira and arya break the roof of the chamber a sixty foot wide star sapphire distracting durza long enough for eragon to stab him through the heart
freed from durza is spells the urgals are driven back into the tunnels
while eragon lies unconscious after the battle he is telepathically contacted by a being who identifies himself as togira ikonoka the cripple who is whole
he offers answers to all of eragon is questions and urges eragon to seek him in ellesmera where the elves live
when eragon wakes he finds that despite angela is best efforts he has been left with a huge scar similar to murtagh is
dismayed he also realizes that he only slew durza through sheer luck and that he desperately needs more training