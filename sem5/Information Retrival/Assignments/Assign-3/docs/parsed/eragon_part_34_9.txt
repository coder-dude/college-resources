a man argued in a loud voice you can not go in ** the orders were clear no one is to see **
really will you be the one to die stopping me captain cut in a smooth voice
there was a subdued no
but the king
iwill handle the king interrupted the second person
now unlock the door
after a pause keys jangled outside eragon is cell
he tried to adopt a languorous ** have to act like i do not understand what is going on
i can not show surprise no matter what this person says
the door opened
his breath caught as he looked into the shade is face
it was like gazing at a death mask or a polished skull with skin pulled over it to give the appearance of life
greetings said the shade with a cold smile showing his filed teeth
i ve waited a long time to meet you
who who re you asked eragon slurring his words
no one of consequence answered the shade his maroon eyes alight with controlled menace
he sat with a flourish of his cloak
my name does not matter to one in your position
it would not mean a thing to you anyway
it is you that i am interested in
who are you
the question was posed innocently enough but eragon knew there had to be a catch or trap in it though it eluded him
he pretended to struggle over the question for a while then slowly said frowning i am not sure
m name is eragon but that is not all i am is it
the shade is narrow lips stretched tautly over his mouth as he laughed sharply
no it is not
you have an interesting mind my young ** he leaned forward
the skin on his forehead was thin and translucent
it seems i must be more direct
what is your name
** not that ** the shade cut him off with a wave of his hand
do not you have another one one that you use only rarely
he wants my true name so he can control ** ** i can not tell him
i do not even know it myself
he thought quickly trying to invent a deception that would conceal his ** if i made up a name he hesitated it could easily give him away then raced to create a name that would withstand scrutiny
as he was about to utter it he decided to take a chance and try to scare the shade
he deftly switched a few letters then nodded foolishly and said brom told it to me once
it was
the pause stretched for a few seconds then his face brightened as he appeared to remember
it was du sundavar ** which meant almost literally death of the shadows
a grim chill settled over the cell as the shade sat motionless eyes veiled
he seemed to be deep in thought pondering what he had learned
eragon wondered if he had dared too much
he waited until the shade stirred before asking ingenuously why are you here
the shade looked at him with contempt in his red eyes and smiled
to gloat of course
what use is a victory if one cannot enjoy it there was confidence in his voice but he seemed uneasy as if his plans had been disrupted
he stood suddenly
i must attend to certain matters but while i am gone you would do well to think on who you would rather serve a rider who betrayed your own order or a fellow man like me though one skilled in arcane arts
when the time comes to choose there will be no middle ** he turned to leave then glanced at eragon is water pitcher and stopped his face granite hard
** he snapped
a broad shouldered man rushed into the cell sword in hand
what is it my lord he asked alarmed
put that toy away instructed the shade
he turned to eragon and said in a deadly quiet voice the boy has not been drinking his water
why is that
i talked with the jailer earlier
every bowl and plate was scraped clean
very well said the shade mollified
but make sure that he starts drinking ** he leaned toward the captain and murmured into his ear
eragon caught the last few words
extra dose just in ** the captain nodded
the shade returned his attention to eragon
we will talk again tomorrow when i am not so pressed for time
you should know i have an endless fascination for names
i will greatly enjoy discussing yours inmuch greater detail
the way he said it gave eragon a sinking feeling
once they left he lay on the cot and closed his eyes
brom is lessons proved their worth now he relied on them to keep himself from panicking and to reassure ** has been provided for me i only have to take advantage of it