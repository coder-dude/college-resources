but would that be enough to resist the combined power of the twins
do not worry so much i will help you said ** are two of us as well
he touched her gently on the leg relieved by her words
the twins looked at eragon and asked and how do you answer us eragon
overlooking the puzzled expressions of his companions he said flatly no
sharp lines appeared at the corners of the twins mouths
they turned so they faced eragon obliquely and bending at the waists drew a large pentagram on the ground
they stepped in the middle of it then said harshly we begin now
you will attempt to complete the tasks we assign you
that is all
one of the twins reached into his robe produced a polished rock the size of eragon is fist and set it on the ground
lift it to eye level
that is easy enough commented eragon to saphira
stenr ** the rock wobbled then smoothly rose from the ground
before it went more than a foot an unexpected resistance halted it in midair
a smile touched the twins lips
eragon stared at them enraged they were trying to make him ** if he became exhausted now it would be impossible to complete the harder tasks
obviously they were confident that their combined strength could easily wear him down
but i am not alone either snarled eragon to ** ** her mind melded with his and the rock jerked through the air to stop quivering at eye level
the twins eyes narrowed cruelly
very
good they hissed
fredric looked unnerved by the display of magic
now move the stone in a ** again eragon struggled against their efforts to stop him and again to their obvious anger he prevailed
the exercises quickly increased in complexity and difficulty until eragon was forced to think carefully about which words to use
and each time the twins fought him bitterly though the strain never showed on their faces
it was only with saphira is support that eragon was able to hold his ground
in a break between two of the tasks he asked her why do they continue this testing our abilities were clear enough from what they saw in my mind
she cocked her head ** know what he said grimly as comprehension came to ** re using this as an opportunity to figure out what ancient words i know and perhaps learn new ones themselves
speak softly then so that they cannot hear you and use the simplest words possible
from then on eragon used only a handful of basic words to complete the tasks
but finding ways to make them perform in the same manner as a long sentence or phrase stretched his ingenuity to the limit
he was rewarded by the frustration that contorted the twins faces as he foiled them again and again
no matter what they tried they could not get him to use any more words in the ancient language
more than an hour passed but the twins showed no sign of stopping
eragon was hot and thirsty but refrained from asking for a reprieve he would continue as long as they did
there were many tests manipulating water casting fire scrying juggling rocks hardening leather freezing items controlling the flight of an arrow and healing scratches
he wondered how long it would take for the twins to run out of ideas
finally the twins raised their hands and said there is only one thing left to do
it is simple enough anycompetent user of magic should find this ** one of them removed a silver ring from his finger and smugly handed it to eragon
summon the essence of silver
eragon stared at the ring in confusion
what was he supposed to do the essence of silver what was that and how was it to be summoned saphira had no idea and the twins were not going to help
he had never learned silver is name in the ancient language though he knew it had to be part ofargetlam
in desperation he combined the only word that might work ethgri or invoke witharget
drawing himself upright he gathered together what power he had left and parted his lips to deliver the invocation
suddenly a clear vibrant voice split the air
the word rushed over eragon like cool water the voice was strangely familiar like a half remembered melody
the back of his neck tingled
he slowly turned toward its source