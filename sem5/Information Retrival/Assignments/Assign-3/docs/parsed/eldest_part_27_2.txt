with our help palancar was usurped and banished but he his family and their vassals refused to leave the valley
since we had no wish to murder them we constructed the tower of ristvak baen so the riders could watch over palancar and ensure he would never again rise to power or attack anyone else in alagaesia
before long palancar was killed by a son who did not wish to wait for nature to take its course
thereafter family politics consisted of assassination betrayal and other depravities reducing palancar is house to a shadow of its former grandeur
however his descendants never left and the blood of kings still runs in therinsford and carvahall
lifaen lifted one dark eyebrow
do you it has more significance than you may think
it was this event that convinced anurin vrael is predecessor as head rider to allow humans to become riders in order to prevent similar disputes
orik emitted a bark of laughter
that must have caused some argument
it was an unpopular decision admitted lifaen
even now some question the wisdom of it
it caused such a disagreement between anurin and queen dellanir that anurin seceded from our government and established the riders on vroengard as an independent entity
but if the riders were separated from your government then how could they keep the peace as they were supposed to asked eragon
they could not said lifaen
not until queen dellanir saw the wisdom of having the riders free of any lord or king and restored their access to du weldenvarden
still it never pleased her that any authority could supersede her own
eragon frowned
was not that the whole point though
yes
and no
the riders were supposed to guard against the failings of the different governments and races yet who watched the watchers it was that very problem that caused the fall
no one existed who could descry the flaws within the riders own system for they were above scrutiny and thus they perished
eragon stroked the water first on one side and then the other while he considered lifaen is words
his paddle fluttered in his hands as it cut diagonally across the current
who succeeded dellanir as king or queen
evandar did
he took the knotted throne five hundred years ago when dellanir abdicated in order to study the mysteries of magic and held it until his death
now his mate islanzadi rules us
that is eragon stopped with his mouth open
he was going to sayimpossible but then realized how ridiculous the statement would sound
instead he asked are elves immortal
in a soft voice lifaen said once we were like you bright fleeting and as ephemeral as the morning dew
now our lives stretch endlessly through the dusty years
aye we are immortal although we are still vulnerable to injuries of the flesh
youbecame immortal how the elf refused to elaborate though eragon pressed him for details
finally eragon asked how old is arya
lifaen turned his glittering eyes on him probing eragon with disconcerting acuteness
arya what is your interest in her
i
eragon faltered suddenly unsure of his intentions
his attraction to arya was complicated by the fact that she was an elf and that her age whatever it might be was so much greater than his ** must view me as a child
i do not know he said honestly
but she saved both my life and saphira is and i am curious to know more about her
i feel ashamed said lifaen pronouncing each word carefully for asking such a question
among our kind it is rude to pry into one is affairs
only i must say and i believe that orik agrees with me that you would do well to guard your heart argetlam
now is not the time to lose it nor would it be well placed in this instance
heat suffused eragon as blood rushed to his face like hot tallow melting through him
before he could utter a retort saphira entered his mind and said and now is the time to guard your tongue
they mean well
do not insult them
he took a deep breath and tried to let his embarrassment drain ** you agree with them
i believe eragon that you are full of love and that you are looking for one who will reciprocate your affection
no shame exists in that