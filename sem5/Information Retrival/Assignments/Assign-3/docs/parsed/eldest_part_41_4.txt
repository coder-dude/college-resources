since their enemies might be beyond their mental reach magicians also erect wards around themselves and their warriors to stop or lessen long range attacks such as a pebble sent flying toward their head from a mile away
surely one man can not defend an entire army said eragon
not alone but with enough magicians you can provide a reasonable amount of protection
the greatest danger in this sort of conflict is that a clever magician may think of a unique attack that can bypass your wards without tripping them
that itself could be enough to decide a battle
also said oromis you must keep in mind that the ability to use magic is exceedingly rare among the races
we elves are no exception although we have a greater allotment of spellweavers than most as a result of oaths we bound ourselves with centuries ago
the majority of those blessed with magic have little or no appreciable talent they struggle to heal even so much as a bruise
eragon nodded
he had encountered magicians like that in the varden
but it still takes the same amount of energy to accomplish a task
energy yes but lesser magicians find it harder than you or i do to feel the flow of magic and immerse themselves in it
few magicians are strong enough to pose a threat to an entire army
and those who are usually spend the bulk of their time during battles evading tracking or fighting their opposites which is fortunate from the standpoint of ordinary warriors else they would all soon be killed
troubled eragon said the varden do not have many magicians
that is one reason why you are so important
a moment passed as eragon reflected on what oromis had told him
these wards do they only drain energy from you when they are activated
then given enough time you could acquire countless layers of wards
you could make yourself
he struggled with the ancient language as he attempted to express himself
untouchable
impregnable
impregnable to any assault magical or physical
wards said oromis rely upon the strength of your body
if that strength is exceeded you die
no matter how many wards you have you will only be able to block attacks so long as your body can sustain the output of energy
and galbatorix is strength has been increasing each year
how is that possible
it was a rhetorical question yet when oromis remained silent his almond eyes fixed on a trio of swallows pirouetting overhead eragon realized that the elf was considering how best to answer him
the birds chased each other for several minutes
when they flitted from view oromis said it is not appropriate to have this discussion at the present
then you know exclaimed eragon astonished
i do
but that information must wait until later in your training
you are not ready for ** oromis looked at eragon as if expecting him to object
eragon bowed
as you wish ** he could never prize the information out of oromis until the elf was willing to share it so why try still he wondered what could be so dangerous that oromis dared not tell him and why the elves had kept it secret from the varden
another thought presented itself to him and he said if battles with magicians are conducted like you said then why did ajihad let me fight without wards in farthen dur i did not even know that i needed to keep my mind open for enemies
and why did not arya kill most or all of the urgals no magicians were there to oppose her except for durza and he could not have defended his troops when he was underground
did not ajihad have arya or one of du vrangr gata set defenses around you demanded oromis
oromis is eyes unfocused withdrawing into himself as he stood motionless on the greensward
he spoke without warning i have consulted arya and she says that the twins of the varden were ordered to assess your abilities
they told ajihad you were competent in all magic including wards
neither ajihad nor arya doubted their judgment on that matter
those smooth tongued bald pated tick infested treacherous dogs swore eragon
they tried to get me ** reverting to his own language he indulged in several more pungent oaths
do not befoul the air said oromis mildly