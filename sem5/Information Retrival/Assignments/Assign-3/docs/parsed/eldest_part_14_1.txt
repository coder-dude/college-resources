after roran agreed to their plan horst began distributing shovels pitchforks flails anything that could be used to beat the soldiers and the ra zac away
roran hefted a pick then set it aside
though he had never cared for brom is stories one of them the song of gerand resonated with him whenever he heard it
it told of gerand the greatest warrior of his time who relinquished his sword for a wife and farm
he found no peace however as a jealous lord initiated a blood feud against gerand is family which forced gerand to kill once more
yet he did not fight with his blade but with a simple hammer
going to the wall roran removed a medium sized hammer with a long handle and a rounded blade on one side of the head
he tossed it from hand to hand then went to horst and asked may i have this
horst eyed the tool and roran
use it ** then he said to the rest of the group listen
we want to scare not kill
break a few bones if you want but do not get carried away
and whatever you do do not stand and fight
no matter how brave or heroic you feel remember that they are trained soldiers
when everyone was equipped they left the forge and wound their way through carvahall to the edge of the ra zac is camp
the soldiers had already gone to bed except for four sentries who patrolled the perimeter of the gray tents
the ra zac is two horses were picketed by a smoldering fire
horst quietly issued orders sending albriech and delwin to ambush two of the sentries and parr and roran to ambush the other two
roran held his breath as he stalked the oblivious soldier
his heart began to shudder as energy spiked through his limbs
he hid behind the corner of a house quivering and waited for horst is **
with a roar horst burst from hiding leading the charge into the tents
roran darted forward and swung his hammer catching the sentry on the shoulder with a grisly crunch
the man howled and dropped his halberd
he staggered as roran struck his ribs and back
roran raised the hammer again and the man retreated screaming for help
roran ran after him shouting incoherently
he knocked in the side of a wool tent trampling whatever was inside then smashed the top of a helmet he saw emerging from another tent
the metal rang like a bell
roran barely noticed as loring danced past the old man cackled and hooted in the night as he jabbed the soldiers with a pitchfork
everywhere was a confusion of struggling bodies
whirling around roran saw a soldier attempting to string his bow
he rushed forward and hit the back of the bow with his steel mallet breaking the wood in two
the soldier fled
the ra zac scrambled free of their tent with terrible screeches swords in hand
before they could attack baldor untethered the horses and sent them galloping toward the two scarecrow figures
the ra zac separated then regrouped only to be swept away as the soldiers morale broke and they ran
roran panted in the silence his hand cramped around the hammer is handle
after a moment he picked his way through the crumpled mounds of tents and blankets to horst
the smith was grinning under his beard
that is the best brawl i ve had in years
behind them carvahall jumped to life as people tried to discover the source of the commotion
roran watched lamps flare up behind shuttered windows then turned as he heard soft sobbing
the boy nolfavrell was kneeling by the body of a soldier methodically stabbing him in the chest as tears slid down his chin
gedric and albriech hurried over and pulled nolfavrell away from the corpse
he should not have come said roran
horst shrugged
it was his right
all the same killing one of the ra zac is men will only make it harder to rid ourselves of the ** we should barricade the road and between the houses so they wo not catch us by ** studying the men for any injuries roran saw that delwin had received a long cut on his forearm which the farmer bandaged with a strip torn from his ruined shirt
with a few shouts horst organized their group
he dispatched albriech and baldor to retrieve quimby is wagon from the forge and had loring is sons and parr scour carvahall for items that could be used to secure the village