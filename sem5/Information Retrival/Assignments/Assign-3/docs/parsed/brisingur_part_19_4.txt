met him why i did better than ** i was his apprentice for
for an unfortunate number of years
eragon had never expected angela to willingly reveal anything about her past
eager to learn more he asked when did you meet him and where
long ago and far away
however we parted badly and i have not seen him for many many ** angela frowned
in fact i thought he was already dead
saphira spoke then saying since you were tenga is apprentice do you know what question he is trying to answer
i have not the slightest idea
tenga always had a question he was trying to answer
if he succeeded he immediately chose another one and so on
he may have answered a hundred questions since i last saw him or he may still be gnashing his teeth over the same conundrum as when i left him
whether the phases of the moon influence the number and quality of the opals that form in the roots of the beor mountains as is commonly held among the dwarves
but how could you prove that objected eragon
angela shrugged
if anyone could it would be tenga
he may be deranged but his brilliance is none the less for it
he is a man who kicks at cats said solembum as if that summed up tenga is entire character
then angela clapped her hands together and said no ** eat your sweet eragon and let us go to nasuada