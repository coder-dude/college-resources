what a pretty thing for one so
insignificant
maybe i will keep ** he leaned closer and sneered or maybe if you behave our master will let you polish ** his moist breath smelled like raw meat
then he turned the sword over in his hands and screeched as he saw the symbol on the scabbard
his companion rushed over
they stood over the sword hissing and clicking
at last they faced eragon
you will serve our master very well yesss
eragon forced his thick tongue to form words if i do i will kill you
they chuckled coldly
oh no we are too valuable
but you
you aredisposable
a deep snarl came from saphira smoke roiled from her nostrils
the ra zac did not seem to care
their attention was diverted when brom groaned and rolled onto his side
one of the ra zac grabbed his shirt and thrust him effortlessly into the air
it isss wearing off
let isss just kill him said the shorter ra zac
he has caused us much grief
the taller one ran his finger down his sword
a good plan
but remember the king is instructions were to keep themalive
we can sssay he was killed when we captured them
and what of thisss one the ra zac asked pointing his sword at eragon
if he talksss
his companion laughed and drew a wicked dagger
he would not dare
there was a long silence then agreed
they dragged brom to the center of the camp and shoved him to his knees
brom sagged to one side
eragon watched with growing ** have to get ** he wrenched at the ropes but they were too strong to break
none of that now said the tall ra zac poking him with a sword
he nosed the air and sniffed something seemed to trouble him
the other ra zac growled yanked brom is head back and swept the dagger toward his exposed throat
at that very moment a low buzz sounded followed by the ra zac is howl
an arrow protruded from his shoulder
the ra zac nearest eragon dropped to the ground barely avoiding a second arrow
he scuttled to his wounded companion and they glared into the darkness hissing angrily
they made no move to stop brom as he blearily staggered upright
get ** cried eragon
brom wavered then tottered toward eragon
as more arrows hissed into the camp from the unseen attackers the ra zac rolled behind some boulders
there was a lull then arrows came from the opposite direction
caught by surprise the ra zac reacted slowly
their cloaks were pierced in several places and a shattered arrow buried itself in one is arm
with a wild cry the smaller ra zac fled toward the road kicking eragon viciously in the side as he passed
his companion hesitated then grabbed the dagger from the ground and raced after him
as he left the camp he hurled the knife at eragon
a strange light suddenly burned in brom is eyes
he threw himself in front of eragon his mouth open in a soundless snarl
the dagger struck him with a soft thump and he landed heavily on his shoulder
his head lolled limply
** screamed eragon though he was doubled over in pain
he heard footsteps then his eyes closed and he knew no more