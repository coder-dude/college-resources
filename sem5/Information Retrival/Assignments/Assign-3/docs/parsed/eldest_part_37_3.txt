other strictures apply but in general yes
lady why do you ask these are basic principles of magic that while not commonly bandied about i am sure you are familiar with
i am
i wished to ensure that i understood them ** without moving from her chair nasuada reached down and lifted the overgown so that trianna could see the mutilated lace
so then within those limits you should be able to devise a spell that will allow you to manufacture lace with magic
a condescending sneer distorted the sorceress is dark lips
du vrangr gata has more important duties than repairing your clothes lady
our art is not so common as to be employed for mere whims
i am sure that you will find your seamstresses and tailors more than capable of fulfilling your request
now if you will excuse me i
be quiet woman said nasuada in a flat voice
astonishment muted trianna in midsentence
i see that i must teach du vrangr gata the same lesson that i taught the council of elders i may be young but i am no child to be patronized
i ask about lace because if you can manufacture it quickly and easily with magic then we can support the varden by selling inexpensive bobbin and needle lace throughout the empire
galbatorix is own people will provide the funds we need to survive
but that is ridiculous protested trianna
even farica looked skeptical
you can not pay for a war withlace
nasuada raised an eyebrow
why not women who otherwise could never afford to own lace will leap at the chance to buy ours
every farmer is wife who longs to appear richer than she is will want it
even wealthy merchants and nobles will give us their gold because our lace will be finer than any thrown or stitched by human hands
we will garner a fortune to rival the dwarves
that is if you are skilled enough in magic to do what i want
trianna tossed her hair
you doubt my abilities
trianna hesitated then took the overgown from nasuada and studied the lace strip for a long while
at last she said it should be possible but i will have to conduct some tests before i know for certain
do so immediately
from now on this is your most important assignment
and find an experienced lace maker to advise you on the patterns
nasuada allowed her voice to soften
good
i also want you to select the brightest members of du vrangr gata and work with them to invent other magical techniques that will help the varden
that is your responsibility not mine
nowyou are excused
report back to me tomorrow morning
satisfied nasuada watched the sorceress depart then closed her eyes and allowed herself to enjoy a moment of pride for what she had accomplished
she knew that no man not even her father would have thought of her solution
this ismy contribution to the varden she told herself wishing that ajihad could witness it
louder she asked did i surprise you farica