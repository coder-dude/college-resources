across from roran two of the men halmar and ferth were sitting in front of their tent and halmar was telling ferth
so when the soldiers came for him he pulled all his men inside his estate and set fire to the pools of oil his servants had poured earlier trapping the soldiers and making it appear to those who came later as if the whole lot of them had burned to death
can you believe it five hundred soldiers he killed at one go without even drawing a **
how did he escape ferth asked
redbeard is grandfather was a cunning bastard he was
he had a tunnel dug all the way from the family hall to the nearest river
with it redbeard was able to get his family and all their servants out alive
he took them to surda then where king larkin sheltered them
it was quite a number of years before galbatorix learned they were still alive
we re lucky to be under redbeard to be sure
he is lost only two battles and those because of magic
halmar fell silent as ulhart stepped into the middle of the row of sixteen tents
the grim faced veteran stood with his legs spread immovable as a deep rooted oak tree and surveyed the tents checking that everyone was present
he said sun is down get to sleep
we ride out two hours before first light
convoy should be seven miles northwest of us
make good time we strike just as they start moving
kill everyone burn everything an we go back
you know how it goes
stronghammer you ride with me
mess up an i will gut you with a dull ** the men chuckled
right get to sleep
raising his hammer overhead roran howled with all his might
the two soldiers started and fumbled with their weapons and shields
one of them dropped his spear and bent to recover it
pulling on snowfire is reins to slow him roran stood upright in his stirrups and drawing abreast of the first soldier struck him on the shoulder splitting his mail hauberk
the man screamed his arm going limp
roran finished him off with a backhand blow
the other soldier had retrieved his spear and he jabbed at roran aiming at his neck
roran ducked behind his round shield the spear jarring him each time it buried itself in the wood
he pressed his legs against snowfire is sides and the stallion reared neighing and pawing at the air with iron shod hooves
one hoof caught the soldier in the chest tearing his red tunic
as snowfire dropped to all fours again roran swung his hammer sideways and crushed the man is throat
leaving the soldier thrashing on the ground roran spurred snowfire toward the next wagon in the convoy where ulhart was battling three soldiers of his own
four oxen pulled each wagon and as snowfire passed the wagon roran had just captured the lead ox tossed his head and the tip of his left horn caught roran in the lower part of his right leg
roran gasped
he felt as if a red hot iron had been laid against his shin
he glanced down and saw a flap of his boot hanging loose along with a layer of his skin and muscle
with another battle cry roran rode up to the closest of the three soldiers ulhart was fighting and felled him with a single swipe of his hammer
the next man evaded roran is subsequent attack then turned his horse and galloped away
get ** ulhart shouted but roran was already in pursuit
the fleeing soldier dug his spurs into the belly of his horse until the animal bled but despite his desperate cruelty his steed could not outrun snowfire
roran bent low over snowfire is neck as the stallion extended himself flying over the ground with incredible speed
realizing flight was hopeless the soldier reined in his mount wheeled about and slashed at roran with a saber
roran lifted his hammer and barely managed to deflect the razor sharp blade
he immediately retaliated with a looping overhead attack but the soldier parried and then slashed at roran is arms and legs twice more
in his mind roran cursed
the soldier was obviously more experienced with swordplay than he was if he could not win the engagement in the next few seconds the soldier would kill him
the soldier must have sensed his advantage for he pressed the attack forcing snowfire to prance backward