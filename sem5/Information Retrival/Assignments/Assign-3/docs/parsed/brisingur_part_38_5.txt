now we must rely upon honest blade work to dispose of them
there are too few of us left to attack the soldiers ** protested roran
they outnumber us better than three to one
then we shall make up in valor what we lack in ** edric bellowed
i was told you had courage stronghammer but obviously rumor is mistaken and you are as timid as a frightened rabbit
now do as you re told and do not question me ** the captain indicated one of roran is warriors
you there lend me your ** after the man dismounted edric pulled himself into the saddle and said half of you on horse follow me i go to reinforce sand
every one else remain with ** kicking his mount in the sides edric galloped away with the men who chose to follow him racing from building to building as they worked their way around the soldiers clumped in the center of the village
roran shook with fury as he watched them depart
never before had he allowed anyone to question his courage without answering his critic with words or blows
so long as the battle persisted however it would be inappropriate for him to confront edric
very well roran thought i shall demonstrate to edric the courage he thinks i lack
but that is all he shall have from me
i will not send the archers to fight the soldiers face to face when they are safer and more effective where they are
roran turned and inspected the men edric had left to him
among those they had rescued roran was delighted to see carn who was scratched and bloody but on the whole unharmed
they nodded to each other and then roran addressed the group you have heard what edric said
i disagree
if we do as he wishes all of us will end up piled in a cairn before sunset
we can still win this battle but not by marching to our own ** what we lack in numbers we can make up with cunning
you know how i came to join the varden
you know i have fought and defeated the empire before and in just such a ** this i can do i swear to you
but i cannot do it alone
will you follow me think carefully
i will claim responsibility for ignoring edric is orders but he and nasuada may still punish everyone who was involved
then they would be fools growled carn
would they prefer that we died here no i think not
you may count on me roran
as carn made his declaration roran saw how the other men squared their shoulders and set their jaws and how their eyes burned with renewed determination and he knew they had decided to cast their lot with him if only because they would not want to be parted from the only magician in their company
many was the warrior of the varden who owed his life to a member of du vrangr gata and the men at arms roran had met would sooner stab themselves in a foot than go into battle without a spellcaster close at hand
aye said harald
you may count on us as well stronghammer
then follow ** said roran
reaching down he pulled carn up onto snowfire behind himself then hurried with his group back around the village to where the bowmen on the roofs continued to fire arrows at the soldiers
as roran and the men with him dashed from house to house quarrels buzzed past them sounding like giant angry insects and one even buried itself halfway through harald is shield
once they were safely behind cover roran had the men who were still mounted give their bows and arrows to the men on foot whom he then sent to climb the houses and join the other archers
as they scrambled to obey him roran beckoned to carn who had jumped off snowfire the moment they ceased moving and said i need a spell of you
can you shield me and ten others from these bolts
a minute an hour who knows
shielding that many people from more than a handful of bolts would soon exceed the bounds of my strength
although if you do not care if i stop the bolts in their tracks i could deflect them from you which
who exactly do you want me to protect
roran pointed at the men he had picked to join him and carn asked each of them their names
standing with his shoulders hunched inward carn began to mutter in the ancient language his face pale and strained
three times he tried to cast the spell and three times he failed