nasuada reasoned with her further but as elva had promised it proved to be a futile prospect
at last nasuada asked angela eragon and saphira to intervene
angela refused on the grounds that she could not improve on nasuada is words and that she believed elva is choice was a personal one and therefore the girl ought to be able to do as she wished without being harried like an eagle by a flock of jays
eragon was of a similar opinion but he consented to say elva i cannot tell you what you should do only you can determine that but do not reject nasuada is request out of hand
she is trying to save us all from galbatorix and she needs our support if we are to have any chance of success
the future is hidden to me but i believe that your ability might be the perfect weapon against galbatorix
you could predict his every attack
you could tell us exactly how to counteract his wards
and above all else you would be able to sense where galbatorix is vulnerable where he is most weak and what we could do to hurt him
you will have to do better than that rider if you want to change my mind
i do not want to change your mind said eragon
i only want to make sure you have given due consideration to the implications of your decision and that you are not being overly hasty
the girl shifted but did not respond
then saphira asked what is in your heart o shining brow
elva answered in a soft tone with no trace of malice
i have spoken my heart saphira
any other words would be redundant
if nasuada was frustrated by elva is obstinacy she did not allow it to show although her expression was stern as befitted the discussion
she said i do not agree with your choice elva but we will abide by it for it is obvious that we cannot sway you
i suppose i cannot fault you as i have no experience with the suffering you are exposed to on a daily basis and if i were in your position it is possible i would act no differently
eragon if you will
at her bidding eragon knelt in front of elva
her lustrous violet eyes bored into him as he placed her small hands between his larger ones
her flesh burned against his as if she had a fever
will it hurt shadeslayer greta asked the old woman is voice quavering
it should not but i do not know for sure
removing spells is a much more inexact art than casting them
magicians rarely if ever attempt it because of the challenges it poses
the wrinkles on her face contorted with worry greta patted elva on the head saying oh be brave my plum
be ** she did not seem to notice the look of irritation elva directed at her
eragon ignored the interruption
elva listen to me
there are two different methods for breaking an enchantment
one is for the magician who originally cast the spell to open himself to the energy that fuels our magic
that is the part i always had difficulty with said angela
it is why i rely more upon potions and plants and objects that are magical in and of themselves than upon incantations
if you do not mind
her cheeks dimpling angela said i am sorry
proceed
right growled eragon
one is for the original magician to open himself
will you please let me finish
eragon saw nasuada fight back a smile
he opens himself to the flow of energy within his body and speaking in the ancient language recants not only the words of his spell but also the intention behind it
this can be quite difficult as you might imagine
unless the magician has the right intent he will end up altering the original spell instead of lifting it
and then he would have to unsay two intertwined spells
the other method is to cast a spell that directly counteracts the effects of the original spell
it does not eliminate the original spell but if done properly it renders it harmless
with your permission this is the method i intend to use
a most elegant solution angela proclaimed but who pray tell provides the continuous stream of energy needed to maintain this counterspell and since someone must ask what can go wrong with this particular method
eragon kept his gaze fixed on elva
the energy will have to come from you he told her pressing her hands with his