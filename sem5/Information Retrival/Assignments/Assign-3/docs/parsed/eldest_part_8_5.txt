the chiefs had to decide whether to accept nasuada or look for another candidate
most have concluded that nasuada should lead the varden but i wish to know where you stand on this eragon before i lend my word to either side
the worst thing a king can do is look foolish
how much can we tell him eragon asked saphira thinking quickly
he is always treated us fairly but we can not know what he may have promised other people
we d best be cautious until nasuada actually takes power
saphira and i have agreed to help her
we wo not oppose her ascension
and eragon wondered if he was going too far i plead that you do the same the varden can not afford to fight among themselves
they need unity
oei said hrothgar leaning back you speak with new authority
your suggestion is a good one but it will cost a question do you think nasuada will be a wise leader or are there other motives in choosing her
it is a test warned ** wants to know whywe ve backed her
eragon felt his lips twitch in a half smile
i think her wise and canny beyond her years
she will be good for the varden
and that is why you support her
hrothgar nodded dipping his long snowy beard
that relieves me
there has been too little concern lately with what is right and good and more about what will bring individual power
it is hard to watch such idiocy and not be angry
an uncomfortable silence fell between them stifling in the long throne room
to break it eragon asked what will be done with the dragonhold will a new floor be laid down
for the first time the king is eyes grew mournful deepening the surrounding lines that splayed like spokes on a wagon wheel
it was the closest eragon had ever seen a dwarf come to weeping
much talk is needed before that step can be taken
it was a terrible deed what saphira and arya did
maybe necessary but terrible
ah it might have been better if the urgals had overrun us before isidar mithrim was ever broken
the heart of tronjheim has been shattered and so has ** hrothgar placed his fist over his breast then slowly unclenched his hand and reached down to grasp volund is leather wrapped handle
saphira touched eragon is mind
he sensed several emotions in her but what surprised him the most was her remorse and guilt
she truly regretted the star rose is demise despite the fact that it had been ** one she said help me
i need to speak with hrothgar
ask him do the dwarves have the ability to reconstruct isidar mithrim out of the shards
as he repeated the words hrothgar muttered something in his own language then said the skill we have but what of it the task would take months or years and the end result would be a ruined mockery of the beauty that once graced ** it is an abomination i will not sanction
saphira continued to stare unblinkingly at the ** tell him if isidar mithrim were put together again with not one piece missing i believe i could make it whole once more
eragon gaped at her forgetting hrothgar in his ** the energy that would ** you told me yourself that you can not use magic at will so what makes you sure you can do this
i can do it if the need is great enough
it will be my gift to the dwarves
remember brom is tomb let that wash your doubt away
and close your mouth it is unbecoming and the king is watching
when eragon conveyed saphira is offer hrothgar straightened with an exclamation
is it possible not even the elves might attempt such a feat
she is confident in her abilities
then we will rebuild isidar mithrim no matter if it takes a hundred years
we will assemble a frame for the gem and set each piece into its original place
not a single chip will be forgotten
even if we must break the larger pieces to move them it will be done with all our skill in working stone so that no dust or flecks are lost
you will come then when we are finished and heal the star rose
we will come agreed eragon bowing
hrothgar smiled and it was like the cracking of a granite wall
such joy you have given me saphira
i feel once more a reason to rule and live
if you do this dwarves everywhere will honor your name for uncounted generations