even under the best of circumstances remnants of your magic will haunt this girl evermore
such is the power of the ancient ** he paused
i see that you understand the gravity of the situation so i will say this only once you bear full responsibility for this girl is doom and because of the wrong you did her it is incumbent upon you to help her if ever the opportunity should arise
by the riders law she is your shame as surely as if you had begotten her out of wedlock a disgrace among humans if i remember correctly
aye whispered eragon
i ** i understand that i forced a defenseless baby to pursue a certain destiny without ever giving her a choice in the matter
can someone be truly good if they never have the opportunity to act badly i made her a slave
he also knew that if he had been bound in that manner without permission he would hate his jailer with every fiber of his being
then we will speak of this no more
eragon was still subdued even depressed by the end of the day
he barely looked up when they went outside to meet saphira and glaedr upon their return
the trees shook from the fury of the gale that the two dragons created with their wings
saphira seemed proud of herself she arched her neck and pranced toward eragon opening her chops in a lupine grin
a stone cracked under glaedr is weight as the ancient dragon turned a giant eye as large as a dinner platter on eragon and asked what are the rules three to spotting downdrafts and the rules five for escaping them
startled out of his reverie eragon could only blink dumbly
i do not know
then oromis confronted saphira and asked what creatures do ants farm and how do they extract food from them
i would not know declared saphira
she sounded affronted
a gleam of anger leaped into oromis is eyes and he crossed his arms though his expression remained calm
after all the two of you have done together i would think that you had learned the most basic lesson of being shur tugal share everything with your partner
would you cut off your right arm would you fly with only one wing never
then why would you ignore the bond that links you by doing so you reject your greatest gift and your advantage over any single opponent
nor should you just talk to each other with your minds but rather mingle your consciousnesses until you act and think as one
i expect both of you to know what either one of you is taught
what about our privacy objected eragon
privacy said ** your thoughts to thyself when you leave here if it pleases you but while we tutor you you have no privacy
eragon looked at saphira feeling even worse than before
she avoided his gaze then stamped a foot and faced him **
they re right
we have been negligent
i did not say that it ** had guessed his opinion though
he resented the attention she lavished on glaedr and how it drew her away from ** will do better wo not we
she declined to offer oromis and glaedr an apology though leaving the task to eragon
we wo not disappoint you again
see that you do not
you will be tested tomorrow on what the other ** oromis revealed a round wood bauble nestled in the middle of his palm
so long as you take care to wind it regularly this device will wake you at the proper time each morning
return here as soon as you have bathed and eaten
the bauble was surprisingly heavy when eragon took it
the size of a walnut it had been carved with deep whorls around a knob wrought in the likeness of a moss rose blossom
he turned the knob experimentally and heard three clicks as a hidden ratchet advanced
thank you he said