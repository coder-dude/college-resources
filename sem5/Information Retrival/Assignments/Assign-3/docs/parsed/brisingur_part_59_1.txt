arya heeded his call and loped over her stride as graceful as a gazelle is
she had acquired a shield a full sized helm and a mail hauberk since they had parted and the metal of her armor gleamed in the gray half light that pervaded the city
as she drew to a stop eragon said saphira and i are going to enter the keep from above and try to capture lady lorana
do you want to come with us
arya agreed with a terse nod
springing from the ground onto one of saphira is front legs eragon climbed into her saddle
arya followed his example an instant later and sat close behind him the links of her hauberk pressing against his back
saphira unfurled her velvety wings and took flight leaving blodhgarm and the other elves gazing up at her with looks of frustration
you should not abandon your guards so lightly arya murmured in eragon is left ear
she wrapped her sword arm around his waist and held him tightly as saphira wheeled above the courtyard
before eragon could respond he felt the touch of glaedr is vast mind
for a moment the city below vanished and he saw and felt only what glaedr saw and felt
glaedr flicked his tongue out and tasted the enticing aroma of burnt wood cooked meat spilled blood
he had been to this place many times before
in his youth it had been known by a different name than gil ead and then the only inhabitants had been the somber laughing quick tongued elves and the friends of elves
his previous visits had always been pleasant but it pained him to remember the two nest mates who had died here slain by the twisted mind forsworn
the lazy one eye sun hovered just above the horizon
to the north the big water isenstar was a rippling sheet of polished silver
below the herd of pointed ears commanded by islanzadi was arrayed around the broken anthill city
their armor glittered like crushed ice
a pall of blue smoke lay over the whole area thick as cold morning mist
and from the south the small angry rip claw thorn winged his way toward gil ead bellowing his challenge for all to hear
morzan son murtagh sat upon his back and in murtagh is right hand zar roc shone as bright as a nail
sorrow filled glaedr as he beheld the two miserable hatchlings
he wished he and oromis did not have to kill them
once more he thought dragon must fight dragon and rider must fight rider and all because of that egg breaker galbatorix
his mood grim glaedr quickened his flapping and spread his claws in preparation for tearing at his oncoming foes
i did
worried eragon glanced back at the saddlebags where glaedr is heart of hearts was hidden and wondered if he and saphira should try to help oromis and glaedr but then reassured himself with the knowledge that there were numerous spellcasters among the elves
his teachers would not want for assistance
what is wrong asked arya her voice loud in eragon is ear
oromis and glaedr are about to fight thorn and murtagh said saphira
eragon felt arya stiffen against him
how do you know she asked
i will explain later
i just hope they do not get hurt
saphira flew high above the keep then floated downward on silent wings and alighted upon the spire of the tallest tower
as eragon and arya clambered onto the steep roof saphira said i will meet you in the chamber below
the window here is too small for me
and she took off the gusts from her wings buffeting them
eragon and arya lowered themselves over the edge of the roof and dropped to a narrow stone ledge eight feet below
ignoring the vertigo inducing fall that awaited him if he slipped eragon inched along the ledge to a cross shaped window where he pulled himself into a large square room lined with sheaves of quarrels and racks of heavy crossbows
if anyone had been in the room when saphira landed they had already fled
arya climbed through the window after him
she inspected the room then gestured at the stairs in the far corner and padded toward them her leather boots silent on the stone floor
as eragon followed her he sensed a strange confluence of energies below them and also the minds of five people whose thoughts were closed to him