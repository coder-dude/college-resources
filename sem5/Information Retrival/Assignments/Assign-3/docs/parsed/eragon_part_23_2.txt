if i am not mistaken either number would prove fatal to ** trevor wearily agreed
you should consider leaving brom continued
this area has become far too perilous for anyone to live in peace
i know but the people here refuse to consider moving
this is their home as well as mine though i have only been here a couple years and they place its worth above their own ** trevor looked at him seriously
we have repulsed individual urgals and that has given the townspeople a confidence far beyond their abilities
i fear that we will all wake up one morning with our throats slashed
the archer hurried out of a house with a pile of goods in his arms
he set them next to the horses and brom paid him
as the man left brom asked why did they choose you to defend daret
trevor shrugged
i was in the king is army for some years
brom dug through the items handed eragon the pair of gloves and packed the rest of the supplies into their saddlebags
eragon pulled the gloves on being careful to keep his palm facing down and flexed his hands
the leather felt good and strong though it was scarred from use
well said brom as i promised we will go now
trevor nodded
when you enter dras leona would you do us this favor alert the empire to our plight and that of the other towns
if word of this has not reached the king by now it is cause for worry
and if it has but he has chosen to do nothing that too is cause for worry
we will carry your message
may your swords stay sharp said brom
the wagons were pulled out of their way and they rode from daret into the trees along the ninor river
eragon sent his thoughts to ** re on our way back
everything turned out all right
her only response was simmering anger
brom pulled at his beard
the empire is in worse condition than i had imagined
when the traders visited carvahall they brought reports of unrest but i never believed that it was this widespread
with all these urgals around it seems that the empire itself is under attack yet no troops or soldiers have been sent out
it is as if the king does not care to defend his domain
brom ducked under a low hanging branch
did you use any of your powers while we were in daret
wrong corrected brom
you could have sensed trevor is intentions
even with my limited abilities i was able to do that
if the villagers had been bent on killing us i would not have just sat there
however i felt there was a reasonable chance of talking our way out of there which is what i did
how could i know what trevor was thinking asked eragon
am i supposed to be able to see into people is minds
come now chided brom you should know the answer to that
you could have discovered trevor is purpose in the same way that you communicate with cadoc or saphira
the minds of men are not so different from a dragon is or horse is
it is a simple thing to do but it is a power you must use sparingly and with great caution
a person is mind is his last sanctuary
you must never violate it unless circumstances force you to
the riders had very strict rules regarding this
if they were broken without due cause the punishment was severe
and you can do this even though you are not a rider asked eragon
as i said before with the right instruction anyone can talk with their minds but with differing amounts of success
whether it is magic though is hard to tell
magical abilities will certainly trigger the talent or becoming linked with a dragon but i ve known plenty who learned it on their own
think about it you can communicate with any sentient being though the contact may not be very clear
you could spend the entire day listening to a bird is thoughts or understanding how an earthworm feels during a rainstorm
but i ve never found birds very interesting
i suggest starting with a cat they have unusual personalities
eragon twisted cadoc is reins in his hands considering the implications of what brom had said
but if i can get into someone is head does not that mean that others can do the same to me how do i know if someone is prying in my mind is there a way to stop that how do i know if brom can tell what i am thinking right now