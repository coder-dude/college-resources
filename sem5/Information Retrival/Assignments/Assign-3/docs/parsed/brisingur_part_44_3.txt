jormundur extended a hand toward roran
it is time
nodding roran rose and allowed jormundur and the guards to escort him to the whipping post outside
row after row of the varden boxed in the area around the post every man woman dwarf and urgal standing with stiff spines and squared shoulders
after his initial glimpse of the assembled army roran gazed off toward the horizon and did his best to ignore the onlookers
the two guards lifted roran is arms above his head and secured his wrists to the crossbeam of the whipping post
while they did jormundur walked around in front of the post and held up a leather wrapped dowel
here bite down on this he said in a low voice
it will keep you from hurting ** grateful roran opened his mouth and allowed jormundur to fit the dowel between his teeth
the tanned leather tasted bitter like green acorns
then a horn and a drumroll sounded and jormundur read out the charges against roran and the guards cut off roran is sackcloth shirt
he shivered as the cold air washed across his bare torso
an instant before it struck roran heard the whip whistling through the air
it felt as if a rod of hot metal had been laid across his flesh
roran arched his back and bit down on the dowel
an involuntary groan escaped him although the dowel muffled the sound so he thought no one else heard
one said the man wielding the whip
the shock of the second blow caused roran to groan again but thereafter he remained silent determined not to appear weak before the whole of the varden
the whipping was as painful as any of the numerous wounds roran had suffered over the past few months but after a dozen or so blows he gave up trying to fight the pain and surrendering to it entered a bleary trance
his field of vision narrowed until the only thing he saw was the worn wood in front of him at times his sight flickered and went blank as he drifted into brief spates of unconsciousness
after an interminable time he heard the dim and faraway voice intone thirty and despair gripped him as he wondered how can i possibly withstand another twenty lashes then he thought of katrina and their unborn child and the thought gave him strength
that is not how i would treat a patient of mine he heard trianna say in a haughty tone
if you treat all of your patients as you were treating roran another woman replied i am amazed that any survived your ** after a moment roran recognized the second voice as belonging to the strange bright eyed herbalist angela
i beg your ** said trianna
i will not stand here and be insulted by a lowly fortuneteller who struggles to cast even the most basic spell
sit then if it pleases you but whether you sit or stand i will continue to insult you until you admit that his back muscle attaches here and not ** roran felt a finger touch him in two different places each a half inch apart
** said trianna and left the tent
katrina smiled at roran and for the first time he noticed the tears streaking her face
roran do you understand me she asked
are you awake
i
i think so he said his voice raspy
his jaw ached from biting the dowel so hard for so long
he coughed then grimaced as every one of the fifty stripes on his back throbbed in unison
there we go said angela
all finished
it is amazing
i did not expect you and trianna to do so much said katrina
nasuada
why would
you will have to ask her yourself
tell him to stay off his back if he can help it
and he ought to be careful twisting from side to side or he might tear open the scabs
behind him angela laughed
think nothing of it roran
or rather think something of it but do not consider it overly important
besides it amuses me to have tended injuries on both your back and eragon is
right then i will be off
watch out for **
when the herbalist had gone roran closed his eyes again
katrina is smooth fingers stroked his forehead
you were very brave she said
aye
jormundur and everyone else i spoke to said that you never cried out or begged for the flogging to stop
** he wanted to know how serious his wounds were but he was reluctant to force her to describe the damage to his back