the spirits she and her companions had summoned were nowhere to be seen
lady lorana was still ensconced in her chair
saphira was in the process of struggling to her feet on the opposite side of the room
and the man who had been sitting on the floor amid the three other spellcasters was standing next to him holding arya in the air by her throat
the color had vanished from the man is skin leaving him bone white
his hair which had been brown was now bright crimson and when he looked at eragon and smiled eragon saw that his eyes had become maroon
in every aspect of appearance and bearing the man resembled durza
our name is varaug said the shade
fear ** arya kicked at him but her blows seemed to have no effect
the burning pressure of the shade is consciousness pressed against eragon is mind trying to break down his defenses
the force of the attack immobilized eragon he could barely repel the burrowing tendrils of the shade is mind much less walk or swing a sword
for whatever reason varaug was even stronger than durza and eragon was not sure how long he could withstand the shade is might
he saw that saphira was also under attack she sat stiff and motionless by the balcony a snarl carved on her face
the veins in arya is forehead bulged and her face turned red and purple
her mouth was open but she was not breathing
with the palm of her right hand she struck the shade is locked elbow and broke the joint with a loud crack
varaug is arm sagged and for a moment arya is toes brushed the floor but then the bones in the shade is arm popped back into place and he lifted her even higher
you shall die growled varaug
you shall all die for imprisoning us in this cold hard clay
knowing that arya is and saphira is lives were in peril stripped eragon of every emotion save that of implacable determination
his thoughts as sharp and clear as a shard of glass he drove himself at the shade is seething consciousness
varaug was too powerful and the spirits that resided within him too disparate for eragon to overwhelm and control so eragon sought to isolate the shade
he surrounded varaug is mind with his own every time varaug attempted to reach out toward saphira or arya eragon blocked the mental ray and every time the shade attempted to shift his body eragon counteracted the urge with a command of his own
they battled at the speed of thought fighting back and forth along the perimeter of the shade is mind which was a landscape so jumbled and incoherent eragon feared it would drive him mad if he gazed at it for long
eragon pushed himself to the utmost as he dueled with varaug striving to anticipate the shade is every move but he knew that their contest could only end with his own defeat
as fast as he was eragon could not outthink the numerous intelligences contained within the shade
eragon is concentration eventually wavered and varaug seized upon the opportunity to force himself further into eragon is mind trapping him
transfixing him
suppressing his thoughts until eragon could do no more than stare at the shade with dumb rage
an excruciating tingling filled eragon is limbs as the spirits raced through his body coursing down every one of his nerves
your ring is full of ** exclaimed varaug his eyes widening with pleasure
beautiful ** it will feed us for a long **
then he growled with anger as arya grabbed his wrist and broke it in three places
she twisted free of varaug is grip before he could heal himself and dropped to the ground gasping for air
varaug kicked at her but she rolled out of the way
she reached for her fallen sword
eragon trembled as he struggled to cast off the shade is oppressive presence
arya is hand closed around the hilt of her sword
a wordless bellow escaped the shade
he pounced on her and they rolled across the floor wrestling for control of the weapon
arya shouted and struck varaug in the side of his head with the pommel of the sword
the shade went limp for an instant and arya scrambled backward pushing herself upright
in a flash eragon freed himself from varaug
without consideration for his own safety he resumed his attack on the shade is consciousness his only thought to restrain the shade for a few moments