you may leave them in my pavilion and i will arrange to have a tent erected for you eragon where you can keep them permanently
i suggest though that you don your armor before parting with your bags
you might need it at any moment
that reminds me we have your armor with us saphira
i shall have it unpacked and brought to you
and what of me lady asked orik
we have several knurlan with us from durgrimst ingeitum who have lent their expertise to the construction of our earthen defenses
you may take command of them if you wish
orik seemed heartened by the prospect of seeing fellow dwarves especially ones from his own clan
he clapped his fist to his chest and said i think i will at that
if you will excuse me i will see to it at ** without a backward glance he trundled off through the camp heading north toward the breastwork
returning to her pavilion with the four who remained nasuada said to eragon report to me once you have settled matters with du vrangr ** then she pushed aside the entrance flap to the pavilion and disappeared with elva through the dark opening
as arya started to follow eragon reached toward her and in the ancient language said ** the elf paused and looked at him betraying nothing
he held her gaze without wavering staring deep into her eyes which reflected the strange light around them
arya i wo not apologize for how i feel about you
however i wanted you to know that iam sorry for how i acted during the blood oath celebration
i was not myself that night otherwise i would have never been so forward with you
and you wo not do it again
he suppressed a humorless laugh
it would not get me anywhere if i did now would it when she remained silent he said no matter
i do not want to trouble you even if you he bit off the end of his sentence before he made a remark he knew he would regret
arya is expression softened
i am not trying to hurt you eragon
you must understand that
i understand he said but without conviction
an awkward pause stretched between them
your flight went well i trust
you encountered no difficulty in the desert
no
i only ** then in an even gentler voice arya asked what of you eragon how have you been since the celebration i heard what you said to nasuada but you mentioned nothing other than your back
i
he tried to lie not wanting her to know how much he had missed her but the ancient language stopped the words dead in his mouth and rendered him mute
finally he resorted to a technique of the elves telling only part of the truth in order to create an impression opposite the whole truth
i am better than before he said meaning in his mind the condition of his back
despite his subterfuge arya appeared unconvinced
she did not press him on the subject though but rather said i am ** nasuada is voice emanated from inside the pavilion and arya glanced toward it before facing him again
i am needed elsewhere eragon
we are both needed elsewhere
a battle is about to take ** lifting the canvas flap she stepped halfway into the gloomy tent then hesitated and added take care eragon shadeslayer
dismay rooted eragon in place
he had accomplished what he wanted to but it seemed to have changed nothing between him and arya
he balled his hands into fists and hunched his shoulders and glared at the ground without seeing it simmering with frustration
he started when saphira nosed him on the ** on little one she said ** can not stay here forever and this saddle is beginning to itch
going to her side eragon pulled on her neck strap muttering under his breath when it caught in the buckle
he almost hoped the leather would break
undoing the rest of the straps he let the saddle and everything tied to it fall to the ground in a jumbled ** feels good to have that off said saphira rolling her massive shoulders
digging his armor out of the saddlebags eragon outfitted himself in the bright dress of war
first he pulled his hauberk over his elven tunic then strapped his chased greaves to his legs and his inlaid bracers to his forearms
on his head went his padded leather cap followed by his coif of tempered steel and then his gold and silver helm