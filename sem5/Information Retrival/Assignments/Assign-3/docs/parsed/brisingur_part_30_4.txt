the second soldier was smarter
he let go of his spear and reached for the sword at his belt but only succeeded in drawing the blade halfway out of the sheath before roran staved in his chest
the third and fourth soldiers were ready for roran by then
they converged on him naked blades outstretched snarls on their faces
roran tried to sidestep them but his torn leg failed him and he stumbled and fell to one knee
the closest soldier slashed downward
with his shield roran blocked the blow then dove forward and crushed the soldier is foot with the flat end of his hammer
cursing the soldier toppled to the ground
roran promptly smashed the soldier is face then flipped onto his back knowing that the last soldier was directly behind him
roran froze his arms and legs splayed to either side
the soldier stood over him holding his sword extended the tip of the gleaming blade less than an inch away from roran is throat
so this is how it ends thought roran
then a thick arm appeared around the soldier is neck yanking him backward and the soldier uttered a choked cry as a sword blade sprouted from the middle of his chest along with a spray of blood
the soldier collapsed into a limp pile and in his place there stood martland redbeard
the earl was breathing heavily and his beard and chest were splattered with gore
martland stuck his sword in the dirt leaned on the pommel and surveyed the carnage within the triangle of wagons
he nodded
you will do i think
he grunted as carn probed especially deep into the gash
sorry said carn
i have to inspect the wound
roran kept staring at the vultures and did not answer
after a minute carn uttered a number of words in the ancient language and a few seconds later the pain in roran is leg subsided to a dull ache
looking down roran saw his leg was whole once more
the effort of healing roran and the two other men before him had left carn gray faced and shaking
the magician slumped against the wagon wrapping his arms around his middle his expression queasy
are you all right roran asked
carn lifted his shoulders in a minuscule shrug
i just need a moment to recover
the ox scratched the outer bone of your lower leg
i repaired the scratch but i did not have the strength to completely heal the rest of your injury
i stitched together your skin and muscle so it wo not bleed or pain you overmuch but only lightly
the flesh there wo not hold much more than your weight not until it mends on its own that is
roran pulled on the remains of his boot
eragon cast wards around me to protect me from injury
they saved my life several times today
why did not they protect me from the ox is horn though
i do not know roran carn said sighing
no one can prepare for every eventuality
that is one reason magic is so perilous
if you overlook a single facet of a spell it may do nothing but weaken you or worse it may do some horrible thing you never intended
it happens to even the best magicians
there must be a flaw in your cousin is wards a misplaced word or a poorly reasoned statement that allowed the ox to gore you
easing himself off the wagon roran limped toward the head of the convoy assessing the result of the battle
five of the varden had been wounded during the fighting including himself and two others had died one a man roran had barely met the other ferth whom he had spoken with on several occasions
of the soldiers and the men who steered the wagons none remained alive
roran paused by the first two soldiers he had killed and studied their corpses
his saliva turned bitter and his gut roiled with revulsion
now i have killed
i do not know how many
he realized that during the madness of the battle of the burning plains he had lost count of the number of men he had slain
that he had sent so many to their deaths he could not remember the full number unsettled him
must i slaughter entire fields of men in order to regain what the empire stole from me an even more disconcerting thought occurred to him and if i do how could i return to palancar valley and live in peace when my soul was stained black with the blood of hundreds