a desperate yearning for hope and comfort filled him confused him left him unsteady upon the face of the earth
part of himself held back and would not allow him to commit to the dwarf gods and bind his identity and his sense of well being to something he did not understand
he also had difficulty accepting that if gods did exist the dwarf gods were the only ones
eragon was certain that if he asked nar garzhvog or a member of the nomad tribes or even the black priests of helgrind if their gods were real they would uphold the supremacy of their deities just as vigorously as glumra would uphold hers
how am i supposed to know which religion is the true religion he wondered
just because someone follows a certain faith does not necessarily mean it is the right path
perhaps no one religion contains all of the truth of the world
perhaps every religion contains fragments of the truth and it is our responsibility to identify those fragments and piece them together
or perhaps the elves are right and there are no gods
but how can i know for sure
with a long sigh glumra murmured a phrase in dwarvish then rose from her knees and drew closed the silk curtain over the alcove
eragon likewise stood wincing as his battle sore muscles stretched and followed her to the table where he returned to his chair
from a stone cupboard set into the wall the dwarf woman took two pewter mugs then retrieved a bladder full of wine from where it hung from the ceiling and poured a drink for both her and eragon
she raised her mug and uttered a toast in dwarvish which eragon struggled to imitate and then they drank
it is good said glumra to know that kvistor still lives on to know that even now he is garbed in robes fit for a king while he enjoys the evening feast in morgothal is hall
may he win much honor in the service of the ** and she drank again
once he had emptied his mug eragon began to bid farewell to glumra but she forestalled him with a motion of her hand
have you a place to stay shadeslayer safe from those who wish you dead whereupon eragon told her how he was supposed to remain hidden underneath tronjheim until orik sent a messenger for him
glumra nodded with a short definitive jerk of her chin and said then you and your companions must wait here until the messenger arrives shadeslayer
i insist upon ** eragon started to protest but she shook her head
i could not allow the men who fought with mine son to languish in the damp and the dark of the caves while i yet have life in mine bones
summon your companions and we shall eat and be merry this gloomy night
eragon realized that he could not leave without upsetting glumra so he called to his guards and his translator
together they helped glumra to prepare a dinner of bread meat and pie and when it was ready the lot of them ate and drank and talked late into the night
glumra was particularly lively she drank the most laughed the loudest and was always the first to make a witty remark
at first eragon was shocked by her behavior but then he noticed how her smiles never reached her eyes and how if she thought no one was looking the mirth would drain from her face and her expression would become one of somber quietude
entertaining them he concluded was her way of celebrating her son is memory as well as fending off her grief at kvistor is death
i have never met anyone like you before he thought as he watched her
long after midnight someone knocked on the door of the hut
hundfast ushered in a dwarf who was garbed in full armor and who seemed edgy and ill at ease he kept glancing at the doors and windows and shadowed corners
with a series of phrases in the ancient language he convinced eragon that he was orik is messenger and then he said i am farn son of flosi
argetlam orik bids you return with all possible haste
he has most important tidings concerning the events of today
at the doorway glumra grasped eragon is left forearm with fingers like steel and as he gazed down into her flinty eyes she said remember your oath shadeslayer and do not let the killers of mine son escape without **