he barely managed to get it free in time to block the first strike
backstepping toward the ra zac roran parried a sword thrust then swung his hammer up under the man is chin sending him to the ground
to ** shouted roran
defend your ** he sidestepped a jab as five men attempted to encircle him
to **
baldor answered his call first then albriech
a few seconds later loring is sons joined him followed by a score of others
from the side streets women and children pelted the soldiers with rocks
stay together ordered roran standing his ground
there are more of us
the soldiers halted as the line of villagers before them continued to thicken
with more than a hundred men at his back roran slowly advanced
attack you foolsss screamed a ra zac dodging loring is pitchfork
a single arrow whizzed toward roran
he caught it on his shield and laughed
the ra zac were level with the soldiers now hissing with frustration
they glared at the villagers from under their inky cowls
suddenly roran felt himself become lethargic and powerless to move it was hard to even think
fatigue seemed to chain his arms and legs in place
then from farther in carvahall roran heard a raw shout from birgit
a second later a rock hurtled over his head and bored toward the lead ra zac who twitched with supernatural speed to avoid the missile
the distraction slight though it was freed roran is mind from the soporific ** that magic he wondered
he dropped the shield grasped his hammer with both hands and raised it far above his head just like horst did when spreading metal
roran went up on tiptoe his entire body bowed backward then whipped his arms down with ** the hammer cartwheeled through the air and bounced off the ra zac is shield leaving a formidable dent
the two attacks were enough to disrupt the last of the ra zac is strange power
they clicked rapidly to each other as the villagers roared and marched forward then the ra zac yanked on their reins wheeling around
retreat they growled riding past the soldiers
the crimson clad warriors sullenly backed out of carvahall stabbing at anyone who came too close
only when they were a good distance from the burning wagons did they dare turn their backs
roran sighed and retrieved his hammer feeling the bruises on his side and back where he had hit the wall
he bowed his head as he saw that the explosion had killed parr
nine other men had died
already wives and mothers rent the night with their wails of grief
roran blinked and stumbled to the middle of the road where baldor stood
a ra zac sat beetle like on a horse only twenty yards away
the creature crooked a finger at roran and said you
you sssmell like your cousin
we never forget a sssmell
what do you want he shouted
why are you here
the ra zac chuckled in a horrible insectile way
we want
information
it glanced over its shoulder where its companions had disappeared then cried release roran and you ssshall be sold as ssslaves
protect him and we will eat you all
we ssshall have your answer when next we come
be sssure it is the right one
light burst into the tunnel as the doors dragged open
eragon winced his eyes sorely unaccustomed to daylight after so long underground
beside him saphira hissed and arched her neck to get a better view of their surroundings
it had taken them two days to traverse the subterranean passage from farthen dur though it felt longer to eragon due to the never ending dusk that surrounded them and the silence it had imposed upon their group
in all he could recall only a handful of words being exchanged during their journey
eragon had hoped to learn more about arya while they traveled together but the only information he had gleaned came simply as a result of observation
he had not supped with her before and was startled to see that she brought her own food and ate no meat
when he asked her why she said you will never again consume an animal is flesh after you have been trained or if you do it will be only on the rarest of occasions
why should i give up meat he scoffed
i cannot explain with words but you will understand once we reach ellesmera