surrounding the hill was a lush meadow that bordered the jiet river which rushed past a hundred feet to eragon is right
the sky was bright and clear sunshine bathed the land with a soft radiance
the air was cool and calm and smelled fresh as if it had just rained
gathered in front of the hill were the villagers from carvahall none of whom had been injured during the fighting and what seemed to be half of the men of the varden
many of the warriors held long spears mounted with embroidered pennants of every color
various horses including snowfire were picketed at the far end of the meadow
despite nasuada is best efforts organizing the assembly had taken longer than anyone had reckoned
wind tousled eragon is hair which was still wet from washing as saphira glided over the congregation and alighted next to him fanning her wings
he smiled and touched her on the shoulder
under normal circumstances eragon would have been nervous about speaking in front of so many people and performing such a solemn and important ceremony but after the earlier fighting everything had assumed an air of unreality as if it were no more than a particularly vivid dream
at the base of the hill stood nasuada arya narheim jormundur angela elva and others of importance
king orrin was absent as his wounds had proved to be more serious than they had first appeared and his healers were still laboring over him in his pavilion
the king is prime minister irwin was attending in his stead
the only urgals present were the two in nasuada is private guard
eragon had been there when nasuada had invited nar garzhvog to the event and he had been relieved when garzhvog had had the good sense to decline
the villagers would never have tolerated a large group of urgals at the wedding
as it was nasuada had difficulty convincing them to allow her guards to remain
with a rustle of cloth the villagers and the varden parted forming a long open path from the hill to the edge of the crowd
then joining their voices the villagers began to sing the ancient wedding songs of palancar valley
the well worn verses spoke of the cycle of the seasons of the warm earth that gave birth to a new crop each year of the spring calving of nesting robins and spawning fish and of how it was the destiny of the young to replace the old
one of blodhgarm is spellcasters a female elf with silver hair withdrew a small gold harp from a velvet case and accompanied the villagers with notes of her own embellishing upon the simple themes of their melodies lending the familiar music a wistful mood
with slow steady steps roran and katrina emerged from either side of the crowd at the far end of the path turned toward the hill and without touching began to advance toward eragon
roran wore a new tunic he had borrowed from one of the varden
his hair was brushed his beard was trimmed and his boots were clean
his face beamed with inexpressible joy
all in all he seemed very handsome and distinguished to eragon
however it was katrina who commanded eragon is attention
her dress was light blue as befitted a bride at her first wedding of a simple cut but with a lace train that was twenty feet long and carried by two girls
against the pale fabric her free flowing locks glowed like polished copper
in her hands was a posy of wildflowers
she was proud serene and beautiful
eragon heard gasps from some of the women as they beheld katrina is train
he resolved to thank nasuada for having du vrangr gata make the dress for katrina for he assumed it was she who was responsible for the gift
three paces behind roran walked horst
and at a similar distance behind katrina walked birgit careful to avoid stepping on the train
when roran and katrina were halfway to the hill a pair of white doves flew out from the willow trees lining the jiet river
the doves carried a circlet of yellow daffodils clutched in their feet
katrina slowed and stopped as they approached her
the birds circled her three times north to east and then dipped down and laid the circlet upon the crown of her head before returning to the river