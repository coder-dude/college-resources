elves though are not like other races
you speak as though you were not one he said echoing her words from farthen dur
i have lived with the varden for enough years to become accustomed to their traditions replied arya in a brittle tone
ah
so then do you mean to say that elves do not have the same emotions as dwarves and humans i find that hard to believe
all living things have the same basic needs and desires
that is not what i mean to ** eragon recoiled then frowned and studied her
it was unusual for her to be so brusque
arya closed her eyes and placed her fingers on her temples taking a long breath
because elves live for so many years we consider courtesy to be the highest social virtue
you cannot afford to give offense when a grudge can be held for decades or centuries
courtesy is the only way to prevent such hostility from accumulating
it does not always succeed but we adhere to our rituals rigorously for they protect us from extremes
nor are elves fecund so it is vital that we avoid conflict among ourselves
if we shared the same rate of crime as you or the dwarves we would soon be extinct
there is a proper way to greet the sentinels in ceris certain patterns and forms that you must observe when presented to queen islanzadi and a hundred different manners in which to greet those around you if it is not better to just remain quiet
with all your customs eragon risked saying it seems as though you ve only made it easier to offend people
a smile flickered across her lips
perhaps
you know as well as i that you will be judged by the highest standards
if you make a mistake the elves will think you did it on purpose
and only harm will come if they discover that it was born of ignorance
far better to be thought rude and capable than rude and incapable else you risk being manipulated like the serpent in a match of runes
our politics move in cycles that are both subtle and lengthy
what you see or hear of an elf one day may only be a slight move in a strategy that reaches back millennia and may have no bearing on how that elf will behave tomorrow
it is a game that we all play but few control a game that you are about to enter
now perhaps you realize why i say elves are not like other races
the dwarves are also long lived yet they are more prolific than us and do not share our restraint or our taste for intrigue
and humans
she let her voice fade into a tactful silence
humans said eragon do the best they can with what they are given
why do not you tell orik all this as well he will be staying in ellesmera same as me
an edge crept into arya is voice
he is already somewhat familiar with our etiquette
however as a rider you would do well to appear better educated than him
eragon accepted her rebuke without protest
what must i learn
so arya began to tutor him and through him saphira in the niceties of elven society
first she explained that when one elf meets another they stop and touch their first two fingers to their lips to indicate that we shall not distort the truth during our ** this is followed by the phrase atra esterni ono thelduin to which one replies atra du evarinya ono varda
and said arya if you are being especially formal a third response is made un atra mor ranr lifa unin hjarta onr which means and may peace live in your ** these lines were adopted from a blessing that was made by a dragon when our pact with them was finalized
it goes
or may good fortune rule over you peace live in your heart and the stars watch over **
how do you know who is supposed to speak first
if you greet someone with greater status than yourself or if you wish to honor a subordinate then speak first
if you greet someone with less status than yourself speak last
but if you are uncertain of your position give your counterpart a chance to speak and if they are silent speak first
such is the rule
does it apply to me as well asked saphira
arya plucked a dry leaf from the ground and crumpled it between her fingers
behind her the camp faded into shadow as the dwarves banked the fire dampening the flames with a layer of dirt so that the coals and embers would survive until morning