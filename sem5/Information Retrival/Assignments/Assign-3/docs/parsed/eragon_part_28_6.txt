there is no way to escape him
eventually you will stand before our master
if you resist he will fill your days with agony
eragon wondered who had the power to bring the urgals under one banner
was there a third great force loose in the land along with the empire and the varden keep your offer and tell your master that the crows can eat his entrails for all i **
rage swept through the urgals their leader howled gnashing his teeth
we will drag you to him ** he waved his arm and the urgals rushed at saphira
raising his right hand eragon barked **
** saphira but it was too late
the monsters faltered as eragon is palm glowed
beams of light lanced from his hand striking each of them in the gut
the urgals were thrown through the air and smashed into trees falling senseless to the ground
fatigue suddenly drained eragon of strength and he tumbled off saphira
his mind felt hazy and dull
as saphira bent over him he realized that he might have gone too far
the energy needed to lift and throw twelve urgals was enormous
fear engulfed him as he struggled to stay conscious
at the edge of his vision he saw one of the urgals stagger to his feet sword in hand
eragon tried to warn saphira but he was too **
he thought feebly
the urgal crept toward saphira until he was well past her tail then raised his sword to strike her **
saphira whirled on the monster roaring savagely
her talons slashed with blinding speed
blood spurted everywhere as the urgal was rent in two
saphira snapped her jaws together with finality and returned to eragon
she gently wrapped her bloody claws around his torso then growled and jumped into the air
the night blurred into a pain filled streak
the hypnotic sound of saphira is wings put him in a bleary trance up down up down up down
when saphira eventually landed eragon was dimly aware of brom talking with her
eragon could not understand what they said but a decision must have been reached because saphira took off again
his stupor yielded to sleep that covered him like a soft blanket