when the spell ended he opened and closed his hand to confirm that it was fully cured
thank you he said
it surprised him that she had taken the initiative when he was perfectly capable of healing his own wounds
arya seemed embarrassed
looking away out over the plains she said i am glad you were by my side today eragon
she favored him with a quick uncertain smile
they lingered on the hillock for another minute neither of them eager to resume their journey
then arya sighed and said we should be off
the shadows lengthen and someone else is bound to appear and raise a hue and cry when they discover this crows feast
abandoning the hillock they orientated themselves in a southwesterly direction angling away from the road and loped out across the uneven sea of grass
behind them the first of the carrion eaters dropped from the sky