roran leaned on one knee and scratched his new beard as he looked down at narda
the small town was dark and compact like a crust of rye bread tamped into a crevasse along the coast
beyond it the wine red sea glimmered with the last rays of the dying sunset
the water fascinated him it was utterly different from the landscape he was accustomed to
leaving the promontory roran walked back to his makeshift tent enjoying deep breaths of the salty air
they had camped high in the foothills of the spine in order to avoid detection by anyone who might alert the empire as to their whereabouts
as he strode among the clumps of villagers huddled beneath the trees roran surveyed their condition with sorrow and anger
the trek from palancar valley had left people sick battered and exhausted their faces gaunt from lack of food their clothes tattered
most everyone wore rags tied around their hands to ward off frostbite during the frigid mountain nights
weeks of carrying heavy packs had bowed once proud shoulders
the worst sight was the children thin and unnaturally still
they deserve better thought ** d be in the clutches of the ra zac right now if they had not protected me
numerous people approached roran most of whom wanted nothing more than a touch on the shoulder or a word of comfort
some offered him bits of food which he refused or when they insisted gave to someone else
those who remained at a distance watched with round pale eyes
he knew what they said about him that he was mad that spirits possessed him that not even the ra zac could defeat him in battle
crossing the spine had been even harder than roran expected
the only paths in the forest were game trails which were too narrow steep and meandering for their group
as a result the villagers were often forced to chop their way through the trees and underbrush a painstaking task that everyone despised not least because it made it easy for the empire to track them
the one advantage to the situation was that the exercise restored roran is injured shoulder to its previous level of strength although he still had trouble lifting his arm at certain angles
other hardships took their toll
a sudden storm trapped them on a bare pass high above the timberline
three people froze in the snow hida brenna and nesbit all of whom were quite old
that night was the first time roran was convinced that the entire village would die because they had followed him
soon after a boy broke his arm in a fall and then southwell drowned in a glacier stream
wolves and bears preyed upon their livestock on a regular basis ignoring the watchfires that the villagers lit once they were concealed from palancar valley and galbatorix is hated soldiers
hunger clung to them like a relentless parasite gnawing at their bellies devouring their strength and sapping their will to continue
and yet they survived displaying the same obstinacy and fortitude that kept their ancestors in palancar valley despite famine war and pestilence
the people of carvahall might take an age and a half to reach a decision but once they did nothing could deter them from their course
now that they had reached narda a sense of hope and accomplishment permeated the camp
no one knew what would happen next but the fact that they had gotten so far gave them confidence
we wo not be safe until we leave the empire thought ** it is up to me to ensure that we are not caught
i ve become responsible for everyone here
a responsibility that he had embraced wholeheartedly because it allowed him to both protect the villagers from galbatorix and pursue his goal of rescuing ** is been so long since she was captured
how can she still be alive he shuddered and pushed the thoughts away
true madness awaited him if he allowed himself to brood over katrina is fate
at dawn roran horst baldor loring is three sons and gertrude set out for narda
they descended from the foothills to the town is main road careful to stay hidden until they emerged onto the lane
here in the lowlands the air seemed thick to roran it felt as if he were trying to breathe underwater