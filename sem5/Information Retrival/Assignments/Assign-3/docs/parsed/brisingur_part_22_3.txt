i do not have time for this eragon snapped his impatience overflowing
i do not have the time to learn a completely different way of fighting
the empire might attack at any moment
i have to concentrate on practicing what i do know not trying to master a whole new set of forms
fredric clapped his hands
i know just the thing for you ** going to a crate filled with arms he began digging through it talking to himself as he did
first this then that and then we will see where we ** from the bottom of the crate he pulled out a large black mace with a flanged head
fredric rapped a knuckle against the mace
you can break swords with this
you can split mail and batter in helms and you wo not do it the slightest bit of harm no matter what you hit
it is a club eragon protested
a metal club
what of it with your strength you can swing it as if it were light as a reed
you will be a terror on the battlefield with this you will
eragon shook his head
no
smashing things is not how i prefer to fight
besides i would never have been able to kill durza by stabbing him through the heart if i had been carrying a mace instead of a sword
then i have only one more suggestion unless you insist upon a traditional ** from another part of the pavilion fredric brought eragon a weapon he identified as a falchion
it was a sword but not a type of sword eragon was accustomed to although he had seen them among the varden before
the falchion had a polished disk shaped pommel bright as a silver coin a short grip made of wood covered with black leather a curved crossguard carved with a line of dwarf runes and a single edged blade that was as long as his outstretched arm and had a thin fuller on either side close to the spine
the falchion was straight until about six inches from the end where the back of the blade flared upward in a small peak before gently curving down to the needle sharp tip
this widening of the blade reduced the likelihood that the point would bend or snap when driven through armor and lent the end of the falchion a fanglike appearance
unlike a double edged sword the falchion was made to be held with the blade and crossguard perpendicular to the ground
the most curious aspect of the falchion though was the bottom half inch of the blade including the edge which was pearly gray and substantially darker than the mirror smooth steel above
the boundary between the two areas was wavy like a silk scarf rippling in the wind
eragon pointed at the gray band
i ve not seen that before
what is it
the thriknzdal said fredric
the dwarves invented it
they temper the edge and the spine separately
the edge they make hard harder than we dare with the whole of our blades
the middle of the blade and the spine they anneal so that the back of the falchion is softer than the edge soft enough to bend and flex and survive the stress of battle without fracturing like a frost ridden file
do the dwarves treat all their blades thusly
fredric shook his head
only their single edged swords and the finest of their double edged ** he hesitated and uncertainty crept into his gaze
you understand why i chose this for you shadeslayer yes
eragon understood
with the blade of the falchion at right angles to the ground unless he deliberately tilted his wrist any blows he caught on the sword would strike the flat of the blade saving the edge for attacks of his own
wielding the falchion would require only a small adjustment to his fighting style
striding out of the pavilion he assumed a ready position with the falchion
swinging it over his head he brought it down upon the head of an imaginary foe then twisted and lunged beat aside an invisible spear sprang six yards to his left and in a brilliant but impractical move spun the blade behind his back passing it from one hand to the next as he did so
his breathing and heartbeat calm as ever he returned to where fredric and blodhgarm were waiting
the speed and balance of the falchion had impressed eragon
it was not the equal of zar roc but it was still a superb sword
fredric detected the reticence in his bearing however for he said and yet you are not entirely pleased shadeslayer