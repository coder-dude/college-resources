you mean it is too dangerous for ahuman woman
i told you before that i am not one of your helpless females
what you fail to realize is that we view our monarchs differently than you or the dwarves
to us a king or queen is highest responsibility is to serve their people however and wherever possible
if that means forfeiting our lives in the process we welcome the opportunity to prove our devotion to as the dwarves say hearth hall and honor
if i had died in the course of my duty then a replacement successor would have been chosen from among our various houses
even now i would not be required to become queen if i found the prospect distasteful
we do not choose leaders who are unwilling to devote themselves wholeheartedly to their ** she hesitated then hugged her knees against her chest and propped her chin on them
i had many years to perfect those arguments with my ** for a minute thewheet wheet of the cicadas went undisturbed in the clearing
then she asked how go your studies with oromis
eragon grunted as his foul temper returned on a wave of unpleasant memories souring his pleasure at being with arya
all he wanted to do was crawl into bed go to sleep and forget the day
oromis elda he said working each word around his mouth before letting it escape is quite thorough
he winced as she gripped his upper arm with bruising strength
what has gone amiss
he tried to shrug her hand off
nothing
i ve traveled with you long enough to know when you re happy angry
or in pain
did something happen between you and oromis if so you have to tell me so that it can be rectified as soon as possible
or was it your back we could
it is not my ** despite his pique eragon noticed that she seemed genuinely concerned which pleased him
ask saphira
she can tell you
i want to hear it from you she said quietly
the muscles in eragon is jaw spasmed as he clenched his teeth
in a low voice no more than a whisper he first described how he had failed at his meditation in the glade then the incident that poisoned his heart like a viper coiled in his chest his blessing
arya released his arm and clutched at the root of the menoa tree as if to steady herself
** the dwarf curse alarmed him he had never heard her use profanity before and this one was particularly apt for it meantill fate
i knew of your act in farthen dur for sure but i never thought
i neversuspected that such a thing could occur
i cry your pardon eragon for forcing you to leave your rooms tonight
i did not comprehend your discomfort
you must want to be alone
no he said
no i appreciate the company and the things you ve shown ** he smiled at her and after a moment she smiled back
together they sat small and still at the base of the ancient tree and watched the moon arch high over the peaceful forest before it hid behind the gathering clouds
i only wonder what will become of the child
high above their heads blagden ruffled his bone white feathers and shrieked **