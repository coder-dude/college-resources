eragon felt as if he were a child again waiting for garrow to tell him what his punishment would be for doing something foolish on their farm
orrin and nasuada remained lost deep in reflection for several minutes then nasuada smoothed the front of her dress and said king orrin may be of a different opinion and if so i look forward to hearing his reasons but for my part i believe that you did the right thing eragon
as do i said orrin surprising them all
you ** exclaimed eragon
he hesitated
i do not mean to sound impertinent for i am glad you approve but i did not expect you to look kindly upon my decision to spare sloan is life
if i may ask why
king orrin interrupted
why do we approve the rule of law must be upheld
if you had appointed yourself sloan is executioner eragon you would have taken for yourself the power that nasuada and i wield
for he who has the audacity to determine who should live and who should die no longer serves the law but dictates the law
and however benevolent you might be that would be no good thing for our species
nasuada and i at least answer to the one lord even kings must kneel before
we answer to angvard in his realm of eternal twilight
we answer to the gray man on his gray horse
death
we could be the worst tyrants in the whole of history and given enough time angvard would bring us to heel
but not you
humans are a short lived race and we should not be governed by one of the undying
we do not need another ** a strange laugh escaped from orrin then and his mouth twisted in a humorless smile
do you understand eragon you are so dangerous we are forced to acknowledge the danger to your face and hope that you are one of the few people able to resist the lure of power
king orrin laced his fingers together underneath his chin and gazed at a fold in his robes
i have said more than i intended
so for all those reasons and others besides i agree with nasuada
you were right to stay your hand when you discovered this sloan in helgrind
as inconvenient as this episode has been it would have been far worse and for you as well if you had killed to please yourself and not in self defense or in service to others
nasuada nodded
that was well spoken
throughout arya listened with an inscrutable expression
whatever her own thoughts on the matter were she did not divulge them
orrin and nasuada pressed eragon with a number of questions about the oaths he had laid upon sloan as well as queries about the remainder of his trip
the interrogation continued for so long nasuada had a tray of cooled cider fruit and meat pies brought into the pavilion along with the haunch of a steer for saphira
nasuada and orrin had ample opportunity to eat between questions however they kept eragon so busy talking he managed to consume only two bites of fruit and a few sips of cider to wet his throat
at long last king orrin bade them farewell and departed to review the status of his cavalry
arya left a minute later explaining that she needed to report to queen islanzadi and to as she said heat a tub of water wash the sand from my skin and return my features to their usual shape
i do not feel myself with the tips of my ears missing my eyes round and level and the bones of my face in the wrong places
when she was alone with eragon and saphira nasuada sighed and leaned her head against the back of the chair
eragon was shocked by how tired she appeared
gone were her previous vitality and strength of presence
gone was the fire from her eyes
she had he realized been pretending to be stronger than she was in order to avoid tempting her enemies and demoralizing the varden with the spectacle of her weakness
she nodded toward her arms
not exactly
it is taking me longer to recuperate than i had anticipated
some days are worse than others
no
thank you but no
do not tempt me
one rule of the trial of the long knives is that you must allow your wounds to heal at their own pace without magic
otherwise the contestants will not have endured the full measure of pain from their cuts
a slow smile touched her lips
maybe so but it is what it is and i would not fail so late in the trial merely because i could not withstand a bit of an ache