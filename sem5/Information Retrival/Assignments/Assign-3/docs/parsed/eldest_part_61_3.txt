you should meet them in person and soon too before they decide to band together and attack
aye although i do not think they pose a threat to us
du vrangr gata their very name betrays their ignorance
properly in the ancient language it should be du gata vrangr
their trip ended near the back of the varden at a large red pavilion flying a pennant embroidered with a black shield and two parallel swords slanting underneath
fredric pulled back the flap and eragon and orik entered the pavilion
behind them saphira pushed her head through the opening and peered over their shoulders
a broad table occupied the center of the furnished tent
nasuada stood at one end leaning on her hands studying a slew of maps and scrolls
eragon is stomach clenched as he saw arya opposite her
both women were armored as men for battle
nasuada turned her almond shaped face toward him
eragon she whispered
he was unprepared for how glad he was to see her
with a broad grin he twisted his hand over his sternum in the elves gesture of fealty and bowed
at your service
** this time nasuada sounded delighted and relieved
arya too appeared pleased
how did you get our message so quickly
i did not i learned about galbatorix is army from my scrying and left ellesmera the same ** he smiled at her again
it is good to be back with the varden
while he spoke nasuada studied him with a wondering expression
what has happened to you eragon
arya must not have told her said saphira
and so eragon gave a full account of what had befallen saphira and him since they left nasuada in farthen dur so long ago
much of what he said he sensed that she had already heard either from the dwarves or from arya but she let him speak without interrupting
eragon had to be circumspect about his training
he had given his word not to reveal oromis is existence without permission and most of his lessons were not to be shared with outsiders but he did his best to give nasuada a good idea of his skills and their attendant risks
of the agaeti blodhren he merely said
and during the celebration the dragons worked upon me the change you see giving me the physical abilities of an elf and healing my back
your scar is gone then asked nasuada
he nodded
a few more sentences served to end his narrative briefly mentioning the reason they had left du weldenvarden and then summarizing their journey thence
she shook her head
what a tale
you and saphira have experienced so much since you left farthen dur
as have ** he gestured at the tent
it is amazing what you ve accomplished
it must have taken an enormous amount of work to get the varden to surda
has the council of elders caused you much trouble
a bit but nothing extraordinary
they seem to have resigned themselves to my ** her mail clinking together nasuada seated herself in a large high backed chair and turned to orik who had yet to speak
she welcomed him and asked if he had aught to add to eragon is tale
orik shrugged and provided a few anecdotes from their stay in ellesmera though eragon suspected that the dwarf kept his true observations a secret for his king
when he finished nasuada said i am heartened to know that if we can weather this onslaught we shall have the elves by our side
did any of you happen to see hrothgar is warriors during your flight from aberon we are counting on their reinforcements
no answered saphira through ** then it was dark and i was often above or between clouds
i could have easily missed a camp under those conditions
in any case i doubt we would have crossed paths for i flew straight from aberon and it seems likely the dwarves would choose a different route perhaps following established roads rather than march through the wilderness
what asked eragon is the situation here
nasuada sighed and then told of how she and orrin had learned about galbatorix is army and the desperate measures they had resorted to since in order to reach the burning plains before the king is soldiers
she finished by saying the empire arrived three days ago
since then we ve exchanged two messages
first they asked for our surrender which we refused and now we wait for their reply