eragon had been in du weldenvarden for so long that he had begun to long for clearings fields or even a mountain instead of the endless tree trunks and meager underbrush
his flights with saphira provided no respite as they only revealed hills of prickly green that rolled unbroken into the distance like a verdant sea
oftentimes the branches were so thick overhead it was impossible to tell from what direction the sun rose and set
that combined with the repetitive scenery made eragon hopelessly lost no matter how many times arya or lifaen troubled to show him the points of the compass
if not for the elves he knew that he could wander in du weldenvarden for the rest of his life without ever finding his way free
when it rained the clouds and the forest canopy plunged them into profound darkness as if they were entombed deep underground
the falling water would collect on the black pine needles above then trickle through and pour a hundred feet or more down onto their heads like a thousand little waterfalls
at such times arya would summon a glowing orb of green magic that floated over her right hand and provided the only light in the cavernous forest
they would stop and huddle underneath a tree until the storm abated but even then water cached in the myriad branches would at the slightest provocation shower them with droplets for hours afterward
as they rode deeper into the heart of du weldenvarden the trees grew thicker and taller as well as farther apart to accommodate the increased span of their branches
the trunks bare brown shafts that towered up into the overarching ribbed ceiling which was smudged and obscured by shadow were over two hundred feet tall higher than any tree in the spine or the beors
eragon paced out the girth of one tree and measured it at seventy feet
he mentioned this to arya and she nodded saying it means that we are near ** she reached out and rested her hand lightly on the gnarled root beside her as if touching with consummate delicacy the shoulder of a friend or lover
these trees are among the oldest living creatures in alagaesia
elves have loved them since first we saw du weldenvarden and we have done everything within our power to help them ** a faint blade of light pierced the dusty emerald branches overhead and limned her arm and face with liquid gold dazzlingly bright against the murky background
we have traveled far together eragon but now you are about to enter my world
tread softly for the earth and air are heavy with memories and naught is as it seems
do not fly with saphira today as we have already triggered certain wards that protect ellesmera
it would be unwise to stray from the path
eragon bowed his head and retreated to saphira who lay curled on a bed of moss amusing herself by releasing plumes of smoke from her nostrils and watching them roil out of sight
without preamble she said there is plenty of room for me on the ground now
i will have no difficulty
** mounted folkvir and followed orik and the elves farther into the empty silent forest
saphira crawled beside him
she and the white horses gleamed in the somber half light
eragon paused overcome by the solemn beauty of his surroundings
everything had a feeling of wintry age as if nothing had changed under the thatched needles for a thousand years and nothing ever would time itself seemed to have fallen into a slumber from which it would never wake
in late afternoon the gloom lifted to reveal an elf standing before them sheathed in a brilliant ray of light that slanted down from the ceiling
he was garbed in flowing robes with a circlet of silver upon his brow
his face was old noble and serene
eragon murmured arya
show him your palm and your ring
baring his right hand eragon raised it so that first brom is ring and then the gedwey ignasia was visible
the elf smiled closed his eyes and spread his arms in a gesture of welcome
he held the posture
the way is clear said arya
at a soft command her steed moved forward
they rode around the elf like water parting at the base of a weathered boulder and when they had all passed he straightened clasped his hands and vanished as the light that illuminated him ceased to exist