** he shouted and slapped snowfire on the rump sending him galloping out of the village
make ** roran bellowed waving at the varden
they cleared a path for him between their steeds and he bounded to the forefront of the fight again sticking his hammer through his belt as he did
a soldier jabbed a spear at roran is chest
he blocked it with his wrist bruising himself on the hard wooden shaft and then yanked the spear out of the soldier is hands
the man fell flat on his face
twirling the weapon roran stabbed the man then lunged forward and lanced two more soldiers
roran took a wide stance planting his feet firmly in the rich soil where once he would have sought to raise crops and shook the spear at his foes shouting come you misbegotten ** kill me if you ** i am roran stronghammer and i fear no man **
the soldiers shuffled forward three men stepping over the bodies of their former comrades to exchange blows with roran
dancing to the side roran drove his spear into the jaw of the rightmost soldier shattering his teeth
a pennant of blood trailed the blade as roran withdrew the weapon and dropping to one knee impaled the central soldier through an armpit
an impact jarred roran is left shoulder
his shield seemed to double in weight
rising he saw a spear buried in the oak planks of his shield and the remaining soldier of the trio rushing at him with a drawn sword
roran lifted his spear above his head as if he were about to throw it and when the soldier faltered kicked him between the fork of his legs
he dispatched the man with a single blow
during the momentary lull in combat that followed roran disengaged his arm from the useless shield and cast it and the attached spear under the feet of his enemies hoping to tangle their legs
more soldiers shuffled forward quailing before roran is feral grin and stabbing spear
a mound of bodies grew before him
when it reached the height of his waist roran bounded to the top of the blood soaked berm and there he remained despite the treacherous footing for the height gave him an advantage
since the soldiers were forced to climb up a ramp of corpses to reach him he was able to kill many of them when they stumbled over an arm or a leg or stepped upon the soft neck of one of their predecessors or slipped on a slanting shield
from his elevated position roran could see that the rest of the soldiers had chosen to join the assault save for a score across the village who were still battling sand is and edric is warriors
he realized he would have no more rest until the battle had concluded
roran acquired dozens of wounds as the day wore on
many of his injuries were minor a cut on the inside of a forearm a broken finger a scratch across his ribs where a dagger had shorn through his mail but others were not
from where he lay on the pile of bodies a soldier stabbed roran through his right calf muscle hobbling him
soon afterward a heavyset man smelling of onions and cheese fell against roran and with his dying breath shoved the bolt of a crossbow into roran is left shoulder which thereafter prevented roran from lifting his arm overhead
roran left the bolt embedded in his flesh for he knew he might bleed to death if he pulled it out
pain became roran is ruling sensation every movement caused him fresh agony but to stand still was to die and so he kept dealing death blows regardless of his wounds and regardless of his weariness
roran was sometimes aware of the varden behind or beside him such as when they threw a spear past him or when the blade of a sword would dart around his shoulder to fell a soldier who was about to brain him but for the most part roran faced the soldiers alone because of the pile of bodies he stood on and the restricted amount of space between the overturned wagon and the sides of the houses
above the archers who still had arrows maintained their lethal barrage their gray goose shafts penetrating bone and sinew alike
late in the battle roran thrust his spear at a soldier and as the tip struck the man is armor the haft cracked and split along its length