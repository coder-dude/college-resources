when dawn arrived roran woke and lay staring at the whitewashed ceiling while he listened to the slow rasp of his own breathing
after a minute he rolled off the bed dressed and proceeded to the kitchen where he procured a chunk of bread smeared it with soft cheese then stepped out onto the front porch to eat and admire the sunrise
his tranquility was soon disrupted when a herd of unruly children dashed through the garden of a nearby house shrieking with delight at their game of catch the cat followed by a number of adults intent on snaring their respective charges
roran watched the cacophonous parade vanish around a corner then placed the last of the bread in his mouth and returned to the kitchen which had filled with the rest of the household
elain greeted him
good morning ** she pushed open the window shutters and gazed up at the sky
it looks like it may rain again
the more the better asserted horst
it will help keep us hidden while we climb narnmor mountain
us inquired roran
he sat at the table beside albriech who was rubbing the sleep from his eyes
horst nodded
sloan was right about the food and supplies we have to help carry them up the falls or else there wo not be enough
will there still be men to defend carvahall
once they all had breakfast roran helped baldor and albriech wrap spare food blankets and supplies into three large bundles that they slung across their shoulders and hauled to the north end of the village
roran is calf pained him but not unbearably
along the way they met the three brothers darmmen larne and hamund who were similarly burdened
just inside the trench that circumnavigated the houses roran and his companions found a large gathering of children parents and grandparents all busy organizing for the expedition
several families had volunteered their donkeys to carry goods and the younger children the animals were picketed in an impatient braying line that added to the overall confusion
roran set his bundle on the ground and scanned the group
he saw svart ivor is uncle and at nearly sixty the oldest man in carvahall seated on a bale of clothes teasing a baby with the tip of his long white beard nolfavrell who was guarded over by birgit felda nolla calitha and a number of other mothers with worried expressions and a great many reluctant people both men and women
roran also saw katrina among the crowd
she glanced up from a knot she was tying on a pack and smiled at him then returned to her task
since no one seemed to be in charge roran did his best to sort out the chaos by overseeing the arranging and packaging of the various supplies
he discovered a shortage of waterskins but when he asked for more he ended up with thirteen too many
delays such as those consumed the early morning hours
in the middle of discussing with loring the possible need for extra shoes roran stopped as he noticed sloan standing at the entrance to an alleyway
the butcher surveyed the mass of activity before him
contempt cut into the lines along his downturned mouth
his sneer hardened into enraged incredulity as he spotted katrina who had shouldered her pack removing any possibility that she was there only to help
a vein throbbed down the middle of sloan is forehead
roran hurried toward katrina but sloan reached her first
he grabbed the top of the pack and shook it violently shouting who made you do this katrina said something about the children and tried to pull free but sloan yanked at the pack twisting her arms as the straps slid off her shoulders and threw it on the ground so that the contents scattered
still shouting sloan grabbed katrina is arm and began to drag her away
she dug in her heels and fought her copper hair swirling over her face like a dust storm
furious roran threw himself at sloan and tore him from katrina shoving the butcher in the chest so that he stumbled backward several yards
** i am the one who wanted her to go
sloan glared at roran and snarled you have no **
i have every ** roran looked at the ring of spectators who had gathered around and then declared so that all could hear katrina and i are engaged to be married and i would not have my future wife treated ** for the first time that day the villagers fell completely silent even the donkeys were quiet