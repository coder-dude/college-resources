now let us go before we waste any more time
hoping that the strain would not trigger the pain in his back eragon picked up his canoe with lifaen and fit it over his shoulders
he was forced to rely on the elf to guide him along the trail as he could only see the ground beneath his feet
an hour later they had topped the ridge and hiked beyond the dangerous white water to where the gaena river was once again calm and glassy
waiting for them was saphira who was busy catching fish in the shallows jabbing her triangular head into the water like a heron
arya called her over and said to both her and eragon beyond the next curve lies ardwen lake and upon its western shore silthrim one of our greatest cities
past that a vast expanse of forest still separates us from ellesmera
we will encounter many elves close to silthrim
however i do not want either of you to be seen until we speak with queen islanzadi
in her musical accent arya answered your presence represents a great and terrible change for our kingdom and such shifts are dangerous unless handled with care
the queen must be the first to meet with you
only she has the authority and wisdom to oversee this transition
you speak highly of her commented eragon
at his words nari and lifaen stopped and watched arya with guarded eyes
her face went blank then she drew herself up proudly
she has led us well
eragon i know you carry a hooded cape from tronjheim
until we are free of possible observers will you wear it and keep your head covered so that none can see your rounded ears and know that you are human he nodded
and saphira you must hide during the day and catch up with us at night
ajihad told me that is what you did in the empire
and i hated every moment of it she growled
it is only for today and tomorrow
after that we will be far enough away from silthrim that we wo not have to worry about encountering anyone of consequence promised arya
saphira turned her azure eyes on ** we escaped the empire i swore that i would always stay close enough to protect you
every time i leave bad things happen yazuac daret dras leona the slavers
you know what i ** i am especially loath to leave since you can not defend yourself with your crippled back
i trust that arya and the others will keep me safe
do not you
saphira ** trust arya
she twisted away and padded up the riverbank sat for a minute then ** well
she broadcast her acceptance to arya adding but i wo not wait any longer than tomorrow night even if you re in the middle of silthrim at the time
i understand said arya
you will still have to be careful when flying after dark as elves can see clearly on all but the blackest nights
if you are sighted by chance you could be attacked by magic
while orik and the elves repacked the boats eragon and saphira explored the dim forest searching for a suitable hiding place
they settled on a dry hollow rimmed by crumbling rocks and blanketed with a bed of pine needles that were pleasantly soft underfoot
saphira curled up on the ground and nodded her ** now
i will be fine
eragon hugged her neck careful to avoid her sharp spines and then reluctantly departed glancing backward
at the river he donned his cape before they resumed their journey
the air was motionless when ardwen lake came into view and as a result the vast mantle of water was smooth and flat a perfect mirror for the trees and clouds
the illusion was so flawless eragon felt as if he were looking through a window at another world and that if they continued forward the canoes would fall endlessly into the reflected sky
he shivered at the thought
in the hazy distance numerous white birch bark boats darted like water striders along both shores propelled to incredible speeds by the elves strength
eragon ducked his head and tugged on the edge of his hood to ensure that it covered his face
his link with saphira grew ever more tenuous the farther apart they became until only a wisp of thought connected them
by evening he could no longer feel her presence even if he strained his mind to its limits
all of a sudden du weldenvarden seemed much more lonely and desolate