morn and tara left the tavern together and joined the crush of spectators
when most of carvahall stood before him roran fell silent tightening his left fist until his fingernails cut into his **
raising his hand he opened it and showed everyone the crimson tears that dripped down his arm
this he said is my pain
look well for it will be yours unless we defeat the curse wanton fate has set upon us
your friends and family will be bound in chains destined for slavery in foreign lands or slain before your eyes hewn open by soldiers merciless blades
galbatorix will sow our land with salt so that it lies forever fallow
this i have seen
this i ** he paced like a caged wolf glowering and swinging his head
he had their attention
now he had to stoke them into a frenzy to match his own
my father was killed by the desecrators
my cousin has fled
my farm was razed
and my bride to be was kidnapped by her own father who murdered byrd and betrayed us ** quimby eaten the hay barn burned along with fisk is and delwin is houses
parr wyglif ged bardrick farold hale garner kelby melkolf albem and elmund all slain
many of you have been injured like me so that you can no longer support your family
is not it enough that we toil every day of our lives to eke a living from the earth subjected to the whims of nature is not it enough that we are forced to pay galbatorix is iron taxes without also having to endure these senseless torments roran laughed maniacally howling at the sky and hearing the madness in his own voice
no one stirred in the crowd
i know now the true nature of the empire and of galbatorix they areevil
galbatorix is an unnatural blight on the world
he destroyed the riders and the greatest peace and prosperity we ever had
his servants are foul demons birthed in some ancient pit
but is galbatorix content to grind us beneath his heel ** he seeks to poison all of alagaesia to suffocate us with his cloak of misery
our children and their descendants shall live in the shadow of his darkness until the end of time reduced to slaves worms vermin for him to torture at his pleasure
unless
roran stared into the villagers wide eyes conscious of his control over them
no one had ever dared say what he was about to
he let his voice rasp low in his throat unless we have the courage to resist evil
we ve fought the soldiers and the ra zac but it means nothing if we die alone and forgotten or are carted away as chattel
we cannot stay here and i wo not allow galbatorix to obliterate everything that is worth living for
i would rather have my eyes plucked out and my hands chopped off than see him ** i choose to ** i choose to step from my grave and let my enemies bury themselves in it
i will cross the spine and take a ship from narda down to surda where i will join the varden who have struggled for decades to free us of this ** the villagers looked shocked at the idea
but i do not wish to go alone
come with me
come with me and seize this chance to forge a better life for yourselves
throw off the shackles that bind you ** roran pointed at his listeners moving his finger from one target to the next
a hundred years from now what names shall drop from the bards lips horst
birgit
kiselt
thane they will recite our sagas
they will sing the epic of carvahall for we were the only village brave enough to defy the empire
tears of pride flooded roran is eyes
what could be more noble than cleansing galbatorix is stain from alagaesia no more would we live in fear of having our farms destroyed or being killed and eaten
the grain we harvest would be ours to keep save for any extra that we might send as a gift to the rightful king
the rivers and streams would run thick with gold
we would be safe and happy and fat
roran held his hand before his face and slowly closed his fingers over the bleeding wounds
he stood hunched over his injured arm crucified by the scores of gazes and waited for a response to his speech
none came
at last he realized that theywanted him to continue they wanted to hear more about the cause and the future he had portrayed