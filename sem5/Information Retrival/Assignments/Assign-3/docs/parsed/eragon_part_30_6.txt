you understand that there will be talk of us now this was hardly an unobtrusive escape
evading the empire will be harder than ** was an edge to her voice that he was unaccustomed to
they flew low and fast over the road
leona lake receded behind them the land became dry and rocky and filled with tough sharp bushes and tall cactuses
clouds darkened the sky
lightning flashed in the distance
as the wind began to howl saphira glided steeply down to brom
he stopped the horses and asked what is wrong
it is not that bad objected brom
it is up there said eragon pointing at the sky
brom swore and handed him cadoc is reins
they trotted away with saphira following on foot though on the ground she had difficulty keeping up with the horses
the gale grew stronger flinging dirt through the air and twisting like a dervish
they wrapped scarves around their heads to protect their eyes
brom is robe flapped in the wind while his beard whipped about as if it had a life of its own
though it would make them miserable eragon hoped it would rain so their tracks would be obliterated
soon darkness forced them to stop
with only the stars to guide them they left the road and made camp behind two boulders
it was too dangerous to light a fire so they ate cold food while saphira sheltered them from the wind
after the sparse dinner eragon asked bluntly how did they find us
brom started to light his pipe but thought better of it and put it away
one of the palace servants warned me there were spies among them
somehow word of me and my questions must have reached tabor
and through him the ra zac
we can not go back to dras leona can we asked eragon
brom shook his head
not for a few years
eragon held his head between his hands
then should we draw the ra zac out if we let saphira be seen they will come running to wherever she is
and when they do there will be fifty soldiers with them said brom
at any rate this is not the time to discuss it
right now we have to concentrate on staying alive
tonight will be the most dangerous because the ra zac will be hunting us in the dark when they are strongest
we will have to trade watches until morning
right said eragon standing
he hesitated and squinted
his eyes had caught a flicker of movement a small patch of color that stood out from the surrounding nightscape
he stepped toward the edge of their camp trying to see it better
what is it asked brom as he unrolled his blankets
eragon stared into the darkness then turned back
i do not know
i thought i saw something
it must have been a ** pain erupted in the back of his head and saphira roared
then eragon toppled to the ground unconscious
adull throbbing roused eragon
every time blood pulsed through his head it brought a fresh wave of pain
he cracked his eyes open and winced tears rushed to his eyes as he looked directly into a bright lantern
he blinked and looked away
when he tried to sit up he realized that his hands were tied behind his back
he turned lethargically and saw brom is arms
eragon was relieved to see that they were bound together
why was that he struggled to figure it out until the thought suddenly came to him they would not tie up a dead ** but then who were they he swiveled his head further then stopped as a pair of black boots entered his vision
eragon looked up right into the cowled face of a ra zac
fear jolted through him
he reached for the magic and started to voice a word that would kill the ra zac but then halted puzzled
he could not remember the word
frustrated he tried again only to feel it slip out of his grasp
above him the ra zac laughed chillingly
the drug is working yesss i think you will not be bothering us again
there was a rattle off to the left and eragon was appalled to see the second ra zac fit a muzzle over saphira is head
her wings were pinioned to her sides by black chains there were shackles on her legs
eragon tried to contact her but felt nothing
she was most cooperative once we threatened to kill you hissed the ra zac
squatting by the lantern he rummaged through eragon is bags examining and discarding various items until he removed zar roc