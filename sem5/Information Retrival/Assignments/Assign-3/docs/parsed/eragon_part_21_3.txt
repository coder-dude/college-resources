at the thought of their fate a burning fiery power gathered from every part of his body
it was more than a desire for justice
it was his entire being rebelling against the fact of death that he would cease to exist
the power grew stronger and stronger until he felt ready to burst from the contained force
he stood tall and straight all fear gone
he raised his bow smoothly
the urgals laughed and lifted their shields
eragon sighted down the shaft as he had done hundreds of times and aligned the arrowhead with his target
the energy inside him burned at an unbearable level
he had to release it or it would consume him
a word suddenly leapt unbidden to his lips
he shot yelling **
the arrow hissed through the air glowing with a crackling blue light
it struck the lead urgal on the forehead and the air resounded with an explosion
a blue shock wave blasted out of the monster is head killing the other urgal instantly
it reached eragon before he had time to react and it passed through him without harm dissipating against the houses
eragon stood panting then looked at his icy palm
the gedwey ignasia was glowing like white hot metal yet even as he watched it faded back to normal
he clenched his fist then a wave of exhaustion washed over him
he felt strange and feeble as if he had not eaten for days
his knees buckled and he sagged against a wall