saphira be more ** he exclaimed
oops
she lowered her head and rubbed her dust caked snout against the edge of one foreleg scratching at her nostrils
the mead tickles
really you ought to know better by now he grumbled as he climbed onto her back
after rubbing her snout against her foreleg once more saphira leaped high into the air and gliding over the varden is camp returned eragon to his tent
he slid off her then stood looking up at saphira
for a time they said nothing allowing their shared emotions to speak for them
saphira blinked and he thought her eyes glistened more than normal
this is a test she said
if we pass it we shall be the stronger for it as dragon and rider
we must be able to function by ourselves if necessary else we will forever be at a disadvantage compared with others
yes
she gouged the earth with her clenching claws
knowing that does nothing to ease my pain however
a shiver ran the length of her sinuous body
she shuffled her wings
may the wind rise under your wings and the sun always be at your back
travel well and travel fast little one
eragon felt that if he remained with her any longer he would never leave so he whirled around and without a backward glance plunged into the dark interior of his tent
the connection between them which had become as integral to him as the structure of his own flesh he severed completely
they would soon be too far apart to sense each other is minds anyway and he had no desire to prolong the agony of their parting
he stood where he was for a moment gripping the hilt of the falchion and swaying as if he were dizzy
already the dull ache of loneliness suffused him and he felt small and isolated without the comforting presence of saphira is consciousness
i did this before and i can do this again he thought and forced himself to square his shoulders and lift his chin
from underneath his cot he removed the pack he had made during his trip from helgrind
into it he placed the carved wooden tube wrapped in cloth that contained the scroll of the poem he had written for the agaeti blodhren which oromis had copied for him in his finest calligraphy the flask of enchanted faelnirv and the small soapstone box of nalgask that were also gifts from oromis the thick book domia abr wyrda which was jeod is present his whetstone and his strop and after some hesitation the many pieces of his armor for he reasoned if the occasion arises where i need it i will be more happy to have it than i will be miserable carrying it all the way to farthen dur
or so he hoped
the book and the scroll he took because after having done so much traveling he had concluded that the best way to avoid losing the objects he cared about was to keep them with him wherever he went
the only extra clothes he decided to bring were a pair of gloves which he stuffed inside his helmet and his heavy woolen cloak in case it got cold when they stopped nights
all the rest he left rolled up in saphira is saddlebags
if i really am a member of durgrimst ingeitum he thought they will clothe me properly when i arrive at bregan hold
cinching off the pack he lay his unstrung bow and quiver across the top and lashed them to the frame
he was about to do the same with the falchion when he realized that if he leaned to the side the sword could slide out of the sheath
therefore he tied the sword flat against the rear of the pack angling it so the hilt would ride between his neck and his right shoulder where he could still draw it if need be
eragon donned the pack and then stabbed through the barrier in his mind feeling the energy surging in his body and in the twelve diamonds mounted on the belt of beloth the wise
tapping into that flow of force he murmured the spell he had cast but once be fore that which bent rays of light around him and rendered him invisible
a slight pall of fatigue weakened his limbs as he released the spell
he glanced downward and had the disconcerting experience of looking through where he knew his torso and legs to be and seeing the imprint of his boots on the dirt below