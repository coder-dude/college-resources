you came to supervise eragon is training orik vodhr
unless you give me your word you may as well return to farthen dur
at last orik said i believe that you mean no harm to dwarves or to the varden else i would never agree
and i hold you to the honor of your hall and clan that this is not a ploy to deceive us
tell me what to say
while the queen tutored orik in the correct pronunciation of the desired phrase eragon asked saphira should i do it
do we have a choice eragon remembered that arya had asked the same question yesterday and he began to have an inkling of what she had meant the queen left no room to maneuver
when orik finished islanzadi looked expectantly at eragon
he hesitated then delivered the oath as did saphira
thank you said islanzadi
now we may proceed
at the top of the knoll the trees were replaced by a bed of red clover that ran several yards to the edge of a stone cliff
the cliff extended a league in either direction and dropped a thousand feet to the forest below which pooled outward until it merged with the sky
it felt as if they stood on the edge of the world staring across an endless expanse of forest
i know this place realized eragon remembering his vision of togira ikonoka
** air shivered from the strength of the **
another dull blow made eragon is teeth **
he jammed his fingers in his ears trying to protect them from the painful spikes in pressure
the elves stood **
the clover bent under a sudden gust of wind
** below the edge of the cliff rose a huge gold dragon with a rider on its back