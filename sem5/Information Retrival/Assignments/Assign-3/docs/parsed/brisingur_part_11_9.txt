alagaesia does not need another tyrant king
arya rubbed her temples
why does everything have to be so complicated with you eragon no matter where you go you seem to get yourself mired in difficult situations
it is as if you make an effort to walk through every bramble in the land
your mother said much the same
i am not surprised
very well let it be
neither of us is about to change our opinions and we have more pressing concerns than arguing about justice and morality
in the future though you would do well to remember who you are and what you mean to the races of alagaesia
i never ** eragon paused waiting for her response but arya let his statement pass unchallenged
sitting on the edge of the table he said you did not have to come looking for me you know
i was fine
i guessed which route you would take from helgrind
luckily for me my guess placed me forty miles west of here and that was close enough for me to locate you by listening to the whispers of the land
a rider does not walk unnoticed in this world eragon
those who have the ears to hear and the eyes to see can interpret the signs easily enough
the birds sing of your coming the beasts of the earth heed your scent and the very trees and grass remember your touch
the bond between rider and dragon is so powerful that those who are sensitive to the forces of nature can feel it
you will have to teach that trick to me sometime
it is no trick merely the art of paying attention to what is already around you
why did you come to eastcroft though it would have been safer to meet me outside the village
circumstances forced me here as i assume they did you
you did not come here willingly no
no
he rolled his shoulders weary from the day is traveling
pushing back sleep he waved a hand at her dress and said have you finally abandoned your shirt and trousers
a small smile appeared on arya is face
only for the duration of this trip
i ve lived among the varden for more years than i care to recall yet i still forget how humans insist upon separating their women from their men
i never could bring myself to adopt your customs even if i did not conduct myself entirely as an elf
who was to say yea or nay to me my mother she was on the other side of ** arya seemed to catch herself then as if she had said more than she intended
she continued
in any event i had an unfortunate encounter with a pair of ox herders soon after i left the varden and i stole this dress directly afterward
one of the advantages of being a spellcaster is that you never have to wait for a tailor
eragon laughed for a moment
then he asked what now
now we rest
tomorrow before the sun rises we shall slip out of eastcroft and no one shall be the wiser
as the empty hours crept by eragon stared at the beams above his head and traced the cracks in the wood unable to calm his racing thoughts
he tried every method he knew to relax but his mind kept returning to arya to his surprise at meeting her to her comments about his treatment of sloan and above all else to the feelings he had for her
what those were exactly he was unsure
he longed to be with her but she had rejected his advances and that tarnished his affection with hurt and anger and also frustration for while eragon refused to accept that his suit was hopeless he could not think of how to proceed
an ache formed in his chest as he listened to the gentle rise and fall of arya is breathing
it tormented him to be so close and yet be unable to approach her
he twisted the edge of his tunic between his fingers and wished there was something he could do instead of resigning himself to an unwelcome fate
he wrestled with his unruly emotions deep into the night until finally he succumbed to exhaustion and drifted into the waiting embrace of his waking dreams
there he wandered for a few fitful hours until the stars began to fade and it was time for him and arya to leave eastcroft
together they opened the window and jumped from the sill to the ground twelve feet below a small drop for one with an elf is abilities
as she fell arya grasped the skirt of her dress to keep it from billowing around her