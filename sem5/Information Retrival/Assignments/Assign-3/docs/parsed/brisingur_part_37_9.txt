you can not say he
still your tongue if you please orik said gannel
shouting will not settle this point
orik nado iorunn if you will come with me
worry began to gnaw at eragon as the four dwarves went and conferred with the readers of law for several minutes
surely they wo not let vermund escape punishment just because of some verbal ** he thought
returning to the table iorunn said the readers of law are unanimous
even though eragon is a sworn member of durgrimst ingeitum he also holds positions of importance beyond our realm namely that of dragon rider but also that of an official envoy of the varden sent by nasuada to witness the coronation of our next ruler and also that of a friend of high influence with queen islanzadi and her race as a whole
for those reasons eragon is due the same hospitality we would extend to any visiting ambassador prince monarch or other person of ** the dwarf woman glanced sidelong at eragon her dark flashing eyes bold upon his limbs
in short he is our honored guest and we should treat him as such
which every knurla who is not cave mad ought to know
aye he is our guest concurred nado
his lips were pinched and white and his cheeks drawn as if he had just bitten into an apple only to discover it was not yet ripe
what say you now vermund demanded gannel
rising from his seat the purple veiled dwarf looked around the table gazing at each of the clan chiefs in turn
i say this and hear me well grimstborithn if any clan turns their ax against az sweldn rak anhuin because of these false accusations we shall consider it an act of war and we shall respond appropriately
if you imprison me that too we shall consider an act of war and we shall respond ** eragon saw vermund is veil twitch and he thought the dwarf might have smiled underneath
if you strike at us in any possible way whether with steel or with words no matter how mild your rebuke we shall consider it an act of war and we shall respond appropriately
unless you are eager to rend our country into a thousand bloody scraps i suggest you let the wind waft away this morning is discussion and in its place fill your minds with thoughts of who should next rule from upon the granite throne
the clan chiefs sat in silence for a long while
eragon had to bite his tongue to keep from jumping onto the table and railing against vermund until the dwarves agreed to hang him for his crimes
he reminded himself that he had promised orik that he would follow orik is lead when dealing with the clanmeet
orik is my clan chief and i must let him respond to this as he sees fit
freowin unfolded his hands and slapped the table with a meaty palm
with his hoarse baritone voice which carried throughout the room although it seemed no louder than a whisper the corpulent dwarf said you have shamed our race vermund
we cannot retain our honor as knurlan and ignore your trespass
the elderly dwarf woman hadfala shuffled her sheaf of rune covered pages and said what did you think to accomplish besides our doom by killing eragon even if the varden could unseat galbatorix without him what of the sorrow the dragon saphira would rain down upon us if we slew her rider she would fill farthen dur with a sea of our own blood
not a word came from vermund
laughter broke the quiet
the sound was so unexpected at first eragon did not realize it was coming from orik
his mirth subsiding orik said if we move against you or az sweldn rak anhuin you will consider it an act of war vermund very well then we shall not move against you not at all
vermund is brow beetled
how can this provide you with a source of amusement
orik chuckled again
because i have thought of something you have not vermund
you wish us to leave you and your clan alone then i propose to the clanmeet that we do as vermund wishes
if vermund had acted upon his own and not as a grimstborith he would be banished for his offenses upon pain of death
therefore let us treat the clan as we would treat the person let us banish az sweldn rak anhuin from our hearts and minds until they choose to replace vermund with a grimstborith of a more moderate temperament and until they acknowledge their villainy and repent of it to the clanmeet even if we must wait a thousand years