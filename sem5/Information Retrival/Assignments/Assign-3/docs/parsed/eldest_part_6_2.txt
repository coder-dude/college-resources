no
continue to keep ajihad is instruction secret
it would be unwise to bandy it about as people might take it to mean that he wanted you to succeed him and that would undermine my authority and destabilize the varden
he said what he thought he had to in order to protect the varden
i would have done the same
my father
she faltered briefly
my father is work will not go unfinished even if it takes me to the grave
that is whati want you as a rider to understand
all of ajihad is plans all his strategies and goals they are mine now
i will not fail him by being weak
the empirewill be brought down galbatorixwill be dethroned and the rightful governmentwill be raised
by the time she finished a tear ran down her cheek
eragon stared appreciating how difficult her position was and recognizing a depth of character he had not perceived before
and what of me nasuada what shall i do in the varden
she looked directly into his eyes
you can do whatever you want
the council members are fools if they think to control you
you are a hero to the varden and the dwarves and even the elves will hail your victory over durza when they hear of it
if you go against the council or me we will be forced to yield for the people will support you wholeheartedly
right now you are the most powerful person in the varden
however if you accept my leadership i will continue the path laid down by ajihad you will go with arya to the elves be instructed there then return to the varden
why is she so honest with us wondered ** she is right could we have refused the council is demands
saphira took a moment to ** way it is too late
you have already agreed to their requests
i think nasuada is honest because your spell lets her be and also because she hopes to win our loyalty from the elders
an idea suddenly came to eragon but before sharing it he asked can we trust her to hold to what she is said this is very important
yes said ** spoke with her heart
then eragon shared his proposal with saphira
she consented so he drew zar roc and walked to nasuada
he saw a flash of fear as he approached her gaze darted toward the door and she slipped a hand into a fold in her dress and grasped something
eragon stopped before her then knelt zar roc flat in his hands
nasuada saphira and i have been here for only a short while
but in that time we came to respect ajihad and now in turn you
you fought under farthen dur when others fled including the two women of the council and have treated us openly instead of with deception
therefore i offer you my blade
and my fealty as a rider
eragon uttered the pronouncement with a sense of finality knowing he would never have mouthed it before the battle
seeing so many men fall and die around him had altered his perspective
resisting the empire was no longer something he did for himself but for the varden and all the people still trapped under galbatorix is rule
however long it would take he had dedicated himself to that task
for the time being the best thing he could do was serve
still he and saphira were taking a terrible risk in pledging themselves to nasuada
the council could not object because all eragon had said was that he would swear fealty but not to whom
even so he and saphira had no guarantee that nasuada would make a good ** is better to be sworn to an honest fool than to a lying scholar decided eragon
surprise flitted across nasuada is face
she grasped zar roc is hilt and lifted it staring at its crimson blade then placed the tip on eragon is head
i do accept your fealty with honor rider as you accept all the responsibilities accompanying the station
rise as my vassal and take your sword
eragon did as he was bidden
he said now i can tell you openly as my master the council made me agree to swear to the varden once you were appointed
this was the only way saphira and i could circumvent them
nasuada laughed with genuine delight
ah i see you have already learned how to play our game
very well as my newest and only vassal will you agree to give your fealty to me again in public when the council expects your vow