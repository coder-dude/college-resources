if i did no doubt i would also understand how galbatorix has managed to increase his own strength to such unnatural heights but alas i do not
but oromis does eragon thought
or at least the elf had hinted as much
however he had yet to share the information with eragon and saphira
as soon as they were able to return to du weldenvarden eragon intended to ask the elder rider for the truth of the matter
he has to tell us ** because of our ignorance murtagh defeated us and he could have easily taken us to galbatorix
eragon almost mentioned oromis is comments to arya but held his tongue for he realized that oromis would not have concealed such an important fact for over a hundred years unless secrecy was of the utmost importance
arya signed a stop to the sentence she had been writing on the ground
bending over eragon read adrift upon the sea of time the lonely god wanders from shore to distant shore upholding the laws of the stars above
i do not know she said and smoothed out the line with a sweep of her arm
why is it he asked speaking slowly as he organized his thoughts that no one ever refers to the dragons of the forsworn by name we say morzan is dragon or kialandi is dragon but we never actually name the dragon
surely they were as important as their ** i do not even remember seeing their names in the scrolls oromis gave me
although they must have been there
yes i am certain they were but for some reason they do not stick in my head
is not that strange arya started to answer but before she could do more than open her mouth he said for once i am glad saphira is not here
i am ashamed i have not noticed this before
even you arya and oromis and every other elf i ve met refuse to call them by name as if they were dumb animals undeserving of the honor
do you do it on purpose is it because they were your enemies
did none of your lessons speak of this asked arya
she seemed genuinely surprised
i think he said glaedr mentioned something about it to saphira but i am not exactly sure
i was in the middle of a backbend during the dance of snake and crane so i was not really paying attention to what saphira was ** he laughed a little embarrassed by his lapse and feeling as if he had to explain himself
it got confusing at times
oromis would be talking to me while i was listening to saphira is thoughts while she and glaedr communicated with their minds
what is worse glaedr rarely uses a recognizable language with saphira he tends to use images smells and feelings rather than words
instead of names he sends impressions of the people and objects he means
do you recall nothing of what he said whether with words or not
eragon hesitated
only that it concerned a name that was no name or some such
i could not make heads or tails out of it
what he spoke of said arya was du namar aurboda the banishing of the names
touching her dry blade of grass to the ground she resumed writing in the dirt
it is one of the most significant events that happened during the fighting between the riders and the forsworn
when the dragons realized that thirteen of their own had betrayed them that those thirteen were helping galbatorix to eradicate the rest of their race and that it was unlikely anyone could stop their rampage the dragons grew so angry every dragon not of the forsworn combined their strength and wrought one of their inexplicable pieces of magic
together they stripped the thirteen of their names
awe crawled over eragon
how is that possible
did i not just say it was inexplicable all we know is that after the dragons cast their spell no one could utter the names of the thirteen those who remembered the names soon forgot them and while you can read the names in scrolls and letters where they are recorded and even copy them if you look at only one glyph at a time they are as gibberish
the dragons spared jarnunvosk galbatorix is first dragon for it was not his fault he was killed by urgals and also shruikan for he did not choose to serve galbatorix but was forced to by galbatorix and morzan
what a horrible fate to lose one is name thought eragon