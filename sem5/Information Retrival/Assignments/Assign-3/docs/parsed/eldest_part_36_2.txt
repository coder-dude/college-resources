the constant heat drained her strength and made even the smallest task arduous
she suspected she would feel tired even if it were winter
familiar as she was with the innermost secrets of the varden it still had taken more work than she expected to transport the entire organization from farthen dur through the beor mountains and deliver them to surda and aberon
she shuddered remembering long uncomfortable days spent in the saddle
planning and executing their departure had been exceedingly difficult as was integrating the varden into their new surroundings while simultaneously preparing for an attack on the ** do not have enough time each day to solve all these problems she lamented
finally she dropped the fan and rang the bellpull summoning her handmaid farica
the banner hanging to the right of the cherrywood desk rippled as the door hidden behind it opened
farica slipped out to stand with downcast eyes by nasuada is elbow
are there any more asked nasuada
she tried not to let her relief show
once a week she held an open court to resolve the varden is various disputes
anyone who felt that they had been wronged could seek an audience with her and ask for her judgment
she could not imagine a more difficult and thankless chore
as her father had often said after negotiating with hrothgar a good compromise leaves everyone ** and so it seemed
returning her attention to the matter at hand she told farica i want that gamble reassigned
give him a job where his talent with words will be of some use
quartermaster perhaps just so long as it is a job where he will get full rations
i do not want to see him before me for stealing again
farica nodded and went to the desk where she recorded nasuada is instructions on a parchment scroll
that skill alone made her invaluable
farica asked where can i find him
one of the work gangs in the quarry
yes ma am
oh while you were occupied king orrin asked that you join him in his laboratory
what has he done in there now blind himself nasuada washed her wrists and neck with lavender water then checked her hair in the mirror of polished silver that orrin had given her and tugged on her overgown until the sleeves were straight
satisfied with her appearance she swept out of her chambers with farica in tow
the sun was so bright today that no torches were needed to illuminate the inside of borromeo castle nor could their added warmth have been tolerated
shafts of light fell through the crossletted arrow slits and glowed upon the inner wall of the corridor striping the air with bars of golden dust at regular intervals
nasuada looked out one embrasure toward the barbican where thirty or so of orrin is orange clad cavalry soldiers were setting forth on another of their ceaseless rounds of patrols in the countryside surrounding aberon
not that they could do much good if galbatorix decided to attack us himself she thought bitterly
their only protection against that was galbatorix is pride and she hoped his fear of eragon
all leaders were aware of the risk of usurpation but usurpers themselves were doubly afraid of the threat that a single determined individual could pose
nasuada knew that she was playing an exceedingly dangerous game with the most powerful madman in alagaesia
if she misjudged how far she could push him she and the rest of the varden would be destroyed along with any hope of ending galbatorix is reign
the clean smell of the castle reminded her of the times she had stayed there as a child back when orrin is father king larkin still ruled
she never saw much of orrin then
he was five years older than her and already occupied with his duties as a prince
nowadays though she often felt as if she were the elder one
at the door to orrin is laboratory she had to stop and wait for his bodyguards who were always posted outside to announce her presence to the king
soon orrin is voice boomed out into the stairwell lady ** i am so glad you came
i have something to show you
mentally bracing herself she entered the laboratory with farica
a maze of tables laden with a fantastic array of alembics beakers and retorts confronted them like a glass thicket waiting to snag their dresses on any one of its myriad fragile branches