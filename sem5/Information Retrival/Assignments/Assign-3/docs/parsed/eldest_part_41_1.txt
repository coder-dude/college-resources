bright morning arrived all too soon
jolted to awareness by the buzz of the vibrating timepiece eragon grabbed his hunting knife and sprang out of bed expecting an attack
he gasped as his body shrieked with protest from the abuse of the past two days
blinking away tears eragon rewound the timepiece
orik was gone the dwarf must have slipped away in the wee hours of the morning
with a groan eragon hobbled to the wash closet for his daily ablutions like an old man afflicted by rheumatism
he and saphira waited by the tree for ten minutes before they were met by a solemn black haired elf
the elf bowed touched two fingers to his lips which eragon mirrored and then preempted eragon by saying may good fortune rule over you
and may the stars watch over you replied eragon
did oromis send you
the elf ignored him and said to saphira well met dragon
i am vanir of house ** eragon scowled with annoyance
only then did the elf address eragon i will show you where you may practice with your ** he strode away not waiting for eragon to catch up
the sparring yard was dotted with elves of both sexes fighting in pairs and groups
their extraordinary physical gifts resulted in flurries of blows so quick and fast they sounded like bursts of hail striking an iron bell
under the trees that fringed the yard individual elves performed the rimgar with more grace and flexibility than eragon thought he would ever achieve
after everyone on the field stopped and bowed to saphira vanir unsheathed his narrow blade
if you will guard your sword silver hand we can begin
eragon eyed the inhuman swordsmanship of the other elves with ** do i have to do this he ** will just be humiliated
you will be fine said saphira yet he could sense her concern for him
as he prepared zar roc eragon is hands trembled with dread
instead of throwing himself into the fray he fought vanir from a distance dodging sidestepping and doing everything possible to avoid triggering another fit
despite eragon is evasions vanir touched him four times in rapid succession once each on his ribs shin and both shoulders
vanir is initial expression of stoic impassivity soon devolved into open contempt
dancing forward he slid his blade up zar roc is length while at the same time twirling zar roc in a circle wrenching eragon is wrist
eragon allowed zar roc to fly out of his hand rather than resist the elf is superior strength
vanir dropped his sword onto eragon is neck and said ** shrugging off the sword eragon trudged over to retrieve zar roc
dead said vanir
how do you expect to defeat galbatorix like this i expected better even from a weakling human
then why do not you fight galbatorix yourself instead of hiding in du weldenvarden
vanir stiffened with outrage
because he said cool and haughty i am not a rider
and if i were i would not be such a coward as you
no one moved or spoke on the field
his back to vanir eragon leaned on zar roc and craned his neck toward the sky snarling to ** knows nothing
this is just one more test to overcome
coward i say
your blood is as thin as the rest of your race is
i think that saphira was confused by galbatorix is wiles and made the wrong choice of ** the spectating elves gasped at vanir is words and muttered among themselves with open disapproval for his atrocious breach of etiquette
eragon ground his teeth
he could stand insults to himself but not to saphira
she was already moving when his pent up frustration fear and pain burst within him and he whirled around the tip of zar roc whistling through the air
the blow would have killed vanir had he not blocked it at the last second
he looked surprised by the ferocity of the attack
holding nothing in reserve eragon drove vanir to the center of the field jabbing and slashing like a madman determined to hurt the elf however he could
he nicked vanir on the hip with enough force to draw blood even with zar roc is blunted edge
at that instant eragon is back ruptured in an explosion of agony so intense he experienced it with all five senses as a deafening crashing waterfall of sound a metallic taste that coated his tongue an acrid eye watering stench in his nostrils redolent of vinegar pulsing colors and above all the feeling that durza had just laid open his back