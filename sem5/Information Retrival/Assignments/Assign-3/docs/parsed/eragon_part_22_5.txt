there is not time for formal instruction but we can do much while we travel said brom
i know many techniques you can practice that will give you strength and control but you cannot gain the discipline the riders had overnight
you he looked at eragon humorously will have to amass it on the run
it will be hard in the beginning but the rewards will be great
it may please you to know that no rider your age ever used magic the way you did yesterday with those two urgals
eragon smiled at the praise
thank you
does this language have a name
brom laughed
yes but no one knows it
it would be a word of incredible power something by which you could control the entire language and those who use it
people have long searched for it but no one has ever found it
i still do not understand how this magic works said eragon
exactly how do i use it
brom looked astonished
i have not made that clear
brom took a deep breath and said to work with magic you must have a certain innate power which is very rare among people nowadays
you also have to be able to summon this power at will
once it is called upon you have to use it or let it fade away
understood now if you wish to employ the power you must utter the word or phrase of the ancient language that describes your intent
for example if you had not saidbrisingr yesterday nothing would have happened
so i am limited by my knowledge of this language
exactly crowed brom
also while speaking it it is impossible to practice deceit
eragon shook his head
that can not be
people always lie
the sounds of the ancient words can not stop them from doing that
brom cocked an eyebrow and said fethrblaka eka weohnata neiat haina ono
blaka eom iet ** a bird suddenly flitted from a branch and landed on his hand
it trilled lightly and looked at them with beady eyes
after a moment he said eitha and it fluttered away
how did you do that asked eragon in wonder
i promised not to harm him
he may not have known exactly what i meant but in the language of power the meaning of my words was evident
the bird trusted me because he knows what all animals do that those who speak in that tongue are bound by their word
and the elves speak this language
not quite admitted brom
they maintain that they do not and in a way it is true but they have perfected the art of saying one thing and meaning another
you never know exactly what their intent is or if you have fathomed it correctly
many times they only reveal part of the truth and withhold the rest
it takes a refined and subtle mind to deal with their culture
eragon considered that
what do personal names mean in this language do they give power over people
brom is eyes brightened with approval
yes they do
those who speak the language have two names
the first is for everyday use and has little authority
but the second is their true name and is shared with only a few trusted people
there was a time when no one concealed his true name but this age is not as kind
whoever knows your true name gains enormous power over you
it is like putting your life into another person is hands
everyone has a hidden name but few know what it is
how do you find your true name asked eragon
elves instinctively know theirs
no one else has that gift
the human riders usually went on quests to discover it or found an elf who would tell them which was rare for elves do not distribute that knowledge freely replied brom
i d like to know mine eragon said wistfully
brom is brow darkened
be careful
it can be a terrible knowledge
to know who you are without any delusions or sympathy is a moment of revelation that no one experiences unscathed
some have been driven to madness by that stark reality
most try to forget it
but as much as the name will give others power so you may gain power over yourself if the truth does not break you
and i am sure that it would not stated saphira
i still wish to know said eragon determined
you are not easily dissuaded
that is good for only the resolute find their identity but i cannot help you with this
it is a search that you will have to undertake on your ** brom moved his injured arm and grimaced uncomfortably