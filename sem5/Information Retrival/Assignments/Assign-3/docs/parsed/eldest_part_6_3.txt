good that will take care of the council
now until then leave me
i have much planning to do and i must prepare for the funeral
remember eragon the bond we have just created is equally binding i am as responsible for your actions as you are required to serve me
do not dishonor me
nasuada paused then gazed into his eyes and added in a gentler tone you have my condolences eragon
i realize that others beside myself have cause for sorrow while i have lost my father you have also lost a friend
i liked murtagh a great deal and it saddens me that he is gone
goodbye eragon
eragon nodded a bitter taste in his mouth and left the room with saphira
the hallway outside was empty along its gray length
eragon put his hands on his hips tilted back his head and exhaled
the day had barely begun yet he was already exhausted by all the emotions that had flooded through him
saphira nosed him and said this way
without further explanation she headed down the right side of the tunnel
her polished claws clicked on the hard floor
eragon frowned but followed ** are we going no ** please
she just flicked her tail
resigned to wait he said instead things have certainly changed for us
i never know what to expect from one day to the next except sorrow and bloodshed
all is not bad she ** havewon a great victory
it should be celebrated not mourned
it does not help having to deal with this other nonsense
she snorted angrily
a thin line of fire shot from her nostrils singeing eragon is shoulder
he jumped back with a yelp biting back a string of ** said saphira shaking her head to clear the smoke
** you nearly roasted my side
i did not expect it to happen
i keep forgetting that fire will come out if i am not careful
imagine that every time you raised your arm lightning struck the ground
it would be easy to make a careless motion and destroy something unintentionally
you re right
sorry i growled at you
her bony eyelid clicked as she winked at ** matter
the point i was trying to make is that even nasuada can not force you to do anything
but i gave my word as a rider
maybe so but if i must break it to keep you safe or to do the right thing i will not hesitate
it is a burden i could easily carry
because i am joined to you my honor is inherent in your pledge but as an individual i am not bound by it
if i must i will kidnap you
any disobedience then would be no fault of your own
it should never come to that
if we have to use such tricks to do what is right then nasuada and the varden will have lost all integrity
saphira stopped
they stood before the carved archway of tronjheim is library
the vast silent room seemed empty though the ranks of back to back bookshelves interspersed with columns could conceal many people
lanterns poured soft light across the scroll covered walls illuminating the reading alcoves along their bases
weaving through the shelves saphira led him to one alcove where arya sat
eragon paused as he studied her
she seemed more agitated than he had ever seen her though it manifested itself only in the tension of her movements
unlike before she wore her sword with the graceful crossguard
one hand rested on the hilt
eragon sat at the opposite side of the marble table
saphira positioned herself between them where neither could escape her gaze
what have you done asked arya with unexpected hostility
she lifted her chin
what have you promised the varden what have you done
the last part even reached eragon mentally
he realized just how close the elf was to losing control
a bit of fear touched him
we only did what we had to
i am ignorant of elves customs so if our actions upset you i apologize
there is no cause to be angry
** you know nothing about me
i have spent seven decades representing my queen here fifteen years of which i bore saphira is egg between the varden and the elves
in all that time i struggled to ensure the varden had wise strong leaders who could resist galbatorix and respect our wishes
brom helped me by forging the agreement concerning the new rider you
ajihad was committed to your remaining independent so that the balance of power would not be upset