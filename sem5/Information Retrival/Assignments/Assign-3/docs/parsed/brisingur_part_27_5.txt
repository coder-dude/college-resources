it would do more than you imagine said nasuada
the star sapphire holds a special place in the hearts of dwarves
every dwarf has a love of gemstones but isidar mithrim they love and cherish above all others because of its beauty and most of all because of its immense size
restore it to its previous glory and you will restore the pride of their race
eragon said even if saphira failed to repair isidar mithrim she should be present for the coronation of the dwarves new ruler
you could conceal her absence for a few days by letting it be known among the varden that she and i have left on a brief trip to aberon or some such
by the time galbatorix is spies realized you had deceived them it would be too late for the empire to organize an attack before we returned
nasuada nodded
it is a good idea
contact me as soon as the dwarves set a date for the coronation
you have made your suggestion now out with your request
what is it you wish of me
since you insist i must make this trip with your permission i would like to fly with saphira from tronjheim to ellesmera after the coronation
to consult with the ones who taught us during our last visit to du weldenvarden
we promised them that as soon as events allowed we would return to ellesmera to complete our training
the line between nasuada is eyebrows deepened
there is not the time for you to spend weeks or months in ellesmera continuing your education
no but perhaps we have the time for a brief visit
nasuada leaned her head against the back of her carved chair and gazed down at eragon from underneath heavy lids
and who exactly are your teachers i have noticed you always evade direct questions about them
who was it that taught the two of you in ellesmera eragon
fingering his ring aren eragon said we swore an oath to islanzadi that we would not reveal their identity without permission from her arya or whoever may succeed islanzadi to her throne
by all the demons above and below how many oaths have you and saphira sworn demanded nasuada
you seem to bind yourself to everyone you meet
feeling somewhat sheepish eragon shrugged and had opened his mouth to speak when saphira said to nasuada we do not seek them out but how can we avoid pledging ourselves when we cannot topple galbatorix and the empire without the support of every race in alagaesia oaths are the price we pay for winning the aid of those in power
mmh said nasuada
so i must ask arya for the truth of the matter
aye but i doubt she will tell you the elves consider the identity of our teachers to be one of their most precious secrets
they will not risk sharing it unless absolutely necessary to keep word of it from reaching ** eragon stared at the royal blue gemstone set in his ring wondering how much more information his oath and his honor would allow him to divulge then said know this though we are not so alone as we once assumed
nasuada is expression sharpened
i see
that is good to know eragon
i only wish the elves were more forthcoming with ** after pursing her lips for a brief moment nasuada continued
why must you travel all the way to ellesmera have you no means to communicate with your tutors directly
eragon spread his hands in a gesture of helplessness
if only we could
alas the spell has yet to be invented that can broach the wards that encircle du weldenvarden
the elves did not even leave an opening they themselves can exploit
if they had arya would have contacted queen islanzadi as soon as she was revived in farthen dur rather than physically going to du weldenvarden
i suppose you are right
but then how was it you were able to consult islanzadi about sloan is fate you implied that when you spoke with her the elves army was still situated within du weldenvarden
they were he said but only in the fringe beyond the protective measures of the wards
the silence between them was palpable as nasuada considered his request
outside the tent eragon heard the nighthawks arguing among themselves about whether a bill or a halberd was better suited for fighting large numbers of men on foot and beyond them the creak of a passing oxcart the jangle of armor on men trotting in the opposite direction and hundreds of other indistinct sounds that drifted through the camp