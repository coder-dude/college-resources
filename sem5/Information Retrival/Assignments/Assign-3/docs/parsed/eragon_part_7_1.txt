at dawn the sun is rays streamed through the window warming eragon is face
rubbing his eyes he sat up on the edge of the bed
the pine floor was cold under his feet
he stretched his sore legs and rubbed his back yawning
beside the bed was a row of shelves covered with objects he had collected
there were twisted pieces of wood odd bits of shells rocks that had broken to reveal shiny interiors and strips of dry grass tied into knots
his favorite item was a root so convoluted he never tired of looking at it
the rest of the room was bare except for a small dresser and nightstand
he pulled on his boots and stared at the floor thinking
this was a special day
it was near this very hour sixteen years ago that his mother selena had come home to carvahall alone and pregnant
she had been gone for six years living in the cities
when she returned she wore expensive clothes and her hair was bound by a net of pearls
she had sought out her brother garrow and asked to stay with him until the baby arrived
within five months her son was born
everyone was shocked when selena tearfully begged garrow and marian to raise him
when they asked why she only wept and said i ** her pleas had grown increasingly desperate until they finally agreed
she named him eragon then departed early the next morning and never returned
eragon still remembered how he had felt when marian told him the story before she died
the realization that garrow and marian were not his real parents had disturbed him greatly
things that had been permanent and unquestionable were suddenly thrown into doubt
eventually he had learned to live with it but he always had a nagging suspicion that he had not been good enough for his ** am sure there was a good reason for what she did i only wish i knew what it was
one other thing bothered him who was his father selena had told no one and whoever it might be had never come looking for eragon
he wished that he knew who it was if only to have a name
it would be nice to know his heritage
he sighed and went to the nightstand where he splashed his face shivering as the water ran down his neck
refreshed he retrieved the stone from under the bed and set it on a shelf
the morning light caressed it throwing a warm shadow on the wall
he touched it one more time then hurried to the kitchen eager to see his family
garrow and roran were already there eating chicken
as eragon greeted them roran stood with a grin
roran was two years older than eragon muscular sturdy and careful with his movements
they could not have been closer even if they had been real brothers
roran smiled
i am glad you re back
how was the trip
hard replied eragon
did uncle tell you what happened he helped himself to a piece of chicken which he devoured hungrily
no said roran and the story was quickly told
at roran is insistence eragon left his food to show him the stone
this elicited a satisfactory amount of awe but roran soon asked nervously were you able to talk with katrina
no there was not an opportunity after the argument with sloan
but she will expect you when the traders come
i gave the message to horst he will get it to her
you told horst said roran incredulously
that was private
if i wanted everyone to know about it i could have built a bonfire and used smoke signals to communicate
if sloan finds out he wo not let me see her again
horst will be discreet assured eragon
he wo not let anyone fall prey to sloan least of all ** roran seemed unconvinced but argued no more
they returned to their meals in the taciturn presence of garrow
when the last bites were finished all three went to work in the fields
the sun was cold and pale providing little comfort
under its watchful eye the last of the barley was stored in the barn
next they gathered prickly vined squash then the rutabagas beets peas turnips and beans which they packed into the root cellar
after hours of labor they stretched their cramped muscles pleased that the harvest was finished
the following days were spent pickling salting shelling and preparing the food for winter
nine days after eragon is return a vicious blizzard blew out of the mountains and settled over the valley