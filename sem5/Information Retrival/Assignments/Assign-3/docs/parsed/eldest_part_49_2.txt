where go you little one she asked
he saw arya rise from her mother is side make her way through the gathered elves and then like a forest sprite glide underneath the trees ** walk between the candle and the dark he replied and followed arya
eragon tracked arya by her delicate scent of crushed pine needles by the feathery touch of her foot upon the ground and by the disturbance of her wake in the air
he found her standing alone on the edge of a clearing poised like a wild creature as she watched the constellations turn in the sky above
as eragon emerged in the open arya looked at him and he felt as if she saw him for the first time
her eyes widened and she whispered is that you eragon
what have they done to you
he went to her and together they wandered the dense woods which echoed with fragments of music and voices from the festivities
changed as he was eragon was acutely conscious of arya is presence of the whisper of her clothes over her skin of the soft pale exposure of her neck and of her eyelashes which were coated with a layer of oil that made them glisten and curl like black petals wet with rain
they stopped on the bank of a narrow stream so clear it was invisible in the faint light
the only thing that betrayed its presence was the throaty gurgle of water pouring over rocks
around them the thick pines formed a cave with their branches hiding eragon and arya from the world and muffling the cool still air
the hollow seemed ageless as if it were removed from the world and protected by some magic against the withering breath of time
in that secret place eragon felt suddenly close to arya and all his passion for her sprang to the fore of his mind
he was so intoxicated with the strength and vitality coursing through his veins as well as the untamed magic that filled the forest he ignored caution and said how tall the trees how bright the stars
and how beautiful you are o arya svit ** under normal circumstances he would have considered his deed the height of folly but in that fey madcap night it seemed perfectly sane
she stiffened
eragon
he ignored her warning
arya i will do anything to win your hand
i would follow you to the ends of the earth
i would build a palace for you with nothing but my bare hands
i would
will you stop pursuing me can you promise me that when he hesitated she stepped closer and said low and gentle eragon this cannot be
you are young and i am old and that shall never change
do you feel nothing for me
my feelings for you she said are those of a friend and nothing more
i am grateful to you for rescuing me from gil ead and i find your company pleasant
that is all
relinquish this quest of yours it will only bring you heartache and find someone your own age to spend the long years with
his eyes brimmed with tears
how can you be so cruel
i am not cruel but kind
you and i are not meant for each other
in desperation he suggested you could give me your memories and then i would have the same amount of experience and knowledge as you
it would be an ** arya lifted her chin her face grave and solemn and brushed with silver from the glimmering stars
a hint of steel entered her voice hear me well eragon
this cannot nor ever shall be
and until you master yourself our friendship must cease to exist for your emotions do nothing but distract us from our ** she bowed to him
goodbye eragon ** then she strode past and vanished into du weldenvarden
now the tears spilled down eragon is cheeks and dropped to the moss below where they lay unabsorbed like pearls strewn across a blanket of emerald velvet
numb eragon sat upon a rotting log and buried his face in his hands weeping that his affection for arya was doomed to remain unrequited and weeping that he had driven her further away
within moments saphira joined ** little one
she nuzzled ** did you have to inflict this upon yourself you knew what would happen if you tried to woo arya again
i could not stop ** wrapped his arms around his belly and rocked back and forth on the log reduced to hiccuping sobs by the strength of his misery