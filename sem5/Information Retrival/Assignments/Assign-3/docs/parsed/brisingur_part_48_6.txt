i doubt you would be in any more danger if the whole world knew you were brom is heir
murtagh eragon said
he believes we are full brothers
he told me so in the ancient language
and i am sure galbatorix does as well
it was the twins who figured out that murtagh is mother and your mother were one and the same person and this they conveyed to the king
but they could not have informed him of brom is involvement for there was no one among the varden who was privy to that information
eragon glanced up as a pair of swallows swooped by overhead and he allowed himself a wry half smile
why do you smile oromis asked
i am not sure you would understand
the elf folded his hands in his lap
i might not that is true
but then you cannot know for certain unless you try to explain
it took eragon a while to find the words he needed
when i was younger before
all of this he gestured at saphira and oromis and glaedr and the world in general i used to amuse myself by imagining that because of her great wit and beauty my mother had been taken in among the courts of galbatorix is nobles
i imagined that she had traveled from city to city and supped with the earls and ladies in their halls and that
well she had fallen desperately in love with a rich and powerful man but for some reason she was forced to hide me from him so she gave me to garrow and marian for safekeeping and one day she would return and tell me who i was and that she had never wanted to leave me behind
that is not so different from what happened said oromis
no it is not but
i imagined that my mother and my father were people of importance and i was someone of importance as well
fate gave me what i wanted but the truth of it is not as grand or as happy as i thought it would be
i was smiling at my own ignorance i suppose and also at the unlikeliness of everything that has befallen me
a light breeze swept across the clearing feathering the grass at their feet and stirring the branches of the forest around them
eragon watched the fluttering of the grass for a few moments then slowly asked was my mother a good person
i could not say eragon
the events of her life were complicated
it would be foolish and arrogant of me to presume to pass judgment on one i know so little of
but i need to ** eragon clasped his hands pressing his fingers between the calluses on his knuckles
when i asked brom if he had known her he said that she was proud and dignified and that she always helped the poor and those less fortunate than her
how could she though how could she be that person and also the black hand jeod told me stories about some of the things horrible terrible things she did while she was in morzan is service
was she evil then did she not care if galbatorix ruled or not why did she go with morzan in the first place
oromis paused
love can be a terrible curse eragon
it can make you overlook even the largest flaws in a person is behavior
i doubt that your mother was fully aware of morzan is true nature when she left carvahall with him and once she had he would not have allowed her to disobey his wishes
she became his slave in all but name and it was only by changing her very identity that she was able to escape his control
but jeod said that she enjoyed what she did as the black hand
an expression of faint disdain altered oromis is features
accounts of past atrocities are often exaggerated and distorted
that much you should keep in mind
no one but your mother knows exactly what she did nor why nor how she felt about it and she is not still among the living to explain herself
whom should i believe though pleaded eragon
brom or jeod
when you asked brom about your mother he told you what he thought were her most important qualities
my advice would be to trust in his knowledge of her
if that does not quell your doubts remember that whatever crimes she may have committed while acting as the hand of morzan ultimately your mother sided with the varden and went to extraordinary lengths to protect you
knowing that you should not torment yourself further about the nature of her character