murtagh hesitated then handed over his long hand and a half sword
eragon blocked the edges with magic the way brom had taught him
while murtagh examined the blade eragon said i can undo that once we re finished
murtagh checked the balance of his sword
satisfied he said it will ** eragon safed zar roc settled into a crouch then swung at murtagh is shoulder
their swords met in midair
eragon disengaged with a flourish thrust and then riposted as murtagh parried dancing away
they struggled back and forth trying to batter each other down
after a particularly intense series of blows murtagh started laughing
not only was it impossible for either of them to gain an advantage but they were so evenly matched that they tired at the same rate
acknowledging with grins each other is skill they fought on until their arms were leaden and sweat poured off their sides
finally eragon called enough ** murtagh stopped in mid blow and sat down with a gasp
eragon staggered to the ground his chest heaving
none of his fights with brom had been this fierce
as he gulped air murtagh exclaimed you re ** i ve studied swordplay all my life but never have i fought one like you
you could be the king is weapon master if you wanted to
you re just as good observed eragon still panting
the man who taught you tornac could make a fortune with a fencing school
people would come from all parts of alagaesia to learn from him
thus it became their custom to fight in the evening which kept them lean and fit like a pair of matched blades
with his return to health eragon also resumed practicing magic
murtagh was curious about it and soon revealed that he knew a surprising amount about how it worked though he lacked the precise details and could not use it himself
whenever eragon practiced speaking in the ancient language murtagh would listen quietly occasionally asking what a word meant
on the outskirts of gil ead they stopped the horses side by side
it had taken them nearly a month to reach it during which time spring had finally nudged away the remnants of winter
eragon had felt himself changing during the trip growing stronger and calmer
he still thought about brom and spoke about him with saphira but for the most part he tried not to awaken painful memories
from a distance they could see the city was a rough barbaric place filled with log houses and yapping dogs
there was a rambling stone fortress at its center
the air was hazy with blue smoke
the place seemed more like a temporary trading post than a permanent city
five miles beyond it was the hazy outline of isenstar lake
they decided to camp two miles from the city for safety
while their dinner simmered murtagh said i am not sure you should be the one to go into gil ead
why i can disguise myself well enough said eragon
and dormnad will want to see the gedwey ignasia as proof that i really am a rider
perhaps said murtagh but the empire wants you much more than me
if i am captured i could eventually escape
but ifyou are taken they will drag you to the king where you will be in for a slow death by torture unless you join him
plus gil ead is one of the army is major staging points
those are not houses out there they re barracks
going in there would be like handing yourself to the king on a gilded platter
eragon asked saphira for her opinion
she wrapped her tail around his legs and lay next to ** should not have to ask me he speaks sense
there are certain words i can give him that will convince dormnad of his truthfulness
and murtagh is right if anyone is to risk capture it should be him because he would live through it
he ** do not like letting him put himself in danger for us
all right you can go he said reluctantly
but if anything goes wrong i am coming after you
murtagh laughed
that would be fit for a legend how a lone rider took on the king is army single ** he chuckled again and stood
is there anything i should know before going
should not we rest and wait until tomorrow asked eragon cautiously
why the longer we stay here the greater the chance that we will be discovered