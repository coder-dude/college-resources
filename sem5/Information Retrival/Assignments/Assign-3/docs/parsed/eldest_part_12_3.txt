** almost laughed
only a little while ago he would have been calling people sir not the other way around
he glanced at saphira
are you done or should we wait a few minutes
rolling her eyes she fit the rest of the meat into her mouth and split the bone with a loud ** am done
all right said eragon standing you can go jarsha
we know the way
it took almost half an hour to reach the study because of the city mountain is size
as during ajihad is rule the door was guarded but instead of two men an entire squad of battle hardened warriors now stood before it alert for the slightest hint of danger
they would clearly sacrifice themselves to protect their new leader from ambush or attack
though the men could not have failed to recognize eragon and saphira they barred the way while nasuada was alerted of her visitors
only then were the two allowed to enter
eragon immediately noticed a change a vase of flowers in the study
the small purple blossoms were unobtrusive but they suffused the air with a warm fragrance that for eragon evoked summers of fresh picked raspberries and scythed fields turning bronze under the sun
he inhaled appreciating the skill with which nasuada had asserted her individuality without obliterating ajihad is memory
she sat behind the broad desk still cloaked in the black of mourning
as eragon seated himself saphira beside him she said ** it was a simple statement neither friendly nor hostile
she turned away briefly then focused on him her gaze steely and intent
i have spent the last few days reviewing the varden is affairs such as they are
it was a dismal exercise
we are poor overextended and low on supplies and few recruits are joining us from the empire
i mean to change that
the dwarves cannot support us much longer as it is been a lean year for farming and they ve suffered losses of their own
considering this i have decided to move the varden to surda
it is a difficult proposition but one i believe necessary to keep us safe
once in surda we will finally be close enough to engage the empire directly
even saphira stirred with ** work that would ** said ** could take months to get everyone is belongings to surda not to mention all the people
and they d probably be attacked along the way
i thought king orrin did not dare openly oppose galbatorix he protested
nasuada smiled grimly
his stance has changed since we defeated the urgals
he will shelter and feed us and fight by our side
many varden are already in surda mainly women and children who could not or would not fight
they will also support us else i will strip our name from them
how asked eragon did you communicate with king orrin so quickly
the dwarves use a system of mirrors and lanterns to relay messages through their tunnels
they can send a dispatch from here to the western edge of the beor mountains in less than a day
couriers then transport it to aberon capital of surda
fast as it is that method is still too slow when galbatorix can surprise us with an urgal army and give us less than a day is notice
i intend to arrange something far more expedient between du vrangr gata and hrothgar is magicians before we go
opening the desk drawer nasuada removed a thick scroll
the varden will depart farthen dur within the month
hrothgar has agreed to provide us with safe passage through the tunnels
moreover he sent a force to orthiad to remove the last vestiges of urgals and seal the tunnels so no one can invade the dwarves by that route again
as this may not be enough to guarantee the varden is survival i have a favor to ask of you
eragon nodded
he had expected a request or order
that was the only reason for her to have summoned them
i am yours to command
** her eyes flicked to saphira for a second
in any case this is not a command and i want you to think carefully before replying
to help rally support for the varden i wish to spread word throughout the empire that a new rider named eragon shadeslayer and his dragon saphira have joined our cause
i would like your permission before doing so however
word of our presence here will reach the empire anyway pointed out ** varden will want to brag about their victory and durza is death