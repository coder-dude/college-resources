next rhunon had saphira reheat the two bars of harder steel
rhunon lay the shining rods side by side on her anvil grasped both of them at either end with a pair of tongs and then twisted the rods around each other seven times
sparks shot into the air as she hammered upon the twists to weld them into a single piece of metal
the resulting mass of brightsteel rhunon folded welded and pounded back out to length another six times
when she was pleased with the quality of the metal rhunon flattened the brightsteel into a thick rectangular sheet cut the sheet in half lengthwise with a sharp chisel and bent each of the two halves down their middle so they were in the shape of long shallow v is
and all that eragon estimated rhunon was able to accomplish within the course of an hour and a half
he marveled at her speed even though it was his own body that carried out the tasks
never before had he seen a smith shape metal with such ease what would have taken horst hours took her only minutes
and yet no matter how demanding the forging was rhunon continued to sing weaving a fabric of spells within the brightsteel and guiding eragon is arm with infallible accuracy
amid the frenzy of noise fire sparks and exertion eragon thought he glimpsed as rhunon raked his eyes across the forge a trio of slender figures standing by the edge of the atrium
saphira confirmed his suspicion a moment later when she said eragon we are not alone
who are they he asked
saphira sent him an image of the short wizened werecat maud in human form standing between two pale elves who were no taller than she
one of the elves was male the other female and they were both extraordinarily beautiful even by the standards of the elves
their solemn teardrop faces seemed wise and innocent in equal measure which made it impossible for eragon to judge their age
their skin displayed a faint silvery sheen as if the two elves were so filled with energy it was seeping out of their very flesh
eragon queried rhunon as to the identity of the elves when she paused to allow his body a brief rest
rhunon glanced at them affording him a slightly better view then without interrupting her song she said with her thoughts they are alanna and dusan the only elf children in ellesmera
there was much rejoicing when they were conceived twelve years ago
they are like no other elves i have met he said
our children are special shadeslayer
they are blessed with certain gifts gifts of grace and gifts of power which no grown elf can hope to match
as we age our blossom withers somewhat although the magic of our early years never completely abandons us
rhunon wasted no more time talking
she had eragon place the wedge of brightsteel between the two v shaped strips and hammer on them until the strips nearly enveloped the wedge and friction held the three pieces together
then rhunon welded the pieces into a whole and while the metal was still hot she began to draw it out and form a rough blank of the sword
the soft wedge became the spine of the blade while the two harder strips became the sides edges and point
once the blank was nearly as long as the finished sword the work slowed as rhunon returned to the tang and carefully hammered her way up the blade establishing the final angles and proportions
rhunon had saphira heat the blade in segments of no more than six or seven inches at a time which rhunon arranged by holding the blade over one of saphira is nostrils through which saphira would release a single jet of fire
a host of writhing shadows fled toward the perimeter of the atrium every time the fire sprang into existence
eragon watched with amazement as his hands transformed the crude lump of metal into an elegant instrument of war
with every blow the outline of the blade became clearer as if the brightsteel wanted to be a sword and was eager to assume the shape rhunon desired
at last the forging came to a close and there on the anvil lay a long black blade which although it was still rough and incomplete already radiated a sense of deadly purpose