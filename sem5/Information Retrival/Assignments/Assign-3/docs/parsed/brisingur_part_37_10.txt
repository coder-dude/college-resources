the wrinkled skin around vermund is eyes went pale
you would not dare
orik smiled
ah but we would not lay a finger upon you or your kind
we will simply ignore you and refuse to trade with az sweldn rak anhuin
will you declare war upon us for doing nothing vermund for if the meet agrees with me that is exactly what we shall do nothing
will you force us at swordpoint to buy your honey and your cloth and your amethyst jewelry you have not the warriors to compel us ** turning to the rest of the table orik asked what say the rest of you
the clanmeet did not take long to decide
one by one the clan chiefs stood and voted to banish az sweldn rak anhuin
even nado galdhiem and havard vermund is erstwhile allies supported orik is proposal
with every vote of affirmation what skin was visible of vermund is face grew ever whiter until he appeared like a ghost dressed in the clothes of his former life
when the vote was finished gannel pointed toward the door and said begone vargrimstn vermund
leave tronjheim this very day and may none of az sweldn rak anhuin trouble the clanmeet until they have fulfilled the conditions we have set forth
until such time as that happens we shall shun every member of az sweldn rak anhuin
know this however while your clan may absolve themselves of their dishonor you vermund shall always remain vargrimstn even unto your dying day
such is the will of the clan ** his declaration concluded gannel sat
vermund remained where he was his shoulders quivering with an emotion eragon could not identify
it is you who have shamed and betrayed our race he growled
the dragon riders killed all of our clan save anhuin and her guards
you expect us to forget this you expect us to forgive this ** i spit on the graves of your ancestors
we at least have not lost our beards
we shall not cavort with this puppet of the elves while our dead family members still cry out for vengeance
outrage gripped eragon when none of the other clan chiefs replied and he was about to answer vermund is tirade with harsh words of his own when orik glanced over at him and shook his head ever so slightly
difficult as it was eragon kept his anger in check although he wondered why orik would allow such dire insults to pass uncontested
it is almost as if
oh
pushing himself away from the table vermund stood his hands balled into fists and his shoulders hunched high
he resumed speaking berating and disparaging the clan chiefs with increasing passion until he was shouting at the top of his lungs
no matter how vile vermund is imprecations were however the clan chiefs did not respond
they gazed into the distance as if pondering complex dilemmas and their eyes slid over vermund without pause
when in his fury vermund grasped hreidamar by the front of his mail hauberk three of hreidamar is guards jumped forward and pulled vermund away but as they did eragon noticed their expressions remained bland and unchanging as if they were merely helping hreidamar to straighten his hauberk
once they released vermund the guards did not look at him again
a chill crept up eragon is spine
the dwarves acted as if vermund had ceased to exist
so this is what it means to be banished among the dwarves
eragon thought he would rather be killed than suffer such a fate and for a moment he felt a stir of pity for vermund but his pity vanished an instant later as he remembered kvistor is dying expression
with a final oath vermund strode out of the room followed by those of his clan who had accompanied him to the meet
the mood among the remaining clan chiefs eased as the doors swung shut behind vermund
once again the dwarves gazed around without restriction and they resumed talking in loud voices discussing what else they would need to do with regard to az sweldn rak anhuin
then orik rapped the pommel of his dagger against the table and everyone turned to hear what he had to say
now that we have dealt with vermund there is another issue i wish the meet to consider
our purpose in assembling here is to elect hrothgar is successor
we have all had much to say upon the topic but now i believe the time is ripe to put words behind us and allow our actions to speak for us