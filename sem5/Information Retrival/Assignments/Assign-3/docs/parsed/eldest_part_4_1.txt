the songs of the dead are the lamentations of the living
so thought eragon as he stepped over a twisted and hacked urgal listening to the keening of women who removed loved ones from the blood muddied ground of farthen dur
behind him saphira delicately skirted the corpse her glittering blue scales the only color in the gloom that filled the hollow mountain
it was three days since the varden and dwarves had fought the urgals for possession of tronjheim the mile high conical city nestled in the center of farthen dur but the battlefield was still strewn with carnage
the sheer number of bodies had stymied their attempts to bury the dead
in the distance a mountainous fire glowed sullenly by farthen dur is wall where the urgals were being burned
no burial or honored resting place for them
since waking to find his wound healed by angela eragon had tried three times to assist in the recovery effort
on each occasion he had been racked by terrible pains that seemed to explode from his spine
the healers gave him various potions to drink
arya and angela said that he was perfectly sound
nevertheless he hurt
nor could saphira help only share his pain as it rebounded across their mental link
eragon ran a hand over his face and looked up at the stars showing through farthen dur is distant top which were smudged with sooty smoke from the ** days
three days since he had killed durza three days since people began calling him shadeslayer three days since the remnants of the sorcerer is consciousness had ravaged his mind and he had been saved by the mysterious togira ikonoka the cripple who is whole
he had told no one about that vision but saphira
fighting durza and the dark spirits that controlled him had transformed eragon although for better or for worse he was still unsure
he felt fragile as if a sudden shock would shatter his reconstructed body and consciousness
and now he had come to the site of the combat driven by a morbid desire to see its aftermath
upon arriving he found nothing but the uncomfortable presence of death and decay not the glory that heroic songs had led him to expect
before his uncle garrow was slain by the ra zac months earlier the brutality that eragon had witnessed between the humans dwarves and urgals would have destroyed him
now it numbed him
he had realized with saphira is help that the only way to stay rational amid such pain was todo things
beyond that he no longer believed that life possessed inherent meaning not after seeing men torn apart by the kull a race of giant urgals and the ground a bed of thrashing limbs and the dirt so wet with blood it soaked through the soles of his boots
if any honor existed in war he concluded it was in fighting to protect others from harm
he bent and plucked a tooth a molar from the dirt
bouncing it on his palm he and saphira slowly made a circuit through the trampled plain
they stopped at its edge when they noticed jormundur ajihad is second in command in the varden hurrying toward them from tronjheim
when he came near jormundur bowed a gesture eragon knew he would never have made just days before
i am glad i found you in time ** he clutched a parchment note in one hand
ajihad is returning and he wants you to be there when he arrives
the others are already waiting for him by tronjheim is west gate
we will have to hurry to get there in time
eragon nodded and headed toward the gate keeping a hand on saphira
ajihad had been gone most of the three days hunting down urgals who had managed to escape into the dwarf tunnels that honeycombed the stone beneath the beor mountains
the one time eragon had seen him between expeditions ajihad was in a rage over discovering that his daughter nasuada had disobeyed his orders to leave with the other women and children before the battle
instead she had secretly fought among the varden is archers
murtagh and the twins had accompanied ajihad the twins because it was dangerous work and the varden is leader needed the protection of their magical skills and murtagh because he was eager to continue proving that he bore the varden no ill will