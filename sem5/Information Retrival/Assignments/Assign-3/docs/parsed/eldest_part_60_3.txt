thus eragon was aware of the alarm that gripped the people below as word of saphira ** he told ** do not want them to attack us
dirt billowed into the air with each beat of saphira is powerful wings as she settled in the middle of the courtyard sinking her claws into the bare ground to steady herself
the horses tethered in the yard neighed with fear creating such an uproar that eragon finally inserted himself in their minds and calmed them with words from the ancient language
eragon dismounted after orik eyeing the many soldiers that lined the parapets and the drawn ballistae they manned
he did not fear the weapons but he had no desire to become engaged in a fight with his allies
a group of twelve men some soldiers hurried out of the keep toward saphira
they were led by a tall man with the same dark skin as nasuada only the third person eragon had met with such a complexion
halting ten paces away the man bowed as did his followers then said welcome rider
i am dahwar son of kedar
i am king orrin is seneschal
eragon inclined his head
and i eragon shadeslayer son of none
and i saphira daughter of vervada said saphira using eragon as her mouthpiece
dahwar bowed again
i apologize that no one of higher rank than myself is present to greet guests as noble as you but king orrin lady nasuada and all the varden have long since marched to confront galbatorix is ** eragon nodded
he had expected as much
they left orders that if you came here seeking them you should join them directly for your prowess is needed if we are to prevail
can you show us on a map how to find them asked eragon
of course sir
while i have that fetched would you care to step out of the heat and partake of some refreshments
eragon shook his head
we have no time to waste
besides it is not i who needs to see the map but saphira and i doubt she would fit in your halls
that seemed to catch the seneschal off guard
he blinked and ran his eyes over saphira then said quite right sir
in either case our hospitality is yours
if there is aught you and your companions desire you have but to ask
for the first time eragon realized that he could issue commands and expect them to be followed
we need a week is worth of provisions
for me only fruit vegetables flour cheese bread things like that
we also need our waterskins ** he was impressed that dahwar did not question his avoidance of meat
orik added his requests then for jerky bacon and other such products
snapping his fingers dahwar sent two servants running back into the keep to collect the supplies
while everyone in the ward waited for the men to return he asked may i assume by your presence here shadeslayer that you completed your training with the elves
my training shall never end so long as i am alive
i ** then after a moment dahwar said please excuse my impertinence sir for i am ignorant of the ways of the riders but are you not human i was told you were
that he is growled orik
he was
changed
and you should be glad he was or our predicament would be far worse than it ** dahwar was tactful enough not to pursue the subject but from his thoughts eragon concluded that the seneschal would have paid a handsome price for further details any information about eragon or saphira was valuable in orrin is government
the food water and map were soon brought by two wide eyed pages
at eragon is word they deposited the items beside saphira looking terribly frightened as they did then retreated behind dahwar
kneeling on the ground dahwar unrolled the map which depicted surda and the neighboring lands and drew a line northwest from aberon to cithri
he said last i heard king orrin and lady nasuada stopped here for provender
they did not intend to stay however because the empire is advancing south along the jiet river and they wished to be in place to confront galbatorix is army when it arrives
the varden could be anywhere between cithri and the jiet river
this is only my humble opinion but i would say the best place to look for them would be the burning plains
dahwar smiled
you may know them by their old name then the name the elves use du vollar eldrvarya