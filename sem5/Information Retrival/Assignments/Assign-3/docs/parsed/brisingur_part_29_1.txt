at the end of the second day garzhvog said firesword i must eat and i must sleep
eragon leaned against a nearby stump panting and nodded
he had not wanted to speak first but he was just as hungry and exhausted as the kull
soon after leaving the varden he had discovered that while he was faster than garzhvog at distances of up to five miles beyond that garzhvog is endurance was equal to or greater than his own
i will help you hunt he said
that is not needed
make us a big fire and i will bring us food
as garzhvog strode off toward a thicket of beech trees north of them eragon untied the strap around his waist and with a sigh of relief dropped his pack next to the stump
blasted armor he muttered
even in the empire he had not run so far while carrying such a load
he had not anticipated how arduous it would be
his feet hurt his legs hurt his back hurt and when he tried to crouch his knees refused to bend properly
trying to ignore his discomfort he set about gathering grass and dead branches for a fire which he piled on a patch of dry rocky ground
he and garzhvog were somewhere just east of the southern tip of lake tudosten
the land was wet and lush with fields of grass that stood six feet high through which there roamed herds of deer gazelles and wild oxen with black hides and wide backswept horns
the riches of the area were due eragon knew to the beor mountains which caused the formation of huge banks of clouds that drifted for many leagues over the plains beyond bringing rain to places that would otherwise have been as dry as the hadarac desert
although the two of them had already run an enormous number of leagues eragon was disappointed by their progress
between the jiet river and lake tudosten they had lost several hours while hiding and taking detours to avoid being seen
now that lake tudosten was behind them he hoped that their pace would increase
nasuada did not foresee this delay now did she oh no
she thought i could run flat out from there to farthen dur
** kicking at a branch that was in his way he continued to gather wood grumbling to himself the entire time
garzhvog strode toward him holding the carcass of a plump doe under his left arm
as if it weighed no more than a sack of rags he lifted the doe and wedged its head in the fork of a tree twenty yards from the fire
then he drew a knife and began to clean the carcass
eragon stood feeling as if his joints had turned to stone and stumbled toward garzhvog
how did you kill it he asked
do you intend to cook it on a spit or do urgals eat their meat raw
garzhvog turned his head and gazed through the coil of his left horn at eragon a deep set yellow eye gleaming with some enigmatic emotion
we are not beasts firesword
i did not say you were
with a grunt the urgal returned to his work
it will take too long to cook on a spit said eragon
i thought a stew and we can fry what is left on a rock
stew how we do not have a pot
reaching down garzhvog scrubbed his right hand clean on the ground then removed a square of folded material from the pouch at his belt and tossed it at eragon
eragon tried to catch it but was so tired he missed and the object struck the ground
it looked like an exceptionally large piece of vellum
as he picked it up the square fell open and he saw it had the shape of a bag perhaps a foot and a half wide and three feet deep
the rim was reinforced with a thick strip of leather upon which were sewn metal rings
he turned the container over amazed by its softness and the fact that it had no seams
the stomach of the cave bear i killed the year i first got my horns
hang it from a frame or put it in a hole then fill it with water and drop hot stones in it
stones heat water and stew tastes good
wo not the stones burn through the stomach
no magic
strong ** garzhvog is breath huffed out as he grasped the deer is hips on either side and with a single movement broke its pelvis in two
the sternum he split using his knife
it must have been a big bear eragon said
garzhvog made a ruk ruk sound deep in his throat
it was bigger than i am now shadeslayer