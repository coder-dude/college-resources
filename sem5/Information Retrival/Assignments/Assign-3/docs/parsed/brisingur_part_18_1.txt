after checking to ensure that none of his few possessions had been disturbed during his absence eragon unburdened himself of his pack and carefully removed his armor storing it beneath his cot
it needed to be wiped and oiled but that was a task that would have to wait
then he reached even farther underneath the cot his fingers scraping the fabric wall beyond and groped in the darkness until his hand came into contact with a long hard object
grasping it he lay the heavy cloth wrapped bundle across his knees
he picked apart the knots in the wrapping and then starting at the thickest end of the bundle began to unwind the coarse strips of canvas
inch by inch the scuffed leather hilt of murtagh is hand and a half sword came into view
eragon stopped when he had exposed the hilt the crossguard and a fair expanse of the gleaming blade which was as jagged as a saw from where murtagh had blocked eragon is blows with zar roc
eragon sat and stared at the weapon conflicted
he did not know what had prompted him but the day after the battle he had returned to the plateau and retrieved the sword from the morass of trampled dirt where murtagh had dropped it
even after only a single night exposed to the elements the steel had acquired a mottled veil of rust
with a word he had dispelled the scrim of corrosion
perhaps it was because murtagh had stolen his own sword that eragon felt compelled to take up murtagh is as if the exchange unequal and involuntary though it was minimized his loss
perhaps it was because he wished to claim a memento of that bloody conflict
and perhaps it was because he still harbored a sense of latent affection for murtagh despite the grim circumstances that had turned them against each other
no matter how much eragon abhorred what murtagh had become and pitied him for it too he could not deny the connection that existed between them
theirs was a shared fate
if not for an accident of birth he would have been raised in uru baen and murtagh in palancar valley and then their current positions might well have been reversed
their lives were inexorably intertwined
as he gazed at the silver steel eragon composed a spell that would smooth the wrinkles from the blade close the wedge shaped gaps along the edges and restore the strength of the temper
he wondered however if he ought to
the scar that durza had given him he had kept as a reminder of their encounter at least until the dragons erased it during the agaeti blodhren
should he keep this scar as well then would it be healthy for him to carry such a painful memory on his hip and what sort of message would it send to the rest of the varden if he chose to wield the blade of another betrayer zar roc had been a gift from brom eragon could not have refused to accept it nor was he sorry he had
but he was under no such compulsion to claim as his own the nameless blade that rested upon his thighs
i need a sword he thought
but not this sword
he wrapped the blade again in its shroud of canvas and slid it back under the cot
then with a fresh shirt and tunic tucked under his elbow he left the tent and went to bathe
when he was clean and garbed in the fine lamarae shirt and tunic he set out to meet with nasuada near the tents of the healers as she had requested
saphira flew for as she said it is too cramped for me on the ground i keep knocking over tents
besides if i walk with you such a herd of people will gather around us we will hardly be able to move
nasuada was waiting for him by a row of three flagpoles upon which a half dozen gaudy pennants hung limp in the cooling air
she had changed since they had parted and now wore a light summer frock the color of pale straw
her dense mosslike hair she had piled high on her head in an intricate mass of knots and braids
a single white ribbon held the arrangement in place
she smiled at eragon
he smiled in return and quickened his pace
as he drew close his guards mingled with her guards with a conspicuous display of suspicion on the part of the nighthawks and studied indifference on the part of the elves