just then he noticed a break in the middle of the column
carried between two burly men was an unconscious woman
her long midnight black hair obscured her face despite a leather strip bound around her head to hold the tresses back
she was dressed in dark leather pants and shirt
wrapped around her slim waist was a shiny belt from which hung an empty sheath on her right hip
knee high boots covered her calves and small feet
her head lolled to the side
eragon gasped feeling like he had been struck in the stomach
she was the woman from his dreams
her sculpted face was as perfect as a painting
her round chin high cheekbones and long eyelashes gave her an exotic look
the only mar in her beauty was a scrape along her jaw nevertheless she was the fairest woman he had ever seen
eragon is blood burned as he looked at her
something awoke in him something he had never felt before
it was like an obsession except stronger almost a fevered madness
then the woman is hair shifted revealing pointed ears
a chill crept over him
she was an elf
the soldiers continued marching taking her from his sight
next strode a tall proud man a sable cape billowing behind him
his face was deathly white his hair was red
red like blood
as he walked by eragon is cell the man turned his head and looked squarely at him with maroon eyes
his upper lip pulled back in a feral smile revealing teeth filed to points
eragon shrank back
he knew what the man ** ** help me
a shade
the procession continued and the shade vanished from view
eragon sank to the floor hugging himself
even in his bewildered state he knew that the presence of a shade meant that evil was loose in the land
whenever they appeared rivers of blood were sure to ** is a shade doing here the soldiers should have killed him on ** then his thoughts returned to the elf woman and he was grasped by strange emotions again
i have to ** with his mind clouded his determination quickly faded
he returned to the cot
by the time the hallway fell silent he was fast asleep
as soon as eragon opened his eyes he knew something was different
it was easier for him to think he realized that he was in gil ** made a mistake the drug is wearing ** hopeful he tried to contact saphira and use magic but both activities were still beyond his reach
a pit of worry twisted inside him as he wondered if she and murtagh had managed to escape
he stretched his arms and looked out the window
the city was just awakening the street was empty except for two beggars
he reached for the water pitcher ruminating about the elf and shade
as he started to drink he noticed that the water had a faint odor as if it contained a few drops of rancid perfume
grimacing he set the pitcher ** drug must be in there and maybe in the food as ** he remembered that when the ra zac had drugged him it had taken hours to wear ** i can keep from drinking and eating for long enough i should be able to use magic
then i can rescue the elf
the thought made him smile
he sat in a corner dreaming about how it could be done
the portly jailer entered the cell an hour later with a tray of food
eragon waited until he departed then carried the tray to the window
the meal was composed only of bread cheese and an onion but the smell made his stomach grumble hungrily
resigning himself to a miserable day he shoved the food out the window and onto the street hoping that no one would notice
eragon devoted himself to overcoming the drug is effects
he had difficulty concentrating for any length of time but as the day progressed his mental acuity increased
he began to remember several of the ancient words though nothing happened when he uttered them
he wanted to scream with frustration
when lunch was delivered he pushed it out the window after his breakfast
his hunger was distracting but it was the lack of water that taxed him most
the back of his throat was parched
thoughts of drinking cool water tortured him as each breath dried his mouth and throat a bit more
even so he forced himself to ignore the pitcher
he was diverted from his discomfort by a commotion in the hall