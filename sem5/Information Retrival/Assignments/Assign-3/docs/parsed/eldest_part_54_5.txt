the creak of bending wood filled the air as every man hauled back on the twisted cords
javelins were slotted in place
once again nolfavrell made his run
roran could feel the vibration in his feet as the ballista in front of him sent its deadly projectile winging on its way
the fire quickly spread along the waterfront forming an impenetrable barrier that prevented soldiers from reaching thedragon wing though teirm is east gate
roran had counted on the pillar of smoke to hide the ship from the archers on the battlements but it was a near thing a flight of arrows tugged at the rigging and one dart embedded itself in the deck by gertrude before the soldiers lost sight of the ship
from the bow uthar shouted pick your targets at **
the villagers were running pell mell down the beach now
they reached the north end of the wharf and a handful of them stumbled and fell as the soldiers in teirm redirected their aim
children screamed in terror
then the villagers regained momentum
they pounded down the planks past a warehouse engulfed in flame and along the pier
the panting mob charged onto the ship in a confused mass of jostling bodies
birgit and gertrude guided the stream of people to the fore and aft hatches
in a few minutes the various levels of the ship were packed to their limit from the cargo hold to the captain is cabin
those who could not fit below remained huddled on deck holding fisk is shields over their heads
as roran had asked in his message all able bodied men from carvahall clustered around the mainmast waiting for instructions
roran saw mandel among them and tossed him a proud salute
then uthar pointed at a sailor and barked you there ** get those swabs to the capstans and weigh anchors then down to the oars
double ** to the rest of the men at the ballistae he ordered half of you leave off and take the port ballistae
drive away any boarding parties
roran was one of those who switched sides
as he prepared the ballistae a few laggards staggered out of the acrid smoke and onto the ship
beside him jeod and helen hoisted the six prisoners one by one onto the gangway and rolled them onto the pier
before roran quite knew it anchors had been raised the gangway was cut loose and a drum pounded beneath his feet setting the tempo for the oarsmen
ever so slowly thedragon wing turned to starboard toward the open sea and then with gathering speed pulled away from the dock
roran accompanied jeod to the quarterdeck where they watched the crimson inferno devour everything flammable between teirm and the ocean
through the filter of smoke the sun appeared a flat bloated bloody orange disk as it rose over the city
how many have i killed now wondered roran
echoing his thoughts jeod observed this will harm a great many innocent people
guilt made roran respond with more force than he intended would you rather be in lord risthart is prisons i doubt many will be injured in the blaze and those that are not wo not face death like we will if the empire catches us
you need not lecture me roran
i know the arguments well enough
we did what we had to
just do not ask me to take pleasure in the suffering we ve caused to ensure our own safety
by noon the oars had been stowed and thedragon wing sailed under her own power propelled by favorable winds from the north
the gusts of air caused the rigging overhead to emit a low hum
the ship was miserably overcrowded but roran was confident that with some careful planning they could make it to surda with a minimum of discomfort
the worst inconvenience was that of limited rations if they were to avoid starvation food would have to be dispensed in miserly portions
and in such cramped quarters disease was an all too likely possibility
after uthar gave a brief speech about the importance of discipline on a ship the villagers applied themselves to the tasks that required their immediate attention such as tending to their wounded unpacking their meager belongings and deciding upon the most efficient sleeping arrangement for each deck
they also had to choose people to fill the various positions on thedragon wing who would cook who would train as sailors under uthar is men and so forth