sick at heart that he would not look at her maghara climbed the tallest mountain in the spine and she called out to rahna to help her
rahna is mother of us all and it was she who invented weaving and farming and she who raised the beor mountains when she was fleeing the great dragon
rahna she of the gilded horns she answered maghara and she asked why maghara had summoned her
make me pretty honored mother so i can attract the ram i want said maghara
and rahna answered you do not need to be pretty maghara
you have bright horns and long hair and a pleasant laugh
with those you can catch a ram who is not so foolish as to look at only a female is ** and maghara she threw herself down upon the ground and said i will not be happy unless i can have this ram honored mother
please make me ** rahna she smiled then and said if i do this child how will you repay me for this favor and maghara said i will give you anything you want
rahna was well pleased with her offer and so she made maghara pretty then and maghara returned to her village and everyone wondered at her beauty
with her new face maghara became the brood mate of the ram she wanted and they had many children and they lived in happiness for seven years
then rahna came to maghara and rahna said you have had seven years with the ram you wanted
have you enjoyed them and maghara said i ** and rahna said then i have come for my ** and she looked around their house of stone and she seized hold of maghara is eldest son and said i will have ** maghara begged she of the gilded horns not to take her eldest son but rahna would not relent
at last maghara took her brood mate is club and she struck at rahna but the club shattered in her hands
in punishment rahna stripped maghara is beauty from her and then rahna left with maghara is son for her hall where the four winds dwell and she named the boy hegraz and raised him to be one of the mightiest warriors who has ever walked this land
and so one should learn from maghara to never fight one is fate for you will lose that which you hold most dear
eragon watched the glowing rim of the crescent moon appear above the eastern horizon
tell me something about your villages
anything
i experienced hundreds of memories when i was in your mind and in khagra is and in otvek is but i can recall only a handful of them and those imperfectly
i am trying to make sense of what i saw
there is much i could tell you rumbled garzhvog
his heavy eyes pensive he worked his makeshift toothpick around one of his fangs and then said we take logs and we carve them with faces of the animals of the mountains and these we bury upright by our houses so they will frighten away the spirits of the wild
sometimes the poles almost seem to be alive
when you walk into one of our villages you can feel the eyes of all the carved animals watching you
the bone paused in the urgal is fingers then resumed its back and forth motion
by the doorway of each hut we hang the namna
it is a strip of cloth as wide as my outstretched hand
the namna are brightly colored and the patterns on them depict the history of the family that lives in that hut
only the oldest and most skilled weavers are allowed to add to a namna or to reweave one if it becomes damaged
the bone disappeared inside of garzhvog is fist
during the months of winter those who have mates work with them on their hearth rug
it takes at least five years to finish such a rug so by the time it is done you know whether you have made a good choice of mate
i ve never seen one of your villages said eragon
they must be very well hidden
well hidden and well defended
few who see our homes live to tell of it
focusing on the kull and allowing an edge to creep into his voice eragon said how is it you learned this language garzhvog
was there a human who lived among you did you keep any of us as slaves
garzhvog returned eragon is gaze without flinching
we have no slaves firesword
i tore the knowledge from the minds of the men i fought and i shared it with the rest of my tribe
you have killed many humans have not you