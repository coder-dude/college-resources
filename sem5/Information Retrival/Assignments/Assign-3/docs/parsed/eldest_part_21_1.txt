eragon and his company followed the az ragni until it joined the edda river which then drifted into the unknown east
at the juncture between the rivers they visited the dwarves trading outpost hedarth and exchanged their rafts for donkeys
dwarves never used horses on account of their size
arya refused the steed offered to her saying i willnot return to the land of my ancestors on the back of a donkey
thorv frowned
how will you keep pace with us
i will ** and run she did outstripping snowfire and the donkeys only to sit waiting for them at the next hill or copse
despite her exertions she displayed no sign of weariness when they stopped for the night nor any inclination to utter more than a few words between breakfast and supper
with every step she seemed to grow tenser
from hedarth they trekked north going up the edda river toward its point of origin at eldor lake
du weldenvarden came into view within three days
the forest first appeared as a hazy ridge on the horizon then quickly expanded into an emerald sea of ancient oaks beeches and maples
from saphira is back eragon saw that the woods reached unbroken to the horizon both north and west and he knew they extended far beyond that stretching the entire length of alagaesia
to him the shadows underneath the trees arching boughs seemed mysterious and enticing as well as dangerous for there lived the elves
hidden somewhere in the dappled heart of du weldenvarden lay ellesmera where he would complete his training as well as osilon and other elven cities few outsiders had visited since the fall of the riders
the forest was a perilous place for mortals eragon felt certain to be riddled with strange magic and stranger creatures
it is like another world he observed
a pair of butterflies spiraled around each other as they rose from the dark interior of the forest
i hope said saphira there will be room for me within the trees on whatever path the elves use
i cannot fly the whole time
i am sure they found ways to accommodate dragons during the time of the riders
that night just as eragon was about to seek his blankets arya appeared by his shoulder like a spirit materializing out of the air
her stealth made him jump he could never understand how she moved so quietly
before he could ask what she wanted her mind touched his and she said follow me as silently as you can
the contact surprised him as much as the request
they had shared thoughts during the flight to farthen dur it had been the only way eragon could speak to her through her self induced coma but since arya is recovery he had made no attempt to touch her mind again
it was a profoundly personal experience
whenever he reached out to another person is consciousness it felt as if a facet of his bare soul rubbed against theirs
it seemed boorish and rude to initiate something so private without an invitation as well as a betrayal of arya is trust slender as it was
also eragon was afraid that such a link would reveal his new and confused feelings for arya and he had no desire to be ridiculed for them
he accompanied her as she slipped out from the ring of tents carefully evaded trihga who had taken the first watch and passed beyond the dwarves hearing
within him saphira kept a close watch on his progress ready to leap to his side if need be
arya squatted on a moss eaten log and wrapped her arms around her knees without looking at him
there are things you must know before we reach ceris and ellesmera so that you do not shame yourself or me through your ignorance
such as he crouched opposite her curious
arya hesitated
during my years as islanzadi is ambassador it was my observation that humans and dwarves are quite similar
you share many of the same beliefs and passions
more than one human has lived comfortably among the dwarves because he or she can understand their culture as they understand yours
you both love lust hate fight and create in much the same manner
your friendship with orik and your acceptance into durgrimst ingeitum are examples of ** eragon nodded although their differences seemed greater to him than that