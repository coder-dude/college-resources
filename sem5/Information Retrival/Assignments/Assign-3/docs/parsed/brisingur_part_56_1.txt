before we go to the crags of tel naeir he said there is one more thing i must do in ellesmera
i wo not be content unless i do
saphira leaped out from the tree house
she glided westward until the number of buildings began to diminish and then she angled downward for a soft landing upon a narrow moss covered path
after asking for and getting directions from an elf who was sitting in the branches of a nearby tree eragon and saphira continued through the woods until they arrived at a small one room house grown out of the bole of a fir tree that stood at an acute angle as if a constant wind pressed against it
to the left of the house was a soft bank of earth taller by several feet than eragon
a rivulet of water tumbled over the edge of the bank and poured itself into a limpid pool before meandering off into the dim recesses of the forest
white orchids lined the pool
a bulbous root protruded out of the ground from among the slender flowers that grew along the near shore and sitting cross legged upon the root was sloan
eragon held his breath not wanting to alert the other man to his presence
the butcher wore robes of brown and orange after the fashion of the elves
a thin black strip of cloth was tied around his head concealing the gaping holes where his eyes had been
in his lap he held a length of seasoned wood which he was whittling with a small curved knife
his face was covered with far more lines than eragon remembered and upon his hands and arms were several new scars livid against the surrounding skin
wait here eragon said to saphira and slipped off her back
as eragon approached him sloan paused in his carving and cocked his head
go away he rasped
not knowing how to respond eragon stopped where he was and remained silent
the muscles in his jaw rippling sloan removed another few curls from the wood he held then tapped the tip of his knife against the root and said blast you
can you not leave me alone with my misery for a few hours i do not want to listen to any bard or minstrel of yours and no matter how many times you ask me i wo not change my mind
now go on
away with you
pity and anger welled up inside eragon and also a sense of displacement at seeing a man he had grown up around and had so often feared and disliked brought to such a state
are you comfortable eragon asked in the ancient language adopting a light lilting tone
sloan uttered a growl of disgust
you know i cannot understand your tongue and i do not wish to learn it
the words ring in my ears longer than they ought to
if you will not speak in the language of my race then do not speak to me at all
despite sloan is entreaty eragon did not repeat the question in their common language nor did he depart
with a curse sloan resumed his whittling
after every other stroke he ran his right thumb over the surface of the wood checking the progress of whatever he was carving
several minutes passed and then in a softer voice sloan said you were right having something to do with my hands calms my thoughts
sometimes
sometimes i can almost forget what i have lost but the memories always return and i feel as if i am choking on them
i am glad you sharpened the knife
a man is knives should always be sharp
eragon watched him for a minute more then he turned away and walked back to where saphira was waiting
as he pulled himself into the saddle he said sloan does not seem to have changed very much
and saphira replied you cannot expect him to become someone else entirely in such a short time
no but i had hoped he would learn something of wisdom here in ellesmera and that maybe he would repent of his crimes
if he does not wish to acknowledge his mistakes eragon nothing can force him to
in any event you have done all you can for him
now he must find a way to reconcile himself with his lot
if he cannot then let him seek the solace of the everlasting grave
from a clearing close to sloan is house saphira launched herself up and over the surrounding trees and headed north toward the crags of tel naeir flapping as hard and fast as she could
the morning sun sat full upon the horizon and the rays of light that streamed out over the treetops created long dark shadows that as one pointed to the west like purple pennants