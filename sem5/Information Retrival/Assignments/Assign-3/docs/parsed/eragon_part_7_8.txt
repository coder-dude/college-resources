the elves too fought bitterly against galbatorix but they were overthrown and forced to flee to their secret places from whence they come no more
only vrael leader of the riders could resist galbatorix and the forsworn
ancient and wise he struggled to save what he could and keep the remaining dragons from falling to his enemies
in the last battle before the gates of doru areaba vrael defeated galbatorix but hesitated with the final blow
galbatorix seized the moment and smote him in the side
grievously wounded vrael fled to utgard mountain where he hoped to gather strength
but it was not to be for galbatorix found him
as they fought galbatorix kicked vrael in the fork of his legs
with that underhanded blow he gained dominance over vrael and removed his head with a blazing sword
then as power rushed through his veins galbatorix anointed himself king over all alagaesia
and from that day he has ruled us
with the completion of the story brom shuffled away with the troubadours
eragon thought he saw a tear shining on his cheek
people murmured quietly to each other as they departed
garrow said to eragon and roran consider yourselves fortunate
i have heard this tale only twice in my life
if the empire knew that brom had recited it he would not live to see a new month
the evening after their return from carvahall eragon decided to test the stone as merlock had
alone in his room he set it on his bed and laid three tools next to it
he started with a wooden mallet and lightly tapped the stone
it produced a subtle ringing
satisfied he picked up the next tool a heavy leather hammer
a mournful peal reverberated when it struck
lastly he pounded a small chisel against it
the metal did not chip or scratch the stone but it produced the clearest sound yet
as the final note died away he thought he heard a faint squeak
merlock said the stone was hollow there could be something of value inside
i do not know how to open it though
there must have been a good reason for someone to shape it but whoever sent the stone into the spine has not taken the trouble to retrieve it or does not know where it is
but i do not believe that a magician with enough power to transport the stone would not be able to find it again
so was i meant to have it he could not answer the question
resigned to an unsolvable mystery he picked up the tools and returned the stone to its shelf
that night he was abruptly roused from sleep
he listened carefully
all was quiet
uneasy he slid his hand under the mattress and grasped his knife
he waited a few minutes then slowly sank back to sleep
a squeak pierced the silence tearing him back to wakefulness
he rolled out of bed and yanked the knife from its sheath
fumbling with a tinderbox he lit a candle
the door to his room was closed
though the squeak was too loud for a mouse or rat he still checked under the bed
nothing
he sat on the edge of the mattress and rubbed the sleep from his eyes
another squeak filled the air and he started violently
where was the noise was coming from nothing could be in the floor or walls they were solid wood
the same went for his bed and he would have noticed if anything had crawled into his straw mattress during the night
his eyes settled on the stone
he took it off the shelf and absently cradled it as he studied the room
a squeak rang in his ears and reverberated through his fingers it came from the stone
the stone had given him nothing but frustration and anger and now it would not even let him ** it ignored his furious glare and sat solidly occasionally peeping
then it gave one very loud squeak and fell silent
eragon warily put it away and got back under the sheets
whatever secret the stone held it would have to wait until morning
the moon was shining through his window when he woke again
the stone was rocking rapidly on the shelf knocking against the wall
it was bathed in cool moonlight that bleached its surface
eragon jumped out of bed knife in hand
the motion stopped but he remained tense
then the stone started squeaking and rocking faster than ever
with an oath he began dressing