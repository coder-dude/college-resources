he asked horst under his breath how long should we wait
albriech you and baldor run around as fast as you can and check if anyone else is coming
otherwise we will ** the brothers dashed off in opposite directions
half an hour later baldor returned with fisk isold and their borrowed horse
leaving her husband isold hurried toward horst shooing her hands at anyone who got in her way oblivious to the fact that most of her hair had escaped imprisonment in its bun and stuck out in odd tufts
she stopped wheezing for breath
iam sorry we re so late but fisk had trouble closing up the shop
he could not pick which planers or chisels to ** she laughed in a shrill tone almost hysterical
it was like watching a cat surrounded by mice trying to decide which one to chase
first this one then that one
a wry smile tugged at horst is lips
i understand perfectly
roran strained for a glimpse of albriech but to no avail
he gritted his teeth
whereis he
horst tapped his shoulder
right over there i do believe
albriech advanced between the houses with three beer casks tied to his back and an aggrieved look that was comic enough to make baldor and several others laugh
on either side of albriech walked morn and tara who staggered under the weight of their enormous packs as did the donkey and two goats that they towed behind them
to roran is astonishment the animals were burdened with even more casks
they wo not last a mile said roran growing angry at the couple is foolishness
and they do not have enough food
do they expect us to feed them or
with a chuckle horst cut him off
i would not worry about the food
morn is beer will be good for morale and that is worth more than a few extra meals
you will see
as soon as albriech had freed himself of the casks roran asked him and his brother is that everyone when they answered in the affirmative roran swore and struck his thigh with a clenched fist
excluding ivor three families were determined to remain in palancar valley ethlbert is parr is and knute ** can not force them to come
he sighed
all right
there is no sense in waiting longer
excitement rippled through the villagers the moment had finally arrived
horst and five other men pulled open the wall of trees then laid planks across the trench so that the people and animals could walk over
horst gestured
i think that you should go first roran
** fisk ran up and with evident pride handed roran a blackened six foot long staff of hawthorn wood with a knot of polished roots at the top and a blued steel ferrule that tapered into a blunt spike at the base
i made it last night said the carpenter
i thought that you might have need of it
roran ran his left hand over the wood marveling at its smoothness
i could not have asked for anything better
your skill is masterful
thank ** fisk grinned and backed away
conscious of the fact that the entire crowd was watching roran faced the mountains and the igualda falls
his shoulder throbbed beneath the leather strap
behind him lay his father is bones and everything he had known in life
before him the jagged peaks piled high into the pale sky and blocked his way and his will
but he would not be denied
and he would not look back
lifting his chin roran strode forward
his staff knocked against the hard planks as he crossed the trench and passed out of carvahall leading the villagers into the wilderness
bright as a flaming sun the dragon hung before eragon and everyone clustered along the crags of tel naeir buffeting them with gusts from its mighty wings
the dragon is body appeared to be on fire as the brilliant dawn illuminated its golden scales and sprayed the ground and trees with dazzling chips of light
it was far larger than saphira large enough to be several hundred years old and proportionally thicker in its neck limbs and tail
upon its back sat the rider robes startling white against the brilliance of the scales
eragon fell to his knees his face ** am not alone
awe and relief coursed through him
no more would he have to bear the responsibility of the varden and of galbatorix by himself
here was one of the guardians of old resurrected from the depths of time to guide him a living symbol and a testament to the legends he had been raised with