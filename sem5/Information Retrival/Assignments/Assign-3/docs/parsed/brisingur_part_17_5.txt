then they fester and i shall pay the price for my mistake
but i doubt they will while angela ministers to me
she has an amazing storehouse of knowledge where medicinal plants are concerned
i half believe she could tell you the true name of every species of grass on the plains east of here merely by feeling their leaves
saphira who had been so still she appeared asleep now yawned nearly touching the floor and the ceiling with the tips of her open jaws and shook her head and neck sending the flecks of light reflected by her scales spinning about the tent with dizzying speed
straightening in her seat nasuada said ah i am sorry
i know this has been tedious
you have both been very patient
thank you
eragon knelt and placed his right hand over hers
you do not need to worry about me nasuada
i know my duty
i have never aspired to rule that is not my destiny
and if ever i am offered the chance to sit upon a throne i shall refuse and see that it goes to someone who is better suited than i to lead our race
you are a good person eragon murmured nasuada and pressed his hand between hers
then she chuckled
what with you roran and murtagh i seem to spend most of my time worrying about members of your family
eragon bridled at the statement
murtagh is no family of mine
of course
forgive me
but still you must admit it is startling how much bother the three of you have caused both the empire and the varden
it is a talent of ours joked eragon
it runs in their blood said saphira
wherever they go they get themselves entangled in the worst danger possible
she nudged eragon in the arm
especially this one
what else can you expect of people from palancar valley descendants all of a mad king
but not mad themselves said nasuada
at least i do not think so
it is hard to tell at ** she laughed
if you roran and murtagh were locked in the same cell i am not sure who would survive
eragon laughed as well
roran
he is not about to let a little thing like death stand between him and katrina
nasuada is smile became slightly strained
no i suppose he would not at ** for a score of heartbeats she was silent then goodness me how selfish i am
the day is almost done and here i am detaining you merely so i can enjoy a minute or two of idle conversation
yes but there are better places than this for talk among friends
after what you have been through i expect you would like a wash a change and a hearty meal no you must be ** eragon glanced at the apple he still held and regretfully concluded it would be impolite to continue eating it when his audience with nasuada was drawing to a close
nasuada caught his look and said your face answers for you shadeslayer
you have the guise of a winter starved wolf
well i shall not torment you any longer
go and bathe and garb yourself in your finest tunic
when you are presentable i would be most pleased if you would consent to join me for my evening meal
understand you would not be my only guest for the affairs of the varden demand my constant attention but you would brighten the proceedings considerably for me if you chose to attend
eragon fought back a grimace at the thought of having to spend hours more parrying verbal thrusts from those who sought to use him for their own advantage or to satisfy their curiosity about riders and dragons
still nasuada was not to be denied so he bowed and agreed to her request