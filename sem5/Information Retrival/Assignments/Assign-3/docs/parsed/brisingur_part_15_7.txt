my ring now he told himself
i have to stop thinking of it as brom is
he cast a critical gaze at the large sapphire that sparkled in its gold setting on his finger
i do not know if there is any energy in aren
i ve never stored any there myself and i never checked if brom ** even as he spoke he extended his consciousness toward the sapphire
the instant his mind came into contact with the gem he felt the presence of a vast swirling pool of energy
to his inner eye the sapphire thrummed with power
he wondered that it did not explode from the amount of force contained within the boundaries of its sharp edged facets
after he used the energy to wash away his aches and pains and restore strength to his limbs the treasure trove inside aren was hardly diminished
his skin tingling eragon severed his link with the gem
delighted by his discovery and his sudden sense of well being he laughed out loud then told arya what he had found
brom must have squirreled away every bit of energy he could spare the whole time he was hiding in ** he laughed again marveling
all those years
with what is in aren i could tear apart an entire castle with a single spell
he knew he would need it to keep the new rider safe when saphira hatched observed arya
also i am sure aren was a way for him to protect himself if he had to fight a shade or some other similarly powerful opponent
it was not by accident that he managed to frustrate his enemies for the better part of a century
if i were you i would save the energy he left you for your hour of greatest need and i would add to it whenever i could
it is an incredibly valuable resource
you should not squander it
no thought eragon that i will not
he twirled the ring around his finger admiring how it gleamed in the firelight
since murtagh stole zar roc this saphira is saddle and snowfire are the only things i have of brom and even though the dwarves brought snowfire from farthen dur i rarely ride him nowadays
aren is really all i have to remember him by
my only legacy of him
my only inheritance
i wish he were still ** i never had a chance to talk with him about oromis murtagh my father
oh the list is endless
what would he have said about my feelings for arya eragon snorted to himself
i know what he would have said he would have berated me for being a love struck fool and for wasting my energy on a hopeless cause
and he would have been right too i suppose but ah how can i help it she is the only woman i wish to be with
the fire cracked
a flurry of sparks flew upward
eragon watched with half closed eyes contemplating arya is revelations
then his mind returned to a question that had been bothering him ever since the battle on the burning plains
arya do male dragons grow any faster than female dragons
because of thorn
he is only a few months old and yet he is already nearly as big as saphira
i do not understand it
picking a dry blade of grass arya began sketching in the loose soil tracing the curved shapes of glyphs from the elves script the liduen kvaedhi
most likely galbatorix accelerated his growth so thorn would be large enough to hold his own with saphira
ah
is not that dangerous though oromis told me that if he used magic to give me the strength speed endurance and other skills i needed i would not understand my new abilities as well as if i had gained them the ordinary way by hard work
he was right too
even now the changes the dragons made to my body during the agaeti blodhren still sometimes catch me by surprise
arya nodded and continued sketching glyphs in the dirt
it is possible to reduce the undesirable side effects by certain spells but it is a long and arduous process
if you wish to achieve true mastery of your body it is still best to do so through normal means
the transformation galbatorix has forced upon thorn must be incredibly confusing for him
thorn now has the body of a nearly grown dragon and yet his mind is still that of a youngling
eragon fingered the newly formed calluses on his knuckles
do you also know why murtagh is so powerful
more powerful than i am