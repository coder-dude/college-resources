he was **
murtagh seemed perplexed by eragon is wrath
well we could not keep him around hewas dangerous
the others ran off
without a horse he would not have made it far
i did not want the urgals to find him and learn about arya
so i thought it would
but tokill him interrupted eragon
saphira sniffed torkenbrand is head curiously
she opened her mouth slightly as if to snap it up then appeared to decide better of it and prowled to eragon is side
i am only trying to stay alive stated murtagh
no stranger is life is more important than my own
but you can not indulge in wanton violence
where is your empathy growled eragon pointing at the head
empathy empathy what empathy can i afford my enemies shall i dither about whether to defend myself because it will cause someone pain if that had been the case i would have died years ** you must be willing to protect yourself and what you cherish no matter what the cost
eragon slammed zar roc back into its sheath shaking his head savagely
you can justify any atrocity with that reasoning
do you think i enjoy this murtagh shouted
my life has been threatened from the day i was ** all of my waking hours have been spent avoiding danger in one form or another
and sleep never comes easily because i always worry if i will live to see the dawn
if there ever was a time i felt secure it must have been in my mother is womb though i was not safe even ** you do not understand if you lived with thisfear you would have learned the same lesson i did do not take chances
he gestured at torkenbrand is body
he was a risk that i removed
i refuse to repent and i wo not plague myself over what is done and past
eragon shoved his face into murtagh is
it was still the wrong thing to ** he lashed arya to saphira then climbed onto snowfire
let is ** murtagh guided tornac around torkenbrand is prone form in the bloodstained dust
they rode at a rate that eragon would have thought impossible a week ago leagues melted away before them as if wings were attached to their feet
they turned south between two outstretched arms of the beor mountains
the arms were shaped like pincers about to close the tips a day is travel apart
yet the distance seemed less because of the mountains size
it was as if they were in a valley made for giants
when they stopped for the day eragon and murtagh ate dinner in silence refusing to look up from their food
afterward eragon said tersely i will take the first ** murtagh nodded and lay on his blankets with his back to eragon
do you want to talk asked saphira
not right now murmured ** me some time to think i am
confused
she withdrew from his mind with a gentle touch and a ** love you little one
and i you he said
she curled into a ball next to him lending him her warmth
he sat motionless in the dark wrestling with his disquiet
in the morning saphira took off with both eragon and arya
eragon wanted to get away from murtagh for a time
he shivered pulling his clothes tighter
it looked like it might snow
saphira ascended lazily on an updraft and asked what are you thinking
eragon contemplated the beor mountains which towered above them even though saphira flew far above the ** was murder yesterday
i ve no other word for it
saphira banked to the left
it was a hasty deed and ill considered but murtagh tried to do the right thing
the men who buy and sell other humans deserve every misfortune that befalls them
if we were not committed to helping arya i would hunt down every slaver and tear them apart
yes said eragon miserably but torkenbrand was helpless
he could not shield himself or run
a moment more and he probably would have surrendered
murtagh did not give him that chance
if torkenbrand had at least been able to fight it would not have been so bad
eragon even if torkenbrand had fought the results would have been the same
you know as well as i do that few can equal you or murtagh with the blade
torkenbrand would have still died though you seem to think it would have been more just or honorable in a mismatched duel
i do not know what is ** eragon ** are not any answers that make sense