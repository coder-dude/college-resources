after they considered jeod is proposal from every possible angle and agreed to abide by it with a few modifications roran sent nolfavrell to fetch gertrude and mandel from the green chestnut for jeod had offered their entire party his hospitality
now if you will excuse me said jeod rising i must go reveal to my wife that which i should never have hidden from her and ask if she will accompany me to surda
you may take your pick of rooms on the second floor
rolf will summon you when supper is ** with long slow steps he departed the study
is it wise to let him tell that ogress asked loring
roran shrugged
wise or not we can not stop him
and i do not think he will be at peace until he does
instead of going to a room roran wandered through the mansion unconsciously evading the servants as he pondered the things jeod had said
he stopped at a bay window open to the stables at the rear of the house and filled his lungs with the brisk and smoky air heavy with the familiar smell of manure
he started and turned to see birgit silhouetted in the doorway
she pulled her shawl tight around her shoulders as she approached
who he asked knowing full well
roran looked at the darkening sky
i do not know
i hate him for causing the death of my father but he is still family and for that i love him
i suppose that if i did not need eragon to save katrina i would have nothing to do with him for a long while yet
as i need and hate you stronghammer
he snorted with grim amusement
aye we re joined at the hip are not we you have to help me find eragon in order to avenge quimby on the ra zac
and to have my vengeance on you afterward
that ** roran stared into her unwavering eyes for a moment acknowledging the bond between them
he found it strangely comforting to know that they shared the same drive the same angry fire that quickened their steps when others faltered
in her he recognized a kindred spirit
returning through the house roran stopped by the dining room as he heard the cadence of jeod is voice
curious he fit his eye to a crack by the middle door hinge
jeod stood opposite a slight blond woman who roran assumed was helen
if what you say is true how can you expect me to trust you
yet you ask me to become a fugitive for you
you once offered to leave your family and wander the land with me
you begged me to spirit you away from teirm
once
i thought you were terribly dashing then what with your sword and your scar
i still have those he said softly
i made many mistakes with you helen i understand that now
but i still love you and want you to be safe
i have no future here
if i stay i will only bring grief to your family
you can return to your father or you can come with me
do what will make you the happiest
however i beg you to give me a second chance to have the courage to leave this place and shed the bitter memories of our life here
we can start anew in surda
she was quiet for a long time
that young man who was here is he really a rider
he is
the winds of change are blowing helen
the varden are about to attack the dwarves are gathering and even the elves stir in their ancient haunts
war approaches and if we re fortunate so does galbatorix is downfall
are you important among the varden
they owe me some consideration for my part in acquiring saphira is egg
then you would have a position with them in surda
i imagine ** he put his hands on her shoulders and she did not draw away
she whispered jeod jeod do not press me
i cannot decide yet
she shivered
oh yes
i will think about it
roran is heart pained him as he left
that night at dinner roran noticed helen is eyes were often upon him studying and measuring comparing him he was sure to eragon
after the meal roran beckoned to mandel and led him out into the courtyard behind the house
what is it sir asked mandel
i wished to talk with you in private
roran fingered the pitted blade of his hammer and reflected on how much he felt like garrow when his father gave a lecture on responsibility roran could even feel the same phrases rising in his ** so one generation passes to the next he thought