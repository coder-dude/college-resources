you shall not prevail said oromis
even gods do not endure **
at that galbatorix uttered a foul oath
your philosophy does not constrain me ** i am the greatest of magicians and soon i will be even greater still
death will not take me
you however shall die
but first you will suffer
you will both suffer beyond imagining and then i will kill you oromis and i shall take your heart of hearts glaedr and you will serve me until the end of **
and glaedr again heard the clash of swords on armor
glaedr had excluded oromis from his mind for the duration of the fight but their bond ran deeper than conscious thought so he felt it when oromis stiffened incapacitated by the searing pain of his bone blight nerve rot
alarmed glaedr released thorn is leg and tried to kick the red dragon away
thorn howled at the impact but remained where he was
galbatorix is spell held the two of them in place neither able to move more than a few feet in any direction
there was another metallic clang from above and then glaedr saw naegling fall past him
the golden sword flashed and gleamed as it tumbled toward the ground
for the first time the cold claw of fear gripped glaedr
most of oromis is word will energy was stored within the sword and his wards were bound to the blade
without it he would be defenseless
glaedr threw himself against the limits of galbatorix is spell struggling with all his might to break free
in spite of his efforts however he could not escape
and just as oromis began to recover glaedr felt zar roc slash oromis from shoulder to hip
he howled as oromis had howled when glaedr lost his leg
an inexorable force gathered inside of glaedr is belly
without pausing to consider whether it was possible he pushed thorn and murtagh away with a blast of magic sending them flying like windblown leaves and then tucked his wings against his sides and dove toward gil ead
if he could get there fast enough then islanzadi and her spellcasters would be able to save oromis
the city was too far away though
oromis is consciousness was faltering
fading
slipping away
glaedr poured his own strength into oromis is ruined frame trying to sustain him until they reached the ground
but for all the energy he gave to oromis he could not stop the bleeding the terrible bleeding
glaedr
release me oromis murmured with his mind
a moment later in an even fainter voice he whispered do not mourn me
and then the partner of glaedr is life passed into the void
a crimson haze descended over the world throbbing in unison with his pulse
he flared his wings and looped back the way he had come searching for thorn and his rider
he would not let them escape he would catch them and tear at them and burn them until he had eradicated them from the world
glaedr saw the red shrike dragon diving toward him and he roared his grief and redoubled his speed
the red dragon swerved at the last moment in an attempt to flank him but he was not fast enough to evade glaedr who lunged and snapped and bit off the last three feet of the red dragon is tail
a fountain of blood sprayed from the stump
yelping in agony the red dragon wriggled away and darted behind glaedr
glaedr started to twist around to face him but the smaller dragon was too quick too nimble
glaedr felt a sharp pain at the base of his skull and then his vision flickered and failed
he was alone and in the dark
he was alone and in the dark and he could not move or see
he could feel the minds of other creatures close by but they were not the minds of thorn and murtagh but of arya eragon and saphira
and then glaedr realized where he was and the true horror of the situation broke upon him and he howled into the darkness
he howled and he howled and he abandoned himself to his agony not caring what the future might bring for oromis was dead and he was alone
he was curled into a ball
tears streaked his face
gasping he pushed himself up off the floor and looked for saphira and arya
it took him a moment to comprehend what he saw
the female spellcaster eragon had been about to attack lay before him slain by a single sword thrust