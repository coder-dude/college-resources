morzan and galbatorix then fled together sealing our doom
you cannot begin to fathom the effect morzan is betrayal had on brom until you understand the depth of brom is affection for him
and when galbatorix at last revealed himself and the forsworn killed brom is dragon brom focused all of his anger and pain on the one who he felt was responsible for the destruction of his world morzan
oromis paused his face grave
do you know why losing your dragon or vice versa usually kills the survivor
i can imagine said eragon
he quailed at the thought
the pain is shock enough although it is not always a factor but what really causes the damage is feeling part of your mind part of your identity die
when it happened to brom i fear that he went mad for a time
after i was captured and escaped i brought him to ellesmera for safety but he refused to stay instead marching with our army to the plains of ilirea where king evandar was slain
the confusion then was indescribable
galbatorix was busy consolidating his power the dwarves were in retreat the southwest was a mass of war as the humans rebelled and fought to create surda and we had just lost our king
driven by his desire for vengeance brom sought to use the turmoil to his advantage
he gathered together many of those who had been exiled freed some who had been imprisoned and with them he formed the varden
he led them for a few years then surrendered the position to another so that he was free to pursue his true passion which was morzan is downfall
brom personally killed three of the forsworn including morzan and he was responsible for the deaths of five others
he was rarely happy during his life but he was a good rider and a good man and i am honored to have known him
i never heard his name mentioned in connection to the forsworn is deaths objected eragon
galbatorix did not want to publicize the fact that any still existed who could defeat his servants
much of his power resides in the appearance of invulnerability
once again eragon was forced to revise his conception of brom from the village storyteller that eragon had first taken him to be to the warrior and magician he had traveled with to the rider he was at last revealed as and now firebrand revolutionary leader and assassin
it was hard to reconcile all of those ** feel as if i barely knew him
i wish that we had had a chance to talk about all of this at least once
he was a good man agreed eragon
he looked out one of the round windows that faced the edge of the cliff and allowed the afternoon warmth to suffuse the room
he watched saphira noting how she acted with glaedr seeming both shy and coy
one moment she would twist around to examine some feature of the clearing the next she would shuffle her wings and make small advances on the larger dragon weaving her head from side to side the tip of her tail twitching as if she were about to pounce on a deer
she reminded eragon of a kitten trying to bait an old tomcat into playing with her only glaedr remained impassive throughout her machinations
saphira he said
she responded with a distracted flicker of her thoughts barely acknowledging ** answer me
i know you re excited but do not make a fool of yourself
you ve made a fool of yourself plenty of times she snapped
her reply was so unexpected it stunned him
it was the sort of casually cruel remark that humans often make but that he had never thought to hear from her
he finally managed to say that does not make it any better
she grunted and closed her mind to his although he could still feel the thread of her emotions connecting them
eragon returned to himself to find oromis is gray eyes heavy upon him
the elf is gaze was so perceptive eragon was sure that oromis understood what had transpired
eragon forced a smile and motioned toward saphira
even though we re linked i can never predict what she is going to do
the more i learn about her the more i realize how different we are
then oromis made his first statement that eragon thought was truly wise those whom we love are often the most alien to ** the elf paused