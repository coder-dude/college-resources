it is a narrow focus though
you have the same problem here that you do with your meditation
you must relax broaden your field of vision and allow yourself to absorb everything around you without judging what is important or ** setting aside the picture oromis took a second blank tablet from the grass and gave it to eragon
try again with what i
startled eragon turned and saw orik and arya emerge side by side from the forest
the dwarf raised his arm in greeting
his beard was freshly trimmed and braided his hair was pulled back into a neat ponytail and he wore a new tunic courtesy of the elves that was red and brown and embroidered with gold thread
his appearance gave no indication of his condition the previous night
eragon oromis and arya exchanged the traditional greeting then abandoning the ancient language oromis asked to what may i attribute this visit you are both welcome to my hut but as you can see i am in the midst of working with eragon and that is of paramount importance
i apologize for disturbing you oromis elda said arya but
the fault is mine said orik
he glanced at eragon before continuing i was sent here by hrothgar to ensure that eragon receives the instruction he is due
i have no doubt that he is but i am obliged to see his training with my own eyes so that when i return to tronjheim i may give my king a true account of events
oromis said that which i teach eragon is not to be shared with anyone else
the secrets of the riders are for him alone
and i understand that
however we live in uncertain times the stone that once was fixed and solid is now unstable
we must adapt to survive
so much depends on eragon we dwarves have a right to verify that his training proceeds as promised
do you believe our request is an unreasonable one
well spoken master dwarf said oromis
he tapped his fingers together inscrutable as always
may i assume then that this is a matter of duty for you
and neither will allow you to yield on this point
i fear not oromis elda said orik
very well
you may stay and watch for the duration of this lesson
will that satisfy you
orik frowned
are you near the end of the lesson
then yes i will be satisfied
for the moment at least
while they spoke eragon tried to catch arya is eye but she kept her attention centered on oromis
he blinked jolted out of his reverie
yes master
do not wander eragon
i want you to make another fairth
keep your mind open like i told you before
yes ** eragon hefted the tablet his hands slightly damp at the thought of having orik and arya there to judge his performance
he wanted to do well in order to prove that oromis was a good teacher
even so he could not concentrate on the pine needles and sap arya tugged at him like a lodestone drawing his attention back to her whenever he thought of something else
at last he realized that it was futile for him to resist the attraction
he composed an image of her in his head which took but a heartbeat since he knew her features better than his own and voiced the spell in the ancient language pouring all of his adoration love and fear of her into the currents of fey magic
the fairth depicted arya is head and shoulders against a dark indistinct background
she was bathed in firelight on her right side and gazed out at the viewer with knowing eyes appearing not just as she was but as he thought of her mysterious exotic and the most beautiful woman he had ever seen
it was a flawed imperfect picture but it possessed such intensity and passion that it evoked a visceral response from ** this how i really see her whoever this woman was she was so wise so powerful and so hypnotic she could consume any lesser man
from a great distance he heard saphira whisper be careful
what have you wrought eragon demanded oromis
i
i do not ** eragon hesitated as oromis extended his hand for the fairth reluctant to let the others examine his work especially arya
after a long terrifying pause eragon pried his fingers off the tablet and released it to oromis
the elf is expression grew stern as he looked at the fairth then back at eragon who quailed under the weight of his stare