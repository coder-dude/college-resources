i will make sure some food is ** he muttered a string of words under his breath then left shaking his head
the bolt was secured once again on the outside of the door
eragon sat feeling strangely dreamy from the day is excitement and their forced march
his eyelids were heavy
saphira settled next to ** must be careful
it seems we have as many enemies here as we did in the empire
he nodded too tired to talk
murtagh eyes glazed and empty leaned against the far wall and slid to the shiny floor
he held his sleeve against the cut on his neck to stop the bleeding
are you all right asked eragon
murtagh nodded jerkily
did he get anything from you
how were you able to keep him out he is so strong
i ve
i ve been well ** there was a bitter note to his voice
silence enshrouded them
eragon is gaze drifted to one of the lanterns hanging in a corner
his thoughts meandered until he abruptly said i did not let them know who you are
murtagh looked relieved
he bowed his head
thank you for not betraying me
and you still say that you are morzan is son
eragon started to speak but stopped when he felt hot liquid splash onto his hand
he looked down and was startled to see a drop of dark blood roll off his skin
it had fallen from saphira is ** forgot
you re ** he exclaimed getting up with an ** d better heal you
be careful
it is easy to make mistakes when you re this tired
i ** unfolded one of her wings and lowered it to the floor
murtagh watched as eragon ran his hands over the warm blue membrane saying waise heill whenever he found an arrow hole
luckily all the wounds were relatively easy to heal even those on her nose
task completed eragon slumped against saphira breathing hard
he could feel her great heart beating with the steady throb of life
i hope they bring food soon said murtagh
eragon shrugged he was too exhausted to be hungry
he crossed his arms missing zar roc is weight by his side
why are you here
if you really are morzan is son galbatorix would not let you wander around alagaesia freely
how is it that you managed to find the ra zac by yourself why is it i ve never heard of any of the forsworn having children and what are you doing here his voice rose to a near shout at the end
murtagh ran his hands over his face
it is a long story
we re not going anywhere rebutted eragon
there probably wo not be time for it tomorrow
murtagh wrapped his arms around his legs and rested his chin on his knees rocking back and forth as he stared at the floor
it is not a he said then interrupted himself
i do not want to stop
so make yourself comfortable
my story will take a ** eragon shifted against saphira is side and nodded
saphira watched both of them intently
murtagh is first sentence was halting but his voice gained strength and confidence as he spoke
as far as i know
i am the only child of the thirteen servants or the forsworn as they re called
there may be others for the thirteen had the skill to hide whatever they wanted but i doubt it for reasons i will explain later
my parents met in a small village i never learned where while my father was traveling on the king is business
morzan showed my mother some small kindness no doubt a ploy to gain her confidence and when he left she accompanied him
they traveled together for a time and as is the nature of these things she fell deeply in love with him
morzan was delighted to discover this not only because it gave him numerous opportunities to torment her but also because he recognized the advantage of having a servant who would not betray him
thus when morzan returned to galbatorix is court my mother became the tool he relied upon most
he used her to carry his secret messages and he taught her rudimentary magic which helped her remain undiscovered and on occasion extract information from people
he did his best to protect her from the rest of the thirteen not out of any feelings for her but because they would have used her against him given the chance
for three years things proceeded in this manner until my mother became pregnant
murtagh paused for a moment fingering a lock of his hair