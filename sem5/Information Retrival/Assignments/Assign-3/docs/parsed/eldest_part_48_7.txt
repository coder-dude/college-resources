the elf maids twined their hands and arms together so that the dragon appeared to be a continuous whole rippling from one body to the next without interruption
then they each lifted a bare foot and brought it down on the packed ground with a softthump
on the thirdthump the musicians struck their drums in rhythm
athump later the harpists plucked the strings of their gilt instruments and a moment after that those elves with flutes joined the throbbing melody
slowly at first but with gathering speed iduna and neya began to dance marking time with the stamp of their feet on the dirt and undulating so that it was not they who seemed to move but the dragon upon them
round and round they went and the dragon flew endless circles across their skin
then the twins added their voices to the music building upon the pounding beat with their fierce cries their lyrics verses of a spell so complex that its meaning escaped eragon
like the rising wind that precedes a storm the elves accompanied the incantation singing with one tongue and one mind and one intent
eragon did not know the words but found himself mouthing them along with the elves swept along by the inexorable cadence
he heard saphira and glaedr hum in concordance a deep pulse so strong that it vibrated within his bones and made his skin tingle and the air shimmer
faster and faster spun iduna and neya until their feet were a dusty blur and their hair fanned about them and they glistened with a film of sweat
the elf maids accelerated to an inhuman speed and the music climaxed in a frenzy of chanted phrases
then a flare of light ran the length of the dragon tattoo from head to tail and the dragon stirred
at first eragon thought his eyes had deceived him until the creature blinked raised his wings and clenched his talons
a burst of flame erupted from the dragon is maw and he lunged forward and pulled himself free of the elves skin climbing into the air where he hovered flapping his wings
the tip of his tail remained connected to the twins below like a glowing umbilical cord
the giant beast strained toward the black moon and loosed an untamed roar of ages past then turned and surveyed the assembled elves
as the dragon is baleful eye fell upon him eragon knew that the creature was no mere apparition but a conscious being bound and sustained by magic
saphira and glaedr is humming grew ever louder until it blocked all other sound from eragon is ears
above the specter of their race looped down over the elves brushing them with an insubstantial wing
it came to a stop before eragon engulfing him in an endless whirling gaze
bidden by some instinct eragon raised his right hand his palm tingling
in his mind echoed a voice of fire our gift so you may do what you must
the dragon bent his neck and with his snout touched the heart of eragon is gedwey ignasia
a spark jumped between them and eragon went rigid as incandescent heat poured through his body consuming his insides
his vision flashed red and black and the scar on his back burned as if branded
fleeing to safety he fell deep within himself where darkness grasped him and he had not the strength to resist it
last he again heard the voice of fire say our gift to you