he turned away from the crumpled ruins of the latest tunnel and surveyed the land with interest
a mass exodus of women and children along with the varden is elders streamed out of tronjheim
everyone carried loads of provisions clothes and belongings
a small group of warriors predominantly boys and old men accompanied them
most of the activity however was at the base of tronjheim where the varden and dwarves were assembling their army which was divided into three battalions
each section bore the varden is standard a white dragon holding a rose above a sword pointing downward on a purple field
the men were silent ironfisted
their hair flowed loosely from under their helmets
many warriors had only a sword and a shield but there were several ranks of spear and pikemen
in the rear of the battalions archers tested their bowstrings
the dwarves were garbed in heavy battle gear
burnished steel hauberks hung to their knees and thick roundshields stamped with the crests of their clan rested on their left arms
short swords were sheathed at their waists while in their right hands they carried mattocks or war axes
their legs were covered with extra fine mail
they wore iron caps and brass studded boots
a small figure detached itself from the far battalion and hurried toward eragon and saphira
it was orik clad like the other dwarves
ajihad wants you to join the army he said
there are no more tunnels to cave in
food is waiting for both of you
eragon and saphira accompanied orik to a tent where they found bread and water for eragon and a pile of dried meat for saphira
they ate it without complaint it was better than going hungry
when they finished orik told them to wait and disappeared into the battalion is ranks
he returned leading a line of dwarves burdened with tall piles of plate armor
orik lifted a section of it and handed it to eragon
what is this asked eragon fingering the polished metal
the armor was intricately wrought with engraving and gold filigree
it was an inch thick in places and very heavy
no man could fight under that much weight
and there were far too many pieces for one person
a gift from hrothgar said orik looking pleased with himself
it has lain so long among our other treasures that it was almost forgotten
it was forged in another age before the fall of the riders
but what is itfor asked eragon
why it is dragon armor of ** you do not think that dragons went into battle unprotected complete sets are rare because they took so long to make and because dragons were always growing
still saphira is not too big yet so this should fit her reasonably well
dragon ** saphira nosed one of the pieces eragon asked what do you think
let is try it on she said a fierce gleam in her eye
after a good deal of struggling eragon and orik stepped back to admire the result
saphira is entire neck except for the spikes along its ridge was covered with triangular scales of overlapping armor
her belly and chest were protected by the heaviest plates while the lightest ones were on her tail
her legs and back were completely encased
her wings were left bare
a single molded plate lay on top of her head leaving her lower jaw free to bite and snap
saphira arched her neck experimentally and the armor flexed smoothly with ** will slow me down but it will help stop the arrows
how do i look
very intimidating replied eragon truthfully
that pleased her
orik picked up the remaining items from the ground
i brought you armor as well though it took much searching to find your size
we rarely forge arms for men or elves
i do not know who this was made for but it has never been used and should serve you well
over eragon is head went a stiff shirt of leather backed mail that fell to his knees like a skirt
it rested heavily on his shoulders and clinked when he moved
he belted zar roc over it which helped keep the mail from swinging
on his head went a leather cap then a mail coif and finally a gold and silver helm
bracers were strapped to his forearms and greaves to his lower legs
for his hands there were mail backed gloves
last orik handed him a broad shield emblazoned with an oak tree