the day is done the stars are bright
the leaves are still the moon is white
laugh at woe and laugh at foe
menoa is scion now is safe this night
a forest child we lost to strife
a sylvan daughter caught by life
freed of fear and freed of flame
she tore a rider from the shadows rife
again the dragons rise on wing
strong of blade and strong of arm
the time is ripe for us to kill a king