it is going to be dangerous enough without unnecessary surprises
finally murtagh turned to eragon
his breathing was hard and fast like that of a cornered wolf
he paused then said with a tortured voice you have a right to know
i
i am the son of morzan first and last of the forsworn
eragon was speechless
disbelief roared through his mind as he tried to reject murtagh is ** forsworn never had any children least of all morzan
** the man who betrayed the riders to galbatorix and remained the king is favorite servant for the rest of his life
could it be true
saphira is own shock reached him a second later
she crashed through trees and brush as she barreled from the river to his side fangs bared tail raised ** ready for anything she ** may be able to use magic
you are his heir asked eragon surreptitiously reaching for zar ** could he want with me is he really working for the king
i did not choose ** cried murtagh anguish twisting his face
he ripped at his clothes with a desperate air tearing off his tunic and shirt to bare his torso
** he pleaded and turned his back to eragon
unsure eragon leaned forward straining his eyes in the darkness
there against murtagh is tanned and muscled skin was a knotted white scar that stretched from his right shoulder to his left hip a testament to some terrible agony
see that demanded murtagh bitterly
he talked quickly now as if relieved to have his secret finally revealed
i was only three when i got it
during one of his many drunken rages morzan threw his sword at me as i ran by
my back was laid open by the very sword you now carry the only thing i expected to receive as inheritance until brom stole it from my father is corpse
i was lucky i suppose there was a healer nearby who kept me from dying
you must understand i do not love the empire or the king
i have no allegiance to them nor do i mean you ** his pleas were almost frantic
eragon uneasily lifted his hand from zar roc is pommel
then your father he said in a faltering voice was killed by
yes brom said murtagh
he pulled his tunic back on with a detached air
a horn rang out behind them prompting eragon to cry come run with ** murtagh shook the horses reins and forced them into a tired trot eyes fixed straight ahead while arya bounced limply in snowfire is saddle
saphira stayed by eragon is side easily keeping pace with her long ** could walk unhindered in the riverbed he said as she was forced to smash through a dense web of branches
i will not leave you with him
eragon was glad for her ** is ** he said between strides your tale is hard to believe
how do i know you are not lying
murtagh interrupted him quickly
i can not prove anything to you now
keep your doubts until we reach the varden
they will recognize me quickly enough
i must know pressed eragon
do you serve the empire
no
and if i did what would i accomplish by traveling with you if i were trying to capture or kill you i would have left you in ** murtagh stumbled as he jumped over a fallen log
you could be leading the urgals to the varden
then said murtagh shortly why am i still with you i know where the varden are now
what reason could i have for delivering myself to them if i were going to attack them i d turn around and join the urgals
maybe you re an assassin stated eragon flatly
maybe
you can not really know can you
her tail swished over his ** he wanted to harm you he could have done it long ago
a branch whipped eragon is neck causing a line of blood to appear on his skin
the waterfall was growing ** want you to watch murtagh closely when we get to the varden
he may do something foolish and i do not want him killed by accident
i will do my best she said as she shouldered her way between two trees scraping off slabs of bark
the horn sounded behind them again
eragon glanced over his shoulder expecting urgals to rush out of the darkness
the waterfall throbbed dully ahead of them drowning out the sounds of the night
the forest ended and murtagh pulled the horses to a stop
they were on a pebble beach directly to the left of the mouth of the beartooth river