with an easy loping stride eragon ran underneath the massive timber gate that protected the southern entrance to the city mountain hearing the guards cry hail ** as he flew past
twenty yards beyond for the gate was recessed into the base of tronjheim he sped between the pair of giant gold griffins that stared with sightless eyes toward the horizon and then emerged into the open
the air was cool and moist and smelled like fresh fallen rain
though it was morning gray twilight enveloped the flat disk of land that surrounded tronjheim land upon which no grass grew only moss and lichen and the occasional patch of pungent toadstools
above farthen dur rose over ten miles to a narrow opening through which pale indirect light entered the immense crater
eragon had difficulty grasping the scale of the mountain when he gazed upward
as he ran he listened to the monotonous pattern of his breathing and to his light quick footsteps
he was alone save for a curious bat that swooped overhead emitting shrill squeaks
the tranquil mood that permeated the hollow mountain comforted him freed him of his usual worries
he followed the cobblestone path that extended from tronjheim is south gate all the way to the two black thirty foot high doors set into the southern base of farthen dur
as he drew to a halt a pair of dwarves emerged from hidden guardrooms and hurried to open the doors revealing the seemingly endless tunnel beyond
eragon continued forward
marble pillars studded with rubies and amethysts lined the first fifty feet of the tunnel
past them the tunnel was bare and desolate the smooth walls broken only by a single flameless lantern every twenty yards and at infrequent intervals by a closed gate or door
i wonder where they lead eragon thought
then he imagined the miles of stone pressing down on him from overhead and for a moment the tunnel seemed unbearably oppressive
he quickly pushed the image away
halfway through the tunnel eragon felt her
** he shouted with both his mind and his voice her name echoing off the stone walls with the force of a dozen yells
** an instant later the faint thunder of a distant roar rolled toward him from the other end of the tunnel
redoubling his speed eragon opened his mind to saphira removing every barrier around who he was so that they might join together without reservation
like a flood of warm water her consciousness rushed into him even as his rushed into her
eragon gasped and tripped and nearly fell
they enveloped each other within the folds of their thoughts holding each other with an intimacy no physical embrace could replicate allowing their identities to merge once again
their greatest comfort was a simple one they were no longer alone
to know that you were with one who cared for you and who understood every fiber of your being and who would not abandon you in even the most desperate of circumstances that was the most precious relationship a person could have and both eragon and saphira cherished it
it was not long before eragon sighted saphira hurrying toward him as swiftly as she could without banging her head on the ceiling or scraping her wings against the walls
her claws screeched on the stone floor as she slid to a stop in front of eragon fierce sparkling glorious
crying out with joy eragon leaped upward and ignoring her sharp scales wrapped his arms around her neck and hugged her as tightly as he could his feet dangling several inches in the air
little one said saphira her tone warm
she lowered him to the floor then snorted and said little one unless you wish to choke me you should loosen your arms
sorry
grinning he stepped back then laughed and pressed his forehead against her snout and began to scratch behind both corners of her jaw
saphira is low humming filled the tunnel
i have never flown so far so fast
i stopped only once after i left the varden and i would not have stopped at all except i became too thirsty to continue
do you mean you have not slept or eaten for three days
she blinked at him concealing her brilliant sapphire eyes for an instant