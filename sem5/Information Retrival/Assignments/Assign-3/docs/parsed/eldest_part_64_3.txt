her brilliant scales glittered like stars and nearly blinded her foes with their reflected light
next they saw eragon running alongside saphira
he moved faster than the soldiers could react and with strength beyond men splintered shields with a single blow rent armor and clove the swords of those who opposed him
shot and dart cast at him fell to the pestilent ground ten feet away stopped by his wards
it was harder for eragon and by extension saphira to fight his own race than it had been to fight the urgals in farthen dur
every time he saw a frightened face or looked into a soldier is mind he thought this could be me
but he and saphira could afford no mercy if a soldier stood before them he died
three times they sallied forth and three times eragon and saphira slew every man in the empire is first few ranks before retreating to the main body of the varden to avoid being surrounded
by the end of their last attack eragon had to reduce or eliminate certain wards around arya orik nasuada saphira and himself in order to keep the spells from exhausting him too quickly
for though his strength was great so too were the demands of battle
ready he asked saphira after a brief respite
she growled an affirmative
a cloud of arrows whistled toward eragon the instant he dove back into combat
fast as an elf he dodged the bulk of them since his magic no longer protected him from such missiles caught twelve on his shield and stumbled as one struck his belly and one his side
neither shaft pierced his armor but they knocked the wind out of him and left bruises the size of ** not ** you ve dealt with worse pain than this before he told himself
rushing a cluster of eight soldiers eragon darted from one to the next knocking aside their pikes and jabbing zar roc like a deadly bolt of lightning
the fighting had dulled his reflexes though and one soldier managed to drive his pike through eragon is hauberk slicing his left triceps
the soldiers cringed as saphira roared
eragon took advantage of the distraction to fortify himself with energy stored within the ruby in zar roc is pommel and then to kill the three remaining soldiers
sweeping her tail over him saphira knocked a score of men out of his way
in the lull that followed eragon looked over at his throbbing arm and said waise ** he also healed his bruises relying upon zar roc is ruby as well as the diamonds in the belt of beloth the wise
then the two of them pressed onward
eragon and saphira choked the burning plains with mountains of their enemies and yet the empire never faltered or fell back
for every man they killed another stepped forth to take his place
a sense of hopelessness engulfed eragon as the mass of soldiers gradually forced the varden to retreat toward their own camp
he saw his despair mirrored in the faces of nasuada arya king orrin and even angela when he passed them in battle
all our training and we still can not stop the empire raged ** are just too many ** we can not keep this up forever
and zar roc and the belt are almost depleted
you can draw energy from your surroundings if you have to
i wo not not unless i kill another of galbatorix is magicians and can take it from the soldiers
otherwise i will just be hurting the rest of the varden since there are no plants or animals here i can use to support us
as the long hours dragged by eragon grew sore and weary and stripped of many of his arcane defenses accumulated dozens of minor injuries
his left arm went numb from the countless blows that hammered his mangled shield
a scratch on his forehead kept blinding him with rivulets of hot sweat mixed blood
he thought one of his fingers might be broken
saphira fared no better
the soldiers armor tore the inside of her mouth dozens of swords and arrows cut her unprotected wings and a javelin punctured one of her own plates of armor wounding her in the shoulder
eragon saw the spear coming and tried to deflect it with a spell but was too slow
whenever saphira moved she splattered the ground with hundreds of drops of blood
beside them three of orik is warriors fell and two of the kull