late that night when the feast was finally over eragon and saphira walked back to his tent gazing at the stars and talking about what had been and what yet might be
and they were happy
when they arrived at their destination eragon paused and looked up at saphira and his heart was so full of love he thought it might stop beating