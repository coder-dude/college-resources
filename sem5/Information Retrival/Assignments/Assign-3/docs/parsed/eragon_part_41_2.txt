guard her until the ceremony is completed
i will have new orders for you by ** the warriors nodded curtly and carried arya out of the room
eragon watched them go wishing that he could accompany her
his attention snapped back to the bald man as he said enough of this we have wasted too much time already
prepare to be examined
eragon did not want this hairless threatening man inside his mind laying bare his every thought and feeling but he knew that resistance would be useless
the air was strained
murtagh is gaze burned into his forehead
finally he bowed his head
i am ready
he was interrupted as orik said abruptly you d better not harm him egraz carn else the king will have words for you
the bald man looked at him irritably then faced eragon with a small smile
only if he ** he bowed his head and chanted several inaudible words
eragon gasped with pain and shock as a mental probe clawed its way into his mind
his eyes rolled up into his head and he automatically began throwing up barriers around his consciousness
the attack was incredibly powerful
do not do ** saphira
her thoughts joined his filling him with ** re putting murtagh at ** eragon faltered gritted his teeth then forced himself to remove his shielding exposing himself to the ravening probe
disappointment emanated from the bald man
his battering intensified
the force coming from his mind felt decayed and unwholesome there was something profoundly wrong about it
he wants me to fight ** eragon as a fresh wave of pain racked him
a second later it subsided only to be replaced by another
saphira did her best to suppress it but even she could not block it entirely
give him what he wants she said quickly but protect everything else
i will help you
his strength is no match for mine i am already shielding our words from him
then why does it still hurt
eragon winced as the probe dug in farther hunting for information like a nail being driven through his skull
the bald man roughly seized his childhood memories and began sifting through ** does not need those get him out of ** growled eragon angrily
i can not not without endangering you said ** can conceal things from his view but it must be done before he reaches them
think quickly and tell me what you want hidden
eragon tried to concentrate through the pain
he raced through his memories starting from when he had found saphira is egg
he hid sections of his discussions with brom including all the ancient words he had been taught
their travels through palancar valley yazuac daret and teirm he left mostly untouched
but he had saphira conceal everything he remembered of angela is fortunetelling and solembum
he skipped from their burglary at teirm to brom is death to his imprisonment in gil ead and lastly to murtagh is revelation of his true identity
eragon wanted to hide that as well but saphira ** varden have a right to know who they shelter under their roof especially if it is a son of the forsworn
just do it he said tightly fighting another wave of ** wo not be the one to unmask him at least not to this man
it will be discovered as soon as murtagh is scanned warned saphira sharply
with the most important information hidden there was nothing else for eragon to do but wait for the bald man to finish his inspection
it was like sitting still while his fingernails were extracted with rusty tongs
his entire body was rigid jaw locked tightly
heat radiated from his skin and a line of sweat rolled down his neck
he was acutely aware of each second as the long minutes crept by
the bald man wound through his experiences sluggishly like a thorny vine pushing its way toward the sunlight
he paid keen attention to many things eragon considered irrelevant such as his mother selena and seemed to linger on purpose so as to prolong the suffering
he spent a long time examining eragon is recollections of the ra zac and then later the shade
it was not until his adventures had been exhaustively analyzed that the bald man began to withdraw from eragon is mind
the probe was extracted like a splinter being removed