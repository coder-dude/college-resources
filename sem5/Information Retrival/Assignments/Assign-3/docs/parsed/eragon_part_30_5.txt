his shoulders shrieked with pain as he scrambled onto the wall and dropped to the other side
he stumbled then regained his balance and darted down an alley just as the ra zac leapt over the wall
galvanized eragon put on another burst of speed
he ran for over a mile before he had to stop and catch his breath
unsure if he had lost the ra zac he found a crowded marketplace and dived under a parked ** did they find me he wondered ** should not have known where i was
unless something happened to ** he reached out with his mind to saphira and said the ra zac found me
we re all in ** check if brom is all right
if he is warn him and have him meet me at the inn
and be ready to fly here as fast as you can
we may need your help to escape
she was silent then said curtly he will meet you at the inn
do not stop moving you re in great danger
do not i know it muttered eragon as he rolled out from under the wagon
he hurried back to the golden globe quickly packed their belongings saddled the horses then led them to the street
brom soon arrived staff in hand scowling dangerously
he swung onto snowfire and asked what happened
i was in the cathedral when the ra zac just appeared behind me said eragon climbing onto cadoc
i ran back as fast as possible but they could be here at any second
saphira will join us once we re out of dras leona
we have to get outside the city walls before they close the gates if they have not already said brom
if they re shut it will be nigh impossible for us to leave
whatever you do do not get separated from ** eragon stiffened as ranks of soldiers marched down one end of the street
brom cursed lashed snowfire with his reins and galloped away
eragon bent low over cadoc and followed
they nearly crashed several times during the wild hazardous ride plunging through masses of people that clogged the streets as they neared the city wall
when the gates finally came into view eragon pulled on cadoc is reins with dismay
the gates were already half closed and a double line of pikemen blocked their way
they will cut us to ** he exclaimed
we have to try and make it said brom his voice hard
i will deal with the men but you have to keep the gates open for ** eragon nodded gritted his teeth and dug his heels into cadoc
they plowed toward the line of unwavering soldiers who lowered their pikes toward the horses chests and braced the weapons against the ground
though the horses snorted with fear eragon and brom held them in place
eragon heard the soldiers shout but kept his attention on the gates inching shut
as they neared the sharp pikes brom raised his hand and spoke
the words struck with precision the soldiers fell to each side as if their legs had been cut out from under them
the gap between the gates shrank by the second
hoping that the effort would not prove too much for him eragon drew on his power and shouted du grind **
a deep grating sound emanated from the gates as they trembled then ground to a stop
the crowd and guards fell silent staring with amazement
with a clatter of the horses hooves brom and eragon shot out from behind dras leona is wall
the instant they were free eragon released the gates
they shuddered then boomed shut
he swayed with the expected fatigue but managed to keep riding
brom watched him with concern
their flight continued through the outskirts of dras leona as alarm trumpets sounded on the city wall
saphira was waiting for them by the edge of the city hidden behind some trees
her eyes burned her tail whipped back and forth
go ride her said brom
and this time stay in the air no matter what happens to me
i will head south
fly nearby i do not care if saphira is ** eragon quickly mounted saphira
as the ground dwindled away beneath him he watched brom gallop along the road
yes said ** only because we were very lucky
a puff of smoke blew from her ** the time we ve spent searching for the ra zac was useless
i know he said letting his head sag against her ** the ra zac had been the only enemies back there i would have stayed and fought but with all the soldiers on their side it was hardly a fair match