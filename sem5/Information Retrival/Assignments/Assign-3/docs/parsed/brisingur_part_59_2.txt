wary of a mental attack eragon withdrew into himself and concentrated upon reciting a scrap of elvish poetry
he touched arya on the shoulder and whispered do you feel that
she nodded
we should have brought blodhgarm with us
together they descended the stairs making every effort to be quiet
the next room in the tower was much larger than the last the ceiling was over thirty feet high and from it hung a lantern with faceted panes of glass
a yellow flame burned inside
hundreds of oil paintings covered the walls portraits of bearded men in ornate robes and expressionless women sitting amid children with sharp flat teeth gloomy windswept seascapes depicting the drowning of sailors and scenes of battle where humans slaughtered bands of grotesque urgals
a row of tall wooden shutters set within the northern wall opened onto a balcony with a stone balustrade
opposite the window near the far wall was a collection of small round tables littered with scrolls three padded chairs and two oversized brass urns filled with bouquets of dried flowers
a stout gray haired woman garbed in a lavender dress sat in one of the chairs
she bore a strong resemblance to several of the men in the paintings
a silver diadem adorned with jade and topaz rested upon her head
in the center of the room stood the three magicians eragon had glimpsed before in the city
the two men and a woman were facing each other the hoods of their robes thrown back and their arms extended out to each side so that the tips of their fingers touched
they swayed in unison murmuring an unfamiliar spell in the ancient language
a fourth person sat in the middle of the triangle they formed a man garbed in an identical fashion but who said nothing and who grimaced as if in pain
eragon threw himself at the mind of one of the male spellcasters but the man was so focused on his task eragon failed to gain entry to his consciousness and thus was unable to subordinate him to his will
the man did not even seem to notice the attack
arya must have attempted the same thing for she frowned and whispered they were trained well
do you know what they are doing he murmured
then the woman in the lavender dress looked up and saw eragon and arya crouched upon the stone stairs
to eragon is surprise the woman did not call for help but rather placed a finger upon her lips then beckoned
eragon exchanged a perplexed glance with arya
it could be a trap he whispered
it most likely is she said
then let us go and greet our host
matching their steps they padded down the remaining stairs and snuck across the room never taking their eyes off the engrossed magicians
are you lady lorana asked arya in a soft voice as they halted before the seated woman
the woman inclined her head
that i am fair ** she turned her gaze upon eragon then and said and are you the dragon rider of whom we have heard so much about recently are you eragon shadeslayer
a relieved expression appeared upon the woman is distinguished face
ah i had hoped you would come
you must stop them ** and she gestured at the magicians
why do not you order them to surrender whispered eragon
i cannot said lorana
they answer only to the king and his new rider
i have sworn myself to galbatorix i had no choice in the matter so i cannot raise a hand against him or his servants otherwise i would have arranged their destruction myself
why asked arya
what is it you fear so much
the skin around lorana is eyes tightened
they know they cannot hope to drive off the varden as they are and galbatorix has not sent reinforcements to our aid
so they are attempting i do not know how to create a shade in the hope that the monster will turn against the varden and spread sorrow and confusion throughout your ranks
horror enveloped eragon
he could not imagine having to fight another durza
but a shade might just as easily turn against them and everyone else in feinster as it would against the varden
lorana nodded
they do not care
they only wish to cause as much pain and destruction as they can before they die
they are insane shadeslayer