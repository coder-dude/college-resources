then to his astonishment the tip of the dagger caught on one of the flameless lanterns mounted on the wall
eragon whirled away before he could see more but an instant later a burning hot hand seemed to strike him from behind throwing him a good twenty feet through the hall until he fetched up against the edge of an open archway instantly accumulating a new collection of scrapes and bruises
a booming report deafened him
feeling as if someone were driving splinters into his eardrums eragon clapped his hands over his ears and curled into a ball howling
when the noise and the pain had subsided he lowered his hands and staggered to his feet clenching his teeth as his injuries announced their presence with a myriad of unpleasant sensations
groggy and confused he gazed upon the site of the explosion
the blast had blackened a ten foot length of the hallway with soot
soft flakes of ash tumbled through the air which was as hot as the air from a heated forge
the dwarf who had been about to strike eragon lay on the ground thrashing his body covered with burns
after a few more convulsions he grew still
eragon is three remaining guards lay at the edge of the soot where the explosion had thrown them
even as he watched they staggered upright blood dripping from their ears and gaping mouths their beards singed and in disarray
the links along the fringe of their hauberks glowed red but their leather under armor seemed to have protected them from the worst of the heat
eragon took a single step forward then stopped and groaned as a patch of agony bloomed between his shoulder blades
he tried to twist his arm around to feel the extent of the wound but as his skin stretched the pain became too great to continue
nearly losing consciousness he leaned against the wall for support
he glanced at the burnt dwarf again
i must have suffered similar injuries on my back
forcing himself to concentrate he recited two of the spells designed to heal burns that brom had taught him during their travels
as they took effect it felt as if cool soothing water were flowing across his back
he sighed with relief and straightened
are you hurt he asked as his guards hobbled over
the lead dwarf frowned tapped his right ear and shook his head
eragon muttered a curse and only then did he notice he could not hear his own voice
again drawing upon the reserves of energy within his body he cast a spell to repair the inner mechanisms of his ears and of theirs
as the incantation concluded an irritating itch squirmed inside his ears then faded along with the spell
the dwarf on the right a burly fellow with a forked beard coughed and spat out a glob of congealed blood then growled nothing that time wo not mend
what of you shadeslayer
testing the floor with every step eragon entered the soot blackened area and knelt beside kvistor hoping that he might still save the dwarf from the clutches of death
as soon as he beheld kvistor is wound again he knew it was not to be
eragon bowed his head the memory of recent and former bloodshed bitter to his soul
he stood
why did the lantern explode
they are filled with heat and light argetlam one of his guards replied
if they are broken all of it escapes at once and then it is better to be far away
gesturing at the crumpled corpses of their attackers eragon asked do you know of which clan they are
the dwarf with the forked beard rifled through the clothes of several of the black garbed dwarves then said ** they carry no marks upon them such as you would recognize argetlam but they carry ** he held up a bracelet made of braided horsehair set with polished cabochons of amethyst
this amethyst said the dwarf and tapped one of the cabochons with a soot streaked fingernail this particular variety of amethyst it grows in only four parts of the beor mountains and three of them belong to az sweldn rak anhuin
eragon frowned
grimstborith vermund ordered this attack
i cannot say for sure argetlam
another clan might have left the bracelet for us to find
they might want us to think it was az sweldn rak anhuin so we do not realize who our foes really are