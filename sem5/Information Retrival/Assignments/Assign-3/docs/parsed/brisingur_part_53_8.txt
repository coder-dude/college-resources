you are tired and even with my magic you are liable to ruin the sword if you continue to work on it
now that the blade is done i can attend to the rest without interference from my oath so go
you will find a bed on the second floor of my house
if you are hungry there is food in the pantry
eragon hesitated reluctant to leave then nodded and shambled away from the bench his feet dragging in the dirt
as he passed her he ran a hand over saphira is wing and bade her good night too weary to say more
in return she tousled his hair with a warm puff of air and said i shall watch and remember for you little one
eragon paused on the threshold of rhunon is house and looked across the shadowy atrium to where maud and the two elf children were still standing
he raised a hand in greeting and maud smiled at him baring her sharp pointed teeth
a tingle crawled down eragon is neck as the elf children gazed at him their large slanted eyes were slightly luminous in the gloom
when they made no other motion he ducked his head and hurried inside eager to lie down upon a soft mattress