her countenance became one of enigmatic observation a beautiful mask that concealed her thoughts and feelings and one that eragon could not penetrate no matter how hard he strove
when she spoke she said as you have seen fit to save this man is life at no little trouble and effort on your own part i cannot refuse your request and thereby render your sacrifice meaningless
if sloan survives the ordeal you have set before him then gilderien the wise shall allow him to pass and sloan shall have a room and a bed and food to eat
more i cannot promise for what happens afterward will depend on sloan himself but if the conditions you named are met then yes we shall light his darkness
thank you your majesty
you are most generous
no not generous
this war does not allow me to be generous only practical
go and do what you must and be you careful eragon shadeslayer
your ** he bowed
if i may ask one last favor would you please refrain from telling arya nasuada or any of the varden of my current situation i do not want them to worry about me any longer than they have to and they will learn of it soon enough from saphira
eragon waited but when she remained silent and it became clear she had no intention of announcing her decision he bowed a second time and again said thank you
the glowing image on the surface of the water flickered and then vanished into darkness as eragon ended the spell he had used to create it
he leaned back on his heels and gazed up at the multitude of stars allowing his eyes to readjust to the faint glimmering light they provided
then he left the crumbling rock with the pool of water and retraced his path across the grass and scrub to the camp where sloan still sat upright rigid as cast iron
eragon struck a pebble with his foot and the resulting noise revealed his presence to sloan who snapped his head around quick as a bird
have you made up your mind demanded sloan
i have said eragon
he stopped and squatted in front of the butcher steadying himself with one hand on the ground
hear me well for i do not intend to repeat myself
you did what you did because of your love for katrina or so you say
whether you admit it or not i believe you also had other baser motives in wanting to separate her from roran anger
hate
vindictiveness
and your own hurt
sloan is lips hardened into thin white lines
you wrong me
no i do not think so
since my conscience prevents me from killing you your punishment is to be the most terrible i could invent short of death
i am convinced that what you said before is true that katrina is more important to you than anything else
therefore your punishment is this you shall not see touch or talk with your daughter again even unto your dying day and you shall live with the knowledge that she is with roran and they are happy together without you
sloan inhaled through his clenched teeth
that is your punishment ** you cannot enforce it you have no prison to put me in
i am not finished
i will enforce it by having you swear oaths in the elves tongue in the language of truth and magic to abide by the terms of your sentence
you can not force me to give my word sloan growled
not even if you torture me
i can and i wo not torture you
furthermore i will lay upon you a compulsion to travel northward until you reach the elf city of ellesmera which stands deep in the heart of du weldenvarden
you can try to resist the urge if you want but no matter how long you fight it the spell will irritate you like an unscratched itch until you obey its demands and travel to the elves realm
do not you have the guts to kill me yourself asked sloan
you re too much of a coward to put a blade to my neck so you will make me wander the wilderness blind and lost until the weather or the beasts do me in he spat to the left of eragon
you re nothing but the yellow bellied offspring of a canker ridden bunter
you re a bastard you are and an unlicked cub a dung splattered tallowfaced rock gnasher a puking villain and a noxious toad the runty mewling spawn of a greasy sow
i would not give you my last crust if you were starving or a drop of water if you were burning or a beggar is grave if you were dead