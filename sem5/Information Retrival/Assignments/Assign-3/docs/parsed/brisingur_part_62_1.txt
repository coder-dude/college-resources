while this is of great historical interest practically it often leads to confusion as to the correct pronunciation
unfortunately there are no set rules for the neophyte
you must learn each name upon its own terms unless you can immediately place its language of origin
the matter grows even more confusing when you realize that in many places the resident population altered the spelling and pronunciation of foreign words to conform to their own language
the anora river is a prime example
originally anora was spelled aenora which means broad in the ancient language
in their writings the humans simplified the word to anora and this combined with a vowel shift wherein ae ** ** was said as the easier a ** created the name as it appears in eragon is time
to spare readers as much difficulty as possible i have compiled the following list with the understanding that these are only rough guidelines to the actual pronunciation
the enthusiast is encouraged to study the source languages in order to master their true intricacies