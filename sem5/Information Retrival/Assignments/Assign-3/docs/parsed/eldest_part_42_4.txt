think not that i am devoid of sympathy eragon
everyone experiences ardor like yours at one point or another during their lives
it is part of growing up
i also know how hard it is for you to deny yourself the usual comforts of life but it is necessary if we are to prevail
they sat at the kitchen table and oromis began to lay out writing materials for eragon to practice the liduen kvaedhi
it would be unreasonable of me to expect you to forget your fascination with arya but i do expect you to prevent it from interfering with my instruction again
can you promise me that
and arya what would be the honorable thing to do about her predicament
eragon hesitated
i do not want to lose her friendship
therefore
i will go to her i will apologize and i will reassure her that i never intend to cause her such hardship ** it was difficult for him to say but once he did he felt a sense of relief as if acknowledging his mistake cleansed him of it
oromis appeared pleased
by that alone you prove that you have matured
the sheets of paper were smooth underneath eragon is hands as he pressed them flat against the tabletop
he stared at the blank white expanse for a moment then dipped a quill in ink and began to transcribe a column of glyphs
each barbed line was like a streak of night against the paper an abyss into which he could lose himself and try to forget his confused feelings