may his name live on in glory
then he bowed his head and mourned freely
he stood like a living statue until evening when light faded from the land
that night he dreamed of the imprisoned woman again
he could tell that something was wrong with her
her breathing was irregular and she shook whether from cold or pain he did not know
in the semidarkness of the cell the only thing clearly illuminated was her hand which hung over the edge of the cot
a dark liquid dripped from the tips of her fingers
eragon knew it was blood