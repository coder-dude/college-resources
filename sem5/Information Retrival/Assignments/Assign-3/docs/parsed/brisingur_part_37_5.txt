so my second question for the meet is this who is responsible for this ill thought of maneuvering and if none are willing to admit their misconduct i move most strongly that we order all warriors regardless of their clan expelled from tronjheim for the duration of the meet and that we immediately appoint a reader of law to investigate these doings and determine whom we should censure
gannel is revelation question and subsequent proposal aroused a flurry of heated conversation among the clan chiefs with the dwarves hurling accusations denials and counteraccusations at each other with increasing vitriol until at last when an infuriated thordris was shouting at a red faced galdhiem orik cleared his throat again causing everyone to stop and stare at him
in a mild tone orik said this too i believe i can explain to you gannel at least in part
i cannot speak to the activities of the other clans but several hundred of the warriors who have been hurrying through the servants halls in tronjheim have been of durgrimst ingeitum
this i freely admit
all was silent until iorunn said and what explanation have you for this belligerent behavior orik thrifk is son
as i said before fair iorunn my answer must of necessity be a lengthy one so if you gannel have any other questions to ask i suggest you proceed forthwith
gannel is frown deepened until his projecting eyebrows nearly touched
he said i will withhold mine other questions for the time being for they all pertain to those i have already put to the meet and it seems we must wait upon your pleasure to learn any more of those subjects
however since you are involved fist and foot with these doubtful activities a new question has occurred to me that i would ask of you specifically grimstborith orik
for what reason did you desert yesterday is meet and let me warn you i will brook no evasions
you have already intimated you have knowledge of these affairs
well time is for you to provide a full accounting of yourself grimstborith orik
orik stood even as gannel sat and he said it shall be mine pleasure
lowering his bearded chin until it rested upon his chest orik paused for a brief span and then began to speak in a sonorous voice but he did not begin as eragon had expected nor eragon surmised as the rest of the congregation had expected
instead of describing the attempt on eragon is life and thus explaining why he had terminated the previous clanmeet prematurely orik commenced by recounting how at the dawn of history the race of dwarves had migrated from the once verdant fields of the hadarac desert to the beor mountains where they had excavated their uncounted miles of tunnels built their magnificent cities both above and below the ground and waged lusty war between their various factions as well as with the dragons whom for thousands of years the dwarves had regarded with a combination of hate fear and reluctant awe
then orik spoke of the elves arrival in alagaesia and of how the elves had fought with the dragons until they nearly destroyed each other and of how as a result the two races had agreed to create the dragon riders to maintain the peace thereafter
and what was our response when we learned of their intentions demanded orik his voice ringing loud in the chamber
did we ask to be included in their pact did we aspire to share in the power that would be the dragon riders ** we clung to our old ways our old hatreds and we rejected the very thought of bonding with the dragons or allowing anyone outside our realm to police us
to preserve our authority we sacrificed our future for i am convinced that if some of the dragon riders had been knurlan galbatorix might have never risen to power
even if i am wrong and i mean not to belittle eragon who has proven himself a fine rider the dragon saphira might have hatched for one of our race and not a human
and then what glory might have been ours
instead our importance in alagaesia has diminished ever since queen tarmunora and eragon is namesake made peace with the dragons
at first our lessened status was not so bitter a draught to swallow and often it was easier to deny than to accept