eragon froze for a moment astonished
yes ** from within the folds of his white tunic oromis withdrew a shingle of thin gray slate which he passed to eragon
the stone was cool and smooth between eragon is fingers
on the other side of it he knew he would find a perfect likeness of his mother painted by means of a spell with pigments an elf had set within the slate many years ago
a flutter of uneasiness ran through eragon
he had always wanted to see his mother but now that the opportunity was before him he was afraid that the reality might disappoint him
with an effort he turned the slate over and beheld an image clear as a vision seen through a window of a garden of red and white roses lit by the pale rays of dawn
a gravel path ran through the beds of roses
and in the middle of the path was a woman kneeling cupping a white rose between her hands and smelling the flower her eyes closed and a faint smile upon her lips
she was very beautiful eragon thought
her expression was soft and tender yet she wore clothes of padded leather with blackened bracers upon her forearms and greaves upon her shins and a sword and dagger hanging from her waist
in the shape of her face eragon could detect a hint of his own features as well as a certain resemblance to garrow her brother
the image fascinated eragon
he pressed his hand against the surface of the fairth wishing that he could reach into it and touch her on the arm
oromis said brom gave me the fairth for safekeeping before he left for carvahall and now i give it to you
without looking up eragon asked would you keep it safe for me as well it might get broken during our traveling and fighting
the pause that followed caught eragon is attention
he wrenched his gaze away from his mother to see that oromis appeared melancholy and preoccupied
no eragon i cannot
you will have to make other arrangements for the preservation of the fairth
why eragon wanted to ask but the sorrow in oromis is eyes dissuaded him
then oromis said your time here is limited and we still have many matters to discuss
shall i guess which subject you would like to address next or will you tell me
with great reluctance eragon placed the fairth on the table and rotated it so that the image was upside down
the two times we have fought murtagh and thorn murtagh has been more powerful than any human ought to be
on the burning plains he defeated saphira and me because we did not realize how strong he was
if not for his change of heart we would be prisoners in uru baen right now
you once mentioned that you know how galbatorix has become so powerful
will you tell us now master for our own safety we need to know
it is not my place to tell you this said oromis
then whose is it demanded eragon
you can not
behind oromis glaedr opened one of his molten eyes which was as large as a round shield and said it is mine
the source of galbatorix is power lies in the hearts of dragons
from us he steals his strength
without our aid galbatorix would have fallen to the elves and the varden long ago
eragon frowned
i do not understand
why would you help galbatorix and how could you there are only four dragons and an egg left in alagaesia
are not there
many of the dragons whose bodies galbatorix and the forsworn slew are still alive today
still alive
bewildered eragon glanced at oromis but the elf remained quiet his face inscrutable
even more disconcerting was that saphira did not seem to share eragon is confusion
the gold dragon turned his head on his paws to better look at eragon his scales scraping against one another
unlike with most creatures he said a dragon is consciousness does not reside solely within our skulls
there is in our chests a hard gemlike object similar in composition to our scales called the eldunari which means the heart of ** when a dragon hatches their eldunari is clear and lusterless
usually it remains so all through a dragon is life and dissolves along with the dragon is corpse when they die
however if we wish we can transfer our consciousness into the eldunari
then it will acquire the same color as our scales and begin to glow like a coal