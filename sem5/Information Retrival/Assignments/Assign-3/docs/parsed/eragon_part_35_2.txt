still it will leave a bigger ** at his word the dirt shone cherry red though it did not burn his hand
all right just do not put that in ** yelped the soldier
the elf is in the last cell to the ** i do not know about your sword but it is probably in the guardroom upstairs
all the weapons are there
eragon nodded then murmured ** the soldier is eyes rolled up in his head and he collapsed limply
eragon looked at the stranger who was now only a few paces away
he narrowed his eyes trying to see past the beard
** is that you he exclaimed
yes said murtagh briefly lifting the beard from his shaven face
i do not want my face seen
did you kill him
no he is only asleep
how did you get in
there is no time to explain
we have to get up to the next floor before anyone finds us
there will be an escape route for us in a few minutes
we do not want to miss it
did not you hear what i said asked eragon gesturing at the unconscious soldier
there is an elf in the prison
i saw ** we have to rescue her
i need your help
an elf
** murtagh hurried down the hall growling this is a mistake
we should flee while we have the ** he stopped before the cell the soldier had indicated and produced a ring of keys from under his ragged cloak
i took it from one of the guards he explained
eragon motioned for the keys
murtagh shrugged and handed them to him
eragon found the right one and swung the door open
a single beam of moonlight slanted through the window illuminating the elf is face with cool silver
she faced him tense and coiled ready for whatever would happen next
she held her head high with a queen is demeanor
her eyes dark green almost black and slightly angled like a cat is lifted to eragon is
chills shot through him
their gaze held for a moment then the elf trembled and collapsed soundlessly
eragon barely caught her before she struck the floor
she was surprisingly light
the aroma of freshly crushed pine needles surrounded her
murtagh entered the cell
she is **
we can tend to her later
are you strong enough to carry her eragon shook his head
then i will do it said murtagh as he slung the elf across his shoulders
now ** he handed eragon a dagger then hurried back into the hall littered with soldiers bodies
with heavy footsteps murtagh led eragon to a stone hewn staircase at the end of the hall
as they climbed it eragon asked how are we going to get out without being noticed
that did not allay eragon is fears
he listened anxiously for soldiers or anyone else who might be nearby dreading what might happen if they met the shade
at the head of the stairs was a banquet room filled with broad wooden tables
shields lined the walls and the wood ceiling was trussed with curved beams
murtagh laid the elf on a table and looked at the ceiling worriedly
can you talk to saphira for me
tell her to wait another five minutes
there were shouts in the distance
soldiers marched past the entrance to the banquet room
eragon is mouth tightened with pent up tension
whatever you re planning to do i do not think we have much time
just tell her and stay out of sight snapped murtagh running off
as eragon relayed the message he was alarmed to hear men coming up the stairs
fighting hunger and exhaustion he dragged the elf off the table and hid her underneath it
he crouched next to her holding his breath tightly clenching the dagger
ten soldiers entered the room
they swept through it hurriedly looking under only a couple of tables and continued on their way
eragon leaned against a table leg sighing
the respite made him suddenly aware of his burning stomach and parched throat
a tankard and a plate of half eaten food on the other side of the room caught his attention
eragon dashed from his hiding place grabbed the food then scurried back to the table
there was amber beer in the tankard which he drank in two great gulps
relief seeped through him as the cool liquid ran down his throat soothing the irritated tissue
he suppressed a belch before ravenously tearing into a hunk of bread
murtagh returned carrying zar roc a strange bow and an elegant sword without a sheath