daret was on the banks of the ninor river as it had to be to survive
the village was small and wild looking without any signs of inhabitants
eragon and brom approached it with great caution
saphira hid close to the town this time if trouble arose she would be at their sides within seconds
they rode into daret striving to be silent
brom gripped his sword with his good hand eyes flashing everywhere
eragon kept his bow partially drawn as they passed between the silent houses glancing at each other with ** does not look good commented eragon to saphira
she did not answer but he felt her prepare to rush after them
he looked at the ground and was reassured to see the fresh footprints of ** where are they
brom stiffened as they entered the center of daret and found it empty
wind blew through the desolate town and dust devils swirled sporadically
brom wheeled snowfire about
let is get out of here
i do not like the feel of ** he spurred snowfire into a gallop
eragon followed him urging cadoc onward
they advanced only a few strides before wagons toppled out from behind the houses and blocked their way
cadoc snorted and dug in his hooves sliding to a stop next to snowfire
a swarthy man hopped over the wagon and planted himself before them a broadsword slung at his side and a drawn bow in his hands
eragon swung his own bow up and pointed it at the stranger who commanded ** put your weapons down
you re surrounded by sixty archers
they will shoot if you ** as if on cue a row of men stood up on the roofs of the surrounding houses
stay away ** ** are too many
if you come they will shoot you out of the sky
stay ** she heard but he was unsure if she would obey
he prepared to use ** will have to stop the arrows before they hit me or brom
what do you want asked brom calmly
why have you come here demanded the man
to buy supplies and hear the news
nothing more
we re on the way to my cousin is house in dras leona
so are you said brom
these are dangerous times
** the man looked at them carefully
i do not think you mean us ill but we ve had too many encounters with urgals and bandits for me to trust you only on your word
if it does not matter what we say what happens now countered brom
the men on top of the houses had not moved
by their very stillness eragon was sure that they were either highly disciplined
or frightened for their lives
he hoped it was the latter
you say that you only want supplies
would you agree to stay here while we bring what you need then pay us and leave immediately
all right said the man lowering his bow though he kept it ready
he waved at one of the archers who slid to the ground and ran over
tell him what you want
brom recited a short list and then added also if you have a spare pair of gloves that would fit my nephew i d like to buy those ** the archer nodded and ran off
the name is trevor said the man standing in front of them
normally i d shake your hand but under the circumstances i think i will keep my distance
tell me where are you from
north said brom but we have not lived in any place long enough to call it home
have urgals forced you to take these measures
yes said trevor and worse fiends
do you have any news from other towns we receive word from them rarely but there have been reports that they are also beleaguered
brom turned grave
i wish it was not our lot to bring you these tidings
nearly a fortnight ago we passed through yazuac and found it pillaged
the villagers had been slaughtered and piled together
we would have tried to give them a decent burial but two urgals attacked us
shocked trevor stepped back and looked down with tears in his eyes
alas this is indeed a dark day
still i do not see how two urgals could have defeated all of yazuac
the people there were good fighters some were my friends
there were signs that a band of urgals had ravaged the town stated brom
i think the ones we encountered were deserters
brom fiddled with his saddlebags for a minute
large enough to wipe out yazuac but small enough to go unnoticed in the countryside
no more than a hundred and no less than fifty