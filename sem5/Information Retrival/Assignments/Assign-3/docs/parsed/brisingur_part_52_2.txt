in his left hand he carried a wand of white wood carved with glyphs from the liduen kvaedhi
mounted upon the end was a lustrous pearl
bending at the waist lord fiolr bowed as did eragon
then they exchanged the elves traditional greetings and eragon thanked the lord for being so generous as to allow him to inspect the sword tamerlein
and lord fiolr said long has tamerlein been a prized possession of my family and it is especially dear to my own heart
know you the history of tamerlein shadeslayer
my mate was the most wise and fair naudra and her brother arva was a dragon rider at the time of the fall
naudra was visiting with him in ilirea when galbatorix and the forsworn did sweep down upon the city like a storm from the north
arva fought alongside the other riders to defend ilirea but kialandi of the forsworn dealt him a mortal blow
as he lay dying on the battlements of ilirea arva gave his sword tamerlein to naudra that she might protect herself
with tamerlein naudra fought free of the forsworn and returned here with another dragon and rider although she died soon afterward of her wounds
with a single finger lord fiolr stroked the wand eliciting a soft glow from the pearl in response
tamerlein is as precious to me as the air in my lungs i would sooner part with life than part with it
unfortunately neither i nor my kin are worthy of wielding it
tamerlein was forged for a rider and riders we are not
i am willing to lend you it shadeslayer in order to aid you in your fight against galbatorix
however tamerlein will remain the property of house valtharos and you must promise to return the sword if ever i or my heirs ask for it
eragon gave his word and then lord fiolr led him and saphira to a long polished table grown out of the living wood of the floor
at one end of the table was an ornate stand and resting upon the stand was the sword tamerlein and its sheath
the blade of tamerlein was colored a dark rich green as was its sheath
a large emerald adorned the pommel
the furniture of the sword had been wrought of blued steel
a line of glyphs adorned the crossguard
in elvish they said i am tamerlein bringer of the final sleep
in length the sword was equal to zar roc but the blade was wider and the tip rounder and the build of the hilt was heavier
it was a beautiful deadly weapon but just by looking at it eragon could see that rhunon had forged tamerlein for a person with a fighting style different from his own a style that relied more on cutting and slashing than the faster more elegant techniques brom had taught him
as soon as eragon is fingers closed around tamerlein is hilt he realized that the hilt was too large for his hand and at that moment he knew that tamerlein was not the sword for him
it did not feel like an extension of his arm as had zar roc
and yet despite his realization eragon hesitated for where else could he hope to find so fine a sword arvindr the other blade oromis had mentioned lay in a city hundreds of miles distant
then saphira said do not take it
if you are to carry a sword into battle if your life and mine are to depend upon it then the sword must be perfect
nothing else will suffice
besides i do not like the conditions lord fiolr has attached to his gift
and so eragon replaced tamerlein on its stand and apologized to lord fiolr explaining why he could not accept the sword
the narrow faced elf did not appear overly disappointed to the contrary eragon thought he saw a flash of satisfaction appear in fiolr is fierce eyes
so shadeslayer you are still alive said rhunon without taking her eyes off her work
her voice grated like pitted millstones
oromis told me that you lost zar roc to the son of morzan
eragon winced and nodded even though she was not looking at him
yes rhunon elda
he took it from me on the burning plains
** rhunon concentrated on her hammering tapping the back of her chisel with inhuman speed then she paused and said the sword has found its rightful owner then
i do not like the use to which what is his name ah yes murtagh is putting zar roc but every rider deserves a proper sword and i can think of no better sword for the son of morzan than morzan is own ** the elf woman glanced up at eragon from underneath her lined brow