i also cautioned him to be on guard for certain dangers
is that satisfactory
eragon nodded
he wrapped the leather around the meat and started off
they were careful to stay out of sight until they reached the road then quickened their pace eager to distance themselves from carvahall
eragon plowed ahead determinedly his legs burning
the mindless rhythm of walking freed his mind to ** we get home i wo not travel any farther with brom until i get some answers he told himself firmly
i hope that he can tell me more about the riders and whom i am fighting
as the wreckage of the farm came into view brom is eyebrows beetled with anger
eragon was dismayed to see how swiftly nature was reclaiming the farm
snow and dirt were already piled inside the house concealing the violence of the strangers attack
all that remained of the barn was a rapidly eroding rectangle of soot
brom is head snapped up as the sound of saphira is wings drifted over the trees
she dived past them from behind almost brushing their heads
they staggered as a wall of air buffeted them
saphira is scales glittered as she wheeled over the farm and landed gracefully
brom stepped forward with an expression both solemn and joyous
his eyes were shining and a tear shone on his cheek before it disappeared into his beard
he stood there for a long while breathing heavily as he watched saphira and she him
eragon heard him muttering and edged closer to listen
so
it starts again
but how and where will it end my sight is veiled i cannot tell if this be tragedy or farce for the elements of both are here
however it may be my station is unchanged and i
whatever else he might have said faded away as saphira proudly approached them
eragon passed brom pretended he had heard nothing and greeted her
there was something different between them now as if they knew each other even more intimately yet were still strangers
he rubbed her neck and his palm tingled as their minds touched
a strong curiosity came from her
i ve seen no humans except you and garrow and he was badly injured she said
you ve viewed people through my eyes
it is not the ** came closer and turned her long head so that she could inspect brom with one large blue ** really are queer creatures she said critically and continued to stare at him
brom held still as she sniffed the air and then he extended a hand to her
saphira slowly bowed her head and allowed him to touch her on the brow
with a snort she jerked back and retreated behind eragon
her tail flicked over the ground
what is it he asked
she did not answer
brom turned to him and asked in an undertone what is her name
** a peculiar expression crossed brom is face
he ground the butt of his staff into the earth with such force his knuckles turned white
of all the names you gave me it was the only one she liked
i think it fits eragon added quickly
fit it does said brom
there was something in his voice eragon could not identify
was it loss wonder fear envy he was not sure it could have been none of them or all
brom raised his voice and said greetings saphira
i am honored to meet ** he twisted his hand in a strange gesture and bowed
of course you do everyone enjoys ** touched her on the shoulder and went to the ruined house
saphira trailed behind with brom
the old man looked vibrant and alive
eragon climbed into the house and crawled under a door into what was left of his room
he barely recognized it under the piles of shattered wood
guided by memory he searched where the inside wall had been and found his empty pack
part of the frame was broken but the damage could be easily repaired
he kept rummaging and eventually uncovered the end of his bow which was still in its buckskin tube
though the leather was scratched and scuffed he was pleased to see that the oiled wood was ** some luck
he strung the bow and pulled on the sinew experimentally
it bent smoothly without any snaps or creaks
satisfied he hunted for his quiver which he found buried nearby
many of the arrows were broken
he unstrung the bow and handed it and the quiver to brom who said it takes a strong arm to pull ** eragon took the compliment silently