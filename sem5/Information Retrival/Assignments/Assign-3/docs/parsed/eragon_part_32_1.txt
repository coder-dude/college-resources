wake up ** stirred and groaned
i need your ** is ** tried to ignore the voice and return to sleep
** bellow rang in the cave
he bolted upright fumbling for his bow
saphira was crouched over brom who had rolled off the ledge and was thrashing on the cave floor
his face was contorted in a grimace his fists were clenched
eragon rushed over fearing the worst
help me hold him down
he is going to hurt ** he cried to murtagh clasping brom is arms
his side burned sharply as the old man spasmed
together they restrained brom until his convulsions ceased
then they carefully returned him to the ledge
eragon touched brom is forehead
the skin was so hot that the heat could be felt an inch away
get me water and a cloth he said worriedly
murtagh brought them and eragon gently bathed brom is face trying to cool him down
with the cave quiet again he noticed the sun shining ** long did we sleep he asked saphira
a good while
i ve been watching brom for most of that time
he was fine until a minute ago when he started thrashing
i woke you once he fell to the floor
he stretched wincing as his ribs twinged painfully
a hand suddenly gripped his shoulder
brom is eyes snapped opened and fixed a glassy stare on eragon
** he gasped
bring me the **
brom exclaimed eragon pleased to hear him talk
you should not drink wine it will only make you worse
bring it boy just bring it
sighed brom
his hand slipped off eragon is shoulder
i will be right back hold ** eragon dashed to the saddlebags and rummaged through them frantically
i can not find ** he cried looking around desperately
here take mine said murtagh holding out a leather skin
eragon grabbed it and returned to brom
i have the wine he said kneeling
murtagh retreated to the cave is mouth so they could have privacy
brom is next words were faint and indistinct
good
he moved his arm weakly
now
wash my right hand with it
no ** i have not ** mystified eragon unstoppered the wineskin and poured the liquid onto brom is palm
he rubbed it into the old man is skin spreading it around the fingers and over the back of the hand
more croaked brom
eragon splashed wine onto his hand again
he scrubbed vigorously as a brown dye floated off brom is palm then stopped his mouth agape with amazement
there on brom is palm was the gedwey ignasia
you re a rider he asked incredulously
a painful smile flickered on brom is face
once upon a time that was true
but no more
when i was young
younger than you are now i was chosen
chosen by the riders to join their ranks
while they trained me i became friends with another apprentice
morzan before he was a ** eragon gasped that had been over a hundred years ago
but then he betrayed us to galbatorix
and in the fighting at doru areaba vroengard is city my young dragon was killed
her name
was saphira
why did not you tell me this before asked eragon softly
brom laughed
because
there was no need ** he stopped
his breathing was labored his hands were clenched
i am old eragon
so old
though my dragon was killed my life has been longer than most
you do not know what it is to reach my age look back and realize that you do not remember much of it then to look forward and know that many years still lie ahead of you
after all this time i still grieve for my saphira
and hate galbatorix for what he tore from ** his feverish eyes drilled into eragon as he said fiercely do not let that happen to you
do ** guard saphira with your life for without her it is hardly worth living
you should not talk like this
nothing is going to happen to her said eragon worried
brom turned his head to the side
perhaps i am ** his gaze passed blindly over murtagh then he focused on eragon
brom is voice grew stronger
** i cannot last much longer
this
this is a grievous wound it saps my strength
i have not the energy to fight it
before i go will you take my blessing
everything will be all right said eragon tears in his eyes
you do not have to do this
it is the way of things
i must
will you take my blessing eragon bowed his head and nodded overcome
brom placed a trembling hand on his brow
then i give it to you
may the coming years bring you great ** he motioned for eragon to bend closer