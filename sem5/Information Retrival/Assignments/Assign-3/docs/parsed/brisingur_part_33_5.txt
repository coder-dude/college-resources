before he began eating though he was careful to test the food for poison using the spells oromis had taught him
as eragon washed down the last crust of bread with a sip of thin watered down breakfast beer orik and his contingent of ten warriors entered the hall
the warriors sat at their own tables positioning themselves where they could watch both entrances while orik joined eragon lowering himself onto the stone bench opposite him with a weary sigh
he placed his elbows on the table and rubbed his face with his hands
eragon cast several spells to prevent anyone from eavesdropping then asked did we suffer another setback
no no setback
only these deliberations are trying in the extreme
and everyone noticed your frustration said orik
you must control yourself better hereafter eragon
revealing weakness of any sort to our opponents does nothing but further their cause
i orik fell silent as a portly dwarf waddled up and deposited a plate of steaming food in front of him
eragon scowled at the edge of the table
but are you any closer to the throne have we gained any ground with all of this long winded prattle
orik raised a finger while he chewed on a mouthful of bread
we have gained a great deal
do not be so ** after you left havard agreed to lower the tax on the salt durgrimst fanghur sells to the ingeitum in exchange for summer access to our tunnel to nalsvrid merna so they may hunt the red deer that gather around the lake during the warm months of the year
you should have seen how nado gritted his teeth when havard accepted my **
bah spat eragon
taxes deer what does any of it have to do with who succeeds hrothgar as ruler be honest with me orik
what is your position compared with the other clan chiefs and how much longer is this likely to drag on with every day that passes it becomes more likely that the empire will discover our ruse and galbatorix will strike at the varden when i am not there to fend off murtagh and thorn
orik wiped his mouth on the corner of the tablecloth
my position is sound enough
none of the grimstborithn have the support to call a vote but nado and i command the greatest followings
if either of us can win over say another two or three clans the balance will quickly tip in that person is favor
havard is already wavering
it wo not take too much more encouragement i think to convince him to defect to our camp
tonight we will break bread with him and i will see what i can do toward providing that ** orik devoured a piece of roast mushroom then said as for when the clanmeet will end maybe after another week if we are lucky and maybe two if we re not
eragon cursed in an undertone
he was so tense his stomach churned and rumbled and threatened to reject the meal he had just eaten
reaching across the table orik caught eragon by the wrist
there is nothing you or i can do to further hasten the clanmeet is decision so do not let it upset you overmuch
worry about what you can change and leave the rest to sort itself out eh he released eragon
eragon slowly exhaled and leaned on his forearms against the table
i know
it is only that we have so little time and if we fail
what will be will be said orik
he smiled but his eyes were sad and hollow
no one can escape fate is design
could not you seize the throne by force i know you do not have that many troops in tronjheim but with my support who could stand against you
orik paused with his knife halfway between his plate and his mouth then shook his head and resumed eating
between mouthfuls he said such a ploy would prove disastrous
must i explain our entire race would turn against us and instead of seizing control of our nation i would inherit an empty title
if that came to pass i would not bet a broken sword we would live to see out the year
orik said nothing more until the food on his plate was gone
then he downed a mouthful of beer belched and resumed the conversation we are balanced upon a windy mountain path with a mile high drop on either side
so many of my race hate and fear dragon riders because of the crimes galbatorix the forsworn and now murtagh have committed against us