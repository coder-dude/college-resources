saphira would like something to eat and we have not time for her to hunt as she usually does
quoth looked past him and eyed saphira is bulk and his face grew pale
how much does she normally
ah that is how much do you normally eat saphira i can have six sides of roast beef brought over immediately and another six will be ready in about fifteen minutes
will that be enough or
the knob in his throat jumped as he swallowed
saphira emitted a soft rippling growl which caused quoth to squeak and hop backward
she would prefer a live animal if that is convenient eragon said
in a high pitched voice quoth said convenient oh yes it is ** he bobbed his head twisting at his apron with his grease stained hands
most convenient indeed shadeslayer dragon saphira
king orrin is table will not be lacking this afternoon then oh no
and a barrel of mead saphira said to eragon
white circles appeared around quoth is irises as eragon repeated her request
i i am afraid that the dwarves have purchased most of our stocks of m m mead
we have only a few barrels left and those are reserved for king quoth flinched as a four foot long flame leaped out of saphira is nostrils and singed the grass in front of him
snarled lines of smoke drifted up from the blackened stalks
i i i will have a barrel brought to you at once
if you will f follow me i will take y you to the livestock where you may have whatever beast you like
skirting the fires and tables and groups of harried men the cook led them to a collection of large wooden pens which contained pigs cattle geese goats sheep rabbits and a number of wild deer the varden is foragers had captured during their forays into the surrounding wilderness
close to the pens were coops full of chickens ducks doves quail grouse and other birds
their squawking chirping cooing and crowing formed a cacophony so harsh it made eragon grit his teeth with annoyance
in order to avoid being overwhelmed by the thoughts and feelings of so many creatures he was careful to keep his mind closed to all but saphira
the three of them stopped over a hundred feet from the pens so saphira is presence would not panic the imprisoned animals
is there any here catches your fancy quoth asked gazing up at her and rubbing his hands with nervous dexterity
as she surveyed the pens saphira sniffed and said to eragon what pitiful prey
i am not really that hungry you know
i went hunting only the day before yesterday and i am still digesting the bones of the deer i ate
you re still growing quickly
the food will do you good
not if i can not stomach it
pick something small then
a pig maybe
that would hardly be of any help to you
no
i will take that ** saphira eragon received the image of a cow of medium stature with a splattering of white splotches on her left flank
after eragon pointed out the cow quoth shouted at a line of men idling by the pens
two of them separated the cow from the rest of the herd slipped a rope over its head and pulled the reluctant animal toward saphira
thirty feet from saphira the cow balked and lowed with terror and tried to shake free of the rope and flee
before the animal could escape saphira pounced leaping across the distance separating them
the two men who were pulling on the rope threw themselves flat as saphira rushed toward them her jaws gaping
saphira struck the cow broadside as it turned to run knocking the animal over and holding it in place with her splayed feet
it uttered a single terrified bleat before saphira is jaws closed over its neck
with a ferocious shake of her head she snapped its spine
she paused then crouched low over her kill and looked expectantly at eragon
closing his eyes eragon reached out with his mind toward the cow
the animal is consciousness had already faded into darkness but its body was still alive its flesh thrumming with motive energy which was all the more intense for the fear that had coursed through it moments before
repugnance for what he was about to do filled eragon but he ignored it and placing a hand over the belt of beloth the wise transferred what energy he could from the body of the cow into the twelve diamonds hidden around his waist