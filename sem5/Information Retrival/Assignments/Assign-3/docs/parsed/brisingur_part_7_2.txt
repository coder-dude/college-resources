along the way he often lowered sloan to the floor and left him to explore a chamber or byway that he had not visited before
in them he discovered many evil instruments including four metal flasks of seithr oil which he promptly destroyed so that no one else could use the flesh eating acid to further their malicious plans
hot sunlight stung eragon is cheeks when he stumbled out of the network of tunnels
holding his breath he hurried past the dead lethrblaka and went to the edge of the vast cave where he gazed down the precipitous side of helgrind at the hills far below
to the west he saw a pillar of orange dust billowing above the lane that connected helgrind to dras leona marking the approach of a group of horsemen
his right side was burning from supporting sloan is weight so eragon shifted the butcher onto his other shoulder
he blinked away the beads of sweat that clung to his eyelashes as he struggled to solve the problem of how he was supposed to transport sloan and himself five thousand some feet to the ground
it is almost a mile down he murmured
if there were a path i could easily walk that distance even with sloan
so i must have the strength to lower us with magic
yes but what you can do over a length of time may be too taxing to accomplish all at once without killing yourself
as oromis said the body cannot convert its stockpile of fuel into energy fast enough to sustain most spells for more than a few seconds
i only have a certain amount of power available at any given moment and once it is gone i have to wait until i recover
and talking to myself is not getting me anywhere
securing his hold on sloan eragon fixed his eyes on a narrow ledge about a hundred feet below
this is going to hurt he thought preparing himself for the attempt
then he barked **
eragon felt himself rise several inches above the floor of the cave
fram he said and the spell propelled him away from helgrind and into open space where he hung unsupported like a cloud drifting in the sky
accustomed as he was to flying with saphira the sight of nothing but thin air underneath his feet still caused him unease
by manipulating the flow of magic eragon quickly descended from the ra zac is lair which the insubstantial wall of stone once again hid to the ledge
his boot slipped on a loose piece of rock as he alighted
for a handful of breathless seconds he flailed searching for solid footing but unable to look down as tilting his head could send him toppling forward
he yelped as his left leg went off the ledge and he began to fall
before he could resort to magic to save himself he came to an abrupt halt as his left foot wedged itself in a crevice
the edges of the rift dug into his calf behind his greave but he did not mind for it held him in place
eragon leaned his back against helgrind using it to help him prop up sloan is limp body
that was not too bad he observed
the effort had cost him but not so much that he was unable to continue
i can do this he said
he gulped fresh air into his lungs waiting for his racing heart to slow he felt as if he had sprinted a score of yards while carrying sloan
i can do this
the approaching riders caught his eye again
they were noticeably closer than before and galloping across the dry land at a pace that worried him
it is a race between them and me he realized
i have to escape before they reach helgrind
there are sure to be magicians among them and i am in no fit condition to duel galbatorix is spellcasters
glancing over at sloan is face he said perhaps you can help me a bit eh it is the least you can do considering i am risking death and worse for ** the sleeping butcher rolled his head lost in the world of dreams
with a grunt eragon pushed himself off helgrind
again he said audr and again he became airborne
this time he relied upon sloan is strength meager as it was as well as his own
together they sank like two strange birds along helgrind is rugged flank toward another ledge whose width promised safe haven
in such a manner eragon orchestrated their downward climb
he did not proceed in a straight line but rather angled off to his right so that they curved around helgrind and the mass of blocky stone hid him and sloan from the horsemen