awake
knurla orik waits for ** he bowed again and scurried away
saphira jumped out of her cave landing next to eragon
zar roc was in her claws
she tilted her ** it
you are a rider and should bear a rider is sword
zar roc may have a bloody history but that should not shape your actions
forge a new history for it and carry it with pride
saphira snorted and a puff of smoke rose from her ** it eragon
if you wish to remain above the forces here do not let anyone is disapproval dictate your actions
as you wish he said reluctantly buckling on the sword
he clambered onto her back and saphira flew out of tronjheim
there was enough light in farthen dur now that the hazy mass of the crater walls five miles away in each direction was visible
while they spiraled down to the city mountain is base eragon told saphira about his meeting with angela
as soon as they landed by one of tronjheim is gates orik ran to saphira is side
my king hrothgar wishes to see both of you
dismount quickly
we must hurry
eragon trotted after the dwarf into tronjheim
saphira easily kept pace beside them
ignoring stares from people within the soaring corridor eragon asked where will we meet hrothgar
without slowing orik said in the throne room beneath the city
it will be a private audience as an act of otho of ** you do not have to address him in any special manner but speak to him respectfully
hrothgar is quick to anger but he is wise and sees keenly into the minds of men so think carefully before you speak
once they entered tronjheim is central chamber orik led the way to one of the two descending stairways that flanked the opposite hall
they started down the right hand staircase which gently curved inward until it faced the direction they had come from
the other stairway merged with theirs to form a broad cascade of dimly lit steps that ended after a hundred feet before two granite doors
a seven pointed crown was carved across both doors
seven dwarves stood guard on each side of the portal
they held burnished mattocks and wore gem encrusted belts
as eragon orik and saphira approached the dwarves pounded the floor with the mattocks hafts
a deep boom rolled back up the stairs
the doors swung inward
a dark hall lay before them a good bowshot long
the throne room was a natural cave the walls were lined with stalagmites and stalactites each thicker than a man
sparsely hung lanterns cast a moody light
the brown floor was smooth and polished
at the far end of the hall was a black throne with a motionless figure upon it
orik bowed
the king awaits ** eragon put his hand on saphira is side and the two of them continued forward
the doors closed behind them leaving them alone in the dim throne room with the king
their footsteps echoed through the hall as they advanced toward the throne
in the recesses between the stalagmites and stalactites rested large statues
each sculpture depicted a dwarf king crowned and sitting on a throne their sightless eyes gazed sternly into the distance their lined faces set in fierce expressions
a name was chiseled in runes beneath each set of feet
eragon and saphira strode solemnly between the two rows of long dead monarchs
they passed more than forty statues then only dark and empty alcoves awaiting future kings
they stopped before hrothgar at the end of the hall
the dwarf king himself sat like a statue upon a raised throne carved from a single piece of black marble
it was blocky unadorned and cut with unyielding precision
strength emanated from the throne strength that harked back to ancient times when dwarves had ruled in alagaesia without opposition from elves or humans
a gold helm lined with rubies and diamonds rested on hrothgar is head in place of a crown
his visage was grim weathered and hewn of many years experience
beneath a craggy brow glinted deep set eyes flinty and piercing
over his powerful chest rippled a shirt of mail
his white beard was tucked under his belt and in his lap he held a mighty war hammer with the symbol of orik is clan embossed on its head
eragon bowed awkwardly and knelt