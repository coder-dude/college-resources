going to the stream by the house they quickly disrobed
eragon surreptitiously watched the elf curious as to what he looked like without his clothes
oromis was very thin yet his muscles were perfectly defined etched under his skin with the hard lines of a woodcut
no hair grew upon his chest or legs not even around his groin
his body seemed almost freakish to eragon compared to the men he was used to seeing in carvahall although it had a certain refined elegance to it like that of a wildcat
when they were clean oromis took eragon deep into du weldenvarden to a hollow where the dark trees leaned inward obscuring the sky behind branches and veils of snarled lichen
their feet sank into the moss above their ankles
all was silent about them
pointing to a white stump with a flat polished top three yards across that rested in the center of the hollow oromis said sit ** eragon did as he was told
cross your legs and close your ** the world went dark around him
from his right he heard oromis whisper open your mind eragon
open your mind and listen to the world around you to the thoughts of every being in this glade from the ants in the trees to the worms in the ground
listen until you can hear them all and you understand their purpose and nature
listen and when you hear no more come tell me what you have learned
unsure if oromis had left eragon tentatively lowered the barriers around his mind and reached out with his consciousness like he did when trying to contact saphira at a great distance
initially only a void surrounded him but then pricks of light and warmth began to appear in the darkness strengthening until he sat in the midst of a galaxy of swirling constellations each bright point representing a life
whenever he had contacted other beings with his mind like cadoc snowfire or solembum the focus had always been on the one he wanted to communicate with
but this
this was as if he had been standing deaf in the midst of a crowd and now he could hear the rivers of conversation whirling around him
he felt suddenly vulnerable he was completely exposed to the world
anyone or anything that might want to leap into his mind and control him could now do so
he tensed unconsciously withdrawing back into himself and his awareness of the hollow vanished
remembering one of oromis is lessons eragon slowed his breathing and monitored the sweep of his lungs until he had relaxed enough to reopen his mind
of all the lives he could sense the majority were by far insects
their sheer number astounded him
tens of thousands dwelled in a square foot of moss teeming millions throughout the rest of the small hollow and uncounted masses beyond
their abundance actually frightened eragon
he had always known that humans were scarce and beleaguered in alagaesia but he had never imagined that they were so outnumbered by evenbeetles
since they were one of the few insects that he was familiar with and oromis had mentioned them eragon concentrated his attention on the columns of red ants marching across the ground and up the stems of a wild rosebush
what he gleaned from them were not so much thoughts their brains were too primitive but urges the urge to find food and avoid injury the urge to defend one is territory the urge to mate
by examining the ants instincts he could begin to puzzle out their behavior
it fascinated him to discover that except for the few individuals exploring outside the borders of their province the ants knew exactly where they were going
he was unable to ascertain what mechanism guided them but they followed clearly defined paths from their nest to food and back
their source of food was another surprise
as he had expected the ants killed and scavenged other insects but most of their efforts were directed toward the cultivation of
ofsomething that dotted the rosebush
whatever the life form was it was barely large enough for him to sense
he focused all of his strength on it in an attempt to identify it and satisfy his curiosity
the answer was so simple he laughed out loud when he comprehended it aphids