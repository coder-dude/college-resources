fatigued and haggard but with triumphant smiles they sat around the fire congratulating each other
saphira crowed jubilantly which startled the horses
eragon stared at the flames
he was proud that they had covered roughly sixty leagues in five days
it was an impressive feat even for a rider able to change mounts regularly
i am outside of the ** was a strange thought
he had been born in the empire lived his entire life under galbatorix is rule lost his closest friends and family to the king is servants and had nearly died several times within his domain
now eragon was free
no more would he and saphira have to dodge soldiers avoid towns or hide who they were
it was a bittersweet realization for the cost had been the loss of his entire world
he looked at the stars in the gloaming sky
and though the thought of building a home in the safety of isolation appealed to him he had witnessed too many wrongs committed in galbatorix is name from murder to slavery to turn his back on the empire
no longer was it just vengeance for brom is death as well as garrow is that drove him
as a rider it was his duty to assist those without strength to resist galbatorix is oppression
with a sigh he abandoned his deliberation and observed the elf stretched out by saphira
the fire is orange light gave her face a warm cast
smooth shadows flickered under her cheekbones
as he stared an idea slowly came to him
he could hear the thoughts of people and animals and communicate with them in that manner if he chose to but it was something he had done infrequently except with saphira
he always remembered brom is admonishment not to violate someone is mind unless absolutely necessary
save for the one time he had tried to probe murtagh is consciousness he had refrained from doing so
now however he wondered if it were possible to contact the elf in her comatose ** might be able to learn from her memories why she remains like this
but if she recovers would she forgive me for such an intrusion
whether she does or not i must try
she is been in this condition for almost a week
without speaking of his intentions to murtagh or saphira he knelt by the elf and placed his palm on her brow
eragon closed his eyes and extended a tendril of thought like a probing finger toward the elf is mind
he found it without difficulty
it was not fuzzy and filled with pain as he had anticipated but lucid and clear like a note from a crystal bell
suddenly an icy dagger drove into his mind
pain exploded behind his eyes with splashes of color
he recoiled from the attack but found himself held in an iron grip unable to retreat
eragon fought as hard as he could and used every defense he could think of
the dagger stabbed into his mind again
he frantically threw his own barriers before it blunting the attack
the pain was less excruciating than the first time but it jarred his concentration
the elf took the opportunity to ruthlessly crush his defenses
a stifling blanket pressed down on eragon from all directions smothering his thoughts
the overpowering force slowly contracted squeezing the life out of him bit by bit though he held on unwilling to give up
the elf tightened her relentless grip even more so as to extinguish him like a snuffed candle
he desperately cried in the ancient language eka ai fricai un shur ** i am a rider and ** the deadly embrace did not loosen its hold but its constriction halted and surprise emanated from her
suspicion followed a second later but he knew she would believe him he could not have lied in the ancient language
however while he had said he was a friend that did not mean he meant her no harm
for all she knew eragon believed himself to be her friend making the statement true for him thoughshe might not consider him ** ancient language does have its limitations thought eragon hoping that the elf would be curious enough to risk freeing him
she was
the pressure lifted and the barriers around her mind hesitantly lowered
the elf warily let their thoughts touch like two wild animals meeting for the first time
a cold shiver ran down eragon is side