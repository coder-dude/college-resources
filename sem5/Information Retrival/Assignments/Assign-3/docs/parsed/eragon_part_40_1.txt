when morning came eragon is cheek was raw from chafing against snowfire is neck and he was sore from his fight with murtagh
they had alternated sleeping in their saddles throughout the night
it had allowed them to outdistance the urgal troops but neither of them knew if the lead could be retained
the horses were exhausted to the point of stopping yet they still maintained a relentless pace
whether it would be enough to escape depended on how rested the monsters were
and if eragon and murtagh is horses survived
the beor mountains cast great shadows over the land stealing the sun is warmth
to the north was the hadarac desert a thin white band as bright as noonday snow
i must eat said ** have passed since i last hunted
hunger claws my belly
if i start now i might be able to catch enough of those bounding deer for a few mouthfuls
eragon smiled at her ** if you must but leave arya here
i will be ** untied the elf from her belly and transferred her to snowfire is saddle
saphira soared away disappearing in the direction of the mountains
eragon ran beside the horses close enough to snowfire to keep arya from falling
neither he nor murtagh intruded on the silence
yesterday is fight no longer seemed as important because of the urgals but the bruises remained
saphira made her kills within the hour and notified eragon of her success
eragon was pleased that she would soon return
her absence made him nervous
they stopped at a pond to let the horses drink
eragon idly plucked a stalk of grass twirling it while he stared at the elf
he was startled from his reverie by the steely rasp of a sword being unsheathed
he instinctively grasped zar roc and spun around in search of the enemy
there was only murtagh his long sword held ready
he pointed at a hill ahead of them where a tall brown cloaked man sat on a sorrel horse mace in hand
behind him was a group of twenty horsemen
no one moved
could they be varden asked murtagh
eragon surreptitiously strung his bow
according to arya they re still scores of leagues away
this might be one of their patrols or raiding groups
assuming they re not ** murtagh swung onto tornac and readied his own bow
should we try to outrun them asked eragon draping a blanket over arya
the horsemen must have seen her but he hoped to conceal the fact that she was an elf
it would not do any good said murtagh shaking his head
tornac and snowfire are fine war horses but they re tired and they are not sprinters
look at the horses those men have they re meant for running
they would catch us before we had gone a half mile
besides they may have something important to say
you d better tell saphira to hurry back
eragon was already doing that
he explained the situation then warned do not show yourself unless it is necessary
we re not in the empire but i still do not want anyone to know about you
never mind that she ** magic can protect you where speed and luck fail
he felt her take off and race toward them skimming close to the ground
the band of men watched them from the hill
eragon nervously gripped zar roc
the wire wrapped hilt was secure under his glove
he said in a low voice if they threaten us i can frighten them away with magic
if that does not work there is saphira
i wonder how they d react to a rider so many stories have been told about their powers
it might be enough to avoid a fight
do not count on it said murtagh flatly
if there is a fight we will just have to kill enough of them to convince them we re not worth the ** his face was controlled and unemotional
the man on the sorrel horse signaled with his mace sending the horsemen cantering toward them
the men shook javelins over their heads whooping loudly as they neared
battered sheaths hung from their sides
their weapons were rusty and stained
four of them trained arrows on eragon and murtagh
their leader swirled the mace in the air and his men responded with yells as they wildly encircled eragon and murtagh
eragon is lips twitched
he almost loosed a blast of magic into their midst then restrained ** do not know what they want yet he reminded himself containing his growing apprehension