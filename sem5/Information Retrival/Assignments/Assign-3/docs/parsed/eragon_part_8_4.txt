its growth was explosive it would soon be safe from most dangers
the dragon doubled in size in the first week
four days later it was as high as his knee
it no longer fit inside the hut in the rowan so eragon was forced to build a hidden shelter on the ground
the task took him three days
when the dragon was a fortnight old eragon was compelled to let it roam free because it needed so much food
the first time he untied it only the force of his will kept it from following him back to the farm
every time it tried he pushed it away with his mind until it learned to avoid the house and its other inhabitants
and he impressed on the dragon the importance of hunting only in the spine where there was less chance of being seen
farmers would notice if game started disappearing from palancar valley
it made him feel both safer and uneasy when the dragon was so far away
the mental contact he shared with the dragon waxed stronger each day
he found that although it did not comprehend words he could communicate with it through images or emotions
it was an imprecise method however and he was often misunderstood
the range at which they could touch each other is thoughts expanded rapidly
soon eragon could contact the dragon anywhere within three leagues
he often did so and the dragon in turn would lightly brush against his mind
these mute conversations filled his working hours
there was always a small part of him connected to the dragon ignored at times but never forgotten
when he talked with people the contact was distracting like a fly buzzing in his ear
as the dragon matured its squeaks deepened to a roar and the humming became a low rumble yet the dragon did not breathe fire which concerned him
he had seen it blow smoke when it was upset but there was never a hint of flame
when the month ended eragon is elbow was level with the dragon is shoulder
in that brief span it had transformed from a small weak animal into a powerful beast
its hard scales were as tough as chain mail armor its teeth like daggers
eragon took long walks in the evening with the dragon padding beside him
when they found a clearing he would settle against a tree and watch the dragon soar through the air
he loved to see it fly and regretted that it was not yet big enough to ride
he often sat beside the dragon and rubbed its neck feeling sinews and corded muscles flex under his hands
despite eragon is efforts the forest around the farm filled with signs of the dragon is existence
it was impossible to erase all the huge four clawed footprints sunk deep in the snow and he refused even to try to hide the giant dung heaps that were becoming far too common
the dragon had rubbed against trees stripping off the bark and had sharpened its claws on dead logs leaving gashes inches deep
if garrow or roran went too far beyond the farm is boundaries they would discover the dragon
eragon could imagine no worse way for the truth to come out so he decided to preempt it by explaining everything to them
he wanted to do two things first though give the dragon a suitable name and learn more about dragons in general
to that end he needed to talk with brom master of epics and legends the only places where dragonlore survived
so when roran went to get a chisel repaired in carvahall eragon volunteered to go with him
the evening before they left eragon went to a small clearing in the forest and called the dragon with his mind
after a moment he saw a fast moving speck in the dusky sky
the dragon dived toward him pulled up sharply then leveled off above the trees
he heard a low pitched whistle as air rushed over its wings
it banked slowly to his left and spiraled gently down to the ground
the dragon back flapped for balance with a deep muffledthwump as it landed
eragon opened his mind still uncomfortable with the strange sensation and told the dragon that he was leaving
it snorted with unease
he attempted to soothe it with a calming mental picture but the dragon whipped its tail unsatisfied
he rested his hand on its shoulder and tried to radiate peace and serenity