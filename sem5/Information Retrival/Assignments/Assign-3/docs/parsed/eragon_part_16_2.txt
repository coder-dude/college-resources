take a few steps she commanded then dryly observed at least you wo not have to crawl there
outside a blustery wind blew smoke from the adjacent buildings into their faces
storm clouds hid the spine and covered the valley while a curtain of snow advanced toward the village obscuring the foothills
eragon leaned heavily on gertrude as they made their way through carvahall
horst had built his two story house on a hill so he could enjoy a view of the mountains
he had lavished all of his skill on it
the shale roof shadowed a railed balcony that extended from a tall window on the second floor
each water spout was a snarling gargoyle and every window and door was framed by carvings of serpents harts ravens and knotted vines
the door was opened by elain horst is wife a small willowy woman with refined features and silky blond hair pinned into a bun
her dress was demure and neat and her movements graceful
please come in she said softly
they stepped over the threshold into a large well lit room
a staircase with a polished balustrade curved down to the floor
the walls were the color of honey
elain gave eragon a sad smile but addressed gertrude
i was just about to send for you
he is not doing well
you should see him right away
elain you will have to help eragon up the stairs gertrude said then hurried up them two at a time
it is okay i can do it myself
are you sure asked elain
he nodded but she looked doubtful
well
as soon as you re done come visit me in the kitchen
i have a fresh baked pie you might ** as soon as she left he sagged against the wall welcoming the support
then he started up the stairs one painful step at a time
when he reached the top he looked down a long hallway dotted with doors
the last one was open slightly
taking a breath he lurched toward it
katrina stood by a fireplace boiling rags
she looked up murmured a condolence and then returned to her work
gertrude stood beside her grinding herbs for a poultice
a bucket by her feet held snow melting into ice water
garrow lay on a bed piled high with blankets
sweat covered his brow and his eyeballs flickered blindly under their lids
the skin on his face was shrunken like a cadaver is
he was still save for subtle tremors from his shallow breathing
eragon touched his uncle is forehead with a feeling of unreality
it burned against his hand
he apprehensively lifted the edge of the blankets and saw that garrow is many wounds were bound with strips of cloth
where the bandages were being changed the burns were exposed to the air
they had not begun to heal
eragon looked at gertrude with hopeless eyes
ca not you do anything about these
she pressed a rag into the bucket of ice water then draped the cool cloth over garrow is head
i ve tried everything salves poultices tinctures but nothing works
if the wounds closed he would have a better chance
still things may turn for the better
he is hardy and strong
eragon moved to a corner and sank to the ** is not the way things are supposed to ** silence swallowed his thoughts
he stared blankly at the bed
after a while he noticed katrina kneeling beside him
she put an arm around him
when he did not respond she diffidently left
sometime later the door opened and horst came in
he talked to gertrude in a low voice then approached eragon
come on
you need to get out of ** before eragon could protest horst dragged him to his feet and shepherded him out the door
i want to stay he complained
you need a break and fresh air
do not worry you can go back soon enough consoled horst
eragon grudgingly let the smith help him downstairs into the kitchen
heady smells from half a dozen dishes rich with spices and herbs filled the air
albriech and baldor were there talking with their mother as she kneaded bread
the brothers fell silent as they saw eragon but he had heard enough to know that they were discussing garrow
here sit down said horst offering a chair
eragon sank into it gratefully
thank ** his hands were shaking slightly so he clasped them in his lap
a plate piled high with food was set before him
you do not have to eat said elain but it is there if you ** she returned to her cooking as he picked up a fork