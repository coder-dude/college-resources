eragon thought it over
all right he grumbled we will get horses
but you have to buy them
i do not have any money and i do not want to steal again
it is wrong
that depends on your point of view corrected brom with a slight smile
before you set out on this venture remember that your enemies the ra zac are the king is servants
they will be protected wherever they go
laws do not stop them
in cities they will have access to abundant resources and willing servants
also keep in mind that nothing is more important to galbatorix than recruiting or killing you though word of your existence probably has not reached him yet
the longer you evade the ra zac the more desperate he will become
he will know that every day you will be growing stronger and that each passing moment will give you another chance to join his enemies
you must be very careful as you may easily turn from the hunter into the hunted
eragon was subdued by the strong words
pensive he rolled a twig between his fingers
enough talk said brom
it is late and my bones ache
we can say more ** eragon nodded and banked the fire