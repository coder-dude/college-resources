so if you see enemies racing after you from a league away let them approach before using magic
now back to ** try to lift the pebble again
again asked eragon weakly thinking of the effort it had taken to do it just once
** and this time be quicker about it
they continued with the exercises throughout most of the day
when eragon finally stopped he was tired and ill tempered
in those hours he had come to hate the pebble and everything about it
he started to throw it away but brom said do not
keep ** eragon glared at him then reluctantly tucked the stone into a pocket
we re not done yet warned brom so do not get ** he pointed at a small plant
this is calleddelois ** from there on he instructed eragon in the ancient language giving him words to memorize fromvondr a thin straight stick to the morning star aiedail
that evening they sparred around the fire
though brom fought with his left hand his skill was undiminished
the days followed the same pattern
first eragon struggled to learn the ancient words and to manipulate the pebble
then in the evening he trained against brom with the fake swords
eragon was in constant discomfort but he gradually began to change almost without noticing
soon the pebble no longer wobbled when he lifted it
he mastered the first exercises brom gave him and undertook harder ones and his knowledge of the ancient language grew
in their sparring eragon gained confidence and speed striking like a snake
his blows became heavier and his arm no longer trembled when he warded off attacks
the clashes lasted longer as he learned how to fend off brom
now when they went to sleep eragon was not the only one with bruises
saphira continued to grow as well but more slowly than before
her extended flights along with periodic hunts kept her fit and healthy
she was taller than the horses now and much longer
because of her size and the way her scales sparkled she was altogether too visible
brom and eragon worried about it but they could not convince her to allow dirt to obscure her scintillating hide
they continued south tracking the ra zac
it frustrated eragon that no matter how fast they went the ra zac always stayed a few days ahead of them
at times he was ready to give up but then they would find some mark or print that would renew his hope
there were no signs of habitation along the ninor or in the plains leaving the three companions undisturbed as the days slipped by
finally they neared daret the first village since yazuac
the night before they reached the village eragon is dreams were especially vivid
he saw garrow and roran at home sitting in the destroyed kitchen
they asked him for help rebuilding the farm but he only shook his head with a pang of longing in his heart
i am tracking your killers he whispered to his uncle
garrow looked at him askance and demanded do i look dead to you
i can not help you said eragon softly feeling tears in his eyes
there was a sudden roar and garrow transformed into the ra zac
then die they hissed and leapt at eragon
he woke up feeling ill and watched the stars slowly turn in the sky
all will be well little one said saphira gently