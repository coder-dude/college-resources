throwing open the doors to her rooms nasuada strode to her desk then dropped into a chair blind to her surroundings
her spine was so rigid that her shoulders did not touch the back
she felt frozen by the insoluble quandary the varden faced
the rise and fall of her chest slowed until it was ** have failed was all she could think
jolted from her reverie nasuada looked down to find farica beating at her right arm with a cleaning rag
a wisp of smoke rose from the embroidered sleeve
alarmed nasuada pushed herself out of the chair and twisted her arm trying to find the cause of the smoke
her sleeve and skirt were disintegrating into chalky cobwebs that emitted acrid fumes
get me out of this she said
she held her contaminated arm away from her body and forced herself to remain still as farica unlaced her overgown
the handmaid is fingers scrabbled against nasuada is back with frantic haste fumbling with the knots and then finally loosening the wool shell that encased nasuada is torso
as soon as the overgown sagged nasuada yanked her arms out of the sleeves and clawed her way free of the robe
panting she stood by the desk clad only in her slippers and linen chemise
to her relief the expensive chainsil had escaped harm although it had acquired a foul reek
did it burn you asked farica
nasuada shook her head not trusting her tongue to respond
farica nudged the overgown with the tip of her shoe
what evil is this
one of orrin is foul concoctions croaked nasuada
i spilled it in his ** calming herself with long breaths she examined the ruined gown with dismay
it had been woven by the dwarf women of durgrimst ingeitum as a gift for her last birthday and was one of the finest pieces in her wardrobe
she had nothing to replace it nor could she justify commissioning a new dress considering the varden is financial ** i will have to make do without
farica shook her head
it is a shame to lose such a pretty ** she went round the desk to a sewing basket and returned with a pair of etched scissors
we might as well save as much of the cloth as we can
i will cut off the ruined parts and have them burned
nasuada scowled and paced the length of the room seething with anger at her own clumsiness and at having another problem added to her already overwhelming list of worries
what am i going to wear to court now she demanded
the scissors bit into the soft wool with brisk authority
mayhap your linen dress
it is too casual to appear in before orrin and his nobles
give me a chance with it ma am
i am sure that i can alter it so it is serviceable
by the time i am done it will look twice as grand as this one ever did
no no
it wo not work
they will just laugh at me
it is hard enough to command their respect when i am dressed properly much less if i am wearing patched gowns that advertise our poverty
the older woman fixed nasuada with a stern gaze
itwill work so long as you do not apologize for your appearance
not only that i guarantee that the other ladies will be so taken with your new fashion that they will imitate you
just you wait and ** going to the door she cracked it open and handed the damaged fabric to one of the guards outside
your mistress wants this burned
do it in secret and breathe not a word of this to another soul or you will have me to answer ** the guard saluted
nasuada could not help smiling
how would i function without you farica
after donning her green hunting frock which with its light skirt provided some respite from the day is heat nasuada decided that even though she was ill disposed toward orrin she would take his advice and break with her regular schedule to do nothing more important than help farica rip out stitches from the overgown
she found the repetitive task an excellent way to focus her thoughts
while she pulled on the threads she discussed the varden is predicament with farica in the hope that she might perceive a solution that had escaped nasuada
in the end farica is only assistance was to observe seems most matters in this world have their root in gold
if we had enough of it we could buy galbatorix right off his black throne