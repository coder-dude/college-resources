exhausted by his efforts eragon placed a hand over the belt of beloth the wise and bolstered his flagging strength with some of the energy he had stored within the twelve diamonds hidden inside the belt
he offered the rest of it to saphira who was equally tired but she declined saying keep it for yourself
you have not that much left
besides what i really need is a meal and a full night is sleep
eragon leaned against her and allowed his eyelids to drift halfway closed
soon he said
soon this will all be over
among the warriors who streamed past was angela garbed in her strange flanged armor of green and black and carrying her huthvir the double bladed staff weapon of the dwarf priests
the herbalist paused next to eragon and with an impish expression said an impressive display but do not you think you re overdoing it a bit
what do you mean asked eragon frowning
she lifted an eyebrow
come now was it really necessary to set your sword on fire
eragon is expression cleared as he understood her objection
he laughed
not for the portcullis no but i enjoyed it
besides i can not help it
i named the sword fire in the ancient language and every time i say the word the blade flares up like a branch of dry wood in a bonfire
you named your sword fire angela exclaimed with a note of incredulity
fire what kind of a boring name is that you might as well name your sword blazing blade and be done with it
fire indeed
humph
would not you rather have a sword called sheepbiter or chrysanthemum cleaver or something else with imagination
i already have one sheepbiter here said eragon and laid a hand on saphira
why would i need another
angela broke out into a wide smile
so you re not entirely devoid of wit after ** there just might be hope for ** and she danced off toward the keep twirling her double bladed staff by her side and muttering fire **
a soft growl emanated from saphira and she said be careful whom you call sheepbiter eragon or you might get bitten yourself