Eragon bolted upright, throwing off his blankets as easily as he cast off his waking dreams. His arms and shoulders were sore from his exertions of the previous day. He pulled on his boots, fumbling with the laces in his excitement, grabbed his grimy apron from the floor, and bounded down the elaborately carved stairs to the entryway of Rhunön’s curved house.
Outside, the sky was bright with the first light of dawn, although shadow still enveloped the atrium. Eragon spotted Rhunön and Saphira by the open-walled forge and trotted over to them, combing his hair into place with his fingers.
Rhunön stood leaning against the edge of the bench. There were dark bags under her eyes, and the lines on her face were heavier than before.
The sword lay before her, concealed beneath a length of white cloth.
“I have done the impossible,” she said, the words hoarse and broken. “I made a sword when I swore I would not. What is more, I made it in less than a day and with hands that were not my own. Yet the sword is not crude or shoddy. No! It is the finest sword I have ever forged. I would have preferred to use less magic during the process, but that is my only qualm, and it is a small one compared with the perfection of the results. Behold!”
Grasping the corner of the cloth, Rhunön pulled it aside, revealing the sword.
He had thought that in the handful of hours since he had left her, Rhunön would only have had enough time to fabricate a plain hilt and crossguard for the sword, and maybe a simple wooden scabbard. Instead, the sword Eragon saw on the bench was as magnificent as Zar’roc, Naegling, and Támerlein and, in his opinion, more beautiful than any of them.
Covering the blade was a glossy scabbard of the same dark blue as the scales on Saphira’s back. The color displayed a slight variegation, like the mottled light at the bottom of a clear forest pond. A piece of blued brightsteel carved in the shape of a leaf capped the end of the scabbard while a collar decorated with stylized vines encircled the mouth. The curved crossguard was also made of blued brightsteel, as were the four ribs that held in place the large sapphire that formed the pommel. The hand-and-a-half hilt was made of hard black wood.
Overcome by a sense of reverence, Eragon reached out toward the sword, then paused and glanced at Rhunön. “May I?” he asked.
She inclined her head. “You may. I give it to thee, Shadeslayer.”
Eragon lifted the sword from the bench. The scabbard and the wood of the hilt were cool to the touch. For several minutes, he marveled at the details on the scabbard and the guard and the pommel. Then he tightened his grip around the hilt and unsheathed the blade.
Like the rest of the sword, the blade was blue, but of a slightly lighter shade; it was the blue of the scales in the hollow of Saphira’s throat rather than the blue of those on her back. And as it was on Zar’roc, the color was iridescent; as Eragon moved the sword about, the color would shimmer and shift, displaying any of the many tones of blue present on Saphira herself. Through the wash of color, the cable-like patterns within the brightsteel and the pale bands along the edges were still visible.
With a single hand, Eragon swung the sword through the air, and he laughed at how light and fast it felt. The sword almost seemed alive. He grasped the sword with both his hands then and was delighted to find that they fit perfectly on the longer hilt. Lunging forward, he stabbed at an imaginary enemy and was confident they would have died from the attack.
“Here,” said Rhunön, and pointed at a bundle of three iron rods planted upright in the ground outside the forge. “Try it on those.”
Eragon allowed himself a moment to focus his thoughts, then took a single step toward the rods. With a yell, he slashed downward and cut through all three rods. The blade emitted a single pure note that slowly faded into silence. When Eragon examined the edge where it had struck the iron, he saw that the impact had not damaged it in the slightest.
“Are you well pleased, Dragon Rider?” Rhunön asked.
“More than pleased, Rhunön-elda,” said Eragon, and bowed to her. “I do not know how I can thank you for such a gift.”
“You may thank me by killing Galbatorix. If there is any sword destined to slay that mad king, it is this one.”
“I shall try my hardest, Rhunön-elda.”
The elf woman nodded, appearing satisfied. “Well, you finally have a sword of your own, which is as it ought to be. Now you are truly a Dragon Rider!”
“Yes,” said Eragon, and held the sword up toward the sky, admiring it. “Now I am truly a Rider.”
“Before you leave, one last thing remains for you to do,” said Rhunön.
She flicked a finger toward the sword. “You must name it so I can mark the blade and scabbard with the appropriate glyph.”
Eragon walked over to Saphira and said, What do you think?
    I am not the one who must carry the blade. Name it as you see fit.   
    Yes, but you must have some ideas!   
She lowered her head toward him and sniffed at the sword, then said, Blue-gem-tooth is what I would name it. Or Blue-claw-red.
    That would sound ridiculous to humans.   
    Then what of Reaver or Gutripper? Or maybe Battleclaw or Glitter-thorn or Limbhacker? You could name it Terror or Pain or Armbiter or Eversharp or Ripplescale: that on account of the lines in the steel. There is also Tongue of Death and Elfsteel and Starmetal and many others besides.   
Her sudden outpouring surprised Eragon. You have a talent for this, he said.
    Inventing random names is easy. Inventing the right name, however, can try the patience of even an elf.   
    And what if we actually kill Galbatorix? What, then? Will you do nothing else of worth with your sword?   
    Mmh. Placing the sword alongside Saphira’s left foreleg, Eragon said, It’s exactly the same color as you. . . . I could name it after you.
A low growl sounded in Saphira’s chest. No.
He fought back a smile. Are you sure? Just imagine if we were in battle and—
Her claws sank into the earth. No. I am not a thing for you to wave about and make fun of.
    No, you’re right. I’m sorry. . . . Well, what if I named it Hope in the ancient language? Zar’roc means “misery,” so wouldn’t it be fitting if I were to wield a sword that by its very name would counteract misery?
    A noble sentiment, said Saphira. But do you really want to give your enemies hope? Do you want to stab Galbatorix with hope?
    It’s an amusing pun, he said, chuckling.
Stymied, Eragon grimaced and rubbed his chin, studying the play of light across the glittering blade. As he gazed into the depths of the steel, his eye chanced upon the flamelike pattern that marked the transition between the softer steel of the spine and that of the edges, and he recalled the word Brom had used to light his pipe during the memory Saphira had shared with him. Then Eragon thought of Yazuac, where he had first used magic, and also of his duel with Durza in Farthen Dûr, and in that instant he knew without doubt that he had found the right name for his sword.
Eragon consulted with Saphira, and when she agreed with his choice, he lifted the weapon to shoulder level and said, “I am decided. Sword, I name thee Brisingr!”
And with a sound like rushing wind, the blade burst into fire, an envelope of sapphire-blue flames writhing about the razor-sharp steel.
Uttering a startled cry, Eragon dropped the sword and jumped back, afraid of being burned. The blade continued to blaze on the ground, the translucent flames charring a nearby clump of grass. It was then that Eragon realized it was he who was providing the energy to sustain the unnatural fire. He quickly ended the magic, and the fire vanished from the sword. Puzzled by how he could have cast a spell without intending to, he picked up the sword again and tapped the blade with the tip of a finger. It was no hotter than before.
A heavy scowl on her brow, Rhunön stalked forward, seized the sword from Eragon, and examined it from tip to pommel. “You are fortunate I have already protected it with wards against heat and damage, else you would have just scratched the guard and destroyed the temper of the blade. Do not drop the sword again, Shadeslayer—even if it should turn into a snake—or else I shall take it back and give you a worn-out hammer instead.” Eragon apologized, and appearing somewhat mollified, Rhunön handed the sword back to him. “Did you set fire to it on purpose?” she asked.
“No,” said Eragon, unable to explain what had happened.
“The name, the name, say it again.”
Holding the sword as far away from his body as he could, Eragon exclaimed, “Brisingr!”
A column of flickering flames engulfed the blade of the sword, the heat warming Eragon’s face. This time Eragon noticed the slight drain on his strength from the spell. After a few moments, he extinguished the smokeless fire.
Once more Eragon exclaimed, “Brisingr!” And once more the blade shimmered with blue, wraithlike tongues of flame.
    Now there is a fitting sword for a Rider and dragon! said Saphira in a delighted tone. It breathes fire as easily as I do.
“But I wasn’t trying to cast a spell!” protested Eragon. “All I did was say Brisingr and—” He yelped and swore as the sword again caught fire, which he put out for the fourth time.
“May I?” asked Rhunön, extending a hand toward Eragon. He gave her the sword, and she too said, “Brisingr!” A shiver seemed to run down the blade, but other than that, it remained inanimate. Her expression contemplative, Rhunön returned the sword to Eragon and said, “I can think of two explanations for this marvel. One is that because you were involved with the forging, you imbued the blade with a portion of your personality and therefore it has become attuned to your wishes. My other explanation is that you have discovered the true name of your sword. Perhaps both those things are what has happened. In any event, you have chosen well, Shadeslayer. Brisingr! Yes, I like it. It is a good name for a sword.”
    A very good name, Saphira agreed.
Then Rhunön placed her hand over the middle of Brisingr’s blade and murmured an inaudible spell. The Elvish glyph for fire appeared upon both sides of the blade. She did the same to the front of the scabbard.
Again Eragon bowed to the elf woman, and both he and Saphira expressed their gratitude to her. A smile appeared on Rhunön’s aged face, and she touched each of them upon their brows with her callused thumb. “I am glad I was able to help the Riders this once more. Go, Shadeslayer. Go, Brightscales. Return to the Varden, and may your enemies flee with fear when they see the sword you now wield.”
So Eragon and Saphira bade her farewell, and together they departed Rhunön’s house, Eragon cradling Brisingr in his arms as he would a newborn child.