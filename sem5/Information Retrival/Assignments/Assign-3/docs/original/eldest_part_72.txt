 Text copyright © 2005 by Christopher Paolini
 Illustrations on endpapers, copyright © 2002  by Christopher Paolini
 All rights reserved under International and Pan-American  Copyright Conventions. Published in the United States by Alfred A. Knopf, an  imprint of Random House Children’s Books, a division of Random House, Inc., New  York, and simultaneously in Canada by Random House of Canada Limited, Toronto.  Distributed by Random House, Inc., New York.
 KNOPF,BORZOI BOOKS , and the colophon are  registered trademarks of Random House, Inc.
 Eldest / Christopher Paolini. — 1st ed.
 p. cm. — (Inheritance ; bk. 2)
 SUMMARY: After successfully evading an Urgal  ambush, Eragon is adopted into the Ingeitum clan and sent to finish his  training so he can further help the Varden in their struggle against the  Empire.
 [1. Fantasy. 2. Dragons—Fiction. 3. Youths’  writings.] I. Title. II. Series: Paolini, Christopher. Inheritance ; bk. 2.
As always, this book is for my  family.
And also to my incredible fans.  You made this adventure possible.