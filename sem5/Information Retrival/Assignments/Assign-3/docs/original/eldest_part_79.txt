 Akh sartos oen dûrgrimst!—For family and clan!
 Astim Hefthyn—Sight Guard (inscription on a  necklace given to Eragon)
 Az Sweldn rak Anhûin—The Tears of Anhûin
 Azt jok jordn rast.—Then you may pass.
 barzûl—to curse someone with ill fate
 barzûln—to curse someone with multiple  misfortunes
 Feldûnost—frostbeard (a species of goat native  to the Beor Mountains)
 Formv Hrethcarach . . . formv Jurgencarmeitder  nos eta goroth bahst Tarnag, dûr encesti rak kythn! Jok is warrev az barzûlegûr  dûr dûrgrimst, Az Sweldn rak Anhûin, môgh tor rak Jurgenvren? Né ûdim etal os  rast knurlag. Knurlag ana . . .—This Shadeslayer . . . this Dragon Rider has no  place in Tarnag, our holiest of cities! Do you forget the curse our clan, The  Tears of Anhûin, bears from the Dragon War? We will not let him pass. He is . .  .
 Hert dûrgrimst? Fild rastn?—What clan? Who  passes?
 hûthvir—double-bladed staff weapon used by  Dûrgrimst Quan
 IIf gauhnith.—A peculiar dwarf expression that  means “It is safe and good.” Commonly uttered by the host of a meal, it is a  holdover from days when poisoning of guests was prevalent among the clans.
 Jok is frekk dûrgrimstvren?—Do you want a clan  war?
 Knurlag qana qirânû Dûrgrimst Ingeitum! Qarzûl  ana Hrothgar oen volfild—He was made a member of Clan Ingeitum! Cursed is  Hrothgar and all who—
 Nagra—giant boar, native to the Beor Mountains
 Orik Thrifkz menthiv oen Hrethcarach Eragon  rak Dûrgrimst Ingeitum. Wharn, az vanyali-carharûg Arya. Né oc Ûndinz  grimstbelardn.—Orik, Thrifk’s son, and Shadeslayer Eragon of Clan Ingeitum.  Also, the elf-courier Arya. We are Ûndin’s hall-guests.
 Os il dom qirânû carn dûr thargen, zeitmen,  oen grimst vor formv edaris rak skilfz. Narho is belgond . . .—Let our flesh,  honor, and hall be made as one by this blood of mine. I do pledge . . .
 Shrrg—giant wolf, native to the Beor Mountains
 vanyali—elf (The dwarves borrowed this word  from the ancient language, wherein it meansmagic. )
 werg—an exclamation of disgust (the dwarves’  equivalent ofugh )