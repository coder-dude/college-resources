 Atra esterní ono thelduin/Mor’ranr lífa unin  hjarta onr/Un du evarínya ono varda.—May good fortune rule over you/Peace live  in your heart/And the stars watch over you.
 Atra guliä un ilian tauthr ono un atra ono  waíse skölir fra rauthr.—May luck and happiness follow you and may you be a  shield from misfortune.
 Atra nosu waíse vardo fra eld hórnya.—Let us  be warded from listeners.
 Brakka du vanyalí sem huildar Saphira un  eka!—Reduce the magic that holds Saphira and me!
 Eka fricai un Shur’tugal.—I am a Rider and  friend.
 elda—a gender-neutral honorific of great  praise
 fairth—a picture taken by magical means
 finiarel—an honorific for a young man of great  promise
 Fricai Andlát—death friend (a poisonous  mushroom)
 Gala O Wyrda brunhvitr/Abr Berundal  vandr-fódhr/Burthro laufsblädar ekar undir/Eom kona dauthleikr . . .—Sing O  white-browed Fate/Of ill-marked Berundal/Born under oaken leaves/To mortal  woman . . .
 Gath sem oro un lam iet.—Unite that arrow with  my hand.
 lethrblaka—a bat; the Ra’zac’s mounts  (literally, leather-flapper)
 malthinae—to bind or hold in place; confine
 nalgask—a mixture of beeswax and hazelnut oil  used to moisten the skin
 Sé mor’ranr ono finna.—May you find peace.
 Sé onr sverdar sitja hvass!—May your swords  stay sharp!
 Sé orúm thornessa hávr sharjalví lífs.—May  this serpent have life’s movement.
 Skölir nosu fra brisingr!—Shield us from fire!
 Stydja unin mor’ranr, Hrothgar Könungr.—Rest  in peace, King Hrothgar.
 svit-kona—a formal honorific for an elf woman  of great wisdom
 Togira Ikonoka—the Cripple Who Is Whole
 Vel eïnradhin iet ai Shur’tugal.—Upon my word  as a Rider.
 vodhr—a male honorific of middling praise
 vor—a male honorific for a close friend