In Après Moi, Le Déluge, Davina is feeling the effects of the incomplete Harvest.
Containing the full power of the Harvest she begins to hemorrhage magic. Earth, Wind, Water, and Fire threaten to destroy the city.
She is still angry at Marcel because of the previous events of the episode. Rebekah comes in and makes her fall asleep by injecting a sedative into her body.
Sophie convinces everyone the only way to save the city is to finish the Harvest. Marcel initially refusing to allow Davina's sacrifice, took her to an unknown location.
Rebekah, with Thierry's help, found them and convinced them to complete the ritual. Before her death, Davina forgives Marcel because he saved her and accepts her fate, because she thinks that it would be selfish to take everyone else with her.
Her throat is later slitten by Sophie to complete the Harvest, but she does not come back. She is left to be mourned by who watched it.

In Dance Back from the Grave, Marcel was still mourning the loss of Davina.
He informs Cami of her death.

In Le Grand Guignol, Davina is resurrected through Céleste's death.

In Farewell to Storyville, Davina feels uncomfortable after her journey into death.
She said that unlike the three other girls who were sacrificed during the Harvest, the ancestors hated her because she escaped the Harvest and because of her alliance with Marcel and the vampires.
They said that they would torment her if she continued to use her magic against the witches. She is completely lost and doesn't know who she can trust. When Cami tries to talk to her, Davina tells her that nothing that she learned in books will help her.
She becomes angry when Marcel asks her to help Rebekah, believing that he is using her yet again. Finally, Marcel returns her to the witches so that Genevieve can lower the Boundary spell on the City of the Dead.

In Moon Over Bourbon Street, Davina is having a tough time fitting back into the witches' society.
She can't use her magic because she hasn't been trained by the dead witches and Monique reminds her constantly that she chose to side with the vampires and shouldn't be one of them.
She later meets Josh and tells him about it as well that she is regretting Marcel's decision to bring her back to the Coven. At Elijah's party, the werewolf Oliver flirts with her and is about to ask her to dance when Monique comes and steals him from her.
Later, she returns to the garden with Josh and he convinces her that what her family thinks doesn't matter, using the story of his first and only boyfriend and his family's reaction to demonstrate it.

Screenshot
He then helps her to make a dead rose from earlier alive again.
When Monique returns to the garden, Davina has returned all the garden's roses to life, showing that she once again has control over her magic.

In The Big Uneasy, Davina along with Abigail and Monique are seen chanting and she sees Monique channeling the Ancestors and she tells them it's time to kill Genevieve.
During the festival, after Abigail and Monique show their abilities to control the Elements, Davina sets off fireworks with the Ancestral Element of Fire, making the audience cheer loudly.
She also runs into Elijah and doesn't seem happy, as Klaus is still alive and Tim is dead forever. She is seen at the offering and is given no gifts, thanks to Genevieve and Monique's planning.
Monique continues to smile smugly at her, so she decides to leave. She sees Josh and is happy to see him there, but Klaus shows up and unwillingly drags him. Klaus defends Davina and says it's unfair that she didn't get anything.
He gives her a gift to win her over and after refusing it at first, Klaus says he will pardon Josh, so she accepts it. Davina opens it and discovers that it's a spell for daylight rings.

TheOriginals120-0531
Davina trying to contact Tim.

In A Closer Walk With Thee, Davina is seen with fellow harvest girls and Genevieve, who teaches them about the spiritual world.
Davina tries to communicate with Tim. So, she convinces Abigail to help her do it. The first attempt was unsuccessful. However, later that day, Mikael from the Other Side comes and tells her Tim has moved on and that he can help her take down Klaus if she can bring him back to life.