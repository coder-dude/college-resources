In Bloodletting, it is revealed that the baby's blood is capable of turning werewolves into hybrids.
However, the hybrids created are sired to its mother - likely due to it and her being one and the same for the time being.
Tyler Lockwood kidnaps Hayley and tries to kill the baby, as he believes that Klaus desires to use the baby to sire a second army of hybrids.
Klaus scares him off, however, Tyler then goes to Marcel, and informs him of the baby's existence, and that its abilities will prove a threat to the vampire kingdom.

In The River in Reverse, Klaus mentions how he believes that his child will grow up calling Elijah father because of the way Hayley adores him and how his siblings don't believe that his intentions toward his own child are pure.
He later states that his child is all that matters to him now.

In Reigning Pain in New Orleans, Klaus tells Marcel how the witches forced him to help them take down Marcel by threatening to kill his unborn child.
He goes on to mention how at the beginning he didn't care for his own child, but after remembering how Mikael despised him since birth he had a change of heart because he doesn't want to end up being exactly like his father was to him.
Elijah also reveals that descendants of Klaus' father are still alive , therefore meaning relatives of the child as well.

In Après Moi, Le Déluge, the baby becomes part of the plan to complete the Harvest, and also to prevent Davina Claire's power from destroying all of New Orleans.
Using the fact that the baby is alive, and therefore able to own property, the Original vampires bury and consecrate their mother's remains with Kieran's help and Esther becomes a New Orleans witch.
As her children, The Originals act as a conduit of her magic, so then Sophie can channel the power.

In Dance Back from the Grave, while Rebekah is prisoner forms a spell cast by Papa Tunde, Elijah bites Hayley so he can use the power of the baby's blood to unbalance the spell as the baby is revealed to be a Quarter Witch as well.

In Crescent City, Céleste states Elijah has a niece on the way.
Giving viewers a second confirmation on the sex of the baby.

In Long Way Back From Hell, Elijah justifies his choice to save Hayley because the baby that she's carrying.

In Farewell to Storyville, Rebekah says to Hayley that the Mikaelson Family has many enemies, and that when she is born she will inherit all of them.
She also says that, though Niklaus is a dangerous person, there is hope for him, and the baby is a focal point of that. Finally, she asks that Hayley tells the baby stories of 'her crazy aunt Bex' and, despite her absence, she really does love her niece.

In Moon Over Bourbon Street, Klaus tells Jackson he counts on the werewolves sense of unity to protect his unborn daughter.
Elijah and Hayley also express their concern for the safety of the baby. Elijah suggests to Hayley to come back and live with him and Klaus.
Hayley, however, declines his offer.


In The Big Uneasy, Klaus tells Elijah that, while he feels Hayley and the baby will be better protected in The Bayou with the Crescent wolves, he will bring Hayley back before she goes into labor.
Klaus wishes the baby to born in the compound. The child later kicks for the first time onscreen when the wolves are telling campfire stories, and Hayley happily puts Jackson's hand on her belly so he can feel.
Monique Deveraux later tells Genevieve that the baby must die in exchange for Genevieve's life.

In An Unblinking Death, there is an explosion in the Bayou.
Hayley nor the baby get hurt. However, Klaus is upset by the attack on his child and suspects that Genevieve is behind this.
Genevieve says she has nothing to do with it, she's not a monster.

In A Closer Walk With Thee, Klaus goes to Father Kieran O'Connell's funeral.
When he walks into the church the coffin is closed. He looks confused at everyone who's sitting there and opens the coffin.
When he opens the coffin he sees a baby lying in it. He smiles at the baby and tries to pick her up, but someone drives a stake through his heart.
When he turns around he sees that it's Mikael. He then wakes up and realizes it was a nightmare. He later tells Cami O'Connell that he dreams of Mikael because he's afraid of becoming a father, he doesn't need a psychologist to figure that out.
But Elijah has also been dreaming of Mikael and they realize something else is going on. At Kieran's funeral Hayley starts coughing up blood and faints thanks to a spell Monique Deveraux had cast. Klaus, Elijah and Genevieve take her to the compound.
Elijah can't hear Hayley's heartbeat, but only that of the baby. Genevieve tries to save her by using magic. Klaus gives Hayley his blood, but it doesn't work. Klaus then tells Elijah he won't lose that baby and he wants to deliver the baby now, but Elijah stops him.
Genevieve agrees with Elijah, if they deliver the baby Hayley will bleed to death.
