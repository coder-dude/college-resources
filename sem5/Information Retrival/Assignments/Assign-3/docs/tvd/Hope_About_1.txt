In Always and Forever, Elijah tells Sophie that he believes this child will be a way for Klaus to finally be happy.
Sophie is glad he feels that way, as she blackmails him. If he gets Klaus to co-operate with their demands, since Marcel Gerard already drove the werewolves from the French Quarter, nobody will have to know of the existence of a hybrid baby.
In a later phone conversation with Rebekah Mikaelson, Elijah swears he will not let anything happen to the baby.

In House of the Rising Son, when Rebekah arrives in New Orleans looking for Elijah, she meets Hayley, commenting her "supernatural, miracle baby bump" isn't showing.
After a short conversation with Niklaus about the whereabouts of their elder brother, Rebekah takes Hayley to the basement of their house, showing her the coffins, before warning her to get out of New Orleans while she can, because once the baby is born, Niklaus will be "planning a coffin for her." Hayley comments that the witches cast a spell; if she leaves New Orleans, she and the baby will die.
Rebekah's words clearly affecting her, Hayley heads to Jardin Gris, inquiring wolfsbane in an attempt to abort the baby. She sits on a bench at night, pouring the poison into a cup of tea, urging herself to drink it. She is about to drink when she hears rustling behind her. Vampires appear in front of her, saying wolves aren't welcome. Hayley, fed up of being told what to do by vampires, tosses the cup at him and attempts to flee.
Rebekah suddenly shows up, ripping the heart out of one of the vampire's, and snapping the neck of the other, saving Hayley's life. After Klaus hears of Hayley leaving the house, he berates her and demands to know what she was doing. In a rage, Hayley growls that she wanted to put the baby out of it's misery, a comment which Klaus begins to choke her over, showing he clearly cares for the baby. The day after, Klaus slowly walks into Hayley's room whilst she is supposedly sleeping.
He notices the bottle of wolfsbane and sniffs the top. Hayley then tells him that she didn't use it, Klaus asks her why, as she could have been free from "all of this." Hayley says that when she was being attacked, she wasn't just protecting herself, she was protecting her child and she didn't want to let anyone hurt it.

In Tangled Up In Blue, Hayley is walking by the pool at night, and she suddenly looks up to see herself face to face with a wolf.
Sabine Laurent suddenly appears and tells her that the wolf is drawn to her because of the baby, before telling her that she knows some charms that can reveal the baby's gender, admitting that it is not really magic that she will be using.
Later on, Hayley is lying on a kitchen table with Sabine dangling a pendant over her stomach. She tells her that she thinks it is a girl, this excites Hayley. Hayley suddenly becomes serious and says "don't tell me I'm having a mini-Klaus" before Sabine drops the pendant and gasps, repeating the same sentence leaving Hayley confused at her words.
Sometime later, Klaus walks into the room whilst Hayley is seated on her laptop, and she reveals that she thinks their child is a girl; unseen to her, Klaus smiles at the news. As he leaves, Hayley is seen typing the sentence Sabine repeated onto her laptop.

In Girl in New Orleans, Agnes persuades Hayley to go to a doctor out in The Bayou who agrees after a notice of Rebekah who told her that she must care most about her child.
The doctor tells Hayley that her child is healthy and Hayley reports that she knew it and that her daughter is a tough one, just like her. Then a wolf howls and Hayley understands that she falls into an ambush led by Agnes.
However, she kills her doctor and manages to escape to the warlocks and fight them off until Rebekah eventually comes to save her. However, they are both then shot, and Hayley goes missing. When Hayley shows up later, she has forgotten what has happened but believes that a wolf is looking after her, and has killed the witches after her to protect her.
Her wounds have also all healed and is revealed that her own baby has healed her due to its vampire healing blood, inherited from her father, Klaus.

In Sinners and Saints, Hayley questions Sophie about the fact that her child will be the witches' end.

In Fruit of the Poisoned Tree, while Hayley speaks to Elijah after what happened to her and the baby during his absence and that they decide to find a way to unlike her to Sophie, Agnes enacts a plan to kill the child, by using the link between Sophie and Hayley.
She uses a cursed object, The Needle of Sorrows, to cause Hayley's temperature to spike rapidly in order to cause a miscarriage. While Klaus goes to find Agnes, Sophie works with Elijah and Rebekah to lower Hayley's temperature and heart rate, until Davina, through Elijah's machinations, could unwittingly severe the link between Sophie and Hayley, saving the baby's life.
Later Elijah kills Agnes for trying to kill his niece.
