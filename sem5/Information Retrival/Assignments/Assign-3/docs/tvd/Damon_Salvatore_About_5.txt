In Bloodlines, Damon starts of the episode with a bang when he rescues Elena from her crashed SUV.
When she awakens, Damon reveals that they're in Georgia. He takes her to Bree's Bar, where he reveals that he's known Bree since she was a college freshman, and that she's a witch.
While Elena’s on the phone, Damon questions Bree about another way to open the tomb, and he’s disappointed to find that there is no other way. When she calls Stefan, Damon goes outside and asks if she's okay, to which she responds, cautiously, “Don't pretend to care.” Inside, the two both eat a plate of fries with a burger while Damon explains that Elena can’t possibly be descended from Katherine, because Katherine never had a child.

Damon Elena Bree
Damon, Elena, and Bree.

He also tells her that vampires can't procreate, and that as long as he keeps a healthy supply of blood in his system, his body functions normally .
The two end up getting drunk together and doing shots at the bar, where Elena gets very, very intoxicated. Damon's later seen at the bar, where Bree asks him where Elena is.
She seems to have vanished, but when Damon goes looking for her, he falls into the trap of Lexi's vengeful vampire boyfriend, who was originally understood to be human. Elena manages to save Damon's life by talking about Lexi, but, angry that Bree betrayed him and sent the vampire to kill him, Damon goes back to Bree’s Bar and confronts her.
She attempts to spare her life by telling Damon that there is one more way to get into the tomb, through Emily’s Grimoire, essentially the book she uses to write down her spells. Damon kills by ripping her heart out. On the ride home, Elena points out that she saved Damon’s life. At the very end of the episode, Damon goes to the Mystic Grill and requests a bourbon at the bar, where the new history teacher Alaric Saltzman also happens to be sitting.
Alaric has a flashback of Damon killing his wife, Isobel.
In Unpleasantville, Damon is first seen rummaging through the Salvatore library, where Stefan confronts Damon for taking Elena to Georgia.
He later encounters Bonnie at the Mystic Grill, saying “we need a fresh start.” She warns him to stay away from her, but the conversation is ended by Ben McKittrick, who comes to Bonnie's ‘rescue.’ Later he's seen writing the names of the town's founders, and Stefan brings him their father's journal, saying he knew Damon was looking for it.
Stefan assures him that the journal has nothing helpful in it, but offers his help to Damon if only Damon will leave Mystic Falls. Suspicious of his motives, Damon rejects his offer. Damon also answers Stefan’s phone when Elena calls, telling her that Stefan is on his way .

Damon Dance
Damon harasses Bonnie and Caroline at the 50's Dance.

Damon comes over after Elena is attacked, and the two of them, realizing that he was invited in and therefore able to harm her at any time, decide to go to the 50's Decade Dance to hunt the vampire down.
Alaric notices Damon enter with Elena and Stefan. Damon later sarcastically asks Bonnie to dance, though he still seems to be trying to reconcile with her. He then asks Elena to dance, but she dances with Stefan instead.
While Damon is watching the two of them dance, he's approached by Alaric, who wonders about his presence there. He asks Damon a long series of questions, but when they become too personal, Damon becomes suspicious and stops answering, which leads to Alaric leaving.
Damon’s seen dancing with a blonde girl later, and Elena remarks, “You really can't take him anywhere, can you?” When her vampire stalker attacks her, Damon and Stefan come to Elena's rescue, but Damon notices Alaric lingering near the cafeteria door. When he confronts him, he attempts to compel him to tell him what he knows, and satisfied that he knows nothing, Damon leaves him there.
It is revealed that Alaric has Vervain crushed in his fist to keep Damon out of his mind, indicating that he was prepared for Damon to confront him. In Children of the Damned, Damon is first seen in a flashback to 1864, where he and Katherine feed on a stagecoach full of people. In the present, Damon is sitting on the end of Stefan’s bed when Stefan and Elena wake up, happy to team up with them.
He tells them they need to find Johnathan Gilbert's Journal, which will hold the location of the Grimoire. In flashbacks, Damon is revealed to have a particular distaste for his father's opinion, while in the present, he's in the Gilbert House talking to Jenna Sommers. When Elena arrives, the two of them talk in the kitchen, and Damon questions the absence of Stefan. He asks Elena if he can trust Stefan’s help, asking her, for once, without sarcasm.

Damon and Jeremy
Damon and Jeremy play video games.
