In Miss Mystic Falls, Damon is visited by John Gilbert and Sheriff Forbes.
John suggests that he and Damon work together to track down whoever is stealing from the blood banks.
Anna comes to visit him later, and though he warns her about the Founder's Council being onto the tomb vampires, she says she hasn't been to the blood bank in at least a week, and the other vampires are gone.
Damon goes home and confronts Stefan for being the thief, though he seems elated by Stefan’s new diet. He also finds Stefan's fridge of human blood.

Miss Mystic Falls11
Damon dances with Elena.

John Gilbert comes back to the Salvatore Boarding House to tell Damon that he wants the Gilbert Device.
He wants Damon to get it from Pearl, who was ‘friendly’ with Johnathan Gilbert back in 1864. He rejects the idea of helping John, but when he sees Anna again at the Miss Mystic Falls Pageant, he inquires about the device.
Later, he comes to the changing room upstairs and tells Elena about Stefan’s fridge full of blood. When Stefan vanishes before the dance begins, Damon steps in to escort Elena onto the dance-floor. When the dance is over, Elena pulls Damon aside and they realize one of the contestants is missing.
They search for him in the woods, only to find him feeding on Amber Bradley. Bonnie arrives and manages to subdue Stefan, but, in his right mind again, Stefan flees. Later, Pearl brings Damon the device she stole from Johnathan Gilbert as a peace-offering because Anna wants to stay in Mystic Falls.
After Elena tranquilizes Stefan, Damon carries him to the dungeon and locks him inside, and the two of them sit in the hallway together.

In Blood Brothers, as Elena and Damon take turns ‘baby-sitting’ Stefan at the boarding house.
The two brothers slowly let Elena in on their history, how they both became vampires. Damon completes the story by telling her that Stefan forced him to feed on a human.
Damon also taunts Elena by telling her that Stefan prefers golden retriever puppy blood. Damon and Alaric also go on a quest for information about Isobel, where they encounter one of the tomb vampires, Henry.
The vampire reveals that John Gilbert has been working with him to keep an eye on the other tomb vampires. They end up killing him and having a moderately deep conversation about searching for their ex-lovers.
Damon returns to Elena, where he concludes the story of their vampiric origin. Stefan later approaches Damon while he’s drinking in the living room, and Damon tells him to stop feeling his guilt. He then says that he hated him for all those years not because he forced him to turn, but “because she turned you.” Damon thought Katherine’s love was only for him.

In Isobel, Damon expresses distaste for Stefan’s new ‘cured’ personality while on the phone with Elena.
He also seems concerned by Elena meeting Isobel for the first time. While they’re meeting, Damon is pacing beside Alaric just outside the Grill.
Damon explains that Isobel has a ‘switch’ she can use to turn off her emotions, though he denies that he has humanity himself. Isobel later finds Damon in her temporary home, playing a game of cards involving stripping.
She attempts to threaten him, but Damon continues questioning her about the Gilbert Device. When she kisses him, Damon pins her to the ground and threatens to ‘rip her to bits’ for what she said to Elena. He told her to tell Katherine to come get the device herself.
Later, Elena tries to get the device from Damon so Bonnie can de-spell it; Elena hopes to trade the device for Jeremy, who has been kidnapped by Isobel. Bonnie displays her powers by retrieving Damon's favorite book, Call of the Wild, from the library shelf with only the power of her mind.
He admits that he doesn’t trust Bonnie, but he will trust Elena, so he gives her the device. Damon is also present for the ‘de-spell.’ When they make the trade with Isobel, Damon and Stefan are present. Elena asks Isobel how she knew Damon would give her the device, and Isobel responds, “Because he’s in love with you.” After the exchange, he is standing in the same spot, silently watching Stefan and Elena hug.
Stefan later confronts Damon about it, but he seems to be alright with his love for Elena, because it means he’ll protect her. Damon also reveals that John Gilbert is Elena's real father, and he's amused that the others didn’t put the pieces together as quickly.
