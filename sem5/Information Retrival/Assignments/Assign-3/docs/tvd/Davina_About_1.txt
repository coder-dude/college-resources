In Always and Forever, Davina is experimenting with her powers when Marcel comes and asks her about the witches.
She says they haven't used magic as they know she can sense them if they do and voices her concern that the Originals might hurt him.
Marcel says she has nothing to worry about, as she's stronger than they are.
In House of the Rising Son, it is revealed that Marcel gave Davina Elijah's daggered body, which he received from Klaus as a gesture of good faith.
When Rebekah insisted that Marcel give her back her brother, Marcel took her to Davina's room and told her that she has not been very nice to him. Davina took Marcel's side and threw her out the window with telekinesis.
The next morning, Marcel found her drawing in her room. She informs him that she's worried about Marcel as a result of the Originals being in town, and insisted that they shouldn't be in New Orleans. Marcel replied that it appeared that the Mikaelsons came to town to stay, and asked Davina if she could find a way to kill an Original.

In Tangled Up In Blue, Davina tried to stop Sophie's locator spell and weakened Katie with her magic.
She is able to link herself to Marcel to save his life, but was weakened as a result. She tried to find out where Sophie was, but wasn't able to track where her magic was coming from.
When Marcel arrived to thank her, she assumed that it was Klaus and/or Rebekah who tried to kill him, but he informed her that it was actually Klaus who saved him. He told her he planned to give Elijah back to Klaus, but Davina refused to allow him to give Elijah back until she figured out a way to kill them.

DavinaMarcel1x04
Davina and Marcel at the festival.

In Girl in New Orleans, Davina undaggers Elijah accidentally.
Even though Marcel tries to give Elijah back to Klaus, Davina denies him the request, as she was not done with him yet.
She had discovered that daggers hurt them, but didn't kill them. Davina convinced Marcel to allow her to attend the Dauphine Street music festival, on the condition that she must hang out with Cami, and that his nightwalkers would keep watch on both of them, just to be safe.
Davina happily wonders on the street. She enters the Rousseaus' with Marcel meets Cami. Davina admits her romantic feelings for Tim to her, who was being compelled by Klaus to spy on Marcel. As a result of Klaus' compulsion, Cami encouraged Davina to talk to Tim. Even though Davina dismissed the idea at first, thinking it was stupid, she agreed eventually.

Following her advice, Davina met with Tim in St.
Anne's Church, where she had been living since the Harvest.
She was happy to see Tim again. Tim asked her how she'd been and if she'd come back to school.
Davina told him that she'd to take care of important matters and she would not be coming back to school anytime soon.
She said that she missed seeing him, listening to him as he played his violin. He started to play a song for her, but Klaus appeared and tried first to kindly convince her to join his side instead of Marcel's.
As Davina believed that Marcel was protecting her and refused to join his side, Klaus threatened to kill Tim, which angered Davina so much that she unleashed her powers on him, bursting the church's windows, and destroying a lot of the pews.
This did not injure Klaus in the slightest, but Tim was critically wounded. Klaus manipulated Davina by offering to feed Tim his blood to heal him, and to compel him to forget everything that had happened in exchange for her help in the future when he is in need of it.
He eventually left Davina alone at the church, and when Marcel found her and asked her what happened, she was too angry to tell him. When she arrived back in her room, she found that Elijah had awoken from being daggered, after she had removed the dagger to examine it, not knowing that by doing so, the dagger would no longer be coated in white oak ash and thus ineffective.

Elijah taste Davinas Blood
Elijah and Davina.
