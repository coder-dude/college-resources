When Niklaus and his brother Henrik went to see the wolves transform, Henrik was killed by one of the transformed werewolves.
Devastated at the loss of another child, Mikael convinced Esther to use her magic to protect their remaining children. Upon Mikael's request, Esther performed a spell, called The Immortality Spell, which transformed Niklaus, his siblings, and his step-father into Original Vampires, the first vampire's to exist, and progenitors to the vampire species.
When Niklaus made his first human kill, his werewolf gene was activated and he became a vampire and werewolf hybrid, thus revealing Esther's infidelity. Soon after becoming a Hybrid, Mikael had Elijah help him restrain Niklaus, and Esther to use her Dark Magic to place a curse on him which forced his werewolf side to lie dormant, and then rejected and abandoned him.

Mikael then hunted down and killed Niklaus' father and his entire family, not realizing that doing so would ignite a war between vampires and werewolves that has been perpetuated across the ages.
In retaliation for the curse and rejection, Klaus murdered his mother and framed Mikael for the act.

After killing Esther, his family scattered and Niklaus stayed behind with Rebekah to bury Esther.
Rebekah promised to never turn her back on him, like Esther did and so did Elijah. They swore to each other they would stay together always and forever.

