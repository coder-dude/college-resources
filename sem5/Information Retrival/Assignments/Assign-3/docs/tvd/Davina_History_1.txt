She presumably grew up in the French Quarter with a single, controlling mother, as her father left the picture before Davina was born and was not seen again.
Davina went to public school with a boy named Tim, whom she met at age 10, and for whom she developed romantic feelings. It was mentioned that she took piano lessons.
Davina was also best friends with Monique for some time before the Harvest, and was a member of the French Quarter witches. She was also one of the four young witches chosen for The Harvest ritual.

On the day the Harvest was to be completed, Davina and the other Harvest girls were deceived by the Elders regarding their fates and the events surrounding the sacrifice.
They were told that the knife that was used on their palms for the blood sacrifice would put them in a peaceful limbo. Instead, Bastianna, the lead Elder who was performing the ritual, began to kill the girls by slitting their throats.
As the last girl to be sacrificed, Davina witnessed the slaughter of the other selected girls, and was restrained by a fellow witch in her coven so she could not intervene. She felt betrayed by the Elders, as well as her mother, who stood by and witnessed the devastation and slaughter.

However, when Marcel and his vampires appeared, they effectively ended the sacrifice by killing the majority of the witches in attendance.
In doing so, Marcel saved Davina's life and she lived with him for eight months afterward. Marcel spent the next eight months hiding her from the witches, and using her powers to help keep the witches in subjugation out of punishment for what they did to her and the other girls.
The witches later felt betrayed by Davina, and when she was finally sacrificed in the Harvest and her spirit went back into the earth with the rest of the deceased New Orleans witches who practiced ancestral magic, the Ancestors shunned her until she was resurrected. Once Davina was brought back to life, she returned to her coven for a while, but left after Monique and Abigail were killed.
She then returned to her high school to resume a somewhat normal teenage life.


