DATASET_FILE = "./States.csv"
HEURISTICS_FILE = "./heuristics_bangalore.csv"
INF = 9999

# for 8 puzzle
NO_ROWS = 3
NO_COLS = 3
BLANK_BOX_ROW = 2
BLANK_BOX_COL = 2
