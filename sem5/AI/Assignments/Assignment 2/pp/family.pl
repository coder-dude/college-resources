female("lyarra stark").
female("catelyn stark").
female("lyanna stark").
female("sansa stark").
female("arya stark").
female("rhaella targaryen").
female("elia martell").
female("daenerys targaryen").
female("rhaenys targaryen").


male("rickard stark").
male("eddard stark").
male("brandon stark").
male("benjen stark").
male("robb stark").
male("bran stark").
male("rickon stark").
male("jon snow").
male("aerys targaryen").
male("rhaegar targaryen").
male("viserys targaryen").
male("aegon targaryen").

child("eddard stark","rickard stark").
child("brandon stark","rickard stark").
child("benjen stark","rickard stark").
child("lyanna stark","rickard stark").
child("eddard stark","lyarra stark").
child("brandon stark","lyarra stark").
child("benjen stark","lyarra stark").
child("lyanna stark","lyarra stark").
child("robb stark","eddard stark").
child("sansa stark","eddard stark").
child("arya stark","eddard stark").
child("bran stark","eddard stark").
child("rickon stark","eddard stark").
child("robb stark","catelyn stark").
child("sansa stark","catelyn stark").
child("arya stark","catelyn stark").
child("bran stark","catelyn stark").
child("rickon stark","catelyn stark").
child("rhaegar targaryen","aerys targaryen").
child("viserys targaryen","aerys targaryen").
child("daenerys targaryen","aerys targaryen").
child("rhaegar targaryen","rhaella targaryen").
child("viserys targaryen","rhaella targaryen").
child("daenerys targaryen","rhaella targaryen").
child("jon snow","rhaegar targaryen").
child("jon snow","lyanna stark").
child("rhaenys targaryen","rhaegar targaryen").
child("aegon targaryen","rhaegar targaryen").
child("rhaenys targaryen","elia martell").
child("aegon targaryen","eliam artell").


spouse("eddard stark","catelyn stark").
spouse("catelyn stark","eddard stark").
spouse("lyanna stark","rhaegar targaryen").
spouse("rhaegar targaryen","lyanna stark").
spouse("rhaegar targaryen","elia martell").
spouse("elia martell","rhaegar targaryen").
spouse("rickard stark","lyarra stark").
spouse("lyarra stark","rickard stark").
spouse("aerys targaryen","rhaella targaryen").
spouse("rhaella targaryen","aerys targaryen").
